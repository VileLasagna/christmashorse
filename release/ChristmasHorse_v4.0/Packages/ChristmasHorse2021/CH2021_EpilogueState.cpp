#include "CH2021_EpilogueState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"

CH2021_EpilogueState::CH2021_EpilogueState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2021_EpilogueState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2021_EpilogueState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();

    chalo::TextureManager::Add( "background1", "Packages/ChristmasHorse2021/Graphics/Story/epilogue.png" );

    m_background1.setTexture( chalo::TextureManager::Get( "background1" ) );

//    chalo::InputManager::Setup();

    chalo::UILabel text;

    int x = 100;
    int y = chalo::Application::GetScreenHeight();
    int ft = 50;

    // void Setup( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, std::string text );
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "You and your allies begin to rebuild." );
    m_text.push_back( text );   y += 100;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "Soon you will wage war on" );
    m_text.push_back( text );   y += 50;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "the misguided elves who" );
    m_text.push_back( text );   y += 50;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "are still loyal to the deceased Nick." );
    m_text.push_back( text );   y += 100;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "YOU are the new Santa Claus." );
    m_text.push_back( text );   y += 100;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "This is only just the beginning." );
    m_text.push_back( text );   y += 100;

    m_music.openFromFile( "Packages/ChristmasHorse2021/Audio/ChristmasSabbotage.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2021_EpilogueState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    m_text.clear();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2021_EpilogueState::Update()
{
    chalo::InputManager::Update();

    if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Escape ) || chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Enter ) )
    {
        SetGotoState( "startupstate" );
    }

    for ( auto & text : m_text )
    {
        sf::Vector2f pos = text.GetPosition();
        pos.y -= 1;
        text.SetPosition( pos );
    }
}

void CH2021_EpilogueState::Draw( sf::RenderWindow& window )
{
    window.draw( m_background1 );

    for ( auto & text : m_text )
    {
        window.draw( text.GetText() );
    }
}



