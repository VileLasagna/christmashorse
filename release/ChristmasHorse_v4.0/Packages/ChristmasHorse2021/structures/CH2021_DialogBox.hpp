#ifndef _CH2021_DIALOG_BOX
#define _CH2021_DIALOG_BOX

struct DialogBox
{
    sf::Vector2f position;
    sf::Sprite boxbg;
    sf::Sprite horse1;
    sf::Sprite horse2;
    std::vector<sf::Text> dialogs;
    sf::Text name;
    bool isVisible;

    void Setup( int screenWidth, int screenHeight )
    {
        isVisible = false;
        position = sf::Vector2f( screenWidth/2 - 588/2, 50 );

        boxbg.setTexture( chalo::TextureManager::Get( "dialogbox" ) );
        boxbg.setPosition( position );

        horse1.setTexture( chalo::TextureManager::Get( "dialog-drabis" ) );
        horse1.setPosition( position.x + 8, position.y + 8 );

        horse2.setTexture( chalo::TextureManager::Get( "dialog-horse" ) );
        horse2.setPosition( position.x + 450, position.y + 8 );
    }

    void Draw( sf::RenderWindow& window )
    {
        window.draw( boxbg );
        window.draw( horse1 );
        window.draw( horse2 );
        for ( auto& dialog : dialogs )
        {
            window.draw( dialog );
        }
        window.draw( name );
    }

    void UpdateText( ItemType type, std::vector<sf::Text>& dialogTexts )
    {
        dialogs.clear();

        name.setFont( chalo::FontManager::Get( "main" ) );
        name.setCharacterSize( 18 );
        name.setFillColor( sf::Color::Yellow );
        name.setOutlineColor( sf::Color::Black );
        name.setOutlineThickness( 2 );
        name.setPosition( position.x + 150, position.y + 10 );

        dialogs = dialogTexts;

        if ( type == ItemType::DRABIS )
        {
            name.setString( "Drabis" );
            horse1.setTexture( chalo::TextureManager::Get( "dialog-drabis" ) );
        }
        else if ( type == ItemType::RATCHET )
        {
            horse1.setTexture( chalo::TextureManager::Get( "dialog-ratchet" ) );
            name.setString( "Ratchet" );
        }
        else if ( type == ItemType::SNEEZY )
        {
            horse1.setTexture( chalo::TextureManager::Get( "dialog-sneezy" ) );
            name.setString( "Sneezy" );
        }
        else if ( type == ItemType::WABEKA )
        {
            name.setString( "Wepeka" );
            horse1.setTexture( chalo::TextureManager::Get( "dialog-webeka" ) );
        }
    }
};

#endif
