#include "CH2021_PrologueState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"

CH2021_PrologueState::CH2021_PrologueState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2021_PrologueState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2021_PrologueState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();

//    chalo::InputManager::Setup();

    m_currentSlide = 0;
    m_mouseCooldown = 0;
    m_mouseCooldownMax = 10;

    // Slide 1
    StorySlide slide1;
    slide1.background.setTexture( chalo::TextureManager::AddAndGet( "scene1_bg1",       "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene1_bg.png" ) );

    sf::Sprite sprite1;
    sprite1.setTexture( chalo::TextureManager::AddAndGet( "scene1_horse",     "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene1_horse.png" ) );
    sprite1.setPosition( 0, 100 );

    sf::Sprite sprite2;
    sprite2.setTexture( chalo::TextureManager::AddAndGet( "scene1_santa",     "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene1_santa.png" ) );
    sprite2.setPosition( 700, 100 );

    slide1.sprites.push_back( sprite1 );
    slide1.sprites.push_back( sprite2 );

    sf::Text text1;
    text1.setFont( chalo::FontManager::Get( "main" ) );
    text1.setOutlineColor( sf::Color::Black );
    text1.setOutlineThickness( 5 );
    text1.setCharacterSize( 50 );
    text1.setFillColor( sf::Color::White );
    text1.setString( "Bepis the Horse succeeded in his mission" );
    text1.setPosition( 10, 700-100 );
    slide1.text.push_back( text1 );
    text1.setString( "to defeat Nick. Now, Bepis is Santa." );
    text1.setPosition( 10, 700-50 );
    slide1.text.push_back( text1 );

    m_slides.push_back( slide1 );



    // Slide 2
    StorySlide slide2;
    slide2.background.setTexture( chalo::TextureManager::AddAndGet( "scene2_bg1",       "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene2_bg.png" ) );
    text1.setString( "SANTA'S WORKSHOP" );
    text1.setPosition( 400, 700-150 );
    slide2.text.push_back( text1 );
    text1.setString( "SANTA PLANET" );
    text1.setPosition( 450, 700-100 );
    slide2.text.push_back( text1 );
    text1.setString( "DECEMBER 26TH" );
    text1.setPosition( 440, 700-50 );
    slide2.text.push_back( text1 );

    m_slides.push_back( slide2 );


    // Slide 3
    StorySlide slide3;
    slide3.background.setTexture( chalo::TextureManager::AddAndGet( "ch2_scene3_bg",       "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene3_bg.png" ) );

    sf::Sprite s3_s2;
    s3_s2.setTexture( chalo::TextureManager::AddAndGet( "ch2_scene3_planetleft",               "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene3_planetleft.png" ) );
    s3_s2.setOrigin( 408/2, 568/2 );
    s3_s2.setPosition( 1280/2-200, 720/2 );
    slide3.sprites.push_back( s3_s2 );

    sf::Sprite s3_s3;
    s3_s3.setTexture( chalo::TextureManager::AddAndGet( "ch2_scene3_planetright",               "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene3_planetright.png" ) );
    s3_s3.setOrigin( 460/2, 592/2 );
    s3_s3.setPosition( 1280/2+200, 720/2 );
    slide3.sprites.push_back( s3_s3 );

    sf::Sprite s3_s1;
    s3_s1.setTexture( chalo::TextureManager::AddAndGet( "ch2_scene3_explosion",               "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene3_explosion.png" ) );
    s3_s1.setOrigin( 512/2, 472/2 );
    s3_s1.setPosition( 1280/2, 720/2 );
    s3_s1.setScale( 0.1, 0.1 );
    slide3.sprites.push_back( s3_s1 );

    m_slides.push_back( slide3 );


    // Slide 4
    StorySlide slide4;
    slide4.background.setTexture( chalo::TextureManager::AddAndGet( "scene2_bg2",       "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene4_bg.png" ) );

    sf::Sprite s4_s1;
    s4_s1.setTexture( chalo::TextureManager::AddAndGet( "ch2_scene4_toilet",               "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene4_toilet.png" ) );
    s4_s1.setOrigin( 300/2, 408/2 );
    s4_s1.setPosition( 0200, 720/2+150 );
    slide4.sprites.push_back( s4_s1 );

    sf::Sprite s4_s2;
    s4_s2.setTexture( chalo::TextureManager::AddAndGet( "ch2_scene4_horse",               "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene4_horse.png" ) );
    s4_s2.setOrigin( 388/2, 516/2 );
    s4_s2.setPosition( -200, 720/2 );
    slide4.sprites.push_back( s4_s2 );

    text1.setString( "The planet - destroyed." );
    text1.setPosition( 10, 700-100 );
    slide4.text.push_back( text1 );

    text1.setString( "Retaliation. From Nick's elves." );
    text1.setPosition( 10, 700-50 );
    slide4.text.push_back( text1 );

    m_slides.push_back( slide4 );



    // Slide 5
    StorySlide slide5;
    slide5.background.setTexture( chalo::TextureManager::Get( "ch2_scene3_bg" ) );

    sf::Sprite s5_s1;
    s5_s1.setTexture( chalo::TextureManager::AddAndGet( "ch2_scene5_earth",               "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene5_earth.png" ) );
    s5_s1.setPosition( 0, 720-572 );
    slide5.sprites.push_back( s5_s1 );

    sf::Sprite s5_s2;
    s5_s2.setTexture( chalo::TextureManager::AddAndGet( "ch2_scene5_horse",               "Packages/ChristmasHorse2021/Graphics/Story/ch2_scene5_horse.png" ) );
    s5_s2.setPosition( 1000, 0 );
    slide5.sprites.push_back( s5_s2 );

    text1.setString( "Now it is time to regroup." );
    text1.setPosition( 10, 700-150 );
    slide5.text.push_back( text1 );

    text1.setString( "Gather resources." );
    text1.setPosition( 10, 700-100 );
    slide5.text.push_back( text1 );

    text1.setString( "Build an army." );
    text1.setPosition( 10, 700-50 );
    slide5.text.push_back( text1 );

    m_slides.push_back( slide5 );


    m_music.openFromFile( "Packages/ChristmasHorse2021/Audio/ChristmasSabbotage.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2021_PrologueState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    m_slides.clear();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2021_PrologueState::Update()
{
    chalo::InputManager::Update();


    if ( chalo::InputManager::IsLeftClick() && m_mouseCooldown <= 0 )
    {
        m_currentSlide++;
        if ( m_currentSlide >= m_slides.size() )
        {
            SetGotoState( "ch2021_gamestate" );
        }

        m_mouseCooldown = m_mouseCooldownMax;
    }
    else if ( m_mouseCooldown > 0 )
    {
        m_mouseCooldown--;
    }

    if ( m_currentSlide == 0 )
    {
        for ( auto& sprite : m_slides[m_currentSlide].sprites )
        {
            sf::Vector2f pos = sprite.getPosition();
            pos.x += 0.05;
            sprite.setPosition( pos );
        }
    }
    else if ( m_currentSlide == 2 )
    {
        sf::Vector2f pos = m_slides[m_currentSlide].sprites[0].getPosition();
        m_slides[m_currentSlide].sprites[0].setPosition( pos.x - 0.1, pos.y );

        pos = m_slides[m_currentSlide].sprites[1].getPosition();
        m_slides[m_currentSlide].sprites[1].setPosition( pos.x + 0.1, pos.y );

        sf::Vector2f scale = m_slides[m_currentSlide].sprites[2].getScale();
        m_slides[m_currentSlide].sprites[2].setScale( scale.x + 0.05, scale.y + 0.05 );
        sf::Color color = m_slides[m_currentSlide].sprites[2].getColor();
        color.a -= 0.1;
        m_slides[m_currentSlide].sprites[2].setColor( color );
    }
    else if ( m_currentSlide == 3 )
    {
        sf::Vector2f toiletPos = m_slides[m_currentSlide].sprites[0].getPosition();
        float toiletAngle = m_slides[m_currentSlide].sprites[0].getRotation();
        toiletPos.x += 1.5;
        toiletAngle += 0.5;
        m_slides[m_currentSlide].sprites[0].setPosition( toiletPos );
        m_slides[m_currentSlide].sprites[0].setRotation( toiletAngle );

        sf::Vector2f horsePos = m_slides[m_currentSlide].sprites[1].getPosition();
        float horseAngle = m_slides[m_currentSlide].sprites[1].getRotation();
        horsePos.x += 1;
        horseAngle += 0.5;
        m_slides[m_currentSlide].sprites[1].setPosition( horsePos );
        m_slides[m_currentSlide].sprites[1].setRotation( horseAngle );
    }
    else if ( m_currentSlide == 4 )
    {
        sf::Vector2f horsePos = m_slides[m_currentSlide].sprites[1].getPosition();
        sf::Vector2f horseScale = m_slides[m_currentSlide].sprites[1].getScale();
        horsePos.x -= 4;
        horsePos.y += 1;
        horseScale.x -= 0.005;
        horseScale.y -= 0.005;
        if ( horseScale.x < 0.001 ) { horseScale.x = 0.001; }
        if ( horseScale.y < 0.001 ) { horseScale.y = 0.001; }
        m_slides[m_currentSlide].sprites[1].setPosition( horsePos );
        m_slides[m_currentSlide].sprites[1].setScale( horseScale );
    }
}

void CH2021_PrologueState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::Draw( window );

    StorySlide& slide = m_slides[m_currentSlide];

    window.draw( slide.background );

    for ( auto& sprite : slide.sprites )
    {
        window.draw( sprite );
    }

    for ( auto& text : slide.text )
    {
        window.draw( text );
    }
}



