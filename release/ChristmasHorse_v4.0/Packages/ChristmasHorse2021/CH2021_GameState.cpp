#include "CH2021_GameState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

CH2021_GameState::CH2021_GameState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2021_GameState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2021_GameState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();
    EscapeyState::Setup();

    chalo::TextureManager::Add( "horse",            "Packages/ChristmasHorse2021/Graphics/horse.png" );
    chalo::TextureManager::Add( "dialog-horse",     "Packages/ChristmasHorse2021/Graphics/dialog-horse.png" );
    chalo::TextureManager::Add( "bepis-portrait",   "Packages/ChristmasHorse2021/Graphics/bepis-portrait-200.png" );

    chalo::TextureManager::Add( "drabis",           "Packages/ChristmasHorse2021/Graphics/drabis.png" );
    chalo::TextureManager::Add( "dialog-drabis",    "Packages/ChristmasHorse2021/Graphics/dialog-drabis.png" );
    chalo::TextureManager::Add( "ratchet",          "Packages/ChristmasHorse2021/Graphics/ratchet.png" );
    chalo::TextureManager::Add( "dialog-ratchet",   "Packages/ChristmasHorse2021/Graphics/dialog-ratchet.png" );
    chalo::TextureManager::Add( "sneezy",           "Packages/ChristmasHorse2021/Graphics/sneezy.png" );
    chalo::TextureManager::Add( "dialog-sneezy",    "Packages/ChristmasHorse2021/Graphics/dialog-sneezy.png" );
    chalo::TextureManager::Add( "wabeka",           "Packages/ChristmasHorse2021/Graphics/wabeka.png" );
    chalo::TextureManager::Add( "dialog-webeka",    "Packages/ChristmasHorse2021/Graphics/dialog-webeka.png" );

    chalo::TextureManager::Add( "tree",             "Packages/ChristmasHorse2021/Graphics/tree.png" );
    chalo::TextureManager::Add( "carrot",           "Packages/ChristmasHorse2021/Graphics/carrot.png" );
    chalo::TextureManager::Add( "apple",            "Packages/ChristmasHorse2021/Graphics/apple.png" );
    chalo::TextureManager::Add( "stick",            "Packages/ChristmasHorse2021/Graphics/stick.png" );
    chalo::TextureManager::Add( "snowball",         "Packages/ChristmasHorse2021/Graphics/snowball.png" );
    chalo::TextureManager::Add( "rabbit",           "Packages/ChristmasHorse2021/Graphics/rabbit.png" );
    chalo::TextureManager::Add( "cursor",           "Packages/ChristmasHorse2021/Graphics/UI/cursorssheet.png" );
    chalo::TextureManager::Add( "dialogbox",        "Packages/ChristmasHorse2021/Graphics/UI/dialogbox.png" );
    chalo::TextureManager::Add( "hud1",             "Packages/ChristmasHorse2021/Graphics/UI/hud1.png" );
    chalo::TextureManager::Add( "hud2",             "Packages/ChristmasHorse2021/Graphics/UI/hud2.png" );

//    chalo::InputManager::Setup();

    Helper::SeedRandomNumberGenerator();

    sf::IntRect validRegion = sf::IntRect( 0, 0, 1280, 720 );

    chalo::DrawManager::SetBackgroundColor( sf::Color( 182, 201, 216 ) );

    m_hudBackgrounds[0].setPosition( 0, 0 );
    m_hudBackgrounds[1].setPosition( 1000, 0 );
    m_hudBackgrounds[0].setTexture( chalo::TextureManager::Get( "hud1" ) );
    m_hudBackgrounds[1].setTexture( chalo::TextureManager::Get( "hud2" ) );

    m_hudPortrait.setTexture( chalo::TextureManager::Get( "bepis-portrait" ) );
    m_hudPortrait.setPosition( 25, 35 );

    m_hudMapBg.setPosition( 1054, 585 );
    m_hudMapBg.setSize( sf::Vector2f( 210, 120 ) );
    m_hudMapBg.setFillColor( sf::Color::Black );
    m_hudMapBg.setOutlineColor( sf::Color::Yellow );
    m_hudMapBg.setOutlineThickness( 1 );


    sf::Text textwip;
    textwip.setFont( chalo::FontManager::Get( "main" ) );
    textwip.setFillColor( sf::Color::White );
    textwip.setOutlineColor( sf::Color::Black );
    textwip.setOutlineThickness( 2 );
    textwip.setString( "Speed: 1" );
    textwip.setPosition( 10, 260 );
    textwip.setCharacterSize( 20 );

    m_hudText["speed"] = textwip;
    textwip.setCharacterSize( 16 );
    m_hudText["instr1"] = textwip; m_hudText["instr1"].setString( "Travel the snowfield" ); m_hudText["instr1"].setPosition( 10, 630 );
    m_hudText["instr2"] = textwip; m_hudText["instr2"].setString( "to find potential allies." ); m_hudText["instr2"].setPosition( 10, 650 );
    m_hudText["instr3"] = textwip; m_hudText["instr3"].setString( "Fulfill their quests to" ); m_hudText["instr3"].setPosition( 10, 670 );
    m_hudText["instr4"] = textwip; m_hudText["instr4"].setString( "gain resources for war." ); m_hudText["instr4"].setPosition( 10, 690 );

    m_player.Setup();

    m_cursor.setTexture( chalo::TextureManager::Get( "cursor" ) );
    m_cursor.setTextureRect( sf::IntRect( 0, 0, 32, 32 ) );
    m_cursor.setOrigin( sf::Vector2f( 16, 16 ) );

    m_cameraOffset.x = 0;
    m_cameraOffset.y = 0;

    m_moveSpeed = 3;
    m_hudText["speed"].setString( "Speed: " + Helper::ToString( m_moveSpeed ) );

    int screenWidth = chalo::Application::GetScreenWidth();
    int screenHeight = chalo::Application::GetScreenHeight();

    int mapWidth = screenWidth * 3;
    int mapHeight = screenHeight * 3;

    int westEdgeL   = -screenWidth/2;   int westEdgeR = 0;
    int eastEdgeL   = mapWidth;         int eastEdgeR = mapWidth+screenWidth/2;
    int northEdgeT  = -screenHeight/2;  int northEdgeB = 0;
    int southEdgeT  = mapHeight;        int southEdgeB = mapHeight+screenHeight/2;

    int treeSpacing = 100;

    // Put trees along the west side
    for ( int xInt = westEdgeL; xInt < westEdgeR; xInt += treeSpacing )
    {
        for ( int yInt = -screenHeight/2; yInt < mapHeight+screenHeight/2; yInt += treeSpacing )
        {
            OffsetableSprite tree;
            tree.sprite.setTexture( chalo::TextureManager::Get( "tree" ) );
            tree.originalPosition.x = Helper::GetRandom( xInt, xInt+100 ), tree.originalPosition.y = Helper::GetRandom( yInt, yInt+100 );
            tree.type = ItemType::TREE;
            m_items.push_back( tree );
        }
    }
    // Put trees along the east side
    for ( int xInt = eastEdgeL; xInt < eastEdgeR; xInt += treeSpacing )
    {
        for ( int yInt = -screenHeight/2; yInt < mapHeight+screenHeight/2; yInt += treeSpacing )
        {
            OffsetableSprite tree;
            tree.sprite.setTexture( chalo::TextureManager::Get( "tree" ) );
            tree.originalPosition.x = Helper::GetRandom( xInt, xInt+100 ), tree.originalPosition.y = Helper::GetRandom( yInt, yInt+100 );
            tree.type = ItemType::TREE;
            m_items.push_back( tree );
        }
    }
    // Put trees along the north side
    for ( int yInt = northEdgeT; yInt < northEdgeB; yInt += treeSpacing )
    {
        for ( int xInt = 0; xInt < mapWidth; xInt += treeSpacing )
        {
            OffsetableSprite tree;
            tree.sprite.setTexture( chalo::TextureManager::Get( "tree" ) );
            tree.originalPosition.x = Helper::GetRandom( xInt, xInt+100 ), tree.originalPosition.y = Helper::GetRandom( yInt, yInt+100 );
            tree.type = ItemType::TREE;
            m_items.push_back( tree );
        }
    }
    // Put trees along the south side
    for ( int yInt = southEdgeT; yInt < southEdgeB; yInt += treeSpacing )
    {
        for ( int xInt = 0; xInt < mapWidth; xInt += treeSpacing )
        {
            OffsetableSprite tree;
            tree.sprite.setTexture( chalo::TextureManager::Get( "tree" ) );
            tree.originalPosition.x = Helper::GetRandom( xInt, xInt+100 ), tree.originalPosition.y = Helper::GetRandom( yInt, yInt+100 );
            tree.type = ItemType::TREE;
            m_items.push_back( tree );
        }
    }

    for ( int i = 0; i < 10; i++ )
    {
        OffsetableSprite carrot;
        carrot.sprite.setTexture( chalo::TextureManager::Get( "carrot" ) );
        sf::IntRect rect = carrot.sprite.getTextureRect();
        carrot.sprite.setOrigin( sf::Vector2f( rect.width/2, rect.height/2 ) );

        int x = Helper::GetRandom( 300, mapWidth - 300 );
        int y = Helper::GetRandom( 300, mapHeight - 300 );

        carrot.originalPosition.x = x;
        carrot.originalPosition.y = y;

        carrot.type = ItemType::CARROT;

        m_items.push_back( carrot );
    }

    for ( int i = 0; i < 20; i++ )
    {
        OffsetableSprite stick;
        stick.sprite.setTexture( chalo::TextureManager::Get( "stick" ) );
        sf::IntRect rect = stick.sprite.getTextureRect();
        stick.sprite.setOrigin( sf::Vector2f( rect.width/2, rect.height/2 ) );

        int x = Helper::GetRandom( 300, mapWidth - 300 );
        int y = Helper::GetRandom( 300, mapHeight - 300 );

        stick.originalPosition.x = x;
        stick.originalPosition.y = y;

        stick.type = ItemType::LUMBER;

        m_items.push_back( stick );
    }

    for ( int i = 0; i < 20; i++ )
    {
        OffsetableSprite snowball;
        snowball.sprite.setTexture( chalo::TextureManager::Get( "snowball" ) );
        sf::IntRect rect = snowball.sprite.getTextureRect();
        snowball.sprite.setOrigin( sf::Vector2f( rect.width/2, rect.height/2 ) );

        int x = Helper::GetRandom( 300, mapWidth - 300 );
        int y = Helper::GetRandom( 300, mapHeight - 300 );

        snowball.originalPosition.x = x;
        snowball.originalPosition.y = y;

        snowball.type = ItemType::SNOWBALL;

        m_items.push_back( snowball );
    }

    for ( int i = 0; i < 20; i++ )
    {
        OffsetableSprite apple;
        apple.sprite.setTexture( chalo::TextureManager::Get( "apple" ) );
        sf::IntRect rect = apple.sprite.getTextureRect();
        apple.sprite.setOrigin( sf::Vector2f( rect.width/2, rect.height/2 ) );

        int x = Helper::GetRandom( 300, mapWidth - 300 );
        int y = Helper::GetRandom( 300, mapHeight - 300 );

        apple.originalPosition.x = x;
        apple.originalPosition.y = y;

        apple.type = ItemType::APPLE;

        m_items.push_back( apple );
    }

    for ( int i = 0; i < 20; i++ )
    {
        OffsetableSprite rabbit;
        rabbit.sprite.setTexture( chalo::TextureManager::Get( "rabbit" ) );
        sf::IntRect rect = rabbit.sprite.getTextureRect();
        rabbit.sprite.setOrigin( sf::Vector2f( rect.width/2, rect.height/2 ) );

        int x = Helper::GetRandom( 300, mapWidth - 300 );
        int y = Helper::GetRandom( 300, mapHeight - 300 );

        rabbit.originalPosition.x = x;
        rabbit.originalPosition.y = y;

        rabbit.type = ItemType::RABBIT;

        m_items.push_back( rabbit );
    }



    {
    OffsetableSprite drabis;
    drabis.sprite.setTexture( chalo::TextureManager::Get( "drabis" ) );
    drabis.originalPosition.x = Helper::GetRandom( westEdgeR + 200, eastEdgeL - 500 );
    drabis.originalPosition.y = Helper::GetRandom( northEdgeB + 200, southEdgeT - 500 );
    drabis.type = ItemType::DRABIS;
    m_horses.push_back( drabis );

    OffsetableSprite ratchet;
    ratchet.sprite.setTexture( chalo::TextureManager::Get( "ratchet" ) );
    ratchet.originalPosition.x = Helper::GetRandom( westEdgeR + 200, eastEdgeL - 500 );
    ratchet.originalPosition.y = Helper::GetRandom( northEdgeB + 200, southEdgeT - 500 );
    ratchet.type = ItemType::RATCHET;
    m_horses.push_back( ratchet );

    OffsetableSprite sneezy;
    sneezy.sprite.setTexture( chalo::TextureManager::Get( "sneezy" ) );
    sneezy.originalPosition.x = Helper::GetRandom( westEdgeR + 200, eastEdgeL - 500 );
    sneezy.originalPosition.y = Helper::GetRandom( northEdgeB + 200, southEdgeT - 500 );
    sneezy.type = ItemType::SNEEZY;
    m_horses.push_back( sneezy );

    OffsetableSprite wabeka;
    wabeka.sprite.setTexture( chalo::TextureManager::Get( "wabeka" ) );
    wabeka.originalPosition.x = Helper::GetRandom( westEdgeR + 200, eastEdgeL - 500 );
    wabeka.originalPosition.y = Helper::GetRandom( northEdgeB + 200, southEdgeT - 500 );
    wabeka.type = ItemType::WABEKA;
    m_horses.push_back( wabeka );
    }

    // Collision regions
    {
    CollisionRegion westRegion;
    westRegion.region.left      = westEdgeL;
    westRegion.region.width     = 2*screenWidth/3;
    westRegion.region.top       = northEdgeT;
    westRegion.region.height    = southEdgeB - northEdgeT + screenHeight/2;
    m_collisionRegions.push_back( westRegion );

    CollisionRegion eastRegion;
    eastRegion.region.left      = eastEdgeL;
    eastRegion.region.width     = 2*screenWidth/3;
    eastRegion.region.top       = northEdgeT;
    eastRegion.region.height    = southEdgeB - northEdgeT + screenHeight/2;
    m_collisionRegions.push_back( eastRegion );

    CollisionRegion northRegion;
    northRegion.region.left      = westEdgeL;
    northRegion.region.width     = eastEdgeR - westEdgeL + screenWidth / 2;
    northRegion.region.top       = northEdgeT;
    northRegion.region.height    = 2*screenHeight/3 + 100;
    m_collisionRegions.push_back( northRegion );

    CollisionRegion southRegion;
    southRegion.region.left      = westEdgeL;
    southRegion.region.width     = eastEdgeR - westEdgeL + screenWidth / 2;
    southRegion.region.top       = southEdgeT;
    southRegion.region.height    = 2*screenHeight/3;
    m_collisionRegions.push_back( southRegion );
    }

    m_dialogBox.Setup( screenWidth, screenHeight );

    m_mouseCooldown = 0;
    m_mouseCooldownMax = 10;

    m_music.openFromFile( "Packages/ChristmasHorse2021/Audio/ChristmasQuest.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();

    // Sound effects
    m_soundBuffers["get_rabbit"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_RabbitCaught.wav" );
    m_sounds["get_rabbit"].setBuffer( m_soundBuffers["get_rabbit"] );
    m_sounds["get_rabbit"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["get_snowball"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSPT_carrotexpire.wav" );
    m_sounds["get_snowball"].setBuffer( m_soundBuffers["get_snowball"] );
    m_sounds["get_snowball"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["get_apple"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/INVNSFX_FindArrowhead.wav" );
    m_sounds["get_apple"].setBuffer( m_soundBuffers["get_apple"] );
    m_sounds["get_apple"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["get_carrot"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSPT_carrotadd.wav" );
    m_sounds["get_carrot"].setBuffer( m_soundBuffers["get_carrot"] );
    m_sounds["get_carrot"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["get_stick"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSPT_carrotexpire2.wav" );
    m_sounds["get_stick"].setBuffer( m_soundBuffers["get_stick"] );
    m_sounds["get_stick"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["horse1"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_SpiritHappy.wav" );
    m_sounds["horse1"].setBuffer( m_soundBuffers["horse1"] );
    m_sounds["horse1"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["horse2"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_SpiritExhausted.wav" );
    m_sounds["horse2"].setBuffer( m_soundBuffers["horse2"] );
    m_sounds["horse2"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["horse3"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_SpiritSad.wav" );
    m_sounds["horse3"].setBuffer( m_soundBuffers["horse3"] );
    m_sounds["horse3"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["horse4"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_SpiritPuzzle.wav" );
    m_sounds["horse4"].setBuffer( m_soundBuffers["horse4"] );
    m_sounds["horse4"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["walkies1"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_SpiritClip1.wav" );
    m_sounds["walkies1"].setBuffer( m_soundBuffers["walkies1"] );
    m_sounds["walkies1"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["walkies2"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_SpiritClop1.wav" );
    m_sounds["walkies2"].setBuffer( m_soundBuffers["walkies2"] );
    m_sounds["walkies2"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["walkies3"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_SpiritClip2.wav" );
    m_sounds["walkies3"].setBuffer( m_soundBuffers["walkies3"] );
    m_sounds["walkies3"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffers["walkies4"].loadFromFile( "Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_SpiritClop2.wav" );
    m_sounds["walkies4"].setBuffer( m_soundBuffers["walkies4"] );
    m_sounds["walkies4"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_walkies = 1;
    m_walkiesCooldown = 0;
    m_walkiesCooldownMax = 20;


//
//    m_collectSoundBuffer.loadFromFile( "Packages/ChristmasHorse2020/Audio/carrot-collect.ogg" );
//    m_collectSound.setBuffer( m_collectSoundBuffer );
//    m_collectSound.setVolume( 10 );
//
//    m_walkSoundBuffer.loadFromFile( "Packages/ChristmasHorse2020/Audio/horse-walk.ogg" );
//    m_walkSound.setBuffer( m_walkSoundBuffer );
//    m_walkSound.setVolume( 10 );
//    m_walkSoundCooldown = 0;
//    m_walkSoundCooldownMax = 58;
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2021_GameState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    m_items.clear();
    m_horses.clear();
    m_collisionRegions.clear();
    m_quests.clear();
    m_soundBuffers.clear();
    m_sounds.clear();
    m_hudText.clear();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2021_GameState::Update()
{
    chalo::InputManager::Update();

    EscapeyState::Update();

    if ( IsPaused() )
    {
//      Logger::OutFuncEnd( "paused", CLASSNAME + "::" + std::string( __func__ ) );
      return;
    }

    m_player.Update();
    UpdateCursorState();

    if ( AllQuestsCompleted() )
    {
        // Done with Christmas Horse 2!
        SetGotoState( "ch2021_epilogueState" );
    }

    if ( m_mouseCooldown > 0 )
    {
        m_mouseCooldown--;
    }

    if ( chalo::InputManager::IsLeftClick() )
    {
        sf::Vector2i mousePos = chalo::InputManager::GetMousePosition();

        // Is player trying to talk to horse?
        bool talkies = false;
        for ( auto& horse : m_horses )
        {
            if ( MouseOverHorse( horse, mousePos ) )
            {
                m_cursorState = Direction::TALK;
                talkies = true;
                BeginTalkies( horse );
            }
        }

        // Move
        if ( !talkies )
        {
            Walkies();
            m_dialogBox.isVisible = false;
            m_player.Animate();
            m_player.ChangeDirection( m_cursorState );
            MovePlayer( m_cursorState );
        }
    }

    if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Return ) )
    {
        ResetPosition();
    }

    for ( auto& item : m_items )
    {
        if ( Helper::GetDistance( m_player.GetSpritePosition().x, m_player.GetSpritePosition().y, item.sprite.getPosition().x, item.sprite.getPosition().y ) <= 50 )
        {
            if ( item.type == ItemType::CARROT )
            {
                m_moveSpeed += 0.5;
                m_walkiesCooldownMax -= 0.5;
                m_player.AddAnimationSpeed( 0.01 );
                item.originalPosition = sf::Vector2f( -1000, -1000 ); // TODO: Change this
                m_sounds["get_carrot"].play();
                m_hudText["speed"].setString( "Speed: " + Helper::ToString( m_moveSpeed ) );
            }

            else if ( item.type == ItemType::APPLE )
            {
                int questIndex = IndexOfQuest( "apples" );
                if ( questIndex == -1 )
                {
                    // Don't have this quest yet
                }
                else
                {
                    // Update quest log
                    m_quests[questIndex].GotItem();
                    item.originalPosition = sf::Vector2f( -1000, -1000 ); // TODO: Change this
                    m_sounds["get_apple"].play();
                }
            }

            else if ( item.type == ItemType::LUMBER )
            {
                int questIndex = IndexOfQuest( "sticks" );
                if ( questIndex == -1 )
                {
                    // Don't have this quest yet
                }
                else
                {
                    // Update quest log
                    m_quests[questIndex].GotItem();
                    item.originalPosition = sf::Vector2f( -1000, -1000 ); // TODO: Change this
                    m_sounds["get_stick"].play();
                }
            }

            else if ( item.type == ItemType::RABBIT )
            {
                int questIndex = IndexOfQuest( "rabbits" );
                if ( questIndex == -1 )
                {
                    // Don't have this quest yet
                }
                else
                {
                    // Update quest log
                    m_quests[questIndex].GotItem();
                    item.originalPosition = sf::Vector2f( -1000, -1000 ); // TODO: Change this
                    m_sounds["get_rabbit"].play();
                }
            }

            else if ( item.type == ItemType::SNOWBALL )
            {
                int questIndex = IndexOfQuest( "snowballs" );
                if ( questIndex == -1 )
                {
                    // Don't have this quest yet
                }
                else
                {
                    // Update quest log
                    m_quests[questIndex].GotItem();
                    item.originalPosition = sf::Vector2f( -1000, -1000 ); // TODO: Change this
                    m_sounds["get_snowball"].play();
                }
            }
        }
    }

    for ( auto& region : m_collisionRegions )
    {
        sf::IntRect offsetRegion = region.GetOffsetRegion( m_cameraOffset );

        if ( SFMLHelper::BoundingBoxCollision( offsetRegion, m_player.GetRegion() ) )
        {
            // Horse is in an invalid region
            BumpHorse( region );
        }
    }
}

void CH2021_GameState::MovePlayer( Direction direction )
{
    float xAmt = 0;
    float yAmt = 0;

    m_diagSpeed = m_moveSpeed;// * 0.71; // Look it just feels nicer to go faster when diagonal

    switch( direction )
    {
        case Direction::EAST:      xAmt = -m_moveSpeed;     yAmt = 0;               break;
        case Direction::NORTHEAST: xAmt = -m_diagSpeed;     yAmt = m_diagSpeed;     break;
        case Direction::NORTH:     xAmt = 0;                yAmt = m_moveSpeed;     break;
        case Direction::NORTHWEST: xAmt = m_diagSpeed;      yAmt = m_diagSpeed;     break;
        case Direction::WEST:      xAmt = m_moveSpeed;      yAmt = 0;               break;
        case Direction::SOUTHWEST: xAmt = m_diagSpeed;      yAmt = -m_diagSpeed;    break;
        case Direction::SOUTH:     xAmt = 0;                yAmt = -m_moveSpeed;    break;
        case Direction::SOUTHEAST: xAmt = -m_diagSpeed;     yAmt = -m_diagSpeed;    break;
    }

    // Will Horse hit a collision region if xe moves?
    for ( auto& region : m_collisionRegions )
    {
        sf::Vector2f cameraOffsetIfMoved = m_cameraOffset;
        cameraOffsetIfMoved.x += xAmt;
        cameraOffsetIfMoved.y += yAmt;
        sf::IntRect offsetRegion = region.GetOffsetRegion( cameraOffsetIfMoved );
        sf::IntRect playerRegion = m_player.GetRegion();

        if ( SFMLHelper::BoundingBoxCollision( offsetRegion, playerRegion ) )
        {
            // Collision; cannot move this way.
            return;
        }
    }

//    m_player.Move( -xAmt, -yAmt );
    m_cameraOffset.x += xAmt;
    m_cameraOffset.y += yAmt;
}

bool CH2021_GameState::MouseOverHorse( OffsetableSprite horse, sf::Vector2i mousePos )
{
    sf::IntRect horseRect;
    horseRect.left = horse.sprite.getPosition().x;
    horseRect.top = horse.sprite.getPosition().y;
    horseRect.width = horse.sprite.getTextureRect().width;
    horseRect.height = horse.sprite.getTextureRect().height;
    return SFMLHelper::PointInBoxCollision( mousePos, horseRect );
}

void CH2021_GameState::UpdateCursorState()
{
    sf::Vector2i mousePos = chalo::InputManager::GetMousePosition();
    m_cursor.setPosition( mousePos.x, mousePos.y );
    m_cursorState = m_player.CompareMousePos( mousePos );

    // Check to see if mouse is hovering over a horse
    for ( auto& horse : m_horses )
    {
        if ( MouseOverHorse( horse, mousePos ) )
        {
            m_cursorState = Direction::TALK;
        }
    }

    // Update cursor image
    switch( m_cursorState )
    {
        case Direction::EAST:      m_cursor.setTextureRect( sf::IntRect( 0, 0*32, 32, 32 ) ); break;
        case Direction::NORTHEAST: m_cursor.setTextureRect( sf::IntRect( 0, 1*32, 32, 32 ) ); break;
        case Direction::NORTH:     m_cursor.setTextureRect( sf::IntRect( 0, 2*32, 32, 32 ) ); break;
        case Direction::NORTHWEST: m_cursor.setTextureRect( sf::IntRect( 0, 3*32, 32, 32 ) ); break;
        case Direction::WEST:      m_cursor.setTextureRect( sf::IntRect( 0, 4*32, 32, 32 ) ); break;
        case Direction::SOUTHWEST: m_cursor.setTextureRect( sf::IntRect( 0, 5*32, 32, 32 ) ); break;
        case Direction::SOUTH:     m_cursor.setTextureRect( sf::IntRect( 0, 6*32, 32, 32 ) ); break;
        case Direction::SOUTHEAST: m_cursor.setTextureRect( sf::IntRect( 0, 7*32, 32, 32 ) ); break;
        case Direction::TALK:      m_cursor.setTextureRect( sf::IntRect( 0, 8*32, 32, 32 ) ); break;
        case Direction::CENTER:    m_cursor.setTextureRect( sf::IntRect( 0, 9*32, 32, 32 ) ); break;
    }
}

void CH2021_GameState::BumpHorse( CollisionRegion region )
{
    float xAmt = 0;
    float yAmt = 0;

    sf::Vector2f regionCenter = region.GetCenterPoint( m_cameraOffset );
    sf::Vector2f horseCenter = m_player.GetSpritePosition();

    if ( regionCenter.x < horseCenter.x )                                       // Collision region is to the left
    {
        xAmt = 1; // Move right
    }
    else if ( regionCenter.x > horseCenter.x )                                  // Collision region is to the right
    {
        xAmt = -1; // Move left
    }

    if ( regionCenter.y < horseCenter.y )                                       // Collision region is above
    {
        yAmt = 1; // Move down
    }
    else if ( regionCenter.y > horseCenter.y )                                  // Collision region is below
    {
        yAmt = -1; // Move up
    }

    m_cameraOffset.x -= xAmt;
    m_cameraOffset.y -= yAmt;
}

void CH2021_GameState::ResetPosition()
{
    m_cameraOffset.x = 0;
    m_cameraOffset.y = 0;
}

void CH2021_GameState::BeginTalkies( OffsetableSprite horse )
{
    std::vector<sf::Text> dialogTexts;
    sf::Text text;
    text.setFont( chalo::FontManager::Get( "main" ) );
    text.setCharacterSize( 16 );
    text.setFillColor( sf::Color::White );
    text.setOutlineColor( sf::Color::Black );
    text.setOutlineThickness( 2 );

    int textX = m_dialogBox.position.x + 200;
    int textY = m_dialogBox.position.y + 30;

    if ( m_mouseCooldown > 0 )
    {
        return;
    }
    else
    {
        m_mouseCooldown = m_mouseCooldownMax;
    }

    // Set up dialog text
    if ( horse.type == ItemType::DRABIS )
    {
        m_sounds["horse1"].play();
        if ( HaveQuest( "apples" ) && !IsQuestComplete( "apples" ) )
        {
            text.setString( "Santa Horse --" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
            text.setString( "We need stockpiles of food." );
            text.setPosition( textX, textY + 20 );
            dialogTexts.push_back( text );
            text.setString( "Collect apples for our army." );
            text.setPosition( textX, textY + 40 );
            dialogTexts.push_back( text );
        }
        else if ( HaveQuest( "apples" ) && IsQuestComplete( "apples" ) )
        {
            text.setString( "You can collect carrots" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
            text.setString( "to gain energy to" );
            text.setPosition( textX, textY + 20 );
            dialogTexts.push_back( text );
            text.setString( "walk faster." );
            text.setPosition( textX, textY + 40 );
            dialogTexts.push_back( text );
        }
        else
        {
            text.setString( "Santa Horse --" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
            text.setString( "We need stockpiles of food." );
            text.setPosition( textX, textY + 20 );
            dialogTexts.push_back( text );
            text.setString( "Collect apples for our army." );
            text.setPosition( textX, textY + 40 );
            dialogTexts.push_back( text );

            // Add quest
            Quest apples;
            apples.Setup( m_quests.size(), "apples", "Collect apples", 5 );
            m_quests.push_back( apples );
        }
    }
    else if ( horse.type == ItemType::RATCHET )
    {
        m_sounds["horse2"].play();
        if ( HaveQuest( "sticks" ) && !IsQuestComplete( "sticks" ) )
        {
            text.setString( "In order to build a fortress" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
            text.setString( "we need lumber." );
            text.setPosition( textX, textY + 20 );
            dialogTexts.push_back( text );
            text.setString( "Please pick sticks." );
            text.setPosition( textX, textY + 40 );
            dialogTexts.push_back( text );
        }
        else if ( HaveQuest( "sticks" ) && IsQuestComplete( "sticks" ) )
        {
            text.setString( "Raisins?" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
        }
        else
        {
            text.setString( "In order to build a fortress" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
            text.setString( "we need lumber." );
            text.setPosition( textX, textY + 20 );
            dialogTexts.push_back( text );
            text.setString( "Please pick sticks." );
            text.setPosition( textX, textY + 40 );
            dialogTexts.push_back( text );

            // Add quest
            Quest sticks;
            sticks.Setup( m_quests.size(), "sticks", "Collect sticks", 10 );
            m_quests.push_back( sticks );
        }
    }
    else if ( horse.type == ItemType::SNEEZY )
    {
        m_sounds["horse3"].play();
        if ( HaveQuest( "rabbits" ) && !IsQuestComplete( "rabbits" ) )
        {
            text.setString( "We will need leather for" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
            text.setString( "our armor. Make sure to" );
            text.setPosition( textX, textY + 20 );
            dialogTexts.push_back( text );
            text.setString( "collect rabbits." );
            text.setPosition( textX, textY + 40 );
            dialogTexts.push_back( text );
        }
        else if ( HaveQuest( "rabbits" ) && IsQuestComplete( "rabbits" ) )
        {
            text.setString( "Time to skin some rabbits." );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
        }
        else
        {
            text.setString( "We will need leather for" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
            text.setString( "our armor. Make sure to" );
            text.setPosition( textX, textY + 20 );
            dialogTexts.push_back( text );
            text.setString( "collect rabbits." );
            text.setPosition( textX, textY + 40 );
            dialogTexts.push_back( text );

            // Add quest
            Quest rabbits;
            rabbits.Setup( m_quests.size(), "rabbits", "Collect rabbits", 5 );
            m_quests.push_back( rabbits );
        }
    }
    else if ( horse.type == ItemType::WABEKA )
    {
        m_sounds["horse4"].play();
        if ( HaveQuest( "snowballs" ) && !IsQuestComplete( "snowballs" ) )
        {
            text.setString( "We will need to practice" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
            text.setString( "fighting; collect enough" );
            text.setPosition( textX, textY + 20 );
            dialogTexts.push_back( text );
            text.setString( "snowballs for us to make" );
            text.setPosition( textX, textY + 40 );
            dialogTexts.push_back( text );
            text.setString( "snowelf dummies." );
            text.setPosition( textX, textY + 60 );
            dialogTexts.push_back( text );
        }
        else if ( HaveQuest( "snowballs" ) && IsQuestComplete( "snowballs" ) )
        {
            text.setString( "Thank you, Santa Horse!" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
        }
        else
        {
            text.setString( "We will need to practice" );
            text.setPosition( textX, textY );
            dialogTexts.push_back( text );
            text.setString( "fighting; collect enough" );
            text.setPosition( textX, textY + 20 );
            dialogTexts.push_back( text );
            text.setString( "snowballs for us to make" );
            text.setPosition( textX, textY + 40 );
            dialogTexts.push_back( text );
            text.setString( "snowelf dummies." );
            text.setPosition( textX, textY + 60 );
            dialogTexts.push_back( text );

            // Add quest
            Quest snowballs;
            snowballs.Setup( m_quests.size(), "snowballs", "Collect snowballs", 9 );
            m_quests.push_back( snowballs );
        }
    }

    dialogTexts.push_back( text );

    m_dialogBox.UpdateText( horse.type, dialogTexts );
    m_dialogBox.isVisible = true;
}

void CH2021_GameState::Draw( sf::RenderWindow& window )
{
    for ( auto& item : m_items )
    {
        item.Draw( window, m_cameraOffset );
    }

    for ( auto& item : m_horses )
    {
        item.Draw( window, m_cameraOffset );
    }

    m_player.Draw( window );
    window.draw( m_cursor );

    window.draw( m_hudBackgrounds[0] );
    window.draw( m_hudBackgrounds[1] );
    window.draw( m_hudPortrait );
    for ( auto& tx : m_hudText )
    {
      window.draw( tx.second );
    }

    int minimapX = 1080;
    int minimapY = 720-120;

    // Minimap
    window.draw( m_hudMapBg );
    for ( auto& item : m_items )
    {
        sf::CircleShape shape;
        shape.setFillColor( sf::Color::Green );
        shape.setRadius( 1 );
        shape.setPosition( item.originalPosition.x / 25 + minimapX, item.originalPosition.y / 25 + minimapY );
        window.draw( shape );
    }

    for ( auto& item : m_horses )
    {
        sf::CircleShape shape;
        shape.setFillColor( sf::Color::Yellow );
        shape.setRadius( 2 );
        shape.setPosition( item.originalPosition.x / 25 + minimapX, item.originalPosition.y / 25 + minimapY );
        window.draw( shape );
    }

    sf::CircleShape shape;
    shape.setFillColor( sf::Color::Red );
    shape.setRadius( 3 );
    shape.setPosition( -m_cameraOffset.x / 25 + minimapX, -m_cameraOffset.y / 25 + minimapY );
    window.draw( shape );

    for ( auto& region : m_collisionRegions )
    {
//        region.Draw( window, m_cameraOffset );
    }

    if ( m_dialogBox.isVisible )
    {
        m_dialogBox.Draw( window );
    }

    for ( auto& quest : m_quests )
    {
        quest.Draw( window );
    }

    EscapeyState::Draw( window );

    chalo::DrawManager::Draw( window );
}

bool CH2021_GameState::HaveQuest( std::string questName )
{
    for ( auto& quest : m_quests )
    {
        if ( quest.name == questName ) { return true; }
    }
    return false;
}

bool CH2021_GameState::IsQuestComplete( std::string questName )
{
    for ( auto& quest : m_quests )
    {
        if ( quest.name == questName )
        {
            return quest.completed;
        }
    }
    return false;
}

int CH2021_GameState::IndexOfQuest( std::string questName )
{
    for ( size_t i = 0; i < m_quests.size(); i++ )
    {
        if ( m_quests[i].name == questName )
        {
            return i;
        }
    }
    return -1;
}

bool CH2021_GameState::AllQuestsCompleted()
{
    if ( m_quests.size() < 4 ) { return false; }

    for ( auto& quest : m_quests )
    {
        if ( quest.completed == false )
        {
            return false;
        }
    }
    return true;
}

void CH2021_GameState::Walkies()
{
    if ( m_walkiesCooldown > 0 )
    {
        m_walkiesCooldown--;
    }
    else
    {
        m_walkiesCooldown = m_walkiesCooldownMax;
        std::string walkStr = Helper::ToString( m_walkies );
        m_sounds["walkies" + walkStr].play();

        m_walkies++;
        if ( m_walkies > 4 )
        {
            m_walkies = 1;
        }
    }
}
