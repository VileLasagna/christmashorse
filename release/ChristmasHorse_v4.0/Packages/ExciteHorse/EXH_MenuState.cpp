//#include "EXH_MenuState.hpp"
//
//#include "../../chalo-engine/Application/Application.hpp"
//#include "../../chalo-engine/Managers/MenuManager.hpp"
//#include "../../chalo-engine/Managers/InputManager.hpp"
//#include "../../chalo-engine/Managers/ConfigManager.hpp"
//#include "../../chalo-engine/Utilities/Messager.hpp"
//#include "../../chalo-engine/Utilities/Logger.hpp"
//
//ExciteHorseMenuState::ExciteHorseMenuState()
//    : IState()
//{
//}
//
//void ExciteHorseMenuState::Init( const std::string& name )
//{
////    chalo::Logger::Out( "Parameters - name: " + name, "ExciteHorseMenuState::Init", "function-trace" );
//    IState::Init( name );
//}
//
//void ExciteHorseMenuState::Setup()
//{
////    chalo::Logger::Out( "", "ExciteHorseMenuState::Setup", "function-trace" );
//    IState::Setup();
//
//    // Which state comes before?
//    m_backState = "startupstate";
//
//    chalo::TextureManager::Add( "cursor1",  "Content/Graphics/UI/cursor1.png" );
//    chalo::TextureManager::Add( "cursor2",  "Content/Graphics/UI/cursor2.png" );
//    chalo::TextureManager::Add( "cursor3",  "Content/Graphics/UI/cursor3.png" );
//    chalo::TextureManager::Add( "cursor4",  "Content/Graphics/UI/cursor4.png" );
//
//    chalo::TextureManager::Add( "bmpBackground", "Packages/QuixAttaxDeux/content/quiximgBackgroundMenu.png" );
//    chalo::TextureManager::Add( "bmpWindow", "Packages/QuixAttaxDeux/content/quiximgWindow.png" );
//    chalo::TextureManager::Add( "bmpExit", "Packages/QuixAttaxDeux/content/quiximgExitButton.png" );
//    chalo::TextureManager::Add( "bmpExitDepress", "Packages/QuixAttaxDeux/content/quiximgExitButtonB.png" );
//    chalo::TextureManager::Add( "bmpPlay", "Packages/QuixAttaxDeux/content/quiximgPlayButton.png" );
//    chalo::TextureManager::Add( "bmpPlayDepress", "Packages/QuixAttaxDeux/content/quiximgPlayButtonB.png" );
//    chalo::TextureManager::Add( "bmpCursor", "Packages/QuixAttaxDeux/content/quiximgCursor.png" );
//    chalo::TextureManager::Add( "bmpControlScheme", "Packages/QuixAttaxDeux/content/quiximgKeySchemes.png" );
//    chalo::TextureManager::Add( "bmpArrows", "Packages/QuixAttaxDeux/content/quiximgArrows.png" );
//    chalo::TextureManager::Add( "bmpScreenOpts", "Packages/QuixAttaxDeux/content/quiximgWindowOpts.png" );
//    chalo::TextureManager::Add( "btnCharacter", "Packages/QuixAttaxDeux/content/btnCharacter.png" );
//    chalo::TextureManager::Add( "characters", "Packages/QuixAttaxDeux/content/quiximgPlayerSet.png" );
//    chalo::TextureManager::Add( "btnBackground", "Packages/QuixAttaxDeux/content/btnBackground.png" );
//    chalo::TextureManager::Add( "btnBlue", "Packages/QuixAttaxDeux/content/btnBlue.png" );
//
//    chalo::TextureManager::Add( "horseOptions", "Packages/ExciteHorse/graphics/horse-select2x.png" );
//    chalo::TextureManager::Add( "title", "Packages/ExciteHorse/graphics/title.png" );
//
//    chalo::MenuManager::LoadTextMenu( "excitehorse.chalomenu" );
//
//    sf::Vector2f pos( 360, 700 );
//    sf::IntRect dimensions( 0, 0, 300, 35 );
//
//    SetPlayerCharacter( 0, 0 );
//    SetPlayerCharacter( 1, 1 );
//    SetPlayerCharacter( 2, 2 );
//    SetPlayerCharacter( 3, 3 );
//
//    for ( int i = 0; i < 4; i++ )
//    {
//        m_gamepadCooldown[i] = 0;
//        m_gamepadCooldownMax[i] = 10;
//
//        m_isActive[4] = false;
//        m_isReady[4] = false;
//    }
//
//    m_isActive[0] = true;
//
//    m_fragOption = 0;
//    m_barrierOption = 0;
//
//    // Don't display players 2 - 4 until they're active...
//    for ( int i = 2; i <= 4; i++ )
//    {
//        DeactivatePlayer( i );
//    }
//}
//
//void ExciteHorseMenuState::Cleanup()
//{
////    chalo::Logger::Out( "", "ExciteHorseMenuState::Cleanup", "function-trace" );
//    IState::Cleanup();
//}
//
//void ExciteHorseMenuState::Update()
//{
//    IState::Update();
//
//    // Check player inputs
//    for ( int i = 0; i < 4; i++ )
//    {
//        bool pressedButton = true;
//
//        if ( m_gamepadCooldown[i] > 0 )
//        {
//            m_gamepadCooldown[i]--;
//            continue;
//        }
//
//        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::INPUT_WEST ) ) )
//        {
//            ActivatePlayer( i );
//            ChangePlayerCharacter( i, -1 );
//        }
//        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::INPUT_EAST ) ) )
//        {
//            ActivatePlayer( i );
//            ChangePlayerCharacter( i, 1 );
//        }
//        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::INPUT_ACTION1 ) ) )
//        {
//            if ( m_isActive[i] )
//            {
//                SetPlayerReady( i, true );
//            }
//            else
//            {
//                ActivatePlayer( i );
//            }
//        }
//        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::INPUT_ACTION2 ) ) )
//        {
//            if ( m_isReady[i] )
//            {
//                SetPlayerReady( i, false );
//            }
//            else
//            {
//                DeactivatePlayer( i );
//            }
//        }
//        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::INPUT_NORTH ) ) )
//        {
//            ChangeFragOption( 1 );
//        }
//        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::INPUT_SOUTH ) ) )
//        {
//            ChangeBarrierOption( 1 );
//        }
//        else
//        {
//            pressedButton = false;
//        }
//
//        if ( pressedButton )
//        {
//            m_gamepadCooldown[i] = m_gamepadCooldownMax[i];
//        }
//    }
//
//    // Check inputs - frags/barriers options
//
////    VamooseState::BackButtonHandler();
//}
//
//void ExciteHorseMenuState::Draw( sf::RenderWindow& window )
//{
////    VamooseState::Draw( window );
//}
//
//void ExciteHorseMenuState::SetPlayerCharacter( int index, int character )
//{
//    // Image for their player
//    sf::IntRect frameRect;
//    frameRect.left = character * 128; // index * 128;
//    frameRect.top = 0; //character * 128;
//    frameRect.width = 128;
//    frameRect.height = 128;
//
//    // Update the sprite they're using
//    std::string key = "imgPlayer" + Helper::ToString( index + 1 );
//    chalo::UIImage& img = chalo::MenuManager::GetImage( "main", key );
//    img.SetImageClipRect( frameRect );
//
//    m_selectedCharacters[index] = character;
//}
//
//void ExciteHorseMenuState::ChangePlayerCharacter( int index, int direction )
//{
//    m_selectedCharacters[index] += direction;
//    if ( m_selectedCharacters[index] < 0 )      { m_selectedCharacters[index] = 4; }
//    else if ( m_selectedCharacters[index] > 4 ) { m_selectedCharacters[index] = 0; }
//
//    SetPlayerCharacter( index, m_selectedCharacters[index] );
//}
//
//void ExciteHorseMenuState::ChangeFragOption( int direction )
//{
//    m_fragOption += direction;
//    if      ( m_fragOption < 1 ) { m_fragOption = 5; }
//    else if ( m_fragOption > 5 ) { m_fragOption = 1; }
//    SetFragOption( m_fragOption );
//}
//
//void ExciteHorseMenuState::SetFragOption( int value )
//{
//    // Reset frag buttons
//    for ( int i = 1; i <= 5; i++ )
//    {
//        sf::IntRect defaultRect;
//        defaultRect.left = 0;
//        defaultRect.top = 0;
//        defaultRect.width = 150;
//        defaultRect.height = 50;
//
//        chalo::UIButton& frag = chalo::MenuManager::GetButton( "main", "btnFragLimit" + Helper::ToString( i ) );
//        frag.SetupBackgroundImageClipRect( defaultRect );
//    }
//
//    sf::IntRect activeRect;
//    activeRect.left = 0;
//    activeRect.top = 50;
//    activeRect.width = 150;
//    activeRect.height = 50;
//
//    chalo::UIButton& frag = chalo::MenuManager::GetButton( "main", "btnFragLimit" + Helper::ToString( value ) );
//    frag.SetupBackgroundImageClipRect( activeRect );
//}
//
//void ExciteHorseMenuState::ChangeBarrierOption( int direction )
//{
//    m_barrierOption += direction;
//    if ( m_barrierOption < 1 )      { m_barrierOption = 5; }
//    else if ( m_barrierOption > 5 ) { m_barrierOption = 1; }
//    SetBarrierOption( m_barrierOption );
//}
//
//void ExciteHorseMenuState::SetBarrierOption( int value )
//{
//    // Reset barrier buttons
//    for ( int i = 1; i <= 5; i++ )
//    {
//        sf::IntRect defaultRect;
//        defaultRect.left = 0;
//        defaultRect.top = 0;
//        defaultRect.width = 150;
//        defaultRect.height = 50;
//
//        chalo::UIButton& barrier = chalo::MenuManager::GetButton( "main", "btnBarrierStyle" + Helper::ToString( i ) );
//        barrier.SetupBackgroundImageClipRect( defaultRect );
//    }
//
//    sf::IntRect activeRect;
//    activeRect.left = 0;
//    activeRect.top = 50;
//    activeRect.width = 150;
//    activeRect.height = 50;
//
//    chalo::UIButton& barrier = chalo::MenuManager::GetButton( "main", "btnBarrierStyle" + Helper::ToString( value ) );
//    barrier.SetupBackgroundImageClipRect( activeRect );
//}
//
//void ExciteHorseMenuState::ActivatePlayer( int index )
//{
//    m_isActive[index] = true;
//    std::string key = Helper::ToString( index+1 );
//    chalo::MenuManager::GetLabel( "main", "lblPlayer" + key ).SetIsVisible( true );
//    chalo::MenuManager::GetButton( "main", "btnPlayer" + key + "Box" ).SetIsVisible( true );
//    chalo::MenuManager::GetImage( "main", "imgPlayer" + key ).SetIsVisible( true );
//    chalo::MenuManager::GetLabel( "main", "lblPlayer" + key + "Ready" ).SetIsVisible( true );
//}
//
//
//void ExciteHorseMenuState::DeactivatePlayer( int index )
//{
//    m_isActive[index] = false;
//    std::string key = chalo::StringUtility::IntegerToString( index+1 );
//    chalo::MenuManager::GetLabel( "main", "lblPlayer" + key ).SetIsVisible( false );
//    chalo::MenuManager::GetButton( "main", "btnPlayer" + key + "Box" ).SetIsVisible( false );
//    chalo::MenuManager::GetImage( "main", "imgPlayer" + key ).SetIsVisible( false );
//    chalo::MenuManager::GetLabel( "main", "lblPlayer" + key + "Ready" ).SetIsVisible( false );
//}
//
//void ExciteHorseMenuState::SetPlayerReady( int index, bool value )
//{
//    m_isReady[index] = value;
//
//    std::string key = chalo::StringUtility::IntegerToString( index+1 );
//
//    std::string text = "Not ready";
//    if ( value == true )
//    {
//        text = "Ready!!!!";
//    }
//    chalo::MenuManager::GetLabel( "main", "lblPlayer" + key + "Ready" ).SetText( text );
//
//    // Check if all players are ready
//    if ( GetTotalReady() == GetTotalActive() )
//    {
//        GotoGame();
//    }
//}
//
//int ExciteHorseMenuState::GetTotalReady()
//{
//    int count = 0;
//    for ( int i = 0; i < 4; i++ )
//    {
//        if ( m_isReady[i] )
//        {
//            count++;
//        }
//    }
//    return count;
//}
//
//int ExciteHorseMenuState::GetTotalActive()
//{
//    int count = 0;
//    for ( int i = 0; i < 4; i++ )
//    {
//        if ( m_isActive[i] )
//        {
//            count++;
//        }
//    }
//    return count;
//}
//
//void ExciteHorseMenuState::GotoGame()
//{
//    // Set up game settings to save
//    for ( int i = 0; i < 4; i++ )
//    {
//        std::string index = chalo::StringUtility::IntegerToString( i );
//        chalo::Messager::Set( "IsPlayer"    + index + "Active", m_isActive[i] );
//        chalo::Messager::Set( "Player"      + index + "SelectedCharacter", m_selectedCharacters[i] );
//    }
//
//    chalo::Messager::Set( "BarrierOption", m_barrierOption );
//    chalo::Messager::Set( "FragOption", m_fragOption );
//
//    SetGotoState( "quixattaxdeuxstate" );
//}
//
//
