#include "EXH_Map.hpp"

namespace ExciteHorse
{

ObstaclePlacement::ObstaclePlacement()
{
}

ObstaclePlacement::ObstaclePlacement( int nx, int ny, ObstacleType nt )
{
    x = nx;
    y = ny;
    type = nt;
}

Map::Map()
{
    int trackHeight = 113;
    int xStart = 1280;
    int yStart = 268;
    int yRange = 720 - 268;
    // y range: 268 to 720
    int ys[] = {
        yStart+(trackHeight*0.0), // 0
        yStart+(trackHeight*0.5), // 1
        yStart+(trackHeight*1.0), // 2
        yStart+(trackHeight*1.5), // 3
        yStart+(trackHeight*2.0), // 4
        yStart+(trackHeight*2.5), // 5
        yStart+(trackHeight*3.0), // 6
        yStart+(trackHeight*3.5), // 7
    };


    // Predetermined
    int x = xStart;
    for ( int y = 0; y < 8; y += 2 )
    {
        placements.push_back( ObstaclePlacement( x, ys[y], ObstacleType::SPEEDUP ) );
    }

    x += 250;
    for ( int y = 1; y < 8; y += 2 )
    {
        placements.push_back( ObstaclePlacement( x, ys[y], ObstacleType::SPEEDUP ) );
    }

    x = xStart + 750;
    for ( int y = 0; y < 8; y += 2 )
    {
        placements.push_back( ObstaclePlacement( x, ys[y], ObstacleType::INCLINE ) );
    }

    x += 250;
    for ( int y = 1; y < 8; y += 2 )
    {
        placements.push_back( ObstaclePlacement( x, ys[y], ObstacleType::INCLINE ) );
    }

    x = xStart + 1500;
    for ( int y = 0; y < 8; y += 2 )
    {
        placements.push_back( ObstaclePlacement( x, ys[y], ObstacleType::SLOWDOWN ) );
    }

    x += 250;
    for ( int y = 1; y < 8; y += 2 )
    {
        placements.push_back( ObstaclePlacement( x, ys[y], ObstacleType::SLOWDOWN ) );
    }

    x = xStart + 2000;

    placements.push_back( ObstaclePlacement( x, ys[1], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[6], ObstacleType::SPEEDUP ) );

    x += 300;
    placements.push_back( ObstaclePlacement( x, ys[0], ObstacleType::SLOWDOWN ) );
    placements.push_back( ObstaclePlacement( x, ys[7], ObstacleType::SLOWDOWN ) );

    x += 300;
    placements.push_back( ObstaclePlacement( x, ys[1], ObstacleType::SLOWDOWN ) );
    placements.push_back( ObstaclePlacement( x, ys[6], ObstacleType::SLOWDOWN ) );

    x += 300;
    placements.push_back( ObstaclePlacement( x, ys[2], ObstacleType::SLOWDOWN ) );
    placements.push_back( ObstaclePlacement( x, ys[5], ObstacleType::SLOWDOWN ) );

    x += 200;
    placements.push_back( ObstaclePlacement( x, ys[3], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[4], ObstacleType::SPEEDUP ) );

    x += 300;
    placements.push_back( ObstaclePlacement( x, ys[0], ObstacleType::SLOWDOWN ) );
    placements.push_back( ObstaclePlacement( x, ys[2], ObstacleType::SLOWDOWN ) );
    placements.push_back( ObstaclePlacement( x, ys[3], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[4], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[5], ObstacleType::SLOWDOWN ) );
    placements.push_back( ObstaclePlacement( x, ys[7], ObstacleType::SLOWDOWN ) );

    x += 300;
    placements.push_back( ObstaclePlacement( x, ys[7], ObstacleType::SLOWDOWN ) );        x += 50;
    placements.push_back( ObstaclePlacement( x, ys[6], ObstacleType::SLOWDOWN ) );        x += 50;
    placements.push_back( ObstaclePlacement( x, ys[5], ObstacleType::SLOWDOWN ) );        x += 50;
    placements.push_back( ObstaclePlacement( x, ys[4], ObstacleType::SLOWDOWN ) );        x += 50;
    placements.push_back( ObstaclePlacement( x, ys[3], ObstacleType::SLOWDOWN ) );        x += 50;

    x += 200;
    placements.push_back( ObstaclePlacement( x, ys[0], ObstacleType::SLOWDOWN ) );        x += 50;
    placements.push_back( ObstaclePlacement( x, ys[1], ObstacleType::SLOWDOWN ) );        x += 50;
    placements.push_back( ObstaclePlacement( x, ys[2], ObstacleType::SLOWDOWN ) );        x += 50;
    placements.push_back( ObstaclePlacement( x, ys[3], ObstacleType::SLOWDOWN ) );        x += 50;
    placements.push_back( ObstaclePlacement( x, ys[4], ObstacleType::SLOWDOWN ) );        x += 50;

    x += 400;
    placements.push_back( ObstaclePlacement( x, ys[2], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[3], ObstacleType::INCLINE ) );
    placements.push_back( ObstaclePlacement( x, ys[4], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[5], ObstacleType::INCLINE ) );
    placements.push_back( ObstaclePlacement( x, ys[6], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[7], ObstacleType::INCLINE ) );

    x += 800;
    placements.push_back( ObstaclePlacement( x, ys[0], ObstacleType::INCLINE ) );
    placements.push_back( ObstaclePlacement( x, ys[1], ObstacleType::INCLINE ) );
    placements.push_back( ObstaclePlacement( x, ys[6], ObstacleType::INCLINE ) );
    placements.push_back( ObstaclePlacement( x, ys[7], ObstacleType::INCLINE ) );

    x += 100;
    placements.push_back( ObstaclePlacement( x, ys[3], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[4], ObstacleType::SPEEDUP ) );

    x += 800;
    placements.push_back( ObstaclePlacement( x, ys[0], ObstacleType::INCLINE ) );
    placements.push_back( ObstaclePlacement( x, ys[1], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[6], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[7], ObstacleType::INCLINE ) );

    x += 250;
    placements.push_back( ObstaclePlacement( x, ys[1], ObstacleType::INCLINE ) );
    placements.push_back( ObstaclePlacement( x, ys[2], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[5], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[6], ObstacleType::INCLINE ) );

    x += 250;
    placements.push_back( ObstaclePlacement( x, ys[2], ObstacleType::INCLINE ) );
    placements.push_back( ObstaclePlacement( x, ys[3], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[4], ObstacleType::SPEEDUP ) );
    placements.push_back( ObstaclePlacement( x, ys[5], ObstacleType::INCLINE ) );


    x += 500;
    placements.push_back( ObstaclePlacement( x, ys[0] - 64, ObstacleType::FINISHLINE ) );

}

}
