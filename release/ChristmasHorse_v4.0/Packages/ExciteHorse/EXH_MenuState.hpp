//#ifndef _ExciteHorseMenuState
//#define _ExciteHorseMenuState
//
//#include "../../chalo-engine/States/IState.hpp"
//#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
//#include "../../chalo-engine/Maps/WritableMap.hpp"
//#include "../../chalo-engine/Managers/TextureManager.hpp"
//#include "../../chalo-engine/Managers/FontManager.hpp"
//#include "../../chalo-engine/Managers/DrawManager.hpp"
//
//#include <vector>
//
//class ExciteHorseMenuState : public chalo::IState //: public VamooseState
//{
//public:
//    ExciteHorseMenuState();
//
//    virtual void Init( const std::string& name );
//    virtual void Setup();
//    virtual void Cleanup();
//    virtual void Update();
//    virtual void Draw( sf::RenderWindow& window );
//
//    private:
//    int m_selectedCharacters[4];
//    int m_fragOption;
//    int m_barrierOption;
//
//    bool m_isActive[4];
//    bool m_isReady[4];
//
//    int m_gamepadCooldown[4];       // These should be part of InputManager but it'd be faster
//    int m_gamepadCooldownMax[4];    // to do it here right now.
//    std::string m_backState;
//
//    void SetPlayerCharacter( int index, int character );
//    void ChangePlayerCharacter( int index, int direction );
//    void ChangeFragOption( int direction );
//    void SetFragOption( int value );
//    void ChangeBarrierOption( int direction );
//    void SetBarrierOption( int value );
//
//    void ActivatePlayer( int index );
//    void DeactivatePlayer( int index );
//    void SetPlayerReady( int index, bool value );
//
//    int GetTotalReady();
//    int GetTotalActive();
//
//    void GotoGame();
//};
//
//#endif
