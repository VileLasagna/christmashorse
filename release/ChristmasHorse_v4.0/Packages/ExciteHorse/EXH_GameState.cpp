//#include "EXH_GameState.hpp"
//
//#include "../../chalo-engine/Managers/MenuManager.hpp"
//#include "../../chalo-engine/Managers/InputManager.hpp"
//#include "../../chalo-engine/Managers/ConfigManager.hpp"
//#include "../../chalo-engine/Utilities/Messager.hpp"
//#include "../../chalo-engine/Utilities/Logger.hpp"
//#include "../../chalo-engine/Application/Application.hpp"
//
//#include <SFML/Audio.hpp>
//
//ExciteHorseGameState::ExciteHorseGameState()
//{
//}
//
//void ExciteHorseGameState::Init( const std::string& name )
//{
////    chalo::Logger::Out( "Parameters - name: " + name, "ExciteHorseGameState::Init", "function-trace" );
//    IState::Init( name );
//}
//
//void ExciteHorseGameState::Setup()
//{
////    chalo::Logger::Out( "", "ExciteHorseGameState::Setup", "function-trace" );
//    IState::Setup();
//
//    // Avatars are going to be used instead
////    chalo::TextureManager::Add( "horse1" , "Packages/ExciteHorse/graphics/horse1.png" );
////    chalo::TextureManager::Add( "horse2", "Packages/ExciteHorse/graphics/horse2.png" );
////    chalo::TextureManager::Add( "horse3", "Packages/ExciteHorse/graphics/horse3.png" );
////    chalo::TextureManager::Add( "horse4", "Packages/ExciteHorse/graphics/horse4.png" );
////    chalo::TextureManager::Add( "horse5", "Packages/ExciteHorse/graphics/horse5.png" );
////    chalo::TextureManager::Add( "horse6", "Packages/ExciteHorse/graphics/horse6.png" );
//
//
//
//    chalo::TextureManager::Add( "track1", "Packages/ExciteHorse/graphics/track1.png" );
//    chalo::TextureManager::Add( "incline", "Packages/ExciteHorse/graphics/incline.png" );
//    chalo::TextureManager::Add( "speedup", "Packages/ExciteHorse/graphics/speedup.png" );
//    chalo::TextureManager::Add( "slowdown", "Packages/ExciteHorse/graphics/slowdown.png" );
//    chalo::TextureManager::Add( "finishline", "Packages/ExciteHorse/graphics/finishline.png" );
//
//    m_players.clear();
//    m_obstacles.clear();
//    m_track.clear();
//
//    m_soundBuffers["incline"].loadFromFile( "Packages/ExciteHorse/audio/incline.ogg" );
//    m_soundBuffers["speedup"].loadFromFile( "Packages/ExciteHorse/audio/speedup.ogg" );
//    m_soundBuffers["slowdown"].loadFromFile( "Packages/ExciteHorse/audio/slowdown.ogg" );
//    m_soundBuffers["youwin"].loadFromFile( "Packages/ExciteHorse/audio/youwin.ogg" );
//    m_soundBuffers["fart"].loadFromFile( "Packages/ExciteHorse/audio/fart.ogg" );
//
//    m_sounds["incline"].setBuffer( m_soundBuffers["incline"] );
//    m_sounds["speedup"].setBuffer( m_soundBuffers["speedup"] );
//    m_sounds["slowdown"].setBuffer( m_soundBuffers["slowdown"] );
//    m_sounds["youwin"].setBuffer( m_soundBuffers["youwin"] );
//    m_sounds["fart"].setBuffer( m_soundBuffers["fart"] );
//
//    m_sounds["incline"].setVolume( 5 );
//    m_sounds["speedup"].setVolume( 5 );
//    m_sounds["slowdown"].setVolume( 5 );
//    m_sounds["youwin"].setVolume( 5 );
//    m_sounds["fart"].setVolume( 5 );
//
//    chalo::InputManager::Setup();
//
//    m_screenScrollRate = 2;
//
////    m_screenScrollRate = 8;
//
//    int trackWidth = 122;
//    int trackHeight = 113;
//    int y = chalo::Application::GetScreenHeight() - trackHeight;
//    int maxX = chalo::Application::GetScreenWidth();
//
//    m_trackYs.push_back( chalo::Application::GetScreenHeight() - (trackHeight*4) );
//    m_trackYs.push_back( chalo::Application::GetScreenHeight() - (trackHeight*3) );
//    m_trackYs.push_back( chalo::Application::GetScreenHeight() - (trackHeight*2) );
//    m_trackYs.push_back( chalo::Application::GetScreenHeight() - (trackHeight*1) );
//
//    for ( int j = 0; j < 4; j++ )
//    {
//        for ( int i = 0; i < maxX; i++ )
//        {
//            y = m_trackYs[j];
//            sf::Sprite track;
//            track.setTexture( chalo::TextureManager::Get( "track1" ) );
//            track.setPosition( i * trackWidth, y );
//            m_track.push_back( track );
//        }
//    }
//
//    std::string avatar = GetProfileAvatarSheet();
//
//    ExciteHorse::Horse player;
//    player.Setup();
//    player.SetTexture( chalo::TextureManager::Get( avatar ), 0, 128, 64, 64 );
//    player.SetPosition( 100, m_trackYs[0] + 20 );
//    player.SetDirection( chalo::Direction::EAST );
//    player.SetAnimationInformation( 4, 0.1 );
//    player.AddSpriteSheetInfo( "walk-west", 0, 128 );
//    player.AddSpriteSheetInfo( "walk-east", 0, 192 );
//    player.AddSpriteSheetInfo( "walk-north", 0, 128 );
//    player.AddSpriteSheetInfo( "walk-south", 0, 192 );
//    player.SetOrigin( 32, 32 );
//    player.SetScrollRate( m_screenScrollRate );
//    player.SetObjectCollisionRectangle( sf::IntRect( -32, 0, 64, 32 ) );
//    player.SetBoundary( sf::IntRect( 32, 268, 1280*0.75, 720-64 ) );
//    player.SetProfileKey( "1" );
//    player.SetNameOffset( sf::Vector2f( -35, 35 ) );
//    m_players.push_back( player );
//
//    avatar = GetProfileAvatarSheet( 1 );
//    player.SetTexture( chalo::TextureManager::Get( avatar ), 0, 128, 64, 64 );
//    player.SetPosition( 100, m_trackYs[1] + 20 );
//    player.SetProfileKey( "2" );
//    m_players.push_back( player );
//
//    avatar = GetProfileAvatarSheet( 2 );
//    player.SetTexture( chalo::TextureManager::Get( avatar ), 0, 128, 64, 64 );
//    player.SetPosition( 100, m_trackYs[2] + 20 );
//    player.SetProfileKey( "3" );
//    m_players.push_back( player );
//
//    avatar = GetProfileAvatarSheet( 3 );
//    player.SetTexture( chalo::TextureManager::Get( avatar ), 0, 128, 64, 64 );
//    player.SetPosition( 100, m_trackYs[3] + 20 );
//    player.SetProfileKey( "4" );
//    m_players.push_back( player );
//
//    m_firstToFinish = -1;
//
//    BuildMap();
//}
//
//std::string ExciteHorseGameState::GetProfileAvatarSheet()
//{
//  return "horse1";
//}
//
//void ExciteHorseGameState::Cleanup()
//{
//    chalo::Logger::Out( "", "ExciteHorseGameState::Cleanup", "function-trace" );
//    chalo::MenuManager::Cleanup();
//    chalo::DrawManager::Reset();
//}
//
//void ExciteHorseGameState::Update()
//{
//    VamooseState::Update();
//    VamooseState::BackButtonHandler();
//
//
//    for ( int i = 0; i < 32; i++ )
//    {
//        if ( chalo::InputManager::IsJoystickButtonPressed( 0, i ) )
//        {
//        }
//    }
//
//    int spriteWH = m_players[0].GetDimensions().x;
//    int maxX = chalo::Application::GetScreenWidth() - spriteWH;
//    int maxY = chalo::Application::GetScreenHeight() - spriteWH;
//
//    // Detect player collision with inclines FIRST
//
//
//    // Move the track
//    for ( unsigned int i = 0; i < m_track.size(); i++ )
//    {
//        sf::Vector2f pos = m_track[i].getPosition();
//        pos.x -= m_screenScrollRate;
//        if ( pos.x < -122 )
//        {
//            int maxX = chalo::Application::GetScreenWidth();
//            pos.x = maxX;
//        }
//        m_track[i].setPosition( pos );
//    }
//
//    // Move the inclines
//    std::vector<int> deleteIndexes;
//    for ( unsigned int o = 0; o < m_obstacles.size(); o++ )
//    {
//        sf::Vector2f pos = m_obstacles[o].GetPosition();
//
//        pos.x -= m_screenScrollRate;
//        if ( pos.x < -m_obstacles[o].GetDimensions().x * 2 )
//        {
//            // delete me
//            deleteIndexes.push_back( o );
//        }
//        m_obstacles[o].SetPosition( pos );
//        m_obstacles[o].ForceSpriteUpdate();
//        m_obstacles[o].Update();
//
//        for ( int p = 0; p < m_players.size(); p++ )
//        {
//            m_players[p].IsColliding( false );
//            if ( chalo::BoundingBoxCollision( m_players[p].GetObjectCollisionRegion(), m_obstacles[o].GetObjectCollisionRegion() ) )
//            {
//
//                std::cout << "COLLISION" << std::endl;
//                if ( m_obstacles[o].GetType() == ExciteHorse::ObstacleType::INCLINE )
//                {
//                    m_players[p].OnIncline();
//                    if ( m_sounds["incline"].getStatus() != sf::SoundSource::Status::Playing )
//                    {
//                        m_sounds["incline"].play();
//                    }
//                    std::cout << "Incline" << std::endl;
//                }
//                else if ( m_obstacles[o].GetType() == ExciteHorse::ObstacleType::SPEEDUP )
//                {
//                    m_players[p].Speedup();
//                    if ( m_sounds["speedup"].getStatus() != sf::SoundSource::Status::Playing )
//                    {
//                        m_sounds["speedup"].play();
//                    }
//                    std::cout << "Speedup" << std::endl;
//                }
//                else if ( m_obstacles[o].GetType() == ExciteHorse::ObstacleType::SLOWDOWN )
//                {
//                    m_players[p].Slowdown();
//                    if ( m_sounds["slowdown"].getStatus() != sf::SoundSource::Status::Playing )
//                    {
//                        m_sounds["slowdown"].play();
//                    }
//                    std::cout << "Slowdown" << std::endl;
//                }
//            }
//
//            // Finish line
//            if ( m_obstacles[o].GetType() == ExciteHorse::ObstacleType::FINISHLINE && m_players[p].GetPosition().x >= m_obstacles[o].GetPosition().x )
//            {
//                m_players[p].CrossFinishline();
//                if ( m_firstToFinish == -1 )
//                {
//                    m_sounds["youwin"].play();
//                    m_firstToFinish = p;
//                }
//            }
//        }
//    }
//
////    for ( int i = deleteIndexes.size() - 1; i >= 0; i-- )
////    {
////        m_obstacles.erase( m_obstacles.begin() + i );
////    }
//
//    // Player inputs
//    for ( int i = 0; i < m_players.size(); i++ )
//    {
//        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::ACTION2 ) ) &&
//            chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::ACTION1 ) ) &&
//            m_players[i].CanDrop() )
//        {
//            if ( m_sounds["fart"].getStatus() != sf::SoundSource::Status::Playing )
//            {
//                m_sounds["fart"].play();
//            }
//            AddObstacle( ExciteHorse::ObstacleType::SLOWDOWN, m_players[i].GetPosition() );
//            m_players[i].Dropped();
//        }
//        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::ACTION2 ) ) )
//        {
//            m_players[i].Clip();
//        }
//        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::ACTION1 ) ) )
//        {
//            m_players[i].Clop();
//        }
//        else
//        {
//            m_players[i].FallBack( m_screenScrollRate );
//        }
//
//        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::WEST ) ) )
//        {
//            if ( m_players[i].IsJumping() )
//            {
//                m_players[i].Rotate( -1 );
//            }
//            else
//            {
//                m_players[i].Move( chalo::Direction::WEST );
//            }
//        }
//        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::EAST ) ) )
//        {
//            if ( m_players[i].IsJumping() )
//            {
//                m_players[i].Rotate( 1 );
//            }
//            else
//            {
//                m_players[i].Move( chalo::Direction::EAST );
//            }
//        }
//
//        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::NORTH ) ) )
//        {
//            m_players[i].Move( chalo::Direction::NORTH, m_trackYs );
//        }
//        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::InputAction::SOUTH ) ) )
//        {
//            m_players[i].Move( chalo::Direction::SOUTH, m_trackYs );
//        }
//
//        m_players[i].ForceSpriteUpdate();
//
//        m_players[i].Update();
//
//        // Collision with other players?
//        for ( unsigned int j = 0; j < m_players.size(); j++ )
//        {
//            if ( i == j )
//            {
//                continue;
//            }
//
////            std::cout << "PLAYER " << chalo::StringUtility::IntegerToString( i ) << ": " << chalo::StringUtility::RectangleToString( m_players[i].GetObjectCollisionRegion() ) << std::endl;
////            std::cout << "PLAYER " << chalo::StringUtility::IntegerToString( j ) << ": " << chalo::StringUtility::RectangleToString( m_players[j].GetObjectCollisionRegion() ) << std::endl;
//            if ( chalo::BoundingBoxCollision( m_players[i].GetObjectCollisionRegion(), m_players[j].GetObjectCollisionRegion() ) )
//            {
////                std::cout << "COLLISION" << std::endl;
//                m_players[i].Bounce( m_players[j].GetObjectCollisionRegion() );
//                m_players[j].Bounce( m_players[i].GetObjectCollisionRegion() );
//            }
//        }
//    }
//
//}
//
//void ExciteHorseGameState::Draw( sf::RenderWindow& window )
//{
//    for ( auto& track : m_track )
//    {
//        window.draw( track );
//    }
//
//    for ( auto& obstacle : m_obstacles )
//    {
////        obstacle.DrawDebug( window );
//        window.draw( obstacle.GetSprite() );
//    }
//
//    for ( auto& player : m_players )
//    {
////        window.draw( player.GetSprite() );
////        player.DrawDebug( window );
//        chalo::DrawManager::AddSprite( player.GetSprite() );
//        chalo::DrawManager::AddHudText( player.GetNameText() );
//        player.Draw();
//    }
////    chalo::DrawManager::Draw( window );
//}
//
//void ExciteHorseGameState::BuildMap()
//{
//    int counter = 0;
//    for ( auto& ob : m_mapSpec.placements )
//    {
//        ExciteHorse::TrackObstacle obstacle;
//        obstacle.Setup( 0, 0, "obadob" + Helper::ToString( counter ) );
//        obstacle.SetType( ob.type );
//        obstacle.SetObjectCollisionRectangle( sf::IntRect( 0, 0, 64, 64 ) );
//        obstacle.SetPosition( ob.x, ob.y );
//
//        if ( ob.type == ExciteHorse::ObstacleType::INCLINE )
//        {
//            obstacle.SetTexture( chalo::TextureManager::Get( "incline" ), 0, 0, 64, 64 );
//        }
//        else if ( ob.type == ExciteHorse::ObstacleType::SPEEDUP )
//        {
//            obstacle.SetTexture( chalo::TextureManager::Get( "speedup" ), 0, 0, 64, 64 );
//        }
//        else if ( ob.type == ExciteHorse::ObstacleType::SLOWDOWN )
//        {
//            obstacle.SetTexture( chalo::TextureManager::Get( "slowdown" ), 0, 0, 64, 64 );
//        }
//        else if ( ob.type == ExciteHorse::ObstacleType::FINISHLINE )
//        {
//            obstacle.SetTexture( chalo::TextureManager::Get( "finishline" ), 0, 0, 64, 516 );
//        }
//
//        m_obstacles.push_back( obstacle );
//        counter++;
//    }
//}
//
//void ExciteHorseGameState::AddObstacle( ExciteHorse::ObstacleType type, sf::Vector2f pos )
//{
//    ExciteHorse::TrackObstacle obstacle;
//    obstacle.Setup( 0, 0, "dropped" );
//    obstacle.SetType( type );
//    obstacle.SetPosition( pos.x - 64 - 32, pos.y );
//    obstacle.SetTexture( chalo::TextureManager::Get( "slowdown" ), 0, 0, 64, 64 );
//    obstacle.SetObjectCollisionRectangle( sf::IntRect( 0, 0, 32, 32 ) );
//    obstacle.SetScale( 0.5, 0.5 );
//    m_obstacles.push_back( obstacle );
//}
//
