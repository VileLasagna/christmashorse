#include "CH2020_GameState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

CH2020_GameState::CH2020_GameState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2020_GameState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_GameState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();
    EscapeyState::Setup();

    chalo::TextureManager::Add( "character",    "Packages/ChristmasHorse2020/Graphics/Characters/horse2x.png" );
    chalo::TextureManager::Add( "carrot",       "Packages/ChristmasHorse2020/Graphics/Characters/carrot.png" );
    chalo::TextureManager::Add( "background1",  "Packages/ChristmasHorse2020/Graphics/Backgrounds/level1bg1.png" );
    chalo::TextureManager::Add( "background2",  "Packages/ChristmasHorse2020/Graphics/Backgrounds/level1bg2.png" );

//    chalo::InputManager::Setup();

    Helper::SeedRandomNumberGenerator();

    sf::IntRect validRegion = sf::IntRect( 0, 100, 1280, 720-100 );

    m_player.Setup();
    m_player.SetTexture( chalo::TextureManager::Get( "character" ), 0, 0, 128, 128 );
    m_player.SetPosition( 600, 400 );
    m_player.SetAnimationInformation( 4, 0.1 );
    m_player.SetRestrictMovement( true );
    m_player.SetValidPositionRegion( validRegion );
    m_player.SetObjectCollisionRectangle( sf::IntRect( 29, 95, 67, 23 ) );
//    chalo::DrawManager::AddSprite( m_player.GetSprite() );

    m_background1.setTexture( chalo::TextureManager::Get( "background1" ) );
    m_background2.setTexture( chalo::TextureManager::Get( "background2" ) );
    m_background2.setPosition( 0, 720-302 );

    m_score = 0;

    for ( int i = 0; i < 10; i++ )
    {
        chalo::GameObject carrot;
        carrot.SetTexture( chalo::TextureManager::Get( "carrot" ), 0, 0, 64, 64 );
        int x = Helper::GetRandom( validRegion.left + 50, validRegion.left + validRegion.width - 50 );
        int y = Helper::GetRandom( validRegion.top+75, validRegion.top + validRegion.height - 50 );
        carrot.SetPosition( x, y );

        m_carrots.push_back( carrot );
    }

    chalo::UILabel scoreText;
    scoreText.Setup( "score", "main", 24, sf::Color::Black, sf::Vector2f( 0, 0 ), "Score: 0" );  m_scoreText.push_back( scoreText );
    scoreText.Setup( "score", "main", 24, sf::Color::Black, sf::Vector2f( 2, 0 ), "Score: 0" );  m_scoreText.push_back( scoreText );
    scoreText.Setup( "score", "main", 24, sf::Color::Black, sf::Vector2f( 2, 2 ), "Score: 0" );  m_scoreText.push_back( scoreText );
    scoreText.Setup( "score", "main", 24, sf::Color::Black, sf::Vector2f( 0, 2 ), "Score: 0" );  m_scoreText.push_back( scoreText );
    scoreText.Setup( "score", "main", 24, sf::Color::White, sf::Vector2f( 1, 1 ), "Score: 0" );  m_scoreText.push_back( scoreText );

    chalo::UILabel levelText;
    levelText.Setup( "level", "main", 24, sf::Color::Black, sf::Vector2f( 1165, 0 ), "Level: 1" ); m_levelText.push_back( levelText );
    levelText.Setup( "level", "main", 24, sf::Color::Black, sf::Vector2f( 1167, 0 ), "Level: 1" ); m_levelText.push_back( levelText );
    levelText.Setup( "level", "main", 24, sf::Color::Black, sf::Vector2f( 1167, 2 ), "Level: 1" ); m_levelText.push_back( levelText );
    levelText.Setup( "level", "main", 24, sf::Color::Black, sf::Vector2f( 1165, 2 ), "Level: 1" ); m_levelText.push_back( levelText );
    levelText.Setup( "level", "main", 24, sf::Color::White, sf::Vector2f( 1166, 1 ), "Level: 1" ); m_levelText.push_back( levelText );

    m_collectSoundBuffer.loadFromFile( "Packages/ChristmasHorse2020/Audio/carrot-collect.ogg" );
    m_collectSound.setBuffer( m_collectSoundBuffer );
    m_collectSound.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_walkSoundBuffer.loadFromFile( "Packages/ChristmasHorse2020/Audio/horse-walk.ogg" );
    m_walkSound.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_walkSound.setBuffer( m_walkSoundBuffer );
    m_walkSoundCooldown = 0;
    m_walkSoundCooldownMax = 58;

    m_music.openFromFile( "Packages/ChristmasHorse2020/Audio/level1.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();

    m_insBg.setFillColor( sf::Color::Black );
    m_insBg.setPosition( 0, 0 );
    m_insBg.setSize( sf::Vector2f( chalo::Application::GetScreenWidth(), chalo::Application::GetScreenHeight() ) );

    sf::Text inst;
    inst.setFont( chalo::FontManager::Get( "main" ) );
    inst.setFillColor( sf::Color::White );

    inst.setPosition( 10, 10 ); inst.setString( "LEVEL 1" ); m_insText.push_back( inst );
    inst.setPosition( 10, 100 ); inst.setString( "Bepis the Horse has received intel from their spy:" ); m_insText.push_back( inst );
    inst.setPosition( 10, 150 ); inst.setString( "Santa has long since abandoned his North Pole workshop" ); m_insText.push_back( inst );
    inst.setPosition( 10, 200 ); inst.setString( "and has built SANTA PLANET, his new headquarters." ); m_insText.push_back( inst );
    inst.setPosition( 10, 300 ); inst.setString( "Bepis needs to collect rations before their journey..." ); m_insText.push_back( inst );
    inst.setPosition( 10, 350 ); inst.setString( "Collect carrots to continue the journey. (WASD moves)" ); m_insText.push_back( inst );
    inst.setPosition( 10, 450 ); inst.setString( "Press ENTER to begin." ); m_insText.push_back( inst );

    m_instructions = true;
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_GameState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    m_carrots.clear();
    m_scoreText.clear();
    m_levelText.clear();
    m_insText.clear();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_GameState::Update()
{
    chalo::InputManager::Update();

    EscapeyState::Update();
    if ( IsPaused() )
    {
      return;
    }

    if ( m_instructions )
    {
        if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Enter ) )
        {
            m_instructions = false;
        }
    }
    else
    {
        if ( m_walkSoundCooldown > 0 )
        {
            m_walkSoundCooldown--;
        }

        if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::A ) )
    //        || chalo::InputManager::IsJoystickDpadPressed( 0, chalo::WEST ) )
        {
            m_player.Move( chalo::WEST );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }
        else if (  chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::D ) )
    //            || chalo::InputManager::IsJoystickDpadPressed( 0, chalo::EAST ) )
        {
            m_player.Move( chalo::EAST );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }

        if (    chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::W ) )
    //        ||  chalo::InputManager::IsJoystickDpadPressed( 0, chalo::NORTH ) )
        {
            m_player.Move( chalo::NORTH );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }
        else if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::S ) )
    //            ||  chalo::InputManager::IsJoystickDpadPressed( 0, chalo::SOUTH ) )
        {
            m_player.Move( chalo::SOUTH );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }

        m_player.Update();

        // Check for collision with carrots
        for ( auto& carrot : m_carrots )
        {
            if ( SFMLHelper::BoundingBoxCollision( m_player.GetObjectCollisionRegion(), carrot.GetObjectCollisionRegion() ) )
            {
                m_score++;
                carrot.SetPosition( -100, -100 );
                m_collectSound.play();

                for ( auto& text : m_scoreText )
                {
                    text.SetText( "Score: " + Helper::ToString( m_score ) );
                }
            }
        }

        if ( m_score == 10 )
        {
            chalo::Messager::Set( "score", m_score );
            SetGotoState( "ch2020_gameState2" );
        }
    }
}

void CH2020_GameState::Draw( sf::RenderWindow& window )
{

    if ( m_instructions )
    {
        window.draw( m_insBg );

        for ( auto& txt : m_insText )
        {
            window.draw( txt );
        }
    }
    else
    {
        window.draw( m_background1 );

        for ( auto& carrot : m_carrots )
        {
            window.draw( carrot.GetSprite() );
        }

        window.draw( m_player.GetSprite() );

        window.draw( m_background2 );

        for ( auto& text : m_scoreText )
        {
            window.draw( text.GetText() );
        }
        for ( auto& text : m_levelText )
        {
            window.draw( text.GetText() );
        }
    }


    EscapeyState::Draw( window );

//    chalo::DrawManager::Draw( window );
}



