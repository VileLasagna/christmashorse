#include "CH2020_GameState2.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"
#include "../../chalo-engine/Application/Application.hpp"

#include <list>

CH2020_GameState2::CH2020_GameState2()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2020_GameState2::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_GameState2::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();
    EscapeyState::Setup();

    chalo::TextureManager::Add( "character",    "Packages/ChristmasHorse2020/Graphics/Characters/horse2x.png" );
    chalo::TextureManager::Add( "carrot",       "Packages/ChristmasHorse2020/Graphics/Characters/carrot.png" );
    chalo::TextureManager::Add( "alien1",       "Packages/ChristmasHorse2020/Graphics/Characters/alien-peppermint.png" );
    chalo::TextureManager::Add( "alien2",       "Packages/ChristmasHorse2020/Graphics/Characters/alien-elf1.png" );
    chalo::TextureManager::Add( "star",         "Packages/ChristmasHorse2020/Graphics/Characters/star.png" );
    chalo::TextureManager::Add( "bullet",       "Packages/ChristmasHorse2020/Graphics/Characters/bullet.png" );

//    chalo::InputManager::Setup();

    Helper::SeedRandomNumberGenerator();

    sf::IntRect validRegion = sf::IntRect( 0, 0, 1280, 720 );

    m_player.Setup();
    m_player.SetTexture( chalo::TextureManager::Get( "character" ), 0, 0, 128, 128 );
    m_player.SetPosition( 0, 720/2-64 );
    m_player.Move( chalo::EAST );
    m_player.SetAnimationInformation( 4, 0.1 );
    m_player.SetRestrictMovement( true );
    m_player.SetValidPositionRegion( validRegion );
    m_player.SetObjectCollisionRectangle( sf::IntRect( 29, 95, 67, 23 ) );
//    chalo::DrawManager::AddSprite( m_player.GetSprite() );

    m_barrier.setFillColor( sf::Color::Transparent );
    m_barrier.setOutlineColor( sf::Color( 255, 255, 0 ) );
    m_barrier.setOutlineThickness( 3 );
    m_barrier.setRadius( 64 );

    m_score = Helper::StringToInt( chalo::Messager::Get( "score" ) );

    m_carrotCounter = 10;
    for ( int i = 0; i < m_carrotCounter; i++ )
    {
        chalo::GameObject carrot;
        carrot.SetTexture( chalo::TextureManager::Get( "carrot" ), 0, 0, 64, 64 );
        int x = Helper::GetRandom( 1280, 1280*2 );
        int y = Helper::GetRandom( validRegion.top+50, validRegion.top + validRegion.height - 50 );
        carrot.SetPosition( x, y );

        m_carrots.push_back( carrot );
    }

    for ( int i = 0; i < 100; i++ )
    {
        chalo::GameObject star;
        star.SetTexture( chalo::TextureManager::Get( "star" ), 0, 0, 16, 16 );
        int x = Helper::GetRandom( 0, 1280*4 );
        int y = Helper::GetRandom( validRegion.top, validRegion.top + validRegion.height );
        star.SetPosition( x, y );

        m_stars.push_back( star );
    }

    m_lives = 3;

    std::string score = chalo::Messager::Get( "score" );
    chalo::UILabel scoreText;
    scoreText.Setup( "score", "main", 24, sf::Color::Black, sf::Vector2f( 0, 0 ), "Score: " + score );  m_scoreText.push_back( scoreText );
    scoreText.Setup( "score", "main", 24, sf::Color::Black, sf::Vector2f( 2, 0 ), "Score: " + score );  m_scoreText.push_back( scoreText );
    scoreText.Setup( "score", "main", 24, sf::Color::Black, sf::Vector2f( 2, 2 ), "Score: " + score );  m_scoreText.push_back( scoreText );
    scoreText.Setup( "score", "main", 24, sf::Color::Black, sf::Vector2f( 0, 2 ), "Score: " + score );  m_scoreText.push_back( scoreText );
    scoreText.Setup( "score", "main", 24, sf::Color::White, sf::Vector2f( 1, 1 ), "Score: " + score );  m_scoreText.push_back( scoreText );

    std::string level = "2";
    chalo::UILabel levelText;
    levelText.Setup( "level", "main", 24, sf::Color::Black, sf::Vector2f( 1165, 0 ), "Level: " + level ); m_levelText.push_back( levelText );
    levelText.Setup( "level", "main", 24, sf::Color::Black, sf::Vector2f( 1167, 0 ), "Level: " + level ); m_levelText.push_back( levelText );
    levelText.Setup( "level", "main", 24, sf::Color::Black, sf::Vector2f( 1167, 2 ), "Level: " + level ); m_levelText.push_back( levelText );
    levelText.Setup( "level", "main", 24, sf::Color::Black, sf::Vector2f( 1165, 2 ), "Level: " + level ); m_levelText.push_back( levelText );
    levelText.Setup( "level", "main", 24, sf::Color::White, sf::Vector2f( 1166, 1 ), "Level: " + level ); m_levelText.push_back( levelText );

    chalo::UILabel livesText;
    std::string lives = Helper::ToString( m_lives );
    livesText.Setup( "lives", "main", 24, sf::Color::Black, sf::Vector2f( 1165, 30 ), "Lives: " + lives ); m_levelText.push_back( livesText );
    livesText.Setup( "lives", "main", 24, sf::Color::Black, sf::Vector2f( 1167, 30 ), "Lives: " + lives ); m_levelText.push_back( livesText );
    livesText.Setup( "lives", "main", 24, sf::Color::Black, sf::Vector2f( 1167, 32 ), "Lives: " + lives ); m_levelText.push_back( livesText );
    livesText.Setup( "lives", "main", 24, sf::Color::Black, sf::Vector2f( 1165, 32 ), "Lives: " + lives ); m_levelText.push_back( livesText );
    livesText.Setup( "lives", "main", 24, sf::Color::White, sf::Vector2f( 1166, 31 ), "Lives: " + lives ); m_levelText.push_back( livesText );

    m_collectSoundBuffer.loadFromFile( "Packages/ChristmasHorse2020/Audio/carrot-collect.ogg" );
    m_collectSound.setBuffer( m_collectSoundBuffer );
    m_collectSound.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_hurtSoundBuffer.loadFromFile( "Packages/ChristmasHorse2020/Audio/horse-hurt.ogg" );
    m_hurtSound.setBuffer( m_hurtSoundBuffer );
    m_hurtSound.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_walkSoundBuffer.loadFromFile( "Packages/ChristmasHorse2020/Audio/horse-space.ogg" );
    m_walkSound.setBuffer( m_walkSoundBuffer );
    m_walkSound.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_walkSoundCooldown = 0;
    m_walkSoundCooldownMax = 60;

    m_hitTimer = 0;
    m_hitTimerMax = 64;

    m_background.setFillColor( sf::Color( 17, 3, 62 ) );

    for ( int i = 0; i < 20; i++ )
    {
        int alienType = rand() % 2;
        chalo::Character baddy;
        float speed = 0;
        int x;

        if ( alienType == 0 )
        {
            x = Helper::GetRandom( 1280, 1280*3 );
            baddy.SetTexture( chalo::TextureManager::Get( "alien1" ), 0, 0, 37, 45 );
            speed = 2;
        }
        else if ( alienType == 1 )
        {
            x = Helper::GetRandom( 1280, 1280*4 );
            baddy.SetTexture( chalo::TextureManager::Get( "alien2" ), 0, 0, 56, 64 );
            speed = 3;
        }

        int y = Helper::GetRandom( validRegion.top+50, validRegion.top + validRegion.height - 50 );
        baddy.SetPosition( x, y );

        m_baddies.push_back( baddy );
        m_baddiesSpeeds.push_back( speed );
        m_baddiesTypes.push_back( alienType );
    }

    m_carrotSpeed = 1;


    m_music.openFromFile( "Packages/ChristmasHorse2020/Audio/level2.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();


    m_velocityMax = 5;
    m_accelerationMax = 10;
    m_velocity = sf::Vector2f( 0, 0 );
    m_acceleration = sf::Vector2f( 0.5, 0.5 );


    m_insBg.setFillColor( sf::Color::Black );
    m_insBg.setPosition( 0, 0 );
    m_insBg.setSize( sf::Vector2f( chalo::Application::GetScreenWidth(), chalo::Application::GetScreenHeight() ) );

    sf::Text inst;
    inst.setFont( chalo::FontManager::Get( "main" ) );
    inst.setFillColor( sf::Color::White );

    inst.setPosition( 10, 10 ); inst.setString( "LEVEL 2" ); m_insText.push_back( inst );
    inst.setPosition( 10, 100 ); inst.setString( "As Bepis approaches SANTA PLANET, the Elves begin to attack." ); m_insText.push_back( inst );
    inst.setPosition( 10, 200 ); inst.setString( "They need to continue collecting carrots while also avoiding" ); m_insText.push_back( inst );
    inst.setPosition( 10, 250 ); inst.setString( "the onslaught of Elves and Peppermint Mines." ); m_insText.push_back( inst );
    inst.setPosition( 10, 350 ); inst.setString( "Press ENTER to begin." ); m_insText.push_back( inst );

    m_instructions = true;

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_GameState2::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    m_carrots.clear();
    m_stars.clear();
    m_scoreText.clear();
    m_levelText.clear();
    m_livesText.clear();
    m_baddies.clear();
    m_baddiesSpeeds.clear();
    m_baddiesTypes.clear();
    m_bullets.clear();
    m_insText.clear();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_GameState2::Accelerate( int x, int y )
{
    m_velocity.x += x * m_acceleration.x;
    m_velocity.y += y * m_acceleration.y;

    if ( m_velocity.x > m_accelerationMax )
    {
        m_velocity.x = m_accelerationMax;
    }

    if ( m_velocity.y > m_accelerationMax )
    {
        m_velocity.y = m_accelerationMax;
    }
}

void CH2020_GameState2::Deaccelerate()
{
    if ( m_velocity.x < -1.0 )
    {
        m_velocity.x += m_acceleration.x;
    }
    else if ( m_velocity.x > 1.0 )
    {
        m_velocity.x -= m_acceleration.x;
    }

    if ( m_velocity.x > -1.0 && m_velocity.x < 1.0 )
    {
        m_velocity.x = 0;
    }

    if ( m_velocity.y < -1.0 )
    {
        m_velocity.y += m_acceleration.y;
    }
    else if ( m_velocity.y > 1.0 )
    {
        m_velocity.y -= m_acceleration.y;
    }

    if ( m_velocity.y > -1.0 && m_velocity.y < 1.0 )
    {
        m_velocity.y = 0;
    }
}

void CH2020_GameState2::UpdatePlayerPos()
{
    sf::Vector2f pos = m_player.GetPosition();

    pos.x += m_velocity.x;
    pos.y += m_velocity.y;

    m_player.SetPosition( pos );
    m_player.RestrictMovement();
    m_player.Animate();
}


void CH2020_GameState2::Update()
{
    chalo::InputManager::Update();

    EscapeyState::Update();
    if ( IsPaused() )
    {
      return;
    }

    if ( m_instructions )
    {
        if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Enter ) )
        {
            m_instructions = false;
        }
    }
    else
    {

        if ( m_walkSoundCooldown > 0 )
        {
            m_walkSoundCooldown--;
        }

        if ( m_hitTimer < m_hitTimerMax )
        {
            sf::Vector2f playerPos = m_player.GetPosition();
//            sf::Vector2f barrierPos = m_barrier.getPosition();
//            float radius = m_barrier.getRadius();
            m_hitTimer++;
    //        m_barrier.setOrigin( sf::Vector2f( -32, -32 ) );
            m_barrier.setPosition( sf::Vector2f( playerPos.x + m_hitTimer, playerPos.y + m_hitTimer ) );

            m_barrier.setRadius( 64 - m_hitTimer );
        }


        int accX = 0;
        int accY = 0;
        bool moving = false;
        if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::A ) )
//            || chalo::InputManager::IsJoystickDpadPressed( 0, chalo::WEST ) )
        {
            moving = true;
            accX = -1;
    //        m_player.Move( chalo::WEST );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }
        else if (  chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::D ) )
//                || chalo::InputManager::IsJoystickDpadPressed( 0, chalo::EAST ) )
        {
            moving = true;
            accX = 1;
    //        m_player.Move( chalo::EAST );
            if ( m_walkSoundCooldown <= 0 )
            {
//        sf::Vector2f playerPos = m_player.GetPosition();
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }

        if (    chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::W ) )
//            ||  chalo::InputManager::IsJoystickDpadPressed( 0, chalo::NORTH ) )
        {
            moving = true;
            accY = -1;
    //        m_player.Move( chalo::NORTH );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }
        else if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::S ) )
//                ||  chalo::InputManager::IsJoystickDpadPressed( 0, chalo::SOUTH ) )
        {
            moving = true;
            accY = 1;
    //        m_player.Move( chalo::SOUTH );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }

        if ( moving )
        {
            Accelerate( accX, accY );
        }
        else
        {
            Deaccelerate();
        }

        UpdatePlayerPos();
        m_player.Update();

        // Check for collision with carrots
        for ( auto& carrot : m_carrots )
        {
            // Move carrots left
            sf::Vector2f pos = carrot.GetPosition();
            carrot.SetPosition( pos.x - m_carrotSpeed, pos.y );

            if ( SFMLHelper::BoundingBoxCollision( m_player.GetObjectCollisionRegion(), carrot.GetObjectCollisionRegion() ) )
            {
                m_score++;
                m_carrotCounter--;
                carrot.SetPosition( -100, pos.y );
                m_collectSound.play();

                for ( auto& text : m_scoreText )
                {
                    text.SetText( "Score: " + Helper::ToString( m_score ) );
                }
            }

            pos = carrot.GetPosition();
            if ( pos.x < -50 )
            {
                pos.x += 1350;
                carrot.SetPosition( pos.x - m_carrotSpeed, pos.y );
            }
        }

        // Move aliens
        for ( unsigned int i = 0; i < m_baddies.size(); i++ )
        {
            sf::Vector2f pos = m_baddies[i].GetPosition();
            sf::Vector2f playerPos = m_player.GetPosition();
            m_baddies[i].SetPosition( pos.x - m_baddiesSpeeds[i], pos.y );

            if ( SFMLHelper::BoundingBoxCollision( m_player.GetObjectCollisionRegion(), m_baddies[i].GetObjectCollisionRegion() ) )
            {
                m_hurtSound.play();
                GetHurt();
                m_baddies[i].SetPosition( sf::Vector2f( -100, pos.y ) );
            }

            if ( m_baddiesTypes[i] == 1 && pos.x <= 1280 && pos.x >= 0 && pos.y >= playerPos.y && pos.y + 64 <= playerPos.y + 128 )
            {
                // Shoot
                Shoot( i );
            }

            pos = m_baddies[i].GetPosition();
            if ( pos.x < -50 )
            {
                pos.x += 1350;
                m_baddies[i].SetPosition( pos.x, pos.y );
            }
        }

        // Move stars
        for ( auto& star : m_stars )
        {
            sf::Vector2f pos = star.GetPosition();
            if ( pos.x < -50 )
            {
                star.SetPosition( pos.x  + 1350, pos.y );
            }
            else
            {
                star.SetPosition( pos.x - 1, pos.y );
            }
        }

        // move bullets
        std::list<int> removeIndices;
        for ( unsigned int i = 0; i < m_bullets.size(); i++ )
        {
            sf::Vector2f pos = m_bullets[i].GetPosition();
            m_bullets[i].SetPosition( pos.x - 6, pos.y );

            if ( m_bullets[i].GetPosition().x < -100 )
            {
                removeIndices.push_front( i );
            }

            else if ( SFMLHelper::BoundingBoxCollision( m_player.GetObjectCollisionRegion(), m_bullets[i].GetObjectCollisionRegion() ) )
            {
                m_hurtSound.play();
                GetHurt();
                removeIndices.push_back( i );
            }
        }

        // Destroy bullet
        for ( auto& index : removeIndices )
        {
            m_bullets.erase( m_bullets.begin() + index );
        }

        if ( m_lives == 0 )
        {
            // Ded
        }

        if ( m_carrotCounter == 0 )
        {
            // Next state!
            chalo::Messager::Set( "score", m_score );
            SetGotoState( "ch2020_gamestateboss" );
        }
    }

}

void CH2020_GameState2::Shoot( int index )
{
    sf::Vector2f pos = m_baddies[index].GetPosition();
    chalo::GameObject bullet;
    bullet.SetTexture( chalo::TextureManager::Get( "bullet" ), 0, 0, 16, 16 );
    bullet.SetPosition( pos.x, pos.y + 25 );
    m_bullets.push_back( bullet );
}

void CH2020_GameState2::GetHurt()
{
    if ( m_hitTimer >= m_hitTimerMax )
    {
        sf::Vector2f playerPos = m_player.GetPosition();
        m_hitTimer = 0;
        m_barrier.setRadius( 64 );
        m_lives--;
        m_barrier.setPosition( sf::Vector2f( playerPos.x, playerPos.y ) );
        std::string lives = Helper::ToString( m_lives );
        for ( auto & livesText : m_livesText )
        {
            livesText.SetText( "Lives: " + lives );
        }
    }
}

void CH2020_GameState2::Draw( sf::RenderWindow& window )
{
    if ( m_instructions )
    {
        window.draw( m_insBg );

        for ( auto& txt : m_insText )
        {
            window.draw( txt );
        }
    }
    else
    {
        window.draw( m_background );

        for ( auto& star : m_stars )
        {
            window.draw( star.GetSprite() );
        }

        for ( auto& carrot : m_carrots )
        {
            window.draw( carrot.GetSprite() );
        }
        for ( auto& baddy : m_baddies )
        {
            window.draw( baddy.GetSprite() );
        }

        window.draw( m_player.GetSprite() );

        if ( m_hitTimer < m_hitTimerMax )
        {
            window.draw( m_barrier );
        }

        for ( auto& bullet : m_bullets )
        {
            window.draw( bullet.GetSprite() );
        }

        for ( auto& text : m_scoreText )
        {
            window.draw( text.GetText() );
        }
        for ( auto& text : m_levelText )
        {
            window.draw( text.GetText() );
        }
        for ( auto& text : m_livesText )
        {
            window.draw( text.GetText() );
        }
    }


    EscapeyState::Draw( window );


//    chalo::DrawManager::Draw( window );
}



