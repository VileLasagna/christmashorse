#include "CH2020_GameStateBoss.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

#include <list>

CH2020_GameStateBoss::CH2020_GameStateBoss()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2020_GameStateBoss::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_GameStateBoss::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();
    EscapeyState::Setup();

    chalo::TextureManager::Add( "character",    "Packages/ChristmasHorse2020/Graphics/Characters/horse2x.png" );
    chalo::TextureManager::Add( "background1",  "Packages/ChristmasHorse2020/Graphics/Backgrounds/bosslevelbg.png" );
    chalo::TextureManager::Add( "santa",        "Packages/ChristmasHorse2020/Graphics/Characters/santa.png" );
    chalo::TextureManager::Add( "gift",         "Packages/ChristmasHorse2020/Graphics/Characters/gift.png" );
    chalo::TextureManager::Add( "horsebullet",  "Packages/ChristmasHorse2020/Graphics/Characters/bullet2.png" );
    chalo::TextureManager::Add( "candycane",    "Packages/ChristmasHorse2020/Graphics/Characters/candycane.png" );

//    chalo::InputManager::Setup();

    Helper::SeedRandomNumberGenerator();

    sf::IntRect validRegion = sf::IntRect( 0, 0, 900, 720 );

    m_player.Setup();
    m_player.SetTexture( chalo::TextureManager::Get( "character" ), 0, 0, 128, 128 );
    m_player.SetPosition( 200, 296 );
    m_player.SetAnimationInformation( 4, 0.1 );
    m_player.SetRestrictMovement( true );
    m_player.SetValidPositionRegion( validRegion );
    m_player.SetObjectCollisionRectangle( sf::IntRect( 29, 95, 67, 23 ) );
    m_player.Move( chalo::EAST );
//    chalo::DrawManager::AddSprite( m_player.GetSprite() );

    m_background1.setTexture( chalo::TextureManager::Get( "background1" ) );

    m_lives = 3;
    std::string score = chalo::Messager::Get( "score" );

    m_scoreText.Setup( "score", "main", 24, sf::Color::White, sf::Vector2f( 1, 1 ), "Score: " + score );

    m_levelText.Setup( "level", "main", 24, sf::Color::White, sf::Vector2f( 1166, 1 ), "Level: 3" );

    chalo::UILabel livesText;
    std::string lives = Helper::ToString( m_lives );
    m_livesText.Setup( "lives", "main", 24, sf::Color::White, sf::Vector2f( 1166, 31 ), "Lives: " + lives );

    m_infoText.Setup( "info", "main", 24, sf::Color::White, sf::Vector2f( 501, 6 ), "Press SPACE to shoot" );
    m_info2Text.Setup( "info2", "main", 24, sf::Color::White, sf::Vector2f( 501, 36 ), "Launch candy cane at Santa" );

    m_collectSoundBuffer.loadFromFile( "Packages/ChristmasHorse2020/Audio/carrot-collect.ogg" );
    m_collectSound.setBuffer( m_collectSoundBuffer );
    m_collectSound.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_hurtSoundBuffer.loadFromFile( "Packages/ChristmasHorse2020/Audio/horse-hurt.ogg" );
    m_hurtSound.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_hurtSound.setBuffer( m_hurtSoundBuffer );

    m_music.openFromFile( "Packages/ChristmasHorse2020/Audio/bossbattle.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();

    validRegion = sf::IntRect( 0, 0, 1280, 720-100 );
    m_santa.Setup();
    m_santa.SetTexture( chalo::TextureManager::Get( "santa" ), 0, 0, 108, 107 );
    m_santa.SetPosition( 1100, 306 );
    m_santa.SetRestrictMovement( true );
    m_santa.SetValidPositionRegion( validRegion );
    m_player.SetObjectCollisionRectangle( sf::IntRect( 29, 95, 67, 23 ) );

    for ( int x = 0; x < 3; x++ )
    {
        for ( int y = 0; y < 23; y++ )
        {
            chalo::Character gift;
            gift.SetTexture( chalo::TextureManager::Get( "gift" ), 0, 0, 32, 32 );

            sf::Vector2f pos = sf::Vector2f( 900 + (x * 32), 0 + (y * 32) );
            gift.SetPosition( pos );
            gift.SetObjectCollisionRectangle( sf::IntRect( 0, 0, 32, 32 ) );
            gift.ForceSpriteUpdate();
            m_originalPosition.push_back( pos );
            m_giftCountdowns.push_back( -1 );
            m_gifts.push_back( gift );
        }
    }

    m_shootTimer = 0;
    m_shootTimerMax = 10;

    m_santaMoveCountdown = 0;
    m_candycaneMax = 10;

    m_candycaneRespawnTimer = 0;
    m_moveCandycane = false;
    m_candycane.Setup();
    m_candycane.SetTexture( chalo::TextureManager::Get( "candycane" ), 0, 0, 122, 55 );
    m_candycane.SetPosition( 0, 0 );
    m_candycane.SetObjectCollisionRectangle( sf::IntRect( 0, 0, 122, 55 ) );


    m_insBg.setFillColor( sf::Color::Black );
    m_insBg.setPosition( 0, 0 );
    m_insBg.setSize( sf::Vector2f( chalo::Application::GetScreenWidth(), chalo::Application::GetScreenHeight() ) );

    sf::Text inst;
    inst.setFont( chalo::FontManager::Get( "main" ) );
    inst.setFillColor( sf::Color::White );

    inst.setPosition( 10, 10 ); inst.setString( "LEVEL 3" ); m_insText.push_back( inst );
    inst.setPosition( 10, 100 ); inst.setString( "Bepis encounters Santa Claus, protected by his wall of presents." ); m_insText.push_back( inst );
    inst.setPosition( 10, 150 ); inst.setString( "It is time for the final showdown." ); m_insText.push_back( inst );

    inst.setPosition( 10, 250 ); inst.setString( "The only option is to destroy the gift wall with eggs (use SPACEBAR), " ); m_insText.push_back( inst );
    inst.setPosition( 10, 300 ); inst.setString( "then launch the Candy Cane Missile (touch it) to impale Horse's foe." ); m_insText.push_back( inst );

    inst.setPosition( 10, 400 ); inst.setString( "Press ENTER to begin." ); m_insText.push_back( inst );

    m_instructions = true;

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_GameStateBoss::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    m_gifts.clear();
    m_originalPosition.clear();
    m_giftCountdowns.clear();
    m_bullets.clear();
    m_insText.clear();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_GameStateBoss::Update()
{
    chalo::InputManager::Update();

    EscapeyState::Update();
    if ( IsPaused() )
    {
      return;
    }

    if ( m_instructions )
    {
        if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Enter ) )
        {
            m_instructions = false;
        }
    }
    else
    {

        if ( m_shootTimer > 0 )
        {
            m_shootTimer--;
        }

        if ( m_santaMoveCountdown > 0 )
        {
            m_santaMoveCountdown--;
        }

        if ( m_candycaneRespawnTimer > 0 )
        {
            m_candycaneRespawnTimer--;
            if ( m_candycaneRespawnTimer == 0 )
            {
                m_candycane.SetPosition( 0, 0 );
            }
        }

        if ( m_santaMoveCountdown == 0 )
        {
            int choice = rand() % 3;

            if ( choice == 0 )
            {
                m_santaDirection = 0;
                m_santaMoveCountdown = Helper::GetRandom( 0, 20 );
            }
            else if ( choice == 1 )
            {
                m_santaDirection = -1;
                m_santaMoveCountdown = Helper::GetRandom( 100, 200 );
            }
            else
            {
                m_santaDirection = 1;
                m_santaMoveCountdown = Helper::GetRandom( 100, 200 );
            }
        }

        // Update candycane
        sf::Vector2f playerPos = m_player.GetPosition();
        sf::Vector2f candyPos = m_candycane.GetPosition();

        if ( candyPos.x > 1280 && m_candycaneRespawnTimer <= 0 )
        {
            m_candycaneRespawnTimer = m_candycaneMax;
            m_moveCandycane = false;
        }

        if ( m_moveCandycane )
        {
            candyPos.x += 10;
        }

        if ( !m_moveCandycane && SFMLHelper::GetDistance( m_player.GetObjectCollisionRegion(), m_candycane.GetObjectCollisionRegion(), false ) < 100 )
        {
            m_moveCandycane = true;
            m_collectSound.play();
        }

        else if ( SFMLHelper::BoundingBoxCollision( m_santa.GetObjectCollisionRegion(), m_candycane.GetObjectCollisionRegion() ) )
        {
            // You win
            SetGotoState( "ch2020_epiloguestate" );
        }

        if ( m_candycaneRespawnTimer <= 0 )
        {
            m_candycane.SetPosition( candyPos.x, playerPos.y + 30 );
        }

        // Update santa position
        if ( m_santaDirection != 0 )
        {
            sf::Vector2f pos = m_santa.GetPosition();
            pos.y += ( m_santaDirection * 2 );
            m_santa.SetPosition( pos );
            m_santa.RestrictMovement();
        }

        if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::A ) )
//            || chalo::InputManager::IsJoystickDpadPressed( 0, chalo::WEST ) )
        {
            m_player.Move( chalo::WEST );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }
        else if (  chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::D ) )
//                || chalo::InputManager::IsJoystickDpadPressed( 0, chalo::EAST ) )
        {
            m_player.Move( chalo::EAST );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }

        if (    chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::W ) )
//            ||  chalo::InputManager::IsJoystickDpadPressed( 0, chalo::NORTH ) )
        {
            m_player.Move( chalo::NORTH );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }
        else if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::S ) )
//                ||  chalo::InputManager::IsJoystickDpadPressed( 0, chalo::SOUTH ) )
        {
            m_player.Move( chalo::SOUTH );
            if ( m_walkSoundCooldown <= 0 )
            {
                m_walkSound.play();
                m_walkSoundCooldown = m_walkSoundCooldownMax;
            }
        }

        if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Space ) )
        {
            HorseShoot();
        }

        m_player.Update();

        // move bullets
        std::list<int> removeIndicesBullets;
        for ( unsigned int i = 0; i < m_bullets.size(); i++ )
        {
            sf::Vector2f pos = m_bullets[i].GetPosition();
            m_bullets[i].SetPosition( pos.x + 6, pos.y );

            bool removeBullet = false;
            if ( m_bullets[i].GetPosition().x > 1280 )
            {
                Logger::Out( "1. Log bullet index to remove: " + Helper::ToString( i ), "CH2020_GameStateBoss::Update" );
                removeBullet = true;
            }

            else if ( SFMLHelper::BoundingBoxCollision( m_santa.GetObjectCollisionRegion(), m_bullets[i].GetObjectCollisionRegion() ) )
            {
            }

            for ( unsigned int j = 0; j < m_gifts.size(); j++ )
            {
                if ( SFMLHelper::BoundingBoxCollision( m_gifts[j].GetObjectCollisionRegion(), m_bullets[i].GetObjectCollisionRegion() ) )
                {
                    removeBullet = true;
                    m_giftCountdowns[j] = 600;
                    m_gifts[j].SetPosition( -100, -100 );
                    m_hurtSound.play();
                }
            }

            if ( removeBullet )
            {
                removeIndicesBullets.push_front( i );
            }
        }

        // Destroy bullet
        for ( auto& index : removeIndicesBullets )
        {
            Logger::Out( "2. Remove bullet at index: " + Helper::ToString( index ), "CH2020_GameStateBoss::Update" );
            m_bullets.erase( m_bullets.begin() + index );
        }

        for ( unsigned int j = 0; j < m_giftCountdowns.size(); j++ )
        {
            if ( SFMLHelper::BoundingBoxCollision( m_candycane.GetObjectCollisionRegion(), m_gifts[j].GetObjectCollisionRegion() ) )
            {
                m_candycane.SetPosition( -200, -200 );
                m_candycaneRespawnTimer = m_candycaneMax;
                m_moveCandycane = false;
                m_hurtSound.play();
            }

            if ( m_giftCountdowns[j] > 0 )
            {
                m_giftCountdowns[j]--;

                if ( m_giftCountdowns[j] == 0 )
                {
                    m_gifts[j].SetPosition( m_originalPosition[j] );
                }
            }
        }
    }

}

void CH2020_GameStateBoss::HorseShoot()
{
    if ( m_shootTimer <= 0 )
    {
        sf::Vector2f pos = m_player.GetPosition();
        pos.x += 100;
        pos.y += 32;
        m_shootTimer = m_shootTimerMax;
        chalo::GameObject bullet;
        bullet.SetTexture( chalo::TextureManager::Get( "horsebullet" ), 0, 0, 16, 16 );
        bullet.SetObjectCollisionRectangle( sf::IntRect( 0, 0, 16, 16 ) );
        bullet.SetPosition( pos );
        m_bullets.push_back( bullet );
    }
}

void CH2020_GameStateBoss::Draw( sf::RenderWindow& window )
{
    if ( m_instructions )
    {
        window.draw( m_insBg );

        for ( auto& txt : m_insText )
        {
            window.draw( txt );
        }
    }
    else
    {
        window.draw( m_background1 );

        window.draw( m_candycane.GetSprite() );
        window.draw( m_santa.GetSprite() );
        window.draw( m_player.GetSprite() );

        for ( auto& gift : m_gifts )
        {
            window.draw( gift.GetSprite() );
        }

        for ( auto& bullet : m_bullets )
        {
            window.draw( bullet.GetSprite() );
        }

        window.draw( m_scoreText.GetText() );

        window.draw( m_levelText.GetText() );

        window.draw( m_livesText.GetText() );

        window.draw( m_infoText.GetText() );
        window.draw( m_info2Text.GetText() );
    }

    EscapeyState::Draw( window );

//    chalo::DrawManager::Draw( window );
}



