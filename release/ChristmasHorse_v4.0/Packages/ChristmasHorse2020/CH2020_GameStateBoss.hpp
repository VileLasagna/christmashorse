#ifndef _CH2020_GAME_STATE_BOSS
#define _CH2020_GAME_STATE_BOSS

#include <SFML/Audio.hpp>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/DEPRECATED/Maps/WritableMap.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
//#include "../../chalo-engine/DEPRECATED/Managers/DrawManager.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"
#include "../ProgramBase/EscapeyState.hpp"

#include "CH2020_Character.hpp"

#include <vector>

class CH2020_GameStateBoss : public EscapeyState//public chalo::IState
{
public:
    CH2020_GameStateBoss();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

    void HorseShoot();

private:
    std::string CLASSNAME;
    chalo::Character m_player;
    CH2020_Character m_santa;
    std::vector<chalo::Character> m_gifts;
    std::vector<sf::Vector2f> m_originalPosition;
    std::vector<int> m_giftCountdowns;

    sf::Sprite m_background1;
    int m_score;
    int m_shootTimer;
    int m_shootTimerMax;
    int m_santaDirection;
    int m_santaMoveCountdown;
    chalo::UILabel m_scoreText;
    chalo::UILabel m_levelText;
    chalo::UILabel m_livesText;
    chalo::UILabel m_infoText;
    chalo::UILabel m_info2Text;
    std::vector<chalo::GameObject> m_bullets;
    chalo::GameObject m_candycane;
    bool m_moveCandycane;
    int m_candycaneRespawnTimer;
    int m_candycaneMax;
    sf::SoundBuffer m_hurtSoundBuffer;
    sf::Sound m_hurtSound;

    int m_lives;
    sf::SoundBuffer m_collectSoundBuffer;
    sf::Sound m_collectSound;
    sf::SoundBuffer m_walkSoundBuffer;
    sf::Sound m_walkSound;
    sf::Music m_music;
    int m_walkSoundCooldown;
    int m_walkSoundCooldownMax;

    bool m_instructions;
    sf::RectangleShape m_insBg;
    std::vector<sf::Text> m_insText;
};

#endif
