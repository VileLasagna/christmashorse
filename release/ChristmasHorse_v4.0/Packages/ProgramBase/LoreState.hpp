#ifndef _LoreState
#define _LoreState

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/DEPRECATED/Maps/WritableMap.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
//#include "../../chalo-engine/DEPRECATED/Managers/DrawManager.hpp"

#include <vector>

class LoreState : public chalo::IState
{
public:
    LoreState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    std::string CLASSNAME;
};

#endif
