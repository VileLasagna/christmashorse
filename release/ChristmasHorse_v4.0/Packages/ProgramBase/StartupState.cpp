#include "StartupState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Application/Application.hpp"

StartupState::StartupState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void StartupState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void StartupState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();

    chalo::TextureManager::Add( "game-icons",              "Packages/ProgramBase/Graphics/UI/game-icons.png" );
    chalo::TextureManager::Add( "logo",                    "Packages/ProgramBase/Graphics/UI/logo-moosadee-small.png" );
    chalo::TextureManager::Add( "game-background",         "Packages/ProgramBase/Graphics/UI/game-background.png" );
    chalo::TextureManager::Add( "button-long",             "Packages/ProgramBase/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Packages/ProgramBase/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Packages/ProgramBase/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Packages/ProgramBase/Graphics/UI/button-square-selected.png" );
    chalo::TextureManager::Add( "horse",                   "Packages/ProgramBase/Graphics/UI/horse.png" );

    chalo::MenuManager::LoadCsvMenu( "startup.csv" );
    chalo::MenuManager::GetLabel( "lblHeader" ).SetFont( "title" );

    sf::Vector2f pos( 360, 700 );

    sf::IntRect dimensions( 0, 0, 300, 35 );
    chalo::InputManager::ToggleCursor( true );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void StartupState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    chalo::MenuManager::Cleanup();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void StartupState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if      ( clickedButton == "btnPlay0" )      { SetGotoState( "mh_gameState" ); }
    else if ( clickedButton == "btnPlay1" )      { SetGotoState( "ch2020_gamestate" ); }
    else if ( clickedButton == "btnPlay2" )      { SetGotoState( "ch2021_prologueState" ); }
    else if ( clickedButton == "btnPlay3" )      { SetGotoState( "ch2022_prologueState" ); }
    else if ( clickedButton == "btnPlay4" )      { SetGotoState( "ch2023_gamestate" ); }
    else if ( clickedButton == "btnAbout" )      { SetGotoState( "aboutstate" ); }
    else if ( clickedButton == "btnRecap" )      { SetGotoState( "lorestate" ); }
    else if ( clickedButton == "btnOptions" )    { SetGotoState( "optionsstate" ); }
//    else if ( clickedButton == "btnMinigames" )  { SetGotoState( "excitehorse_state" ); }
    else if ( clickedButton == "btnQuit" )       { chalo::Application::ReadyToQuit(); }
}

void StartupState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
}



