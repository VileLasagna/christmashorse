#include "OptionsState.hpp"

#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Managers/AudioManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"

OptionsState::OptionsState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void OptionsState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    CursorState::Init( name, true );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void OptionsState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();
    CursorState::Setup();

    chalo::TextureManager::Add( "bg",                      "Content/Graphics/UI/menubg.png" );
    chalo::TextureManager::Add( "button-long",             "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Content/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Content/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square2",          "Content/Graphics/UI/button-square2.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Content/Graphics/UI/button-square-selected.png" );
    chalo::TextureManager::Add( "menu-icons",              "Content/Graphics/UI/menu-icons.png" );

    chalo::AudioManager::AddMusic( "music-test",           "Content/Audio/music-test.ogg" );
    chalo::AudioManager::AddSoundBuffer( "sound-test",           "Content/Audio/sound-test.ogg" );

    chalo::MenuManager::LoadCsvMenu( "options.csv" );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );

    LoadSavedOptions();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void OptionsState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    chalo::MenuManager::Cleanup();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void OptionsState::LoadSavedOptions()
{
    int soundVolume = Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) );

    chalo::UIImage& sound0 = chalo::MenuManager::GetImage( "1", "soundVolumeBlip0" );
    chalo::UIImage& sound1 = chalo::MenuManager::GetImage( "1", "soundVolumeBlip1" );
    chalo::UIImage& sound2 = chalo::MenuManager::GetImage( "1", "soundVolumeBlip2" );
    chalo::UIImage& sound3 = chalo::MenuManager::GetImage( "1", "soundVolumeBlip3" );

    if ( soundVolume == 0 )     { sound0.SetImageClipRect( sf::IntRect( 500, 0, 50, 50 ) ); }
    else                        { sound0.SetImageClipRect( sf::IntRect( 550, 0, 50, 50 ) ); }
    if ( soundVolume <= 25  )   { sound1.SetImageClipRect( sf::IntRect( 500, 0, 50, 50 ) ); }
    else                        { sound1.SetImageClipRect( sf::IntRect( 550, 0, 50, 50 ) ); }
    if ( soundVolume <= 50  )   { sound2.SetImageClipRect( sf::IntRect( 500, 0, 50, 50 ) ); }
    else                        { sound2.SetImageClipRect( sf::IntRect( 550, 0, 50, 50 ) ); }
    if ( soundVolume <= 75  )   { sound3.SetImageClipRect( sf::IntRect( 500, 0, 50, 50 ) ); }
    else                        { sound3.SetImageClipRect( sf::IntRect( 550, 0, 50, 50 ) ); }

    int musicVolume = Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) );

    chalo::UIImage& music0 = chalo::MenuManager::GetImage( "1", "musicVolumeBlip0" );
    chalo::UIImage& music1 = chalo::MenuManager::GetImage( "1", "musicVolumeBlip1" );
    chalo::UIImage& music2 = chalo::MenuManager::GetImage( "1", "musicVolumeBlip2" );
    chalo::UIImage& music3 = chalo::MenuManager::GetImage( "1", "musicVolumeBlip3" );

    if ( musicVolume == 0 )     { music0.SetImageClipRect( sf::IntRect( 500, 0, 50, 50 ) ); }
    else                        { music0.SetImageClipRect( sf::IntRect( 550, 0, 50, 50 ) ); }
    if ( musicVolume <= 25  )   { music1.SetImageClipRect( sf::IntRect( 500, 0, 50, 50 ) ); }
    else                        { music1.SetImageClipRect( sf::IntRect( 550, 0, 50, 50 ) ); }
    if ( musicVolume <= 50  )   { music2.SetImageClipRect( sf::IntRect( 500, 0, 50, 50 ) ); }
    else                        { music2.SetImageClipRect( sf::IntRect( 550, 0, 50, 50 ) ); }
    if ( musicVolume <= 75  )   { music3.SetImageClipRect( sf::IntRect( 500, 0, 50, 50 ) ); }
    else                        { music3.SetImageClipRect( sf::IntRect( 550, 0, 50, 50 ) ); }

    if ( chalo::ConfigManager::Get( "USE_OPEN_DYSLEXIC" ) == "1" )
    {
        chalo::MenuManager::GetButton( "btnToggleOpenDyslexic" ).SetupBackgroundImageClipRect( sf::IntRect( 650, 0, 50, 50 ) );
    }
    else
    {
        chalo::MenuManager::GetButton( "btnToggleOpenDyslexic" ).SetupBackgroundImageClipRect( sf::IntRect( 600, 0, 50, 50 ) );
    }
}

void OptionsState::IncreaseSoundVolume()
{
    int soundVolume = Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) );
    soundVolume += 25;
    if ( soundVolume > 100 ) { soundVolume = 100; }
    chalo::ConfigManager::Set( "SOUND_VOLUME", Helper::ToString( soundVolume ) );
    LoadSavedOptions();

    m_soundTest.setBuffer( chalo::AudioManager::GetSoundBuffer( "sound-test" ) );
    m_soundTest.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_soundTest.play();
}

void OptionsState::DecreaseSoundVolume()
{
    int soundVolume = Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) );
    soundVolume -= 25;
    if ( soundVolume < 0 ) { soundVolume = 0; }
    chalo::ConfigManager::Set( "SOUND_VOLUME", Helper::ToString( soundVolume ) );
    LoadSavedOptions();

    m_soundTest.setBuffer( chalo::AudioManager::GetSoundBuffer( "sound-test" ) );
    m_soundTest.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_soundTest.play();
}

void OptionsState::IncreaseMusicVolume()
{
    int musicVolume = Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) );
    musicVolume += 25;
    if ( musicVolume > 100 ) { musicVolume = 100; }
    chalo::ConfigManager::Set( "MUSIC_VOLUME", Helper::ToString( musicVolume ) );
    LoadSavedOptions();
    chalo::AudioManager::PlayMusic( "music-test", false );
}

void OptionsState::DecreaseMusicVolume()
{
    int musicVolume = Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) );
    musicVolume -= 25;
    if ( musicVolume < 0 ) { musicVolume = 0; }
    chalo::ConfigManager::Set( "MUSIC_VOLUME", Helper::ToString( musicVolume ) );
    LoadSavedOptions();
    chalo::AudioManager::PlayMusic( "music-test", false );
}

void OptionsState::ToggleOpenDyslexic()
{
    if ( chalo::ConfigManager::Get( "USE_OPEN_DYSLEXIC" ) == "1" )
    {
        // Turn it off
        chalo::MenuManager::GetButton( "btnToggleOpenDyslexic" ).SetupBackgroundImageClipRect( sf::IntRect( 600, 0, 50, 50 ) );
        chalo::ConfigManager::Set( "USE_OPEN_DYSLEXIC", "0" );
        chalo::MenuManager::DyslexiaFontReload();
    }
    else
    {
        // Turn it on
        chalo::MenuManager::GetButton( "btnToggleOpenDyslexic" ).SetupBackgroundImageClipRect( sf::IntRect( 650, 0, 50, 50 ) );
        chalo::ConfigManager::Set( "USE_OPEN_DYSLEXIC", "1" );
        chalo::MenuManager::DyslexiaFontReload();
    }
}

void OptionsState::ToggleSubtitles()
{
    if ( chalo::ConfigManager::Get( "USE_SUBTITLES" ) == "1" )
    {
        // Turn it off
        chalo::MenuManager::GetButton( "btnToggleSubtitles" ).SetupBackgroundImageClipRect( sf::IntRect( 600, 0, 50, 50 ) );
        chalo::ConfigManager::Set( "USE_SUBTITLES", "0" );
    }
    else
    {
        // Turn it on
        chalo::MenuManager::GetButton( "btnToggleSubtitles" ).SetupBackgroundImageClipRect( sf::IntRect( 650, 0, 50, 50 ) );
        chalo::ConfigManager::Set( "USE_SUBTITLES", "1" );
    }
}

void OptionsState::ToggleCaptions()
{
    if ( chalo::ConfigManager::Get( "USE_CAPTIONS" ) == "1" )
    {
        // Turn it off
        chalo::MenuManager::GetButton( "btnToggleCaptions" ).SetupBackgroundImageClipRect( sf::IntRect( 600, 0, 50, 50 ) );
        chalo::ConfigManager::Set( "USE_CAPTIONS", "0" );
    }
    else
    {
        // Turn it on
        chalo::MenuManager::GetButton( "btnToggleCaptions" ).SetupBackgroundImageClipRect( sf::IntRect( 650, 0, 50, 50 ) );
        chalo::ConfigManager::Set( "USE_CAPTIONS", "1" );
    }
}

void OptionsState::Update()
{
    CursorState::Update();

    std::string clickedButton = chalo::MenuManager::GetClickedButton();
    std::string gamepadButton = chalo::MenuManager::GetHoveredButton( m_cursors[0].GetPosition() );

    // Main menu
    if ( clickedButton == "btnPlay" ||
        ( gamepadButton == "btnPlay" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
    }
    // Volume control
    else if ( clickedButton == "btnSoundEffectsDecrease" ||
        ( gamepadButton == "btnSoundEffectsDecrease" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        DecreaseSoundVolume();
    }
    else if ( clickedButton == "btnSoundEffectsIncrease" ||
        ( gamepadButton == "btnSoundEffectsIncrease" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        IncreaseSoundVolume();
    }
    else if ( clickedButton == "btnMusicDecrease" ||
        ( gamepadButton == "btnMusicDecrease" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        DecreaseMusicVolume();
    }
    else if ( clickedButton == "btnMusicIncrease" ||
        ( gamepadButton == "btnMusicIncrease" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        IncreaseMusicVolume();
    }
    else if ( clickedButton == "btnToggleOpenDyslexic" ||
        ( gamepadButton == "btnToggleOpenDyslexic" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        ToggleOpenDyslexic();
    }
    else if ( clickedButton == "btnToggleSubtitles" ||
        ( gamepadButton == "btnToggleSubtitles" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        ToggleSubtitles();
    }
    else if ( clickedButton == "btnToggleCaptions" ||
        ( gamepadButton == "btnToggleCaptions" && chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) ) )
    {
        ToggleCaptions();
    }
    // Demo select menu
    else if ( clickedButton == "btnBack" || gamepadButton == "btnBack" )
    {
        SetGotoState( "StartupState" );
    }
}

void OptionsState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
    CursorState::Draw( window );
}



