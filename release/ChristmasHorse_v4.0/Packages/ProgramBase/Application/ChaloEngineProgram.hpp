#ifndef _CHALO_ENGINE_PROGRAM_HPP
#define _CHALO_ENGINE_PROGRAM_HPP

#include "../../../chalo-engine/Application/ChaloProgram.hpp"
#include "../../../chalo-engine/Managers/StateManager.hpp"

/**
- TODO: Each year I have "Epilogue", "Prologue" states ... These can be condensed into one class and loaded with the data needed...!
*/

class ChaloEngineProgram : public chalo::ChaloProgram
{
    public:
    ChaloEngineProgram( bool fullscreen = false );
    ~ChaloEngineProgram();

    void Run();
    void ChangeState( std::string key );

    private:
    std::string CLASSNAME;
    chalo::StateManager m_stateManager;

    void Setup( bool fullscreen = false );
    void Teardown();
};

#endif
