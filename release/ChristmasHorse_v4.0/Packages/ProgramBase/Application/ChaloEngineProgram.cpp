#include "ChaloEngineProgram.hpp"

// Engine files
#include "../../../chalo-engine/Managers/FontManager.hpp"
#include "../../../chalo-engine/Application/Application.hpp"
#include "../../../chalo-engine/Utilities/Messager.hpp"

#include "ChaloEngineProgram.hpp"

// Game states
#include "../StartupState.hpp"
#include "../AboutState.hpp"
#include "../LoreState.hpp"
#include "../OptionsState.hpp"

#include "../../MotorHorse/MH_GameState.hpp"

#include "../../ChristmasHorse2020/CH2020_GameState.hpp"
#include "../../ChristmasHorse2020/CH2020_GameState2.hpp"
#include "../../ChristmasHorse2020/CH2020_GameStateBoss.hpp"
#include "../../ChristmasHorse2020/CH2020_EpilogueState.hpp"

#include "../../ChristmasHorse2021/CH2021_GameState.hpp"
#include "../../ChristmasHorse2021/CH2021_PrologueState.hpp"
#include "../../ChristmasHorse2021/CH2021_EpilogueState.hpp"

#include "../../ChristmasHorse2022/CH2022_GameState.hpp"
#include "../../ChristmasHorse2022/CH2022_PrologueState.hpp"
#include "../../ChristmasHorse2022/CH2022_EpilogueState.hpp"

#include "../../ChristmasHorse2023/CH2023_GameState.hpp"
#include "../../ChristmasHorse2023/CH2023_EpilogueState.hpp"

#include "../EscapeyState.hpp"

#include "../StartupState.hpp"
#include "../../ExciteHorse/EXH_GameState.hpp"

ChaloEngineProgram::ChaloEngineProgram( bool fullscreen /* = false */ )
{
    CLASSNAME = std::string( typeid( *this ).name() );
    Logger::OutFuncBegin( "fullscreen=" + Helper::ToString( fullscreen ), CLASSNAME + "::" + std::string( __func__ ) );
    Setup( fullscreen );
    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

ChaloEngineProgram::~ChaloEngineProgram()
{
    Logger::OutFuncBegin( "", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
    Teardown();
    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

void ChaloEngineProgram::Setup( bool fullscreen /* = false */ )
{
    Logger::OutFuncBegin( "", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );

    chalo::ChaloProgram::Setup( "The Christmas Horse Saga" );
    m_stateManager.InitManager();

    ChangeState( "StartupState" );
//    ChangeState( "ch2023_gamestate" );
//    ChangeState( "ch2023_epiloguestate" );

    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

void ChaloEngineProgram::Teardown()
{
    Logger::OutFuncBegin( "", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
    chalo::ChaloProgram::Teardown();
    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

void ChaloEngineProgram::ChangeState( std::string key )
{
    Logger::OutFuncBegin( "key=" + key, std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
    chalo::IState* nextState = nullptr;
    Logger::Out( "Change State to \"" + key + "\"", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );

    key = Helper::ToLower( key );

    if      ( key == "startupstate" )              { nextState = new StartupState; }
    else if ( key == "optionsstate" )              { nextState = new OptionsState; }
//    else if ( key == "helpstate" )                 { nextState = new HelpState; }
    else if ( key == "aboutstate" )                { nextState = new AboutState; }
    else if ( key == "lorestate" )                 { nextState = new LoreState; }

    else if ( key == "mh_gamestate" )              { nextState = new MH_GameState; }
//    else if ( key == "excitehorse_state" )         { nextState = new ExciteHorseGameState; }

    else if ( key == "ch2020_gamestate" )          { nextState = new CH2020_GameState; }
    else if ( key == "ch2020_gamestate2" )         { nextState = new CH2020_GameState2; }
    else if ( key == "ch2020_gamestateboss" )      { nextState = new CH2020_GameStateBoss; }
    else if ( key == "ch2020_epiloguestate" )      { nextState = new CH2020_EpilogueState; }

    else if ( key == "ch2021_gamestate" )          { nextState = new CH2021_GameState; }
    else if ( key == "ch2021_prologuestate" )      { nextState = new CH2021_PrologueState; }
    else if ( key == "ch2021_epiloguestate" )      { nextState = new CH2021_EpilogueState; }

    else if ( key == "ch2022_gamestate" )          { nextState = new CH2022_GameState; }
    else if ( key == "ch2022_prologuestate" )      { nextState = new CH2022_PrologueState; }
    else if ( key == "ch2022_epiloguestate" )      { nextState = new CH2022_EpilogueState; }

    else if ( key == "ch2023_gamestate" )          { nextState = new CH2023_GameState; }
    else if ( key == "ch2023_epiloguestate" )      { nextState = new CH2023_EpilogueState; }

    else
    {
        Logger::Error( "Undefined state " + key + "!", CLASSNAME + "::" + std::string( __func__ ) );
        m_stateManager.ChangeState( "error" );
    }

    m_stateManager.AddState( key, nextState );
    m_stateManager.ChangeState( key );
    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

void ChaloEngineProgram::Run()
{
    while ( chalo::Application::IsRunning() )
    {
//        Logger::Out( "Begin Drawing...", CLASSNAME + "::" + std::string( __func__ ) );
        chalo::Application::BeginDrawing();
//        Logger::Out( "Update...", CLASSNAME + "::" + std::string( __func__ ) );
        chalo::Application::Update();
//        Logger::Out( "Update State...", CLASSNAME + "::" + std::string( __func__ ) );
        m_stateManager.UpdateState();

        if ( m_stateManager.GetGotoState() != "" )
        {
//        Logger::Out( "Change state...", CLASSNAME + "::" + std::string( __func__ ) );
            ChangeState( m_stateManager.GetGotoState() );
        }

        // Drawing
//        Logger::Out( "Draw state...", CLASSNAME + "::" + std::string( __func__ ) );
        m_stateManager.DrawState( chalo::Application::GetWindow() );
//        Logger::Out( "End drawing...", CLASSNAME + "::" + std::string( __func__ ) );
        chalo::Application::EndDrawing();
    }

}




//
//ChaloEngineProgram::ChaloEngineProgram()
//{
//    std::map<std::string, std::string> options = {
//        std::pair<std::string, std::string>( "CONFIG_NAME", "config.chaloconfig" ),
//        std::pair<std::string, std::string>( "TITLEBAR_TEXT", "CHRISTMAS HORSE" ),
//        std::pair<std::string, std::string>( "WINDOW_WIDTH", "1280" ),
//        std::pair<std::string, std::string>( "WINDOW_HEIGHT", "720" ),
//        std::pair<std::string, std::string>( "MENU_PATH", "Packages/ProgramBase/Menus/" ),
//        std::pair<std::string, std::string>( "FULLSCREEN", "0" )
//    };
//
//    Setup( options );
//
//    // Set up menu manager
//    chalo::MenuManager::Setup( "Packages/Menus/" );
//
//    // Set up global assets
//    chalo::FontManager::Add( "main",    "Packages/ProgramBase/Fonts/mononoki-Bold.ttf" );
//    chalo::FontManager::Add( "heading", "Packages/ProgramBase/Fonts/Bradley-Gratis.ttf" );
//    chalo::FontManager::Add( "title",   "Packages/ProgramBase/Fonts/ferrum.otf" );
//
//    // Set up draw managr
//    chalo::DrawManager::Setup();
//
//    // Set up states
//    chalo::Messager::Set( "score", 0 );
//    chalo::IState* startupState     = new StartupState;
//    chalo::IState* aboutState       = new AboutState;
//    chalo::IState* loreState        = new LoreState;
//
//    // Prequel Game
//    chalo::IState* mh_gameState         = new MH_GameState;
//
//    // 2020 Game
//    chalo::IState* ch2020_gameState         = new CH2020_GameState;
//    chalo::IState* ch2020_gameState2        = new CH2020_GameState2;
//    chalo::IState* ch2020_gameStateBoss     = new CH2020_GameStateBoss;
//    chalo::IState* ch2020_epilogueState     = new CH2020_EpilogueState;
//
//    // 2021 Game
//    chalo::IState* ch2021_gameState         = new CH2021_GameState;
//    chalo::IState* ch2021_prologueState     = new CH2021_PrologueState;
//    chalo::IState* ch2021_epilogueState     = new CH2021_EpilogueState;
//
//    // 2022 Game
//    chalo::IState* ch2022_gameState         = new CH2022_GameState;
//    chalo::IState* ch2022_prologueState     = new CH2022_PrologueState;
//    chalo::IState* ch2022_epilogueState     = new CH2022_EpilogueState;
//
//    m_stateManager.InitManager();
//    m_stateManager.AddState( "startupstate",            startupState );
//    m_stateManager.AddState( "aboutState",              aboutState );
//    m_stateManager.AddState( "loreState",               loreState );
//    // TODO: Add options screen to adjust volume
//
//    m_stateManager.AddState( "mh_gameState",            mh_gameState );
//
//    m_stateManager.AddState( "ch2020_gamestate",        ch2020_gameState );
//    m_stateManager.AddState( "ch2020_gamestate2",       ch2020_gameState2 );
//    m_stateManager.AddState( "ch2020_gamestateboss",    ch2020_gameStateBoss );
//    m_stateManager.AddState( "ch2020_epiloguestate",    ch2020_epilogueState );
//
//    m_stateManager.AddState( "ch2021_prologueState",    ch2021_prologueState );
//    m_stateManager.AddState( "ch2021_gamestate",        ch2021_gameState );
//    m_stateManager.AddState( "ch2021_epilogueState",    ch2021_epilogueState );
//
//    m_stateManager.AddState( "ch2022_prologueState",    ch2022_prologueState );
//    m_stateManager.AddState( "ch2022_gameState",        ch2022_gameState );
//    m_stateManager.AddState( "ch2022_epilogueState",    ch2022_epilogueState );
//
//    m_stateManager.ChangeState( "startupstate" );
////    m_stateManager.ChangeState( "ch2022_gameState" );
//}
//
//ChaloEngineProgram::~ChaloEngineProgram()
//{
//    Teardown();
//}
//
//void ChaloEngineProgram::Run()
//{
//    while ( chalo::Application::IsRunning() )
//    {
//        chalo::Application::BeginDrawing();
//        chalo::Application::Update();
//        m_stateManager.UpdateState();
//
//        if ( m_stateManager.GetGotoState() != "" )
//        {
//            m_stateManager.ChangeState( m_stateManager.GetGotoState() );
//        }
//
//        // Drawing
//        m_stateManager.DrawState( chalo::Application::GetWindow() );
//        chalo::Application::EndDrawing();
//    }
//}
