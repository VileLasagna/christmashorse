#include "EscapeyState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Managers/AudioManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"

EscapeyState::EscapeyState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void EscapeyState::Init( const std::string& name )
{
//    Logger::Out( "Parameters - name: " + name, "EscapeyState::Init", "function-trace" );
    IState::Init( name );
}

void EscapeyState::Setup()
{
    Logger::OutFuncBegin( "Begin setup", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();

    chalo::TextureManager::Add( "button-long",             "Packages/ProgramBase/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",    "Packages/ProgramBase/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Packages/ProgramBase/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Packages/ProgramBase/Graphics/UI/button-square-selected.png" );
    chalo::TextureManager::Add( "button-square",           "Content/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square2",          "Content/Graphics/UI/button-square2.png" );
    chalo::TextureManager::Add( "button-square-selected",  "Content/Graphics/UI/button-square-selected.png" );
    chalo::TextureManager::Add( "menu-icons40x40",         "Content/Graphics/UI/menu-icons40x40.png" );

    chalo::AudioManager::AddMusic( "music-test",           "Content/Audio/music-test.ogg" );
    chalo::AudioManager::AddSoundBuffer( "sound-test",     "Content/Audio/sound-test.ogg" );

    m_pauseEnabled = false;

//    chalo::InputManager::Setup();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void EscapeyState::Cleanup()
{
//    Logger::Out( "", "EscapeyState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void EscapeyState::Update()
{
//    Logger::OutFuncBegin( "Begin setup", CLASSNAME + "::" + std::string( __func__ ) );
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::Escape ) )// && !IsPaused() )
    {
        TogglePause( true );
    }

    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    if ( IsPaused() && clickedButton == "btnPlay" )
    {
        TogglePause( false );
    }
    else if ( IsPaused() && clickedButton == "btnMainMenu" )
    {
        SetGotoState( "startupstate" );
    }


    // Volume control
    if ( clickedButton == "btnSoundEffectsDecrease" )
    {
        DecreaseSoundVolume();
    }
    else if ( clickedButton == "btnSoundEffectsIncrease" )
    {
        IncreaseSoundVolume();
    }
    else if ( clickedButton == "btnMusicDecrease" )
    {
        DecreaseMusicVolume();
    }
    else if ( clickedButton == "btnMusicIncrease" )
    {
        IncreaseMusicVolume();
    }
//    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void EscapeyState::Draw( sf::RenderWindow& window )
{
    if ( m_pauseEnabled )
    {
      chalo::DrawManager::AddMenu();
    }
    else
    {
    }
}

void EscapeyState::TogglePause( bool value )
{
    Logger::OutFuncBegin( "Begin setup", CLASSNAME + "::" + std::string( __func__ ) );
    m_pauseEnabled = value;
    if ( m_pauseEnabled )
    {
      chalo::MenuManager::LoadCsvMenu( "pause.csv" );
      chalo::InputManager::ToggleCursor( true );
      LoadSavedOptions();
    }
    else
    {
      chalo::DrawManager::Reset();
    }
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

bool EscapeyState::IsPaused() const
{
    return m_pauseEnabled;
}

void EscapeyState::LoadSavedOptions()
{
    Logger::OutFuncBegin( "Begin setup", CLASSNAME + "::" + std::string( __func__ ) );
    int iconWidth = 40;
    int soundVolume = Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) );

    chalo::UIButton& sound0 = chalo::MenuManager::GetButton( "1", "soundVolumeBlip0" );
    chalo::UIButton& sound1 = chalo::MenuManager::GetButton( "1", "soundVolumeBlip1" );
    chalo::UIButton& sound2 = chalo::MenuManager::GetButton( "1", "soundVolumeBlip2" );
    chalo::UIButton& sound3 = chalo::MenuManager::GetButton( "1", "soundVolumeBlip3" );

    if ( soundVolume == 0 )     { sound0.SetIconImageClipRect( sf::IntRect( iconWidth*10, 0, iconWidth, iconWidth ) ); }
    else                        { sound0.SetIconImageClipRect( sf::IntRect( iconWidth*11, 0, iconWidth, iconWidth ) ); }
    if ( soundVolume <= 25  )   { sound1.SetIconImageClipRect( sf::IntRect( iconWidth*10, 0, iconWidth, iconWidth ) ); }
    else                        { sound1.SetIconImageClipRect( sf::IntRect( iconWidth*11, 0, iconWidth, iconWidth ) ); }
    if ( soundVolume <= 50  )   { sound2.SetIconImageClipRect( sf::IntRect( iconWidth*10, 0, iconWidth, iconWidth ) ); }
    else                        { sound2.SetIconImageClipRect( sf::IntRect( iconWidth*11, 0, iconWidth, iconWidth ) ); }
    if ( soundVolume <= 75  )   { sound3.SetIconImageClipRect( sf::IntRect( iconWidth*10, 0, iconWidth, iconWidth ) ); }
    else                        { sound3.SetIconImageClipRect( sf::IntRect( iconWidth*11, 0, iconWidth, iconWidth ) ); }

    int musicVolume = Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) );

    chalo::UIButton& music0 = chalo::MenuManager::GetButton( "1", "musicVolumeBlip0" );
    chalo::UIButton& music1 = chalo::MenuManager::GetButton( "1", "musicVolumeBlip1" );
    chalo::UIButton& music2 = chalo::MenuManager::GetButton( "1", "musicVolumeBlip2" );
    chalo::UIButton& music3 = chalo::MenuManager::GetButton( "1", "musicVolumeBlip3" );

    if ( musicVolume == 0 )     { music0.SetIconImageClipRect( sf::IntRect( iconWidth*10, 0, iconWidth, iconWidth ) ); }
    else                        { music0.SetIconImageClipRect( sf::IntRect( iconWidth*11, 0, iconWidth, iconWidth ) ); }
    if ( musicVolume <= 25  )   { music1.SetIconImageClipRect( sf::IntRect( iconWidth*10, 0, iconWidth, iconWidth ) ); }
    else                        { music1.SetIconImageClipRect( sf::IntRect( iconWidth*11, 0, iconWidth, iconWidth ) ); }
    if ( musicVolume <= 50  )   { music2.SetIconImageClipRect( sf::IntRect( iconWidth*10, 0, iconWidth, iconWidth ) ); }
    else                        { music2.SetIconImageClipRect( sf::IntRect( iconWidth*11, 0, iconWidth, iconWidth ) ); }
    if ( musicVolume <= 75  )   { music3.SetIconImageClipRect( sf::IntRect( iconWidth*10, 0, iconWidth, iconWidth ) ); }
    else                        { music3.SetIconImageClipRect( sf::IntRect( iconWidth*11, 0, iconWidth, iconWidth ) ); }
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void EscapeyState::IncreaseSoundVolume()
{
    Logger::OutFuncBegin( "Begin setup", CLASSNAME + "::" + std::string( __func__ ) );
    int soundVolume = Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) );
    soundVolume += 25;
    if ( soundVolume > 100 ) { soundVolume = 100; }
    chalo::ConfigManager::Set( "SOUND_VOLUME", Helper::ToString( soundVolume ) );
    LoadSavedOptions();

    m_soundTest.setBuffer( chalo::AudioManager::GetSoundBuffer( "sound-test" ) );
    m_soundTest.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_soundTest.play();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void EscapeyState::DecreaseSoundVolume()
{
    Logger::OutFuncBegin( "Begin setup", CLASSNAME + "::" + std::string( __func__ ) );
    int soundVolume = Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) );
    soundVolume -= 25;
    if ( soundVolume < 0 ) { soundVolume = 0; }
    chalo::ConfigManager::Set( "SOUND_VOLUME", Helper::ToString( soundVolume ) );
    LoadSavedOptions();

    m_soundTest.setBuffer( chalo::AudioManager::GetSoundBuffer( "sound-test" ) );
    m_soundTest.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_soundTest.play();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void EscapeyState::IncreaseMusicVolume()
{
    Logger::OutFuncBegin( "Begin setup", CLASSNAME + "::" + std::string( __func__ ) );
    int musicVolume = Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) );
    musicVolume += 25;
    if ( musicVolume > 100 ) { musicVolume = 100; }
    chalo::ConfigManager::Set( "MUSIC_VOLUME", Helper::ToString( musicVolume ) );
    LoadSavedOptions();
    chalo::AudioManager::PlayMusic( "music-test", false );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void EscapeyState::DecreaseMusicVolume()
{
    Logger::OutFuncBegin( "Begin setup", CLASSNAME + "::" + std::string( __func__ ) );
    int musicVolume = Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) );
    musicVolume -= 25;
    if ( musicVolume < 0 ) { musicVolume = 0; }
    chalo::ConfigManager::Set( "MUSIC_VOLUME", Helper::ToString( musicVolume ) );
    LoadSavedOptions();
    chalo::AudioManager::PlayMusic( "music-test", false );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}


