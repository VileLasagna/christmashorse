#include "CH2023_EpilogueState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"

CH2023_EpilogueState::CH2023_EpilogueState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2023_EpilogueState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_EpilogueState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();

    chalo::TextureManager::Add( "background1", "Packages/ChristmasHorse2023/Graphics/backgrounds/outro.png" );

    m_background1.setTexture( chalo::TextureManager::Get( "background1" ) );

//    chalo::InputManager::Setup();

    int x = 5;
    int y = chalo::Application::GetScreenHeight();
    int ft = 25;

    sf::Text text;
    text.setFont( chalo::FontManager::Get( "main" ) );
    text.setCharacterSize( ft );
    text.setFillColor( sf::Color::White );
    text.setOutlineColor( sf::Color::Black );
    text.setOutlineThickness( 2 );


    text.setPosition( x, y ); text.setString( "Bepis reunites with Wabeka, Sneezy, Drabis, and Ratchet." );
    m_text.push_back( text ); y += 50;

    text.setPosition( x, y ); text.setString( "True friends and allies. A safe haven." );
    m_text.push_back( text ); y += 50;

    text.setPosition( x, y ); text.setString( "They explain that Bepis had been missing for over a year." );
    m_text.push_back( text ); y += 100;

    text.setPosition( x, y ); text.setString( "The destroyed North Pole base abandoned," );
    m_text.push_back( text ); y += 50;

    text.setPosition( x, y ); text.setString( "they've been laying low in a new, secret location." );
    m_text.push_back( text ); y += 100;

    text.setPosition( x, y ); text.setString( "Bepis is still shaken up by the sight of the experiments..." );
    m_text.push_back( text ); y += 50;

    text.setPosition( x, y ); text.setString( "Is Bepis even the real Bepis? What would the others think?" );
    m_text.push_back( text ); y += 100;

    text.setPosition( x, y ); text.setString( "Why did the Chickens betray them?" );
    m_text.push_back( text ); y += 50;

    text.setPosition( x, y ); text.setString( "What were they getting revenge for?" );
    m_text.push_back( text ); y += 100;

    text.setPosition( x, y ); text.setString( "Is Bepis even the real Bepis? What would the others think?" );
    m_text.push_back( text ); y += 50;

    text.setPosition( x, y ); text.setString( "For now, Bepis will attempt to recuperate," );
    m_text.push_back( text ); y += 50;

    text.setPosition( x, y ); text.setString( "finding comfort in the company of companions." );
    m_text.push_back( text ); y += 100;

    text.setPosition( x, y ); text.setString( "There is much to think about." );
    m_text.push_back( text ); y += 50;

    m_music.openFromFile( "Packages/ChristmasHorse2023/Audio/CastleOfEvil.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_EpilogueState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    m_text.clear();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_EpilogueState::Update()
{
    chalo::InputManager::Update();

    if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Escape ) || chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Enter ) )
    {
        SetGotoState( "startupstate" );
    }

    float scrollSpeed = -0.5;

    if ( chalo::InputManager::IsLeftClick() )
    {
      scrollSpeed *= 5;
    }

    for ( auto & text : m_text )
    {
        sf::Vector2f pos = text.getPosition();
        pos.y += scrollSpeed;
        text.setPosition( pos );
    }
}

void CH2023_EpilogueState::Draw( sf::RenderWindow& window )
{
    window.draw( m_background1 );

    for ( auto & text : m_text )
    {
        window.draw( text );
    }
}



