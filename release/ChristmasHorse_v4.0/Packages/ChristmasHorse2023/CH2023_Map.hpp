#ifndef _CH2023_MAP
#define _CH2023_MAP

#include <string>
#include <map>
using namespace std;

#include "CH2023_PACObject.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"

class CH2023_Map
{
  public:
  CH2023_Map();
  CH2023_Map( sf::Vector2f relpos, std::string mapDataPath, std::string name, std::string backgroundPath, int north = -1, int west = -1, int east = -1, int south = -1 );
  void Setup( sf::Vector2f relpos, std::string mapDataPath, std::string name, std::string backgroundPath, int north = -1, int west = -1, int east = -1, int south = -1 );
  void SetNeighbors( int north, int west, int east, int south );

  std::string GetBackgroundPath() const;
  int GetNeighborIndex( std::string direction ) const;
  std::map<std::string, PointAndClickObject> Activate( const std::vector<std::string>& inventory, const std::vector<std::string>& switches );
  sf::Vector2f GetPosition() const;
  void DebugOut();
  std::string GetName();

//  std::map<std::string, PointAndClickObject>& GetObjects();
  std::map<std::string, int>& GetNeighborIndices();

  private:
  std::string CLASSNAME;
  std::string m_name;
  std::string m_backgroundPath;
  std::string m_mapPath;
  std::map<std::string, int> m_neighborIndex;
  std::map<std::string, PointAndClickObject> m_objects;
  sf::Vector2f m_relativePos;

  void LoadMapData( std::string file, const std::vector<std::string>& inventory, const std::vector<std::string>& switches );

  sf::Color StringToColor( std::string rgb );
};

#endif
