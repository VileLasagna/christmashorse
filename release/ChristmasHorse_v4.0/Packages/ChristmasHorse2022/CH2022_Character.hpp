#ifndef _CH2022_CHARACTER
#define _CH2022_CHARACTER

#include <SFML/Graphics.hpp>
#include <string>

class CH2022_Actor
{
    public:
    void Setup( const std::string& name, const sf::Texture& texture, sf::IntRect position, int maxHp );
    void Draw( sf::RenderWindow& window );
    void UpdateSprite();
    void SetSpeed( float speed );
    sf::Sprite GetSprite();

    sf::IntRect GetPosition();
    sf::Vector2f GetPositionVec();
    float GetX();
    float GetY();
    float GetWidth();
    float GetHeight();
    void UpdateHealth( int amount );
    int GetHealth();
    int GetHealthMax();
    void SetPosition( int x, int y );
    float GetDamageAmount();
    void SetDamageAmount( float amount );
    std::string GetName();

    bool IsAlive();
    bool ReadyToShoot();
    void DoneShooting();
    bool GotHurt();
    void DoneHurt();
    void SetStatusText( std::string text );

    void Update();
    void Update( CH2022_Actor& base, std::vector<CH2022_Actor>& targets );
    void UpdateBomb( CH2022_Actor& target );
    void UpdateSniper( CH2022_Actor& base, std::vector<CH2022_Actor>& allies );
    void UpdateBullet();
    void UpdateAntlerHorse( std::vector<CH2022_Actor>& enemies );
    void UpdateSnowMoose( std::vector<CH2022_Actor>& enemies );
    void UpdateTowerHorse( std::vector<CH2022_Actor>& enemies );
    void UpdateChimnken( std::vector<CH2022_Actor>& enemies );
    void UpdateSnowball();
    void UpdateBigChimnken();

    static void CheckAllyEnemyCollision( CH2022_Actor& ally, CH2022_Actor& enemy );

    private:
    sf::Sprite m_sprite;
    sf::RectangleShape m_healthBG, m_health;
    sf::IntRect m_position; // x, y, width, height

    int m_hpMax;
    int m_hp;

    float m_speed;
    float m_damageAmount;

    int m_actionCooldown;
    int m_actionCooldownMax;
    int m_actionCooldown2;
    int m_actionCooldown2Max;
    int m_actionCounter;
    sf::Text m_statusText;

    bool m_isAlly;
    bool m_readyToShoot;
    bool m_gotHurt;
    std::string m_name;
};

#endif
