#include "CH2022_EpilogueState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"

CH2022_EpilogueState::CH2022_EpilogueState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2022_EpilogueState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

sf::Text& CH2022_EpilogueState::CenterText( sf::Text& text, std::string str, int yPos )
{
    text.setString( str );
    text.setOrigin( text.getLocalBounds().width/2, text.getLocalBounds().height/2 );
    text.setPosition( 1280/2, yPos );
    return text;
}

void CH2022_EpilogueState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();

//    chalo::InputManager::Setup();

    m_currentSlide = 0;
    m_mouseCooldown = 0;
    m_mouseCooldownMax = 10;

    sf::Text text;
    text.setFont( chalo::FontManager::Get( "main" ) );
    text.setOutlineColor( sf::Color::Black );
    text.setOutlineThickness( 5 );
    text.setCharacterSize( 50 );
    text.setFillColor( sf::Color::White );
    sf::Sprite sprite;
    StorySlide slide;
    int midX;

    {
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch2022_epilogue_1",       "Packages/ChristmasHorse2022/Graphics/Story/ch2022_epilogue_1.png" ) );
    slide.text.push_back( CenterText( text, "The onslaught ceases.", 50 ) );
    slide.text.push_back( CenterText( text, "The team takes a breath.", 100 ) );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.text.push_back( CenterText( text, "But then...", 75 ) );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch2022_epilogue_1c",       "Packages/ChristmasHorse2022/Graphics/Story/ch2022_epilogue_1c.png" ) );
    sprite.setTexture( chalo::TextureManager::AddAndGet( "ch2022_epilogue_1d",     "Packages/ChristmasHorse2022/Graphics/Story/ch2022_epilogue_1d.png" ) );
    sprite.setPosition( 0, 0 );
    slide.sprites.push_back( sprite );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch2022_epilogue_2",       "Packages/ChristmasHorse2022/Graphics/Story/ch2022_epilogue_2.png" ) );
    slide.text.push_back( CenterText( text, "Inside the workshop,", 50 ) );
    slide.text.push_back( CenterText( text, "a shadowed figure stands...", 100 ) );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch2022_epilogue_3",       "Packages/ChristmasHorse2022/Graphics/Story/ch2022_epilogue_3.png" ) );
    text.setScale( 0.9, 0.9 );
    slide.text.push_back( CenterText( text, "Bepis - Christmas Horse - lays on the ground,", 720/2+50 ) );
    slide.text.push_back( CenterText( text, "the stealthy figure wielding a peppermint gun...", 720/2+100 ) );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch2022_epilogue_4",       "Packages/ChristmasHorse2022/Graphics/Story/ch2022_epilogue_4.png" ) );
    text.setScale( 1, 1 );
    slide.text.push_back( CenterText( text, "And a peppermint bomb...", 50 ) );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch2022_epilogue_5",       "Packages/ChristmasHorse2022/Graphics/Story/ch2022_epilogue_5.png" ) );
    slide.text.push_back( CenterText( text, "In mere seconds...", 50 ) );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch2022_epilogue_1b",       "Packages/ChristmasHorse2022/Graphics/Story/ch2022_epilogue_1b.png" ) );
//    slide.text.push_back( CenterText( text, "Inside the workshop,", 50 ) );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch2022_epilogue_5b",       "Packages/ChristmasHorse2022/Graphics/Story/ch2022_epilogue_5b.png" ) );
    slide.text.push_back( CenterText( text, "Everything is destroyed...", 50 ) );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch3_scene1_bg",       "Packages/ChristmasHorse2022/Graphics/Story/ch3_scene1_bg.png" ) );
    slide.text.push_back( CenterText( text, "The End    ", 720/2 ) );
    m_slides.push_back( slide );
    }

    {
    slide.text.clear();
    slide.sprites.clear();
    slide.background.setTexture( chalo::TextureManager::AddAndGet( "ch3_scene1_bg",       "Packages/ChristmasHorse2022/Graphics/Story/ch3_scene1_bg.png" ) );
    slide.text.push_back( CenterText( text, "The End...?", 720/2 ) );
    m_slides.push_back( slide );
    }


    m_music.openFromFile( "Packages/ChristmasHorse2021/Audio/ChristmasSabbotage.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2022_EpilogueState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    m_slides.clear();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2022_EpilogueState::Update()
{
    chalo::InputManager::Update();


    if ( chalo::InputManager::IsLeftClick() && m_mouseCooldown <= 0 )
    {
        m_currentSlide++;
        if ( m_currentSlide >= m_slides.size() )
        {
            SetGotoState( "startupstate" );
        }

        m_mouseCooldown = m_mouseCooldownMax;
    }
    else if ( m_mouseCooldown > 0 )
    {
        m_mouseCooldown--;
    }

    if ( m_currentSlide == 0 )
    {
//        for ( auto& sprite : m_slides[m_currentSlide].sprites )
//        {
//            sf::Vector2f pos = sprite.getPosition();
//            pos.x += 0.05;
//            sprite.setPosition( pos );
//        }
    }
    else if ( m_currentSlide == 2 )
    {
//        sf::Vector2f pos = m_slides[m_currentSlide].sprites[0].getPosition();
//        m_slides[m_currentSlide].sprites[0].setPosition( pos.x - 0.1, pos.y );
//
//        pos = m_slides[m_currentSlide].sprites[1].getPosition();
//        m_slides[m_currentSlide].sprites[1].setPosition( pos.x + 0.1, pos.y );
//
//        sf::Vector2f scale = m_slides[m_currentSlide].sprites[2].getScale();
//        m_slides[m_currentSlide].sprites[2].setScale( scale.x + 0.05, scale.y + 0.05 );
//        sf::Color color = m_slides[m_currentSlide].sprites[2].getColor();
//        color.a -= 0.1;
//        m_slides[m_currentSlide].sprites[2].setColor( color );
    }
    else if ( m_currentSlide == 3 )
    {
//        sf::Vector2f toiletPos = m_slides[m_currentSlide].sprites[0].getPosition();
//        float toiletAngle = m_slides[m_currentSlide].sprites[0].getRotation();
//        toiletPos.x += 1.5;
//        toiletAngle += 0.5;
//        m_slides[m_currentSlide].sprites[0].setPosition( toiletPos );
//        m_slides[m_currentSlide].sprites[0].setRotation( toiletAngle );
//
//        sf::Vector2f horsePos = m_slides[m_currentSlide].sprites[1].getPosition();
//        float horseAngle = m_slides[m_currentSlide].sprites[1].getRotation();
//        horsePos.x += 1;
//        horseAngle += 0.5;
//        m_slides[m_currentSlide].sprites[1].setPosition( horsePos );
//        m_slides[m_currentSlide].sprites[1].setRotation( horseAngle );
    }
    else if ( m_currentSlide == 4 )
    {
//        sf::Vector2f horsePos = m_slides[m_currentSlide].sprites[1].getPosition();
//        sf::Vector2f horseScale = m_slides[m_currentSlide].sprites[1].getScale();
//        horsePos.x -= 4;
//        horsePos.y += 1;
//        horseScale.x -= 0.005;
//        horseScale.y -= 0.005;
//        if ( horseScale.x < 0.001 ) { horseScale.x = 0.001; }
//        if ( horseScale.y < 0.001 ) { horseScale.y = 0.001; }
//        m_slides[m_currentSlide].sprites[1].setPosition( horsePos );
//        m_slides[m_currentSlide].sprites[1].setScale( horseScale );
    }
}

void CH2022_EpilogueState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::Draw( window );

    StorySlide& slide = m_slides[m_currentSlide];

    window.draw( slide.background );

    for ( auto& sprite : slide.sprites )
    {
        window.draw( sprite );
    }

    for ( auto& text : slide.text )
    {
        window.draw( text );
    }
}



