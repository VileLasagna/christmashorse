#include "MH_GameState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Managers/AudioManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

#include <list>

MH_GameState::MH_GameState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void MH_GameState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void MH_GameState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();
    std::string gfxPath = "Packages/MotorHorse/images/";
    std::string sndPath = "Packages/MotorHorse/audio/";

    chalo::TextureManager::Add( "cloud", gfxPath + "cloud.png" );
    chalo::TextureManager::Add( "textbox", gfxPath + "textbox.png" );
    chalo::TextureManager::Add( "shadow", gfxPath + "transparent.png" );
    chalo::TextureManager::Add( "win", gfxPath + "win.png" );
    chalo::TextureManager::Add( "tblHorse", gfxPath + "horse.png" );
    chalo::TextureManager::Add( "tblBirds", gfxPath + "birds.png" );
    chalo::TextureManager::Add( "tblElves", gfxPath + "elfs.png" );
    chalo::TextureManager::Add( "tblSanta", gfxPath + "santa.png" );
    chalo::TextureManager::Add( "tblBullet", gfxPath + "bullet.png" );
    chalo::TextureManager::Add( "tblStars", gfxPath + "stars.png" );

    m_images["textbox"].setTexture( chalo::TextureManager::Get( "textbox" ) );
    m_images["textbox"].setPosition( 50, 30 );
    m_images["shadow"].setTexture( chalo::TextureManager::Get( "shadow" ) );
    m_images["win"].setTexture( chalo::TextureManager::Get( "win" ) );
    m_images["horse"].setTexture( chalo::TextureManager::Get( "tblHorse" ) );

    m_soundBuffer["collect"].loadFromFile( sndPath + "sfx2.wav" );
    m_soundEffects["collect"].setBuffer( m_soundBuffer["collect"] );
    m_soundEffects["collect"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["success"].loadFromFile( sndPath + "sfx3.wav" );
    m_soundEffects["success"].setBuffer( m_soundBuffer["success"] );
    m_soundEffects["success"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["failure"].loadFromFile( sndPath + "oof.wav" );
    m_soundEffects["failure"].setBuffer( m_soundBuffer["failure"] );
    m_soundEffects["failure"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["fan"].loadFromFile( sndPath + "fan.wav" );
    m_soundEffects["fan"].setBuffer( m_soundBuffer["fan"] );
    m_soundEffects["fan"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["levelComplete"].loadFromFile( sndPath + "yay.wav" );
    m_soundEffects["levelComplete"].setBuffer( m_soundBuffer["levelComplete"] );
    m_soundEffects["levelComplete"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_texts["score"].setPosition( 1, 1 );
    m_texts["score"].setFont( chalo::FontManager::Get( "main" ) );
    m_texts["score"].setCharacterSize( 12 );

    m_gravity = 0.05;
    m_deAccRate = 1;

    m_game.state = "level_intro";
    m_game.scrollSpeed = 0;
    m_game.scrollX = 0;
    m_game.deathAnimation = 0;
    m_game.dead = false;
    m_game.score = 0;
    m_game.scoreMax = 0;
    m_game.stars = 0;

    SetupLevelData( 1 );
    PrepareLevel();
    m_gravity = m_level.gravity;

    m_player.x = 30;
    m_player.y = 100;
    m_player.width = 67;
    m_player.height = 91;
    m_player.acceleration = 0;
    m_player.accelerationMax = m_level.accelerationMax;
    m_player.accelerationIncrement = m_level.accIncrement;
    m_player.velocity = 0;
    m_player.velocityMax = m_level.velocityMax;
    m_player.frame = 1;
    m_player.frameSpeed = 0.5;
    m_player.frameMax = 3;
    m_player.rotation = 0;
    m_player.collisionRect = sf::IntRect( -15, -17, 25, 40 );
    m_player.x = m_level.startX;

    m_background.setFillColor( sf::Color::Black );
    m_background.setPosition( 0, 0 );
    m_background.setSize( sf::Vector2f( 400, 240 ) );

    // Music
    chalo::ConfigManager::Set( "MUSIC_VOLUME", 50 );
    chalo::ConfigManager::Set( "SOUND_VOLUME", 70 );

    m_music.openFromFile( sndPath + "melanie_song.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();

    chalo::DrawManager::SetBackgroundColor( sf::Color( 182, 201, 216 ) );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void MH_GameState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    chalo::MenuManager::Cleanup();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void MH_GameState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    HandleInput();

    if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Space )
        && m_game.state == "level_intro" )
    {
        BeginLevel();
    }

    if ( m_game.state == "play" )
    {
        Update_Game_Physics();
    }

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::Escape ) )
    {
      SetGotoState( "StartupState" );
    }
}

void MH_GameState::SetupLevelData( int level )
{
    if ( level == 1 )
    {
        m_level.instructions = {
          "I am SpyHorse. I stealthily float thru",
          "the clouds using my propeller backpack.",
          "I crank to go up, and stop to go down.",
          "Sometimes, I grab a bird for a snack."
        };
        m_level.startX = 30;
        m_level.scrollSpeed = -3;
        m_level.levelWidth = 600;
        m_level.gravity = 0.05;
        m_level.accelerationMax = 0.5;
        m_level.velocityMax = 1;
        m_level.accIncrement = 0.2;
        CreateObstacle( "bird1", 400, 100 );
        CreateObstacle( "bird1", 500, 150 );
        CreateObstacle( "bird1", 600, 100 );
        CreateObstacle( "bird1", 700, 100 );
        CreateObstacle( "bird1", 800, 50 );
    }
    else if ( level == 2 )
    {
    }
    else if ( level == 3 )
    {
    }
    else if ( level == 4 )
    {
    }
    else if ( level == 5 )
    {
    }
    else if ( level == 6 )
    {
    }
    else if ( level == 7 )
    {
    }
    else if ( level == 8 )
    {
    }
    else if ( level == 9 )
    {
    }
}

void MH_GameState::CreateObstacle( std::string type, float x, float y )
{
    m_level.obstacles.push_back( MH_Obstacle( type, x, y ) );
}

void MH_GameState::PrepareLevel()
{
    m_game.scrollSpeed = m_level.scrollSpeed;

    for ( int x = -25; x <= 450; x += 35 )
    {
        CreateObstacle( "cloudborder", x, -25 );
        CreateObstacle( "cloudborder", x, 240-15 );
    }

    for ( auto& obs : m_level.obstacles )
    {
        m_game.scoreMax += obs.score;
    }

    m_game.instructions = m_level.instructions;

    int x = 60;
    int y = 90;
    int inc = 20;
    sf::Text newText;
    newText.setFont( chalo::FontManager::Get( "main" ) );
    newText.setCharacterSize( 12 );
    newText.setColor( sf::Color::Black );
    for ( auto& inst : m_game.instructions )
    {
        m_texts[inst] = newText;
        m_texts[inst].setString( inst );
        m_texts[inst].setPosition( x, y );
        y += inc;
    }

    y += inc;

    m_texts["confirm"] = newText;
    m_texts["confirm"].setString( "Press SPACE to start" );
    m_texts["confirm"].setPosition( 125, y );
}

void MH_GameState::BeginLevel()
{
    m_game.state = "play";
}

void MH_GameState::Update_Game_Physics()
{
    if ( m_game.dead )
    {
        DeathAnimation();
        return;
    }

    ScreenScroll();
    UpdatePlayerMovement();
    UpdateObstacles();
}

void MH_GameState::UpdatePlayerInput()
{
    bool crankin = false;

    if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Space ) )
    {
        crankin = true;
        m_player.acceleration -= m_player.accelerationIncrement;
    }

    if ( crankin )
    {
        m_player.frame += m_player.frameSpeed;

        if ( m_player.frame >= m_player.frameMax )
        {
            m_player.frame = 1;
        }

        if ( m_soundEffects["fan"].getStatus() != sf::SoundSource::Playing )
        {
            m_soundEffects["fan"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
            m_soundEffects["fan"].play();
        }
    }
    else
    {
        m_player.velocity = 0;
        m_player.frame = 1;

        if ( m_soundEffects["fan"].getStatus() == sf::SoundSource::Playing )
        {
            m_soundEffects["fan"].stop();
        }
    }
}

void MH_GameState::UpdatePlayerMovement()
{
    // Update player velocity
    m_player.velocity += m_player.acceleration;

    // Update player position
    m_player.y += m_player.velocity;

    // Gravity always affects the player
    m_player.acceleration += m_gravity;

    // Limit acceleration
    if ( m_player.acceleration > m_player.accelerationMax )
    {
        m_player.acceleration = m_player.accelerationMax;
    }
    else if ( m_player.acceleration < -m_player.accelerationMax )
    {
        m_player.acceleration = -m_player.accelerationMax;
    }

    // Limit velocity
    if ( m_player.velocity > m_player.velocityMax )
    {
        m_player.velocity = m_player.velocityMax;
    }
    else if ( m_player.velocity < -m_player.velocityMax )
    {
        m_player.velocity = -m_player.velocityMax;
    }
}

void MH_GameState::DeathAnimation()
{
    m_player.rotation += 10;
    m_player.x += 5;
    m_player.y += 5;
    m_game.deathAnimation -= 1;

    if ( m_game.deathAnimation <= 0 )
    {
        //SetGotoState( "levelSelectScreen" );
    }
}

void MH_GameState::ScreenScroll()
{
    if ( -m_game.scrollX < m_level.levelWidth )
    {
        m_game.scrollX += m_game.scrollSpeed;
    }
    else
    {
        m_player.x += -m_game.scrollSpeed;
    }

    // Is player off screen?
    if ( m_player.x > 425 )
    {
        PlayerWin();
    }
}

void MH_GameState::UpdateObstacles()
{
    for ( size_t i = 0; i < m_level.obstacles.size(); i++ )
    {
        auto& ob = m_level.obstacles[i];

        // Scroll 'em
        if ( -m_game.scrollX < m_level.levelWidth )
        {
            ob.x += m_game.scrollSpeed;
        }

        // Loop clouds back and aroudn where they go off screen
        if ( ob.type == "cloudborder" and ob.x < -50 )
        {
            ob.x += 500;
        }

        // Move character based on behavior
        if ( ob.x > -50 && ob.x < 450 )
        {
            if ( ob.type == "bird2" )
            {
                if ( ob.moveCounter < ob.moveCounterMax / 2 )   { ob.y -= 0.5; }
                else                                            { ob.y += 0.5; }
                ob.moveCounter += 5;
            }
            else if ( ob.type == "bird3" )
            {
                if ( ob.moveCounter < ob.moveCounterMax / 2 )   { ob.y -= 1; }
                else                                            { ob.y += 1; }
                ob.x -= 1;
                ob.moveCounter += 5;
            }
            else if ( ob.type == "cloud2" )
            {
                if ( ob.moveCounter < ob.moveCounterMax / 2 )   { ob.y -= 1; }
                else                                            { ob.y += 1; }
                ob.moveCounter += 5;
            }
            else if ( ob.type == "elf" )
            {
                ob.x += ob.speed;
            }
            else if ( ob.type == "elf2" )
            {
                ob.x -= m_game.scrollSpeed;
            }
            else if ( ob.type == "santa" )
            {
            }
            else if ( ob.type == "bullet" )
            {
                ob.x -= m_game.scrollSpeed / 2;
                ob.frameCounter += 0.1;
                if ( ob.frameCounter >= 2 )
                {
                    ob.frameCounter = 0;
                }
            }
        }

        if ( ob.moveCounter != -1 && ob.moveCounter > ob.moveCounterMax )
        {
            ob.moveCounter = 0;
        }

        // Check collision with obstacles
        bool collision = Helper::BoundingBoxCollision( m_player.x, m_player.y, m_player.width, m_player.height, ob.x, ob.y, ob.width, ob.height );
        if ( collision && ob.subtype.find( "enemy" ) != std::string::npos )
        {
            PlayerDie();
        }
        else if ( collision && ob.subtype.find( "trinket" ) != std::string::npos )
        {
            CollectTrinket( i );
        }
    }

    // Remove obstacles that are off screen.
    std::list<int> deleteIds;
    for ( size_t i = 0; i < m_level.obstacles.size(); i++ )
    {
        if ( m_level.obstacles[i].x < -100 )
        {
            deleteIds.push_front( i );
        }
    }

    for ( auto& id : deleteIds )
    {
        m_level.obstacles.erase( m_level.obstacles.begin() + id );
    }
}

void MH_GameState::CollectTrinket( int key )
{
    m_level.obstacles[key].x = -100;
    m_game.score += m_level.obstacles[key].score;
    m_soundEffects["collect"].play();
}

void MH_GameState::PlayerDie()
{
    m_game.deathAnimation = 50;
    m_game.dead = true;
    m_soundEffects["failure"].play();
}

void MH_GameState::PlayerWin()
{
    m_game.stars = 0;

    // Full score for collecting all the trinkets
    if ( m_game.score >= m_game.scoreMax )
    {
        m_game.stars = 3;
    }
    else if ( m_game.score >= m_game.scoreMax / 2 )
    {
        m_game.stars = 2;
    }
    else
    {
        m_game.stars = 1;
    }

    // 1 = locked, 2 = no stars, 3 = one star, 4 = two stars, 5 = three stars

    // Save score

    // Unlock next level

    m_soundEffects["success"].play();
    m_game.state = "level_end";
}

void MH_GameState::Draw( sf::RenderWindow& window )
{
    window.draw( m_background );

    if ( m_game.state == "level_intro" )
    {
        Draw_LevelIntro( window );
    }
    else if ( m_game.state == "play" )
    {
        Draw_Game( window );
    }
    else if ( m_game.state == "level_end" )
    {
        Draw_LevelEnd( window );
    }
    else if ( m_game.state == "game_end" )
    {
        Draw_GameEnd( window );
    }
}

void MH_GameState::Draw_LevelIntro( sf::RenderWindow& window )
{
    window.draw( m_images["textbox"] );

    for ( auto& txt : m_texts )
    {
        window.draw( txt.second );
    }
}

void MH_GameState::Draw_Game( sf::RenderWindow& window )
{
    // Draw horse
    m_images["horse"].setTextureRect( sf::IntRect( m_player.width * int( m_player.frame ), 0, m_player.width, m_player.height ) );
    m_images["horse"].setPosition( m_player.x, m_player.y );
    m_images["horse"].setRotation( m_player.rotation );
    window.draw( m_images["horse"] );

    // Draw obstacles
    for ( auto& ob : m_level.obstacles )
    {
        if ( ob.x > -50 && ob.x < 450 )
        {
            ob.sprite.setPosition( ob.x, ob.y );
            ob.sprite.setTextureRect( sf::IntRect( ob.imageIndex * ob.width, 0, ob.width, ob.height ) );
            window.draw( ob.sprite );
        }
    }

    // HUD
    m_texts["score"].setString( "Score " + Helper::ToString( m_game.score ) + "/" + Helper::ToString( m_game.scoreMax ) );
    window.draw( m_texts["score"] );
}

void MH_GameState::Draw_LevelEnd( sf::RenderWindow& window )
{
    window.draw( m_images["textbox"] );
}

void MH_GameState::Draw_GameEnd( sf::RenderWindow& window )
{
    window.draw( m_images["win"] );
}

void MH_GameState::HandleInput()
{
    if ( m_game.state == "play" )
    {
        UpdatePlayerInput();
    }
    else if ( m_game.state == "level_end" )
    {
        if ( m_game.level == 9 )
        {
            m_game.state = "game_end";
        }
        else
        {
//            SetGotoState( "levelSelectScreen" );
        }
    }
    else if ( m_game.state == "game_end" )
    {
        if ( chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Escape ) )
        {
//            SetGotoState( "levelSelectScreen" );
        }
    }
}

