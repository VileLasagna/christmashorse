Linux:
Run the "christmas-horse-linux" file. You can usually double-click it to run.
Or you can run it from the terminal like this:
./christmas-horse-linux

Windows:
Run the "christmas-horse-win.exe" file by double-clicking it.

Mac:
You'll have to download the source code and build it for Mac.


REPOSITORY:
https://gitlab.com/moosadee/christmashorse


Questions / Comments / Bug reports
Rachel@Moosader.com


Thanks for playing! :)
