// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "Application.hpp"
#include "../Managers/ConfigManager.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"

namespace chalo
{

std::string Application::CLASSNAME = "Application";
sf::RenderWindow Application::m_window;
int Application::m_screenWidth;
int Application::m_screenHeight;
bool Application::m_done;
std::vector<sf::Event> Application::m_polledEvents;

void Application::Setup( const std::string& title, int screenWidth /* = 1280 */, int screenHeight /* = 720 */, bool fullscreen /* = false */, std::string platform /* = "" */ )
{
    Logger::OutFuncBegin( "title=" + title +
        ", screenWidth=" + Helper::ToString( screenWidth ) +
        ", screenHeight=" + Helper::ToString( screenHeight ) +
        ", fullscreen=" + Helper::ToString( fullscreen ) +
        ", platform=" + platform,
        CLASSNAME + "::" + std::string( __func__ ), "function-trace" );
    // create (VideoMode mode, const String &title, Uint32 style=Style::Default, const ContextSettings &settings=ContextSettings())

    if ( fullscreen )
    {
        m_window.create(
            sf::VideoMode( screenWidth, screenHeight ),
            title,
            sf::Style::Fullscreen
        );
    }
    else if ( platform == "PLAYDATE" )
    {
        m_window.create(
            sf::VideoMode( screenWidth, screenHeight ),
            title,
            sf::Style::None
        );
        m_window.setPosition( sf::Vector2i( 0, 0 ) );
    }
    else
    {
        m_window.create(
            sf::VideoMode( screenWidth, screenHeight ),
            title
        );
    }

    m_done = false;

    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;

    m_window.setFramerateLimit( 60 );

    DrawManager::Setup();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void Application::Teardown()
{
    Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
    Logger::Out( "Wait, there's nothing to do...", CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void Application::BeginDrawing()
{
    m_window.clear( DrawManager::GetBackgroundColor() );
    DrawManager::Reset();
}

void Application::EndDrawing()
{
    DrawManager::Draw( m_window );
    m_window.display();
}

bool Application::IsRunning()
{
    return ( m_window.isOpen() && !m_done );
}

void Application::ReadyToQuit()
{
    m_done = true;
}

void Application::Update()
{
    CheckWindowEvents();
}

int Application::GetScreenWidth()
{
    return m_screenWidth;
}

int Application::GetScreenHeight()
{
    return m_screenHeight;
}

void Application::CheckWindowEvents()
{
    m_polledEvents.clear();
    sf::Event event;
    while ( m_window.pollEvent( event ) )
    {
        m_polledEvents.push_back( event );
        if ( event.type == sf::Event::Closed )
        {
            m_window.close();
        }

        if ( event.type == sf::Event::MouseWheelScrolled )
        {
            //Logger::Out( "Mouse wheel: " + chalo::Helper::FloatToString( event.mouseWheel.delta ) );
        }
    }
}

std::vector<sf::Event>& Application::GetPolledEvents()
{
    return m_polledEvents;
}

sf::RenderWindow& Application::GetWindow()
{
    return m_window;
}

}
