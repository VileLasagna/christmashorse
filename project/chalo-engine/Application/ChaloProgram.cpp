// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine
#include "ChaloProgram.hpp"

#include "../Utilities/Logger.hpp"
#include "../Utilities/Messager.hpp"
#include "../Application/Application.hpp"
#include "../Managers/StateManager.hpp"
#include "../Managers/MenuManager.hpp"
#include "../Managers/ConfigManager.hpp"
#include "../Managers/DrawManager.hpp"
#include "../Managers/InputManager.hpp"
#include "../Managers/LanguageManager.hpp"
#include "../Managers/TextureManager.hpp"

#include "ChaloProgram.hpp"

namespace chalo
{

std::string ChaloProgram::CLASSNAME = "ChaloProgram";

void ChaloProgram::Setup( std::string titlebarText )
{
    Logger::Setup();
    Logger::OutFuncBegin( "[" + std::string( typeid( *this ).name() ) + "] titlebarText=" + titlebarText,
        CLASSNAME + "::" + std::string( __func__ ) );

    chalo::ConfigManager::Setup( "../chalo_config.chacha" );

    std::string menuPath        = ConfigManager::Get( "MENU_PATH" );
    std::string languagePath    = ConfigManager::Get( "LANGUAGE_PATH" );
    std::string platform        = ConfigManager::Get( "PLATFORM" );
    int windowWidth             = ConfigManager::GetInt( "WINDOW_WIDTH" );
    int windowHeight            = ConfigManager::GetInt( "WINDOW_HEIGHT" );
    bool fullscreen             = ( ConfigManager::Get( "FULLSCREEN" ) == "1" );

    Application::Setup( titlebarText, windowWidth, windowHeight, fullscreen, platform );

    LanguageManager::SetLanguageBasePath( "../Content/Languages/" );
    MenuManager::Setup( "../Content/Menus/" );
    TextureManager::Setup();
    DrawManager::Setup();
    InputManager::Setup( "../chalo_keybindings.csv" );
    FontManager::Setup( "../chalo_fonts.chacha" );

    if ( ConfigManager::Get( "LANGUAGE_MAIN" ) == "" )
    {
        ConfigManager::Set( "LANGUAGE_MAIN", "en" );
    }
    if ( ConfigManager::Get( "LANGUAGE_MAIN_FONT" ) == "" )
    {
        ConfigManager::Set( "LANGUAGE_MAIN_FONT", "main" );
    }

    // Load languages and fonts
    LanguageManager::AddLanguage(
        ConfigManager::Get( "LANGUAGE_MAIN" ),
        ConfigManager::Get( "LANGUAGE_MAIN_FONT" )
    );

    LanguageManager::AddLanguage(
        ConfigManager::Get( "TARGET_LANGUAGE" ),
        ConfigManager::Get( "TARGET_SUGGESTED_FONT" )
    );

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void ChaloProgram::Teardown()
{
    Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
//    chalo::InputManager::Teardown();
//    chalo::DrawManager::Teardown();
//    chalo::MenuManager::Teardown();
    chalo::Application::Teardown();
    Logger::Out( "OK I'm about to clean up the Logger. No more logs! Bye!!", CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
    Logger::Cleanup();
}

}

