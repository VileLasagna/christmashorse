// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _APPLICATION
#define _APPLICATION

#include <SFML/Graphics.hpp>

#include "../Managers/DrawManager.hpp"

#include <string>
#include <vector>

namespace chalo
{

class Application
{
public:
    static std::string CLASSNAME;

    static void Setup( const std::string& title = "SFML Program", int screenWidth = 1280, int screenHeight = 720, bool fullscreen = false, std::string platform = "" );
    static void Teardown();

    static void BeginDrawing();
    static void EndDrawing();

    static bool IsRunning();
    static void ReadyToQuit();
    static void Update();

    static int GetScreenWidth();
    static int GetScreenHeight();

    static std::vector<sf::Event>& GetPolledEvents();

    static sf::RenderWindow& GetWindow();

private:
    static void CheckWindowEvents();

    static sf::RenderWindow m_window;
    static int m_screenWidth;
    static int m_screenHeight;
    static bool m_done;

    static std::vector<sf::Event> m_polledEvents;
};

}

#endif


