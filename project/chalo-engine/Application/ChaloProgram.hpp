// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _CHALO_PROGRAM_HPP
#define _CHALO_PROGRAM_HPP

#include <map>
#include <string>

namespace chalo
{

class ChaloProgram
{
    public:
    static std::string CLASSNAME;

    void Setup( std::string titlebarText );
    void Teardown();
};

}

#endif
