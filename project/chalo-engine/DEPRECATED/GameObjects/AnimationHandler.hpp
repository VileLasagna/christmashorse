//#ifndef _ANIMATION_HANDLER
//#define _ANIMATION_HANDLER
//
//#include <map>
//#include <string>
//
//// TODO: Pull the code from Character/GameObject into this module.
//class AnimationHandler
//{
//    public:
//    //! Set the dimensions of the sprite being handled
//    void SetDimensions( int width, int height );
//    //! Set up action -> y coordinate mappings
//    void SetActionMapping( std::map< std::string, int > mapping );
//    //! Set information about the animation
//    void SetAnimationInformation( int maxFrames, float animationSpeed );
//
//    //! Set which action is currently happening
//    void SetAction( const std::string& action );
//    //! Get the action that is set as being done
//    std::string GetAction();
//
//    //! Update the animation frame
//    void Animate();
//    //! Get the region of the sprite editor corresponding to the current frame.
//    sf::IntRect GetFrame();
//
//    protected:
//    //! Map an action ("attack-west") to a y-value on the sprite sheet (32)
//    std::map< std::string, int > m_actionMap;
//
//    //! Current action being done
//    std::string m_currentAction;
//
//    //! Current frame of animation, corresponds to x value on sprite sheet.
//    float m_animationFrame;
//
//    //! Maximum amount of frames for this sprite-sheet
//    float m_maxFrames;
//
//    //! The speed at which to animate
//    float m_animationSpeed;
//
//    //! The dimensions of a frame; should be (0, 0) and then (width, height)
//    sf::IntRect m_frameDimensions;
//};
//
//void AnimationHandler::SetDimensions( int width, int height )
//{
//    m_frameDimensions.left = 0;
//    m_frameDimensions.top = 0;
//    m_frameDimensions.width = width;
//    m_frameDimensions.height = height;
//}
//
//void AnimationHandler::SetActionMapping( std::map< std::string, int > mapping )
//{
//    m_actionMap = mapping;
//}
//
//void AnimationHandler::SetAnimationInformation( int maxFrames, float animationSpeed )
//{
//    m_maxFrames = maxFrames;
//    m_animationSpeed = animationSpeed;
//}
//
//void AnimationHandler::SetAction( const std::string& action )
//{
//    m_currentAction = action;
//}
//
//std::string AnimationHandler::GetAction()
//{
//    return m_currentAction;
//}
//
//void AnimationHandler::Animate()
//{
//    m_animationFrame += m_animationSpeed;
//    if ( m_animationFrame >= m_maxFrames )
//    {
//        m_animationFrame = 0;
//    }
//}
//
//sf::IntRect AnimationHandler::GetFrame()
//{
//    sf::IntRect currentFrame = m_frameDimensions;
//    currentFrame.left = (int)( m_animationFrame ) * m_frameDimensions.width;
//    currentFrame.top = m_actionMap[ m_currentAction ];
//}
//
//#endif
