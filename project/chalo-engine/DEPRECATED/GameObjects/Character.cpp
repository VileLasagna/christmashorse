// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "Character.hpp"

namespace chalo
{

Character::Character() : GameObject()
{
//    Logger::Out( "Initializing Character", "Character::Character" );
    m_speed = 5;
    m_direction = SOUTH;
    m_maxFrames = 5;
    m_animationFrame = 0;
    m_animationSpeed = 0.15;
    m_sheetAction = WALKING;
    m_restrictMovement = false;
}

void Character::SetSpeed( int speed )
{
    m_speed = speed;
}

void Character::Update()
{
    GameObject::Update();
    if ( m_sheetAction == ATTACKING )
    {
        Animate();
    }

    // Update direction
    sf::IntRect frame;
    m_textureCoordinates.left = int( m_animationFrame ) * m_textureCoordinates.width;
    m_textureCoordinates.top = m_direction * m_textureCoordinates.height + ( m_sheetAction * 640 ); // TODO: Change hard-coded sprite sheet height...
    m_sprite.setTextureRect( m_textureCoordinates );

    // Check for gravity
//    if ( gravity.GetIsFalling() )
//    {
//        // Fall!
//        m_position.y -= gravity.GetGravity();
//    }

    m_sprite.setPosition( m_position );
}

sf::IntRect Character::GetDesiredPosition( Direction direction )
{
    sf::IntRect desiredNewPosition;
    desiredNewPosition.left = m_position.x;
    desiredNewPosition.top = m_position.y;
    desiredNewPosition.width = m_textureCoordinates.width;
    desiredNewPosition.height = m_textureCoordinates.height;

    if          ( direction == WEST )
    {
        desiredNewPosition.left -= m_speed;
    }
    else if     ( direction == EAST )
    {
        desiredNewPosition.left += m_speed;
    }

    if          ( direction == NORTH )
    {
        desiredNewPosition.top -= m_speed;
    }
    else if     ( direction == SOUTH )
    {
        desiredNewPosition.top += m_speed;
    }

    return desiredNewPosition;
}

//void Character::Move( Direction direction, const Map::ReadableMap& gameMap  )
//{
//    if ( m_sheetAction == ATTACKING )
//    {
//        // Can't move
//        return;
//    }
//
//    m_stateAction = MOVING;
//    m_direction = direction;
//
//    sf::IntRect desiredNewPosition;
//    desiredNewPosition.left = m_position.x;
//    desiredNewPosition.top = m_position.y;
//    desiredNewPosition.width = m_textureCoordinates.width;
//    desiredNewPosition.height = m_textureCoordinates.height;
//
//    if          ( direction == WEST )
//    {
//        desiredNewPosition.left -= m_speed;
//    }
//    else if     ( direction == EAST )
//    {
//        desiredNewPosition.left += m_speed;
//    }
//
//    if          ( direction == NORTH )
//    {
//        desiredNewPosition.top -= m_speed;
//    }
//    else if     ( direction == SOUTH )
//    {
//        desiredNewPosition.top += m_speed;
//    }
//
//    // Only move if it doesn't collide with a collidable tile.
//    // Pass in the new position AND the collision region for object vs. map
//    if ( !gameMap.IsCollision( desiredNewPosition, m_mapCollisionRectangle ) )
//    {
//        m_position.x = desiredNewPosition.left;
//        m_position.y = desiredNewPosition.top;
//    }
//
//    Animate();
//    RestrictMovement();
//}

void Character::Move( Direction direction )
{
    if ( m_sheetAction == ATTACKING )
    {
        // Can't move
        return;
    }

//    Logger::Out( "Move character " + m_name + " direction " + Helper::ToString( direction ), "Character::Move" );
    m_stateAction = MOVING;
    m_direction = direction;

    sf::IntRect desiredNewPosition;
    desiredNewPosition.left = m_position.x;
    desiredNewPosition.top = m_position.y;
    desiredNewPosition.width = m_textureCoordinates.width;
    desiredNewPosition.height = m_textureCoordinates.height;

    if          ( direction == WEST )
    {
        desiredNewPosition.left -= m_speed;
    }
    else if     ( direction == EAST )
    {
        desiredNewPosition.left += m_speed;
    }

    if          ( direction == NORTH )
    {
        desiredNewPosition.top -= m_speed;
    }
    else if     ( direction == SOUTH )
    {
        desiredNewPosition.top += m_speed;
    }

    m_position.x = desiredNewPosition.left;
    m_position.y = desiredNewPosition.top;

    Animate();
    RestrictMovement();
}

void Character::Move( Direction direction, int minX, int minY, int maxX, int maxY )
{
    Move( direction );
    if      ( m_position.x < minX )     { m_position.x = minX; }
    else if ( m_position.x > maxX )     { m_position.x = maxX; }
    if      ( m_position.y < minY )     { m_position.y = minY; }
    else if ( m_position.y > maxY )     { m_position.y = maxY; }

    Animate();
    ForceSpriteUpdate();
}

void Character::MoveClipping( Direction direction )
{
    // This one ignores hitting walls, such as for projectiles.
    m_stateAction = MOVING;
    m_direction = direction;

    sf::IntRect desiredNewPosition;
    desiredNewPosition.left = m_position.x;
    desiredNewPosition.top = m_position.y;
    desiredNewPosition.width = m_textureCoordinates.width;
    desiredNewPosition.height = m_textureCoordinates.height;

    if          ( direction == WEST )
    {
        desiredNewPosition.left -= m_speed;
    }
    else if     ( direction == EAST )
    {
        desiredNewPosition.left += m_speed;
    }

    if          ( direction == NORTH )
    {
        desiredNewPosition.top -= m_speed;
    }
    else if     ( direction == SOUTH )
    {
        desiredNewPosition.top += m_speed;
    }

    m_position.x = desiredNewPosition.left;
    m_position.y = desiredNewPosition.top;

    Animate();
}

void Character::Animate()
{
    m_animationFrame += m_animationSpeed;
    if ( m_animationFrame >= m_maxFrames )
    {
        m_animationFrame = 0;

        if ( m_sheetAction == ATTACKING )
        {
            EndAttack();
        }
    }
}

const sf::Sprite& Character::GetSprite() const
{
    return m_sprite;
}

void Character::BeginAttack()
{
    if ( m_sheetAction == WALKING )
    {
        m_animationFrame = 0;
        // This relates to location/offset on sprite sheet
        m_sheetAction = ATTACKING;
        m_sheetActionTimer = 10;
        // This relates to info being passed to GameState to make game logic decisions
        m_stateAction = BEGIN_ATTACK;
    }
}

void Character::EndAttack()
{
    m_sheetAction = WALKING;
    m_sheetActionTimer = 0;
    m_stateAction = IDLE;
}

void Character::SetDirection( Direction direction )
{
    m_direction = direction;
}

Direction Character::GetDirection() const
{
    return m_direction;
}

void Character::SetAnimationInformation( int maxFrames, float animationSpeed )
{
    m_maxFrames = maxFrames;
    m_animationSpeed = animationSpeed;
}

CharacterType Character::GetCharacterType() const
{
    return m_characterType;
}

void Character::ForceSpriteUpdate()
{
    m_sprite.setPosition( m_position );
}



void Character::RestrictMovement()
{
    if ( m_restrictMovement )
    {
        if      ( m_position.x < m_validPositionRegion.left )
        {
            m_position.x = m_validPositionRegion.left;
        }
        else if ( m_position.x + (m_textureCoordinates.width * m_scale.x) > m_validPositionRegion.left + m_validPositionRegion.width )
        {
            m_position.x = m_validPositionRegion.left + m_validPositionRegion.width - (m_textureCoordinates.width * m_scale.x);
        }

        if      ( m_position.y < m_validPositionRegion.top )
        {
            m_position.y = m_validPositionRegion.top;
        }
        else if ( m_position.y + (m_textureCoordinates.height * m_scale.y) > m_validPositionRegion.top + m_validPositionRegion.height )
        {
            m_position.y = m_validPositionRegion.top + m_validPositionRegion.height - (m_textureCoordinates.height * m_scale.y);
        }
    }
}

void Character::SetRestrictMovement( bool value )
{
    m_restrictMovement = value;
}

void Character::SetValidPositionRegion( sf::IntRect rect )
{
    m_validPositionRegion = rect;
}

sf::IntRect Character::GetValidPositionRegion() const
{
    return m_validPositionRegion;
}


}
