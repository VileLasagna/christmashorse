#ifndef _SFML_HELPER_HPP
#define _SFML_HELPER_HPP

#include <string>
#include <SFML/Graphics.hpp>

class SFMLHelper
{
    public:
    // To String conversions (usually for logger)
    static std::string    CoordinateToString( sf::Vector2f coord );
    static std::string    CoordinateToString( sf::Vector2i coord );
    static std::string    CoordinateToString( sf::Vector2u coord );
    static std::string    CoordinateToString( sf::IntRect coord );
    static std::string    DimensionsToString( sf::IntRect coord );
    static std::string    DimensionsToString( sf::Vector2f coord );
    static std::string    RectangleToString( sf::IntRect rect );
    static std::string    ColorToString( sf::Color color );

    // Math stuff
    static bool BoundingBoxCollision( sf::IntRect a, sf::IntRect b );
    static bool PointInBoxCollision( sf::Vector2f point, sf::IntRect box );
    static bool PointInBoxCollision( sf::Vector2i point, sf::IntRect box );

    static float GetDistance( sf::IntRect a, sf::IntRect b, bool fromCenter );
    static float GetDistance( sf::Vector2f a, sf::Vector2f b );
    static float DotProduct( sf::Vector2f a, sf::Vector2f b );
    static float Length( sf::Vector2f vec );
    static float AngleBetweenTwoPointsRadians( sf::Vector2f a, sf::Vector2f b );
    static float AngleBetweenTwoPointsDegrees( sf::Vector2f a, sf::Vector2f b );

    // Gameplay stuff
    static sf::Vector2f RestrictArea( sf::Vector2f orig, int minX, int minY, int maxX, int maxY );

};


#endif
