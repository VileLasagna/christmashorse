#include "Types.hpp"

namespace chalo
{

InputAction StringToInputAction( std::string key )
{
    if      ( key == "INPUT_NORTH" )       { return INPUT_NORTH; }
    else if ( key == "INPUT_SOUTH" )       { return INPUT_SOUTH; }
    else if ( key == "INPUT_WEST" )        { return INPUT_WEST; }
    else if ( key == "INPUT_EAST" )        { return INPUT_EAST; }
    else if ( key == "INPUT_ACTION1" )     { return INPUT_ACTION1; }
    else if ( key == "INPUT_ACTION2" )     { return INPUT_ACTION2; }
    else if ( key == "INPUT_ACTION3" )     { return INPUT_ACTION3; }
    else if ( key == "INPUT_ACTION4" )     { return INPUT_ACTION4; }
    else if ( key == "INPUT_ACTION5" )     { return INPUT_ACTION5; }
    else if ( key == "INPUT_ACTION6" )     { return INPUT_ACTION6; }
    else                                   { return UNKNOWN; }
}

std::string InputActionToString( InputAction action )
{
    if      ( action == INPUT_NORTH )       { return "INPUT_NORTH"; }
    else if ( action == INPUT_SOUTH )       { return "INPUT_SOUTH"; }
    else if ( action == INPUT_WEST )        { return "INPUT_WEST"; }
    else if ( action == INPUT_EAST )        { return "INPUT_EAST"; }
    else if ( action == INPUT_ACTION1 )     { return "INPUT_ACTION1"; }
    else if ( action == INPUT_ACTION2 )     { return "INPUT_ACTION2"; }
    else if ( action == INPUT_ACTION3 )     { return "INPUT_ACTION3"; }
    else if ( action == INPUT_ACTION4 )     { return "INPUT_ACTION4"; }
    else if ( action == INPUT_ACTION5 )     { return "INPUT_ACTION5"; }
    else if ( action == INPUT_ACTION6 )     { return "INPUT_ACTION6"; }
    else                                    { return "IMPOSSIBLE"; }
}

sf::Keyboard::Key StringToKey( std::string key )
{
    if      ( key == "A"        ) { return sf::Keyboard::A;          }
    else if ( key == "B"        ) { return sf::Keyboard::B;          }
    else if ( key == "C"        ) { return sf::Keyboard::C;          }
    else if ( key == "D"        ) { return sf::Keyboard::D;          }
    else if ( key == "E"        ) { return sf::Keyboard::E;          }
    else if ( key == "F"        ) { return sf::Keyboard::F;          }
    else if ( key == "G"        ) { return sf::Keyboard::G;          }
    else if ( key == "H"        ) { return sf::Keyboard::H;          }
    else if ( key == "I"        ) { return sf::Keyboard::I;          }
    else if ( key == "J"        ) { return sf::Keyboard::J;          }
    else if ( key == "K"        ) { return sf::Keyboard::K;          }
    else if ( key == "L"        ) { return sf::Keyboard::L;          }
    else if ( key == "M"        ) { return sf::Keyboard::M;          }
    else if ( key == "N"        ) { return sf::Keyboard::N;          }
    else if ( key == "O"        ) { return sf::Keyboard::O;          }
    else if ( key == "P"        ) { return sf::Keyboard::P;          }
    else if ( key == "Q"        ) { return sf::Keyboard::Q;          }
    else if ( key == "R"        ) { return sf::Keyboard::R;          }
    else if ( key == "S"        ) { return sf::Keyboard::S;          }
    else if ( key == "T"        ) { return sf::Keyboard::T;          }
    else if ( key == "U"        ) { return sf::Keyboard::U;          }
    else if ( key == "V"        ) { return sf::Keyboard::V;          }
    else if ( key == "W"        ) { return sf::Keyboard::W;          }
    else if ( key == "X"        ) { return sf::Keyboard::X;          }
    else if ( key == "Y"        ) { return sf::Keyboard::Y;          }
    else if ( key == "Z"        ) { return sf::Keyboard::Z;          }
    else if ( key == "Up"       ) { return sf::Keyboard::Up;         }
    else if ( key == "Down"     ) { return sf::Keyboard::Down;       }
    else if ( key == "Left"     ) { return sf::Keyboard::Left;       }
    else if ( key == "Right"    ) { return sf::Keyboard::Right;      }
    else if ( key == "Enter"    ) { return sf::Keyboard::Enter;      }
    else if ( key == "Space"    ) { return sf::Keyboard::Space;      }
    else if ( key == "Backspace") { return sf::Keyboard::Backspace;  }
    else if ( key == "Num0"     ) { return sf::Keyboard::Num0;       }
    else if ( key == "Num1"     ) { return sf::Keyboard::Num1;       }
    else if ( key == "Num2"     ) { return sf::Keyboard::Num2;       }
    else if ( key == "Num3"     ) { return sf::Keyboard::Num3;       }
    else if ( key == "Num4"     ) { return sf::Keyboard::Num4;       }
    else if ( key == "Num5"     ) { return sf::Keyboard::Num5;       }
    else if ( key == "Num6"     ) { return sf::Keyboard::Num6;       }
    else if ( key == "Num7"     ) { return sf::Keyboard::Num7;       }
    else if ( key == "Num8"     ) { return sf::Keyboard::Num8;       }
    else if ( key == "Num9"     ) { return sf::Keyboard::Num9;       }
    else if ( key == "Numpad0"  ) { return sf::Keyboard::Numpad0;    }
    else if ( key == "Numpad1"  ) { return sf::Keyboard::Numpad1;    }
    else if ( key == "Numpad2"  ) { return sf::Keyboard::Numpad2;    }
    else if ( key == "Numpad3"  ) { return sf::Keyboard::Numpad3;    }
    else if ( key == "Numpad4"  ) { return sf::Keyboard::Numpad4;    }
    else if ( key == "Numpad5"  ) { return sf::Keyboard::Numpad5;    }
    else if ( key == "Numpad6"  ) { return sf::Keyboard::Numpad6;    }
    else if ( key == "Numpad7"  ) { return sf::Keyboard::Numpad7;    }
    else if ( key == "Numpad8"  ) { return sf::Keyboard::Numpad8;    }
    else if ( key == "Numpad9"  ) { return sf::Keyboard::Numpad9;    }
    else if ( key == "F1"       ) { return sf::Keyboard::F1;         }
    else if ( key == "F2"       ) { return sf::Keyboard::F2;         }
    else if ( key == "F3"       ) { return sf::Keyboard::F3;         }
    else if ( key == "F4"       ) { return sf::Keyboard::F4;         }
    else if ( key == "F5"       ) { return sf::Keyboard::F5;         }
    else if ( key == "F6"       ) { return sf::Keyboard::F6;         }
    else if ( key == "F7"       ) { return sf::Keyboard::F7;         }
    else if ( key == "F8"       ) { return sf::Keyboard::F8;         }
    else if ( key == "F9"       ) { return sf::Keyboard::F9;         }
    else if ( key == "F10"      ) { return sf::Keyboard::F10;        }
    else if ( key == "F11"      ) { return sf::Keyboard::F11;        }
    else if ( key == "F12"      ) { return sf::Keyboard::F12;        }
}

sf::Joystick::Axis StringToAxis( std::string name )
{
    if ( name == "AxisY" )  { return sf::Joystick::Axis::Y; }
    else                    { return sf::Joystick::Axis::X; }
}

}
