// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _UITEXTBOX_HPP
#define _UITEXTBOX_HPP

#include <string>

#include <SFML/Graphics.hpp>

#include "IWidget.hpp"
#include "UIShape.hpp"
#include "UILabel.hpp"

namespace chalo
{

class UITextBox : public IWidget
{
public:
    void Setup( const std::string& key, sf::Vector2f basePosition, sf::IntRect dimensions );
    void SetupBackground( sf::Color borderColor, int borderThickness, sf::Color fillColor, sf::Vector2f position, sf::Vector2f size );
    void SetupText( const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f offsetPosition, std::string defaultText = "" );

    UIRectangleShape& GetBackground();
    UIRectangleShape& GetCursor();
    UILabel& GetLabel();

    sf::IntRect GetPositionRect();

    void AddCharacter( const std::string& characters );
    void AddCharacter( char character );
    void EraseCharacter();

    void SetFocus( bool value );
    bool HasFocus();

    std::string GetEnteredText();

private:
    sf::Vector2f m_basePosition;
    sf::IntRect m_dimensions;

    sf::Color m_unfocusedColor;
    sf::Color m_focusedColor;

    UIRectangleShape m_background;
    UIRectangleShape m_cursor;
    UILabel m_label;
    std::string m_enteredText;
    bool m_hasFocus;
};

}

#endif
