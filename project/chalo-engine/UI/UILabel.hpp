// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _UILABEL_HPP
#define _UILABEL_HPP

#include <string>

#include <SFML/Graphics.hpp>

#include "IWidget.hpp"

namespace chalo
{

class UILabel : public IWidget
{
public:
    UILabel();
    virtual ~UILabel()
    {
        ;
    }

    void Setup( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, std::string text );
    void SetupW( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, std::wstring text );
    void SetPosition( const sf::Vector2f& pos );
    sf::Vector2f GetPosition();

    sf::Text& GetText();
    void SetText( const std::string& newText );
    void SetOriginalFont( const std::string& fontName );
    void ToggleDyslexiaFont();
    void SetFont( const std::string& fontName );


protected:
    void SetActualFont( const std::string& fontName );

    sf::Text m_text;
    std::string m_originalFont;
};

}

#endif

