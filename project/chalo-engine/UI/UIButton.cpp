// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "UIButton.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Helper.hpp"

namespace chalo
{

UIButton::UIButton() : IWidget()
{
//    m_background = nullptr;
//    m_icon = nullptr;
//    m_label = nullptr;
}

UIButton::~UIButton()
{
}


void UIButton::Setup( const std::string& key, sf::Vector2f basePosition, sf::IntRect dimensions )
{
    IWidget::Setup( key );
    m_basePosition = basePosition;
    m_dimensions = dimensions;
}

sf::IntRect UIButton::GetPositionRect()
{
    sf::IntRect adjustedPosition;
    adjustedPosition.left = m_basePosition.x + m_dimensions.left;
    adjustedPosition.top = m_basePosition.y + m_dimensions.top;
    adjustedPosition.width = m_dimensions.width;
    adjustedPosition.height = m_dimensions.height;
    return adjustedPosition;
}

void UIButton::SetupBackground( const sf::Texture& texture, sf::Vector2f offsetPosition )
{
    m_backgroundOffset = offsetPosition;
    offsetPosition.x += m_basePosition.x;
    offsetPosition.y += m_basePosition.y;

    sf::Vector2u textureSize = texture.getSize();

    m_background.Setup( m_id + "bg", texture, offsetPosition, m_dimensions );
}

void UIButton::SetupBackground( const sf::Texture& texture, sf::Vector2f offsetPosition, sf::IntRect frameRect )
{
    m_backgroundOffset = offsetPosition;
    offsetPosition.x += m_basePosition.x;
    offsetPosition.y += m_basePosition.y;

    sf::Vector2u textureSize = texture.getSize();

    if ( frameRect.left == -1 )
    {
        frameRect = m_dimensions;
    }

    m_frameRect = frameRect;

    m_background.Setup(
        m_id + "bg",        // const std::string& key
        texture,            // const sf::Texture& texture
        offsetPosition,     // // sf::Vector2f position
        //m_dimensions,
        m_frameRect           // sf::IntRect imageClip
    );
}

void UIButton::SetupIcon( const sf::Texture& texture, sf::Vector2f offsetPosition, sf::IntRect imageClip )
{
    m_iconOffset = offsetPosition;
    offsetPosition.x += m_basePosition.x;
    offsetPosition.y += m_basePosition.y;

    m_icon.Setup( m_id + "icon", texture, offsetPosition, imageClip );
}

void UIButton::SetupText( const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f offsetPosition, std::string text )
{
    if ( fontName == "" ) { return; }
    m_textOffset = offsetPosition;
    offsetPosition.x += m_basePosition.x;
    offsetPosition.y += m_basePosition.y;

    m_label.Setup( m_id + "label", fontName, characterSize, fillColor, offsetPosition, text );
}

void UIButton::SetupTextW( const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f offsetPosition, std::wstring text )
{
    if ( fontName == "" ) { return; }
    offsetPosition.x += m_basePosition.x;
    offsetPosition.y += m_basePosition.y;

    m_label.SetupW( m_id + "label", fontName, characterSize, fillColor, offsetPosition, text );
}

void UIButton::SetPosition( const sf::Vector2f& pos )
{
    m_basePosition = pos;
    m_background.SetPosition( sf::Vector2f( m_basePosition.x + m_backgroundOffset.x, m_basePosition.y + m_backgroundOffset.y ) );
    m_icon.SetPosition( sf::Vector2f( m_basePosition.x + m_iconOffset.x, m_basePosition.y + m_iconOffset.y ) );
    m_label.SetPosition( sf::Vector2f( m_basePosition.x + m_textOffset.x, m_basePosition.y + m_textOffset.y ) );
}

void UIButton::SetPosition( const sf::IntRect& pos )
{
    SetPosition( sf::Vector2f( pos.left, pos.top ) );
}

UIImage& UIButton::GetBackground()
{
    return m_background;
}

UIImage& UIButton::GetIcon()
{
    return m_icon;
}

UILabel& UIButton::GetLabel()
{
    return m_label;
}

sf::IntRect UIButton::GetIconImageClipRect()
{
    return m_icon.GetImageClipRect();
}

void UIButton::SetIconImageClipRect( sf::IntRect frameRect )
{
    m_icon.SetImageClipRect( frameRect );
}

sf::IntRect UIButton::GetFrameRect()
{
    return m_frameRect;
}

void UIButton::SetupBackgroundImageClipRect( sf::IntRect frameRect )
{
    m_background.SetImageClipRect( frameRect );
}

void UIButton::SetOriginalFont( const std::string& fontName )
{
    m_label.SetOriginalFont( fontName );
}

void UIButton::ToggleDyslexiaFont()
{
    m_label.ToggleDyslexiaFont();
}

}
