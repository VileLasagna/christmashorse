// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "UILabel.hpp"
#include "../Managers/ConfigManager.hpp"
#include "../Managers/FontManager.hpp"
#include "../Utilities/Logger.hpp"

namespace chalo
{

UILabel::UILabel() : IWidget()
{
}

void UILabel::Setup( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, std::string text )
{
    IWidget::Setup( key );

    SetFont( fontName );

    m_text.setString( text );
    m_text.setCharacterSize( characterSize );
    m_text.setFillColor( fillColor );
    m_text.setPosition( position );
}

void UILabel::SetupW( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, std::wstring text )
{
    IWidget::Setup( key );

    SetFont( fontName );

    m_text.setString( text );
    m_text.setCharacterSize( characterSize );
    m_text.setFillColor( fillColor );
    m_text.setPosition( position );
}

void UILabel::SetFont( const std::string& fontName )
{
//    Logger::Out( "Set font \"" + fontName + "\"", "UILabel::SetFont" );

    SetOriginalFont( fontName );

    if ( chalo::ConfigManager::Get( "USE_OPEN_DYSLEXIC" ) == "1" )
    {
        if ( m_originalFont.find( "-dyslexic" ) == std::string::npos )
        {
            SetActualFont( fontName + "-dyslexic" );
        }
    }
    else
    {
        SetActualFont( fontName );
    }
}

void UILabel::SetActualFont( const std::string& fontName )
{
//    Logger::Out( "Set actual font \"" + fontName + "\"", "UILabel::SetActualFont( consts" );

    sf::Font& font = chalo::FontManager::Get( fontName );

    m_text.setFont( chalo::FontManager::Get( fontName ) );
}

void UILabel::SetPosition( const sf::Vector2f& pos )
{
    m_text.setPosition( pos );
}

sf::Vector2f UILabel::GetPosition()
{
    return m_text.getPosition();
}

sf::Text& UILabel::GetText()
{
    return m_text;
}

void UILabel::SetText( const std::string& newText )
{
    m_text.setString( newText );
}

void UILabel::SetOriginalFont( const std::string& fontName )
{
//    Logger::Out( "Set original font to \"" + fontName + "\"", "UILabel::SetOriginalFont" );
    m_originalFont = fontName;
}

void UILabel::ToggleDyslexiaFont()
{
    Logger::OutFuncBegin( "label text: \"" + m_text.getString() + "\", original font: \"" + m_originalFont + "\"", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );

    if ( m_originalFont == "" )
    {
        m_originalFont = "main";
    }

    if ( chalo::ConfigManager::Get( "USE_OPEN_DYSLEXIC" ) == "1" )
    {
        if ( m_originalFont.find( "-dyslexic" ) == std::string::npos )
        {
            SetActualFont( m_originalFont + "-dyslexic" );
        }
    }
    else
    {
        SetActualFont( m_originalFont );
    }
    Logger::OutFuncEnd( "normal", std::string( typeid( *this ).name() ) + "::" + std::string( __func__ ) );
}

}
