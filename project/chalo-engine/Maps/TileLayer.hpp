//// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine
//
//#ifndef _TILE_LAYER_HPP
//#define _TILE_LAYER_HPP
//
//#include <string>
//#include <vector>
//#include <list>
//
//#include "../Enums/Types.hpp"
//#include "Tile.hpp"
//#include "Shadow.hpp"
//
//namespace chalo
//{
//
//namespace Map
//{
//
//// TODO: I was making this a template, but it made loading
//// the maps harder, but maybe if I had more time I could
//// fix this up better.
//
//class ILayer
//{
//public:
//    ILayer();
//    ILayer( const std::string& name );
//
//    virtual const std::string& GetName() const;
//    virtual void SetName( const std::string& name );
//
//    virtual bool GetIsHidden() const;
//    virtual void SetIsHidden( bool value );
//
//    virtual LayerType GetType() const;
//    virtual void SetType( LayerType type );
//
//    virtual void Reset() = 0;
//    virtual int GetTileCount() = 0;
//    virtual std::string GetLogString() = 0;
//
//protected:
//    std::string m_name;
//    LayerType m_type;
//    bool m_isHidden;
//};
//
////! A layer that includes an array of tiles
//class TileLayer : public ILayer
//{
//protected:
//    std::vector<Tile> m_tiles;
//
//public:
//    TileLayer();
//    TileLayer( const std::string& name );
//
//    virtual void Reset();
//    virtual int GetTileCount();
//
//    void AddTile( Tile tile );
//    void UndoTile();
//    const std::vector<Tile>& GetTiles() const;
//    std::vector<Tile>& GetTilesUpdatable();
//    std::vector<Tile>* GetTilesUpdatablePtr();
//    void SetTileTexture( int index, sf::IntRect textureRegion );
//    void ClearTiles();
//    void RemoveTiles( std::list<int>& tileIndices );
//
//    virtual std::string GetLogString();
//};
//
//class ShadowLayer : public ILayer
//{
//protected:
//    std::vector<Shadow> m_tiles;
//
//public:
//    ShadowLayer();
//    ShadowLayer( const std::string& name );
//
//    virtual void Reset();
//    virtual int GetTileCount();
//
//    void AddTile( Shadow tile );
//    void UndoTile();
//    const std::vector<Shadow>& GetTiles() const;
//    std::vector<Shadow>& GetTilesUpdatable();
//    std::vector<Shadow>* GetTilesUpdatablePtr();
//    void SetTileTexture( int index, sf::IntRect textureRegion );
//    void ClearTiles();
//    void RemoveTiles( std::list<int>& tileIndices );
//
//    virtual std::string GetLogString();
//};
//
//}
//
//}
//
//#endif
