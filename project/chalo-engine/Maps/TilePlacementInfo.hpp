// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _TILE_PLACEMENT_INFO_HPP
#define _TILE_PLACEMENT_INFO_HPP

#include "../Enums/Types.hpp"
#include "../Utilities_SFML/SFMLHelper.hpp"

namespace chalo
{

namespace Map
{

struct TilePlacementInfo
{
    TilePlacementInfo()
    {
        ;
    }
    TilePlacementInfo( int actionIndex, int lastLayerUpdated, LayerType layerType, int tileIndex, bool newTile, sf::IntRect previousTextureRegion = sf::IntRect() )
    {
        this->actionIndex = actionIndex;
        this->lastLayerUpdated = lastLayerUpdated;
        this->layerType = layerType;
        this->tileIndex = tileIndex;
        this->newTile = newTile;
        this->previousTextureRegion = previousTextureRegion;
    }

    std::string GetLogString()
    {
        std::string logInfo =
            " Last layer updated: " + Helper::ToString( lastLayerUpdated ) +
            "... Layer type: " + Helper::ToString( layerType ) +
            "... Tile index: " + Helper::ToString( tileIndex ) +
            "... New tile?:  " + Helper::ToString( newTile ) +
            "... Previous texture region: " + SFMLHelper::CoordinateToString( previousTextureRegion ) +
            "... Action index: " + Helper::ToString( actionIndex );
        return logInfo;
    }

    int lastLayerUpdated;
    LayerType layerType;
    int tileIndex;
    bool newTile;
    sf::IntRect previousTextureRegion;
    int actionIndex;
};

}

}

#endif
