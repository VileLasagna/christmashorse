#ifndef _BRUSH_HPP
#define _BRUSH_HPP

#include <SFML/Graphics.hpp>

#include <string>

#include "../Enums/Types.hpp"
#include "../Utilities/Messager.hpp"
#include "../Utilities/Helper.hpp"

struct Brush
{
    Brush()
    {
        layer = 0;
        dimensions.x = 1;
        dimensions.y = 1;
        layerType = chalo::BELOWTILELAYER;
        imageClip.left = 0;
        imageClip.top = 0;
        imageClip.width = Helper::StringToInt( chalo::Messager::Get( "tilesize" ) );;
        imageClip.height = Helper::StringToInt( chalo::Messager::Get( "tilesize" ) );;
    }

    sf::Sprite sprite;
    sf::IntRect imageClip;
    int layer;
    sf::Vector2u dimensions;
    chalo::LayerType layerType;

    void SetTexture( sf::Texture& texture )
    {
        sprite.setTexture( texture );
    }

    std::string LayerTypeString()
    {
        if ( layerType == chalo::UNDEFINEDLAYER )
        {
            return "Don't Change";
        }
        else if ( layerType == chalo::BELOWTILELAYER )
        {
            return "Below Tiles";
        }
        else if ( layerType == chalo::ABOVETILELAYER )
        {
            return "Above Tiles";
        }
        else if ( layerType == chalo::WARPLAYER )
        {
            return "Warp";
        }
        else if ( layerType == chalo::PLACEMENTLAYER )
        {
            return "Placement";
        }
        else if ( layerType == chalo::COLLISIONLAYER )
        {
            return "Collision";
        }
        else if ( layerType == chalo::SHADOWLAYER )
        {
            return "Shadow";
        }

        return "???";
    }

    void SetType( chalo::LayerType type )
    {
        layerType = type;
    }

    void SetLayer( int value )
    {
        layer = value;
    }

    void DecrementLayer()
    {
        layer -= 1;
        if ( layer < 0 )
        {
            layer = 0;
        }
    }

    void IncrementLayer()
    {
        layer += 1;
    }

    void SetWidth( int value )
    {
        dimensions.x = value;
    }

    void SetHeight( int value )
    {
        dimensions.y = value;
    }

    void NextWidth()
    {
        dimensions.x += 2;
        if ( dimensions.x > 5 )
            dimensions.x = 1;
    }

    void NextHeight()
    {
        dimensions.y += 2;
        if ( dimensions.y > 5 )
            dimensions.y = 1;
    }
};

#endif
