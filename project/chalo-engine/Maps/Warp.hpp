// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _WARP_HPP
#define _WARP_HPP

#include <string>
#include <SFML/Graphics.hpp>

namespace chalo
{

namespace Map
{

//! Map information on what regions warp the player to another map
class Warp
{
public:
    void Setup( const std::string& warpToMap, sf::Vector2f warpToPosition, sf::IntRect warpRectangle );
    void DrawDebug();
    sf::IntRect GetRegion() const;
    std::string GetWarpToMap() const;
    sf::Vector2f GetWarpToPosition() const;

private:
    std::string m_warpToMap;
    sf::Vector2f m_warpToPosition;
    sf::IntRect m_warpRectangle;
};

}

}

#endif
