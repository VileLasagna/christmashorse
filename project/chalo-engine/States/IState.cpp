// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "IState.hpp"

namespace chalo
{

void IState::Init( const std::string& name )
{
    m_name = name;
    m_gotoState = "";
}

void IState::Setup()
{
    m_gotoState = "";
}

std::string IState::GetName()
{
    return m_name;
}

std::string IState::GetGotoState()
{
    return m_gotoState;
}

void IState::SetGotoState( const std::string& stateName )
{
    m_gotoState = stateName;
}

}
