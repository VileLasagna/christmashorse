#include "ErrorState.hpp"

#include "../Application/Application.hpp"
#include "../Managers/MenuManager.hpp"
#include "../Managers/InputManager.hpp"
#include "../Managers/ConfigManager.hpp"
#include "../Utilities/Messager.hpp"

namespace chalo
{

ErrorState::ErrorState()
{
}

void ErrorState::Init( const std::string& name )
{
    Logger::Out( "Parameters - name: " + name, "ErrorState::Init", "function-trace" );
}

void ErrorState::Setup()
{
    Logger::Out( "", "ErrorState::Setup", "function-trace" );
    IState::Setup();

//    chalo::TextureManager::Add( "bg",                      "../Content/Graphics/UI/menubg.png" );
//    chalo::TextureManager::Add( "button-long",             "../Content/Graphics/UI/button-long.png" );

    MenuManager::AddLabel( "main",
        "ohno",
        sf::Color::White,
        "main",
        15,
        sf::Vector2f( 10, 10 ),
        "An error happened!",
        true, true );
    MenuManager::AddLabel( "main",
        "error",
        sf::Color::White,
        "main",
        15,
        sf::Vector2f( 10, 30 ),
        Messager::Get( "error" ),
        true, true );
    MenuManager::AddLabel( "main",
        "error",
        sf::Color::White,
        "main",
        15,
        sf::Vector2f( 10, 50 ),
        "PRESS ESC TO RESET",
        true, true );


//    MenuManager::AddButton( "main",
//        "btnBack",
//        sf::Vector2f( 10, Application::GetScreenHeight() - 100 ),
//        sf::IntRect( 0, 0, 200, 50 ),
//        "button-long",
//        sf::Vector2f( 0, 0 ),
//        sf::IntRect( 0, 0, 200, 50 ),
//        "main",
//        20,
//        sf::Color::Black,
//        sf::Vector2f( 5, 5 ),
//        "Back to start",
//        true, "", 0, 0, 0, 0, 0, 0, true
//        );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );

    chalo::InputManager::Setup();
}

void ErrorState::Cleanup()
{
    Logger::Out( "", "ErrorState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
}

void ErrorState::Update()
{
    std::string clickedButton = chalo::MenuManager::GetClickedButton();

    // Demo select menu
    if ( clickedButton == "btnBack" )
    {
        SetGotoState( "startupstate" );
    }
}

void ErrorState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::AddMenu();
}

}

