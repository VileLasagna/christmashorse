#ifndef _ANIMATION_TABLE
#define _ANIMATION_TABLE

#include <SFML/Graphics.hpp>
#include <map>
#include <string>

namespace chalo
{

struct AnimationProperties
{
    int row;
    bool repeats;
    bool interruptable;
};

/**
Rows represent actions, columns represent frames of animation
*/
class AnimatedSprite
{
    public:
    AnimatedSprite();

    void Setup( const sf::Texture& texture, int maxFrames, float animationSpeed, int width, int height );

    void CreateAction( std::string name, int row, bool repeats, bool interruptable );

    void SetAction( std::string action );
    std::string GetAction();
    void IncrementFrame();
    sf::Sprite& GetClippedSprite();
    void SetPosition( const sf::Vector2f& position );
    const sf::Vector2f& GetPosition();
    void SetScale( const sf::Vector2f& scale );
    const sf::Vector2f& GetScale();
    float GetCurrentFrame();

    private:
    std::map<std::string, AnimationProperties> m_actionRows;
    std::string m_currentActionName;
    std::string m_defaultActionName;
    int m_maxFrames;
    float m_currentFrame;
    float m_animationSpeed;
    sf::IntRect m_frameRect;
    sf::Sprite m_sprite;
};

}

#endif
