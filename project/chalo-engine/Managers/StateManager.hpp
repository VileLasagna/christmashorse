// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _STATE_MANAGER
#define _STATE_MANAGER

#include <map>
#include "../States/IState.hpp"

namespace chalo
{

class StateManager
{
public:
    static std::string CLASSNAME;

    void InitManager();
    void Cleanup();

    void AddState( const std::string& stateName, IState* state );
    void ClearState( const std::string& stateName );
    void ChangeState( const std::string& stateName );
    void SetupState();
    void UpdateState();
    void DrawState( sf::RenderWindow& window );
    std::string GetGotoState();

private:
    std::map< std::string, IState* > m_states;  // TODO: Update to new ptr
    IState* m_ptrCurrentState;                  // TODO: Update to new ptr
    std::string m_nameCurrentState;
};

}

#endif
