// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _INPUT_MANAGER_HPP
#define _INPUT_MANAGER_HPP

#include <SFML/Graphics.hpp>

#include <string>
#include <map>

#include "../Enums/Types.hpp"
#include "../Utilities/CsvParser.hpp"

namespace chalo
{

struct JoystickButtonBinding
{
    JoystickButtonBinding() { }
    JoystickButtonBinding( int index, int button )
    {
        this->index = index;
        this->button = button;
    }

    int index;
    int button;
};

struct JoystickAxisBinding
{
    JoystickAxisBinding() { }
    JoystickAxisBinding( int index, sf::Joystick::Axis axis, int value )
    {
        this->index = index;
        this->axis = axis;
        this->value = value;
    }

    int index;
    sf::Joystick::Axis axis;
    int value;
};

struct InputBinding
{
    InputBinding() {}

    InputBinding( PlayerInputAction action, std::vector<sf::Keyboard::Key> keys, std::vector<JoystickButtonBinding> buttons, std::vector<JoystickAxisBinding> axises )
    {
        this->action = action;
        this->keys = keys;
        this->buttons = buttons;
        this->axises = axises;
    }

    PlayerInputAction action;
    std::vector<sf::Keyboard::Key> keys;
    std::vector<JoystickButtonBinding> buttons;
    std::vector<JoystickAxisBinding> axises;
};

class InputManager
{
public:
    static std::string CLASSNAME;

    static void Setup( std::string keybindingFileName = "chalo_keybindings.csv" );
    static void SetupKeybindings( const rach::CsvDocument& keybindingList );
    static void Update();

    static void SetKeybinding( InputBinding binding );
    static void SetKeybindings( std::vector<InputBinding> bindings );
    static bool IsActionActive( PlayerInputAction action );

    static bool IsJoystickAvailable( int index );

    // Inputs with cooldowns:
    static std::string GetTypedLetter();

    static bool IsKeyPressed( sf::Keyboard::Key keyType );
    static bool IsKeyPressedSmooth( sf::Keyboard::Key keyType );
    static bool IsJoystickButtonPressed( int index, int button );
    static bool IsJoystickDpadPressed( int index, Direction dir );
    static bool IsJoystickDpadPressed( int index, sf::Joystick::Axis axis, int value );

    // Mouse
    static void ToggleCursor( bool value );
    static void PlaceMouse( sf::Vector2i pos );
    static bool IsLeftClick();
    static bool IsLeftClickRelease();
    static void DisableClick();
    static sf::Vector2i GetMousePosition();

private:
    static int m_keyboardCooldown;
    static int m_mouseClickCooldown;
    static bool m_mouseLeftPressed;
    static int m_cooldownMax;
    static int m_mouseCooldownMax;
    static int m_gamepadCooldown[4];
    static int m_gamepadCooldownMax[4];
    static bool m_joystickEnabled[4];
    static std::map<std::string, InputBinding> m_bindings;

    static void DisableKeyboard();
    static void DisableMouse();

    static bool IsKeyboardEnabled();
    static bool IsMouseEnabled();
    static bool IsJoystickEnabled( int index );
};

}

#endif
