// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _FONT_MANAGER
#define _FONT_MANAGER

#include <SFML/Graphics.hpp>

#include <map>
#include <string>

namespace chalo
{

class FontManager
{
public:
    static std::string CLASSNAME;

    static void Setup( std::string assetListFilename = "fonts.chaloassets" );
    static void Load( std::ifstream& input );
    static void Add( const std::string& key, const std::string& path );
    static void Clear();
    static sf::Font& Get( const std::string& key );

private:
    static std::map<std::string, sf::Font> m_assets;
};

}

#endif
