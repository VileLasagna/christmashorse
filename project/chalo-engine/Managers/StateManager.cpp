// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "StateManager.hpp"
#include "InputManager.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/Messager.hpp"
#include "../States/ErrorState.hpp"

namespace chalo
{
std::string StateManager::CLASSNAME = "StateManager";

void StateManager::InitManager()
{
    m_ptrCurrentState = nullptr;
    m_nameCurrentState = "";
    AddState( "error", new ErrorState );
}

void StateManager::Cleanup()
{
    // std::map< std::string, IState* > m_states;
    for ( auto& state : m_states )
    {
        if ( state.second != nullptr )
        {
            delete state.second;
            state.second = nullptr;
        }
    }
}

void StateManager::AddState( const std::string& stateName, IState* state )
{
    Logger::OutFuncBegin( "Add state \"" + stateName + "\"", CLASSNAME + "::" + std::string( __func__ ) );
    state->Init( stateName );
    m_states[stateName] = state;
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void StateManager::ClearState( const std::string& stateName )
{
    Logger::OutFuncBegin( "Clear state \"" + stateName + "\"", CLASSNAME + "::" + std::string( __func__ ) );
    if ( m_states.find( stateName ) == m_states.end() )
    {
        Logger::Error( "Could not find state with name " + stateName + "!", CLASSNAME + "::" + std::string( __func__ ) );
        Logger::OutFuncEnd( "error", CLASSNAME + "::" + std::string( __func__ ) );
        return;
    }

    if ( m_states[stateName] != nullptr )
    {
        delete m_states[stateName];
        m_states[stateName] = nullptr;
    }
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void StateManager::ChangeState( const std::string& stateName )
{
    Logger::OutFuncBegin( "Change state to \"" + stateName + "\"", CLASSNAME + "::" + std::string( __func__ ) );

    // Disable the click for now.
    chalo::InputManager::DisableClick();

    if ( m_ptrCurrentState != nullptr )
    {
        m_ptrCurrentState->Cleanup();
        ClearState( m_nameCurrentState );

    }

    if ( m_states.find( stateName ) == m_states.end() )
    {
        // State not found!
        Logger::Error( "FATAL ERROR! State key \"" + stateName + "\" not found in list of states!", "StateManager::ChangeState" );
        Messager::Set( "error", "FATAL ERROR! State key \"" + stateName + "\" not found in list of states! / StateManager::ChangeState" );

        if ( stateName == "error" )
        {
            // Let's not recurse a bunch of times.
            Logger::Error( "Error! Can't find the \"error\" state!", "StateManager::ChangeState" );
            Logger::Error( "Available state keys:", "StateManager::ChangeState" );
            // std::map< std::string, IState* > m_states;
            for ( auto& state : m_states )
            {
                Logger::Error( "\"" + state.first + "\"", "StateManager::ChangeState" );
            }
        }
        else
        {
            ChangeState( "error" );
        }

        Logger::OutFuncEnd( "error", CLASSNAME + "::" + std::string( __func__ ) );
        return;
    }

    m_nameCurrentState = stateName;
    m_ptrCurrentState = m_states[ m_nameCurrentState ];

    if ( m_ptrCurrentState == nullptr )
    {
        Logger::Error( "Error! State was changed to nullptr when switching to \"" + stateName + "\"!", "StateManager::ChangeState" );
        Logger::OutFuncEnd( "error", CLASSNAME + "::" + std::string( __func__ ) );
        return;
    }

    m_ptrCurrentState->Setup();

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void StateManager::SetupState()
{
    m_ptrCurrentState->Setup();
}

void StateManager::UpdateState()
{
    m_ptrCurrentState->Update();
}

void StateManager::DrawState( sf::RenderWindow& window )
{
    m_ptrCurrentState->Draw( window );
}

std::string StateManager::GetGotoState()
{
    return m_ptrCurrentState->GetGotoState();
}

}
