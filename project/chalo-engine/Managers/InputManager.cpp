// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "InputManager.hpp"
#include "../Application/Application.hpp"
#include "../Utilities/Helper.hpp"
#include "../Utilities/Logger.hpp"
#include "../Data/ConfigLoader.hpp"
#include "ConfigManager.hpp"

namespace chalo
{

std::string InputManager::CLASSNAME = "InputManager";
int  InputManager::m_keyboardCooldown;
int  InputManager::m_mouseClickCooldown;
int  InputManager::m_cooldownMax;
int  InputManager::m_mouseCooldownMax;
bool InputManager::m_joystickEnabled[4];
int InputManager::m_gamepadCooldown[4];     // TODO: Actually implement this
int InputManager::m_gamepadCooldownMax[4];
std::map<std::string, InputBinding> InputManager::m_bindings;
bool InputManager::m_mouseLeftPressed;

#define CLASSNAME std::string( "Application" )



void InputManager::Setup( std::string keybindingFileName /*= "chalo_keybindings.csv"*/ )
{
    Logger::OutFuncBegin( "keybindingFileName=" + keybindingFileName, CLASSNAME + "::" + std::string( __func__ ) );

    m_keyboardCooldown = 0;
    m_mouseClickCooldown = 0;
    m_cooldownMax = 7;
    m_mouseCooldownMax = 10;
    m_mouseLeftPressed = false;

    for ( int i = 0; i < 4; i++ )
    {
        m_gamepadCooldown[i] = 0;
        m_gamepadCooldownMax[i] = 7;
        if ( sf::Joystick::isConnected( i ) && chalo::ConfigManager::GetInt( "PREVENT_JOYSTICK" ) == 0 )
        {
            m_joystickEnabled[i] = true;
            Logger::Out( "Joystick " + Helper::ToString( i ) + " enabled", "InputManager::Setup" );
        }
        else
        {
            m_joystickEnabled[i] = false;
        }
    }

    rach::CsvDocument keybindings = rach::CsvParser::Parse( keybindingFileName );
    SetupKeybindings( keybindings );

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void InputManager::SetupKeybindings( const rach::CsvDocument& keybindingList )
{
    std::string logInfo = "KEYBINDINGS<br><ol>";
    for ( auto& row : keybindingList.rows )
    {
        InputAction action;
        int player;
        sf::Keyboard::Key key;
        bool joystickAxis = false;
        sf::Joystick::Axis axis;
        int amount;
        int button;

        logInfo += "<li>ROW <ul>";
        // Read and convert all the data in the row
        for ( size_t i = 0; i < row.size(); i++ )
        {
            logInfo += "<li>" + keybindingList.header[i] + "=" + row[i] + "</li>";

            if      ( keybindingList.header[i] == "ACTION" )    { action = StringToInputAction( row[i] ); }
            else if ( keybindingList.header[i] == "PLAYER" )    { player = Helper::StringToInt( row[i] ); }
            else if ( keybindingList.header[i] == "KEYBOARD" )  { key = StringToKey( row[i] ); }
            else if ( keybindingList.header[i] == "JOYSTICK" )
            {
                // Button
                if ( row[i].find( "=" ) == std::string::npos )
                {
                    joystickAxis = false;
                    button = Helper::StringToInt( row[i] );
                }
                // Axis
                else
                {
                    joystickAxis = true;
                    std::vector<std::string> axisAndValue = Helper::Split( row[i], "=" );
                    axis = StringToAxis( axisAndValue[0] );
                    amount = Helper::StringToInt( axisAndValue[1] );
                }
            }
        }

        // Create mapping
        if ( joystickAxis )
        {
            SetKeybinding( InputBinding(
                PlayerInputAction( player, action ),
                { key },
                {},
                { JoystickAxisBinding( player, axis, amount ) }
            ) );
        }
        else
        {
            SetKeybinding( InputBinding(
                PlayerInputAction( player, action ),
                { key },
                { JoystickButtonBinding( player, button ) },
                {}
            ) );
        }
        logInfo += "</ul></li>";
    }
    logInfo += "<ol>";
    Logger::Out( logInfo, "InputManager::SetupKeybindings" );
}

void InputManager::SetKeybinding( InputBinding binding )
{
    m_bindings[ binding.action.name ] = binding;
}

void InputManager::SetKeybindings( std::vector<InputBinding> bindings )
{
    for ( auto& binding : bindings )
    {
        SetKeybinding( binding );
    }
}

bool InputManager::IsActionActive( PlayerInputAction action )
{
    // This action has been bound
    if ( m_bindings.find( action.name ) != m_bindings.end() )
    {
        // Are any of the keys being pressed?
        for ( auto& key : m_bindings[ action.name ].keys )
        {
            if ( IsKeyPressedSmooth( key ) )
            {
                return true;
            }
        }

        // Joystick buttons
        if ( m_joystickEnabled[ action.index ] )
        {
            for ( auto& button : m_bindings[ action.name ].buttons )
            {
                if ( IsJoystickButtonPressed( button.index, button.button ) )
                {
                    return true;
                }
            }

            // Joystick axes
            for ( auto& axis : m_bindings[ action.name ].axises )
            {
                if ( IsJoystickDpadPressed( axis.index, axis.axis, axis.value ) )
                {
                    return true;
                }
            }
        }
    }
    return false;
}

bool InputManager::IsJoystickAvailable( int index )
{
    return IsJoystickEnabled( index );
}

void InputManager::Update()
{
    if ( m_keyboardCooldown > 0 )
    {
        m_keyboardCooldown -= 1;
    }

    if ( m_mouseClickCooldown > 0 )
    {
        m_mouseClickCooldown -= 1;
    }

    // Check for mouse button activity
    IsLeftClick();
}

void InputManager::DisableKeyboard()
{
    m_keyboardCooldown = m_cooldownMax;
}

void InputManager::DisableMouse()
{
    m_keyboardCooldown = m_cooldownMax;
}


bool InputManager::IsKeyboardEnabled()
{
    return ( m_keyboardCooldown <= 0 );
}

bool InputManager::IsMouseEnabled()
{
    return ( m_mouseClickCooldown <= 0 );
}

bool InputManager::IsJoystickEnabled( int index )
{
    return ( index >= 0 && index < 4 && m_joystickEnabled[index] );
}

bool InputManager::IsKeyPressed( sf::Keyboard::Key keyType )
{
    if ( IsKeyboardEnabled() && sf::Keyboard::isKeyPressed( keyType ) )
    {
        DisableKeyboard();
        return true;
    }

    return false;
}

bool InputManager::IsKeyPressedSmooth( sf::Keyboard::Key keyType )
{
    return ( IsKeyboardEnabled() && sf::Keyboard::isKeyPressed( keyType ) );
}

/**
Knockoff SNES controller: "X" = 0, "A" = 1, "B" = 2, "Y" = 3,
"START" = 9, "SELECT" = 8, "LEFT" = 4, "RIGHT" = 5;
Gamepad axis has -100 or 100 (no analog stick).
*/
bool InputManager::IsJoystickButtonPressed( int index, int button )
{
    return ( index >= 0 && index < 4 && sf::Joystick::isButtonPressed( index, button ) );
}

bool InputManager::IsJoystickDpadPressed( int index, sf::Joystick::Axis axis, int value )
{
    if ( index < 0 || index >= 4 ) { return false; }

    return ( sf::Joystick::getAxisPosition( index, axis ) == value );
}

bool InputManager::IsJoystickDpadPressed( int index, Direction dir )
{
    if ( index < 0 || index >= 4 ) { return false; }

    if ( dir == NORTH )
    {
        return ( sf::Joystick::getAxisPosition( index, sf::Joystick::Y ) == -100 );
    }
    else if ( dir == SOUTH )
    {
        return ( sf::Joystick::getAxisPosition( index, sf::Joystick::Y ) == 100 );
    }
    else if ( dir == WEST )
    {
        return ( sf::Joystick::getAxisPosition( index, sf::Joystick::X ) == -100 );
    }
    else if ( dir == EAST )
    {
        return ( sf::Joystick::getAxisPosition( index, sf::Joystick::X ) == 100 );
    }

    return false;
}

std::string InputManager::GetTypedLetter()
{
    if ( !IsKeyboardEnabled() )
    {
        return "";
    }

    // Kludgey method for now
    std::string typed;

    if      ( sf::Keyboard::isKeyPressed( sf::Keyboard::A ) )           { typed =  "a"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::B ) )           { typed =  "b"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::C ) )           { typed =  "c"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::D ) )           { typed =  "d"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::E ) )           { typed =  "e"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::F ) )           { typed =  "f"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::G ) )           { typed =  "g"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::H ) )           { typed =  "h"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::I ) )           { typed =  "i"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::J ) )           { typed =  "j"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::K ) )           { typed =  "k"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::L ) )           { typed =  "l"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::M ) )           { typed =  "m"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::N ) )           { typed =  "n"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::O ) )           { typed =  "o"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::P ) )           { typed =  "p"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Q ) )           { typed =  "q"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::R ) )           { typed =  "r"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::S ) )           { typed =  "s"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::T ) )           { typed =  "t"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::U ) )           { typed =  "u"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::V ) )           { typed =  "v"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::W ) )           { typed =  "w"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::X ) )           { typed =  "x"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Y ) )           { typed =  "y"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Z ) )           { typed =  "z"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num0 ) )        { typed =  "0"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num1 ) )        { typed =  "1"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num2 ) )        { typed =  "2"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num3 ) )        { typed =  "3"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num4 ) )        { typed =  "4"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num5 ) )        { typed =  "5"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num6 ) )        { typed =  "6"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num7 ) )        { typed =  "7"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num8 ) )        { typed =  "8"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Num9 ) )        { typed =  "9"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad0 ) )     { typed =  "0"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad1 ) )     { typed =  "1"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad2 ) )     { typed =  "2"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad3 ) )     { typed =  "3"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad4 ) )     { typed =  "4"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad5 ) )     { typed =  "5"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad6 ) )     { typed =  "6"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad7 ) )     { typed =  "7"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad8 ) )     { typed =  "8"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Numpad9 ) )     { typed =  "9"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Subtract ) )    { typed =  "-"; }
    else if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Dash ) )        { typed =  "-"; }

    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::LShift ) || sf::Keyboard::isKeyPressed( sf::Keyboard::RShift ) )
    {
        typed = Helper::ToUpper( typed );
    }

    if ( typed != "" )
    {
        DisableKeyboard();
    }

    return typed;
}

void InputManager::ToggleCursor( bool value )
{
  Application::GetWindow().setMouseCursorVisible( value );
}

void InputManager::PlaceMouse( sf::Vector2i pos )
{
  sf::Mouse::setPosition( pos, Application::GetWindow() );
}

bool InputManager::IsLeftClick()
{
    if ( m_mouseClickCooldown <= 0 && sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
    {
        m_mouseLeftPressed = true;
        return true;
    }
    return false;
}

bool InputManager::IsLeftClickRelease()
{
    if ( m_mouseLeftPressed && !sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
    {
        m_mouseLeftPressed = false;
        return true;
    }
    return false;
}

void InputManager::DisableClick()
{
    m_mouseClickCooldown = m_mouseCooldownMax;
}

sf::Vector2i InputManager::GetMousePosition()
{
    sf::Vector2i localPosition = sf::Mouse::getPosition( Application::GetWindow() );
    return localPosition;
}

}
