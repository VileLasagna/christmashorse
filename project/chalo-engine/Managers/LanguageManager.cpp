#include "LanguageManager.hpp"

#include <string>
#include <fstream>

#include "../Utilities/Helper.hpp"
#include "../Utilities/Logger.hpp"
#include "../Utilities/CsvParser.hpp"
#include "ConfigManager.hpp"

namespace chalo
{
std::string LanguageManager::CLASSNAME = "LanguageManager";

void LanguageSet::Add( const std::string& key, const std::string& value )
{
    m_textMap[ key ] = value;
}

std::string LanguageSet::Text( const std::string& textKey )
{
//    Logger::Out( "Get text with textKey " + textKey, "LanguageSet::Text" );
    if ( m_textMap.find( textKey ) == m_textMap.end() )
    {
        Logger::Error( "Could not find text string with key \"" + textKey + "\"!", "LanguageSet::Text" );
        return "ERROR";
    }

    return m_textMap[ textKey ];
}

void LanguageSet::SetSuggestedFont( const std::string& font )
{
    m_suggestedFont = font;
}

std::string LanguageSet::GetSuggestedFont()
{
    return m_suggestedFont;
}


std::string LanguageManager::s_basePath;
std::string LanguageManager::s_currentLanguage;
std::map< std::string, LanguageSet > LanguageManager::s_languageSets;

void LanguageManager::SetLanguageBasePath( const std::string& path )
{
    s_basePath = path;
}

void LanguageManager::AddLanguage( const std::string& languageKey, const std::string& suggestedFont )
{
    Logger::OutFuncBegin( "languageKey=" + languageKey + ", suggestedFont=" + suggestedFont, CLASSNAME + "::" + std::string( __func__ ) );
    if ( languageKey == "" ) { return; }
    Logger::Out( "Add language \"" + languageKey + "\"", "LanguageManager::AddLanguage", "function-trace" );
    std::string path = s_basePath + languageKey + ".csv";
    std::string space;

    s_languageSets[ languageKey ].SetSuggestedFont( suggestedFont );

    rach::CsvDocument data;
    try
    {
        data = rach::CsvParser::Parse( path );
    }
    catch( const runtime_error& ex )
    {
        Logger::Error( "Unable to open language file \"" + path + "\"", CLASSNAME + "::" + std::string( __func__ ) );
        Logger::OutFuncEnd( "error", CLASSNAME + "::" + std::string( __func__ ) );
        return;
    }

    std::string key, text;
    for ( auto& row : data.rows )
    {
        for ( size_t i = 0; i < row.size(); i++ )
        {
            if      ( data.header[i] == "KEY" )     { key = row[i]; }
            else if ( data.header[i] == "VALUE" )   { text = row[i]; }
        }

        s_languageSets[ languageKey ].Add( key, text );
    }

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void LanguageManager::SetMainLanguage( const std::string& languageKey )
{
    s_currentLanguage = languageKey;
}

std::string LanguageManager::Text( const std::string& textKey )
{
//    Logger::Out( "Get text with textKey " + textKey, "LanguageSet::Text" );
    return s_languageSets[ s_currentLanguage ].Text( textKey );
}

std::string LanguageManager::Text( const std::string& languageKey, const std::string& textKey )
{
//    Logger::Out( "Get text with textKey " + textKey + " with languageKey " + languageKey, "LanguageSet::Text" );
    if ( s_languageSets.find( languageKey ) == s_languageSets.end() )
    {
        Logger::Error( "Could not find language with key textKey \"" + textKey + "\" with languageKey \"" + languageKey + "\"", "LanguageSet::Text" );
        return "ERROR";
    }

    return s_languageSets[ languageKey ].Text( textKey );
}

}

