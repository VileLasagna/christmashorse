#ifndef _EFFECT_MANAGER
#define _EFFECT_MANAGER

#include <SFML/Graphics.hpp>
#include <vector>
#include <string>

namespace chalo
{
  enum class Behavior {
    STAY, FLOAT_UP
  };

  struct Effect
  {
    sf::Text text;
    int lifeCounter;
    Behavior behavior;
  };

  class EffectManager
  {
    public:
    static void Update();
    static void Draw( sf::RenderWindow& window );

    static void AddEffect( Effect effect );

    private:
    static std::string CLASSNAME;
    static std::vector<Effect> m_effects;
  };
}

#endif
