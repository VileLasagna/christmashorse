// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#ifndef _TEXTURE_MANAGER
#define _TEXTURE_MANAGER

#include <SFML/Graphics.hpp>

#include <map>
#include <string>

namespace chalo
{

class TextureManager
{
public:
    static std::string CLASSNAME;

    static void Setup();

    static void Add( const std::string& key, const std::string& path );
    static const sf::Texture& AddAndGet( const std::string& key, const std::string& path );
    static void Clear();
    static const sf::Texture& Get( const std::string& key );

    static void DebugOut();

private:
    static std::map<std::string, sf::Texture> m_assets;
};

}

#endif
