// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine
/*

#ifndef _SESSION_MANAGER
#define _SESSION_MANAGER

#include <map>
#include <vector>
#include "../States/IState.hpp"
#include "StateManager.hpp"
#include "InputManager.hpp"

namespace chalo
{

class SessionManager
{
public:
    static void Setup( std::string startState );
    static void Teardown();

    static void NewSession();

    static void AddState( const std::string& stateName, IState* state );
//    void ChangeState( const std::string& stateName );
//    void SetupState();
//    void UpdateState();
//    void DrawState( sf::RenderWindow& window );
//    std::string GetGotoState();

private:
    static std::string s_startState;
    static int s_activeSessions;
    static std::vector< StateManager > s_stateManagers;
    static std::map< std::string, IState* > s_states;
};

}
#endif

*/
