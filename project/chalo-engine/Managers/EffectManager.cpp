#include "EffectManager.hpp"
#include <string>
#include <list>

namespace chalo
{

std::string EffectManager::CLASSNAME = "EffectManager";
std::vector<Effect> EffectManager::m_effects;

void EffectManager::Update()
{
  std::list<int> removeIndices;
  // Update effects
  for ( size_t i = 0; i < m_effects.size(); i++ )
  {
    sf::Vector2f pos = m_effects[i].text.getPosition();
    if ( m_effects[i].behavior == Behavior::FLOAT_UP )
    {
      pos.y--;
    }
    m_effects[i].text.setPosition( pos );
    m_effects[i].lifeCounter--;
    if ( m_effects[i].lifeCounter <= 0 )
    {
      removeIndices.push_front( i );
    }
  }

  // Remove dead effects
  for ( auto& idx : removeIndices )
  {
    m_effects.erase( m_effects.begin() + idx );
  }
}

void EffectManager::Draw( sf::RenderWindow& window )
{
  for ( auto& effect : m_effects )
  {
    window.draw( effect.text );
  }
}

void EffectManager::AddEffect( Effect effect )
{
  m_effects.push_back( effect );
}

}
