// Chalo Engine, Moos-a-dee (2019-current), https://gitlab.com/moosadee/chalo-engine

#include "FontManager.hpp"

#include "../Utilities/Logger.hpp"
#include "../Data/ConfigLoader.hpp"

#include <stdexcept>

namespace chalo
{
std::string FontManager::CLASSNAME = "FontManager";

std::map<std::string, sf::Font> FontManager::m_assets;

void FontManager::Setup( std::string assetListFilename /*= "fonts.chaloassets"*/ )
{
    Logger::OutFuncBegin( "assetListFilename=" + assetListFilename, CLASSNAME + "::" + std::string( __func__ ) );

    std::string logInfo = "Adding fonts: <ol>";
    std::map<std::string, std::string> fontFileList = LoadConfig( assetListFilename );
    for ( auto& fontfile : fontFileList )
    {
        logInfo += "<li>" + fontfile.first + "=" + fontfile.second + "</li>";
        Add( fontfile.first, fontfile.second );
    }
    logInfo += "</ol>";
    Logger::Out( logInfo, CLASSNAME + "::" + std::string( __func__ ), "asset-loading" );

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void FontManager::Add( const std::string& key, const std::string& path )
{
    sf::Font font;
    if ( !font.loadFromFile( path ) )
    {
        // Error
        Logger::Error( "Unable to load font at path \"" + path + "\"", CLASSNAME + "::" + std::string( __func__ ) );
        return;
    }

    m_assets[ key ] = font;
}

void FontManager::Clear()
{
    m_assets.clear();
}

sf::Font& FontManager::Get( const std::string& key )
{
    if ( key == "" )
    {
      Logger::Error( "Why are you trying to load an empty string as the key for a font?!", "FontManager::Get" );
//      throw std::invalid_argument( "Empty key! - FontManager::Get" );
    }
    if ( m_assets.find( key ) == m_assets.end() )
    {
        Logger::Error( "Could not find font with key \"" + key + "\"!", "FontManager::Get" );
        //throw std::runtime_error( "Could not find font with key \"" + key + "\" - FontManager::Get" );

        // Return some default so the game doesn't crash.
        for ( auto& font : m_assets )
        {
            return font.second;
        }
    }

    return m_assets[ key ];
}

}
