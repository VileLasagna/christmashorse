#ifndef _SCREEN_DRAWER_HPP
#define _SCREEN_DRAWER_HPP

#include <string>
#include <vector>
using namespace std;

struct ScreenTile {
	char symbol;
	int fgIndex;
	int bgIndex;
};

class ScreenDrawer
{
public:
    static void Init();

	static void Clear();
	static void Clear(int x, int y);
	static void Set(int x, int y, char symbol);
	static void Set(int x, int y, int number);
	static void Set(int x, int y, string str);

	static void Draw();

	static int GetScreenWidth();
	static int GetScreenHeight();
	static void SetCoutStyle(int bgIndex, int fgIndex);
	static void ResetCoutStyle();
private:
	static const int SCREEN_WIDTH; // 80
	static const int SCREEN_HEIGHT; // 20

	static bool IsValidRange(int x, int y);
	static void ClearConsole();

	//static char s_grid[80][20];
	static ScreenTile s_grid[80][20];
	static vector<int> foregrounds;
	static vector<int> backgrounds;
};

#endif
