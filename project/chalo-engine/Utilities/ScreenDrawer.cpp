#include "ScreenDrawer.hpp"

#include <iostream>
#include <sstream>
using namespace std;

const int ScreenDrawer::SCREEN_WIDTH = 80;
const int ScreenDrawer::SCREEN_HEIGHT = 20;
//char ScreenDrawer::s_grid[80][20];
ScreenTile ScreenDrawer::s_grid[80][20];

vector<int> ScreenDrawer::foregrounds = {
	30, 31, 32, 33, 34, 35, 36, 37, 90, 91, 92, 93, 94, 95, 96, 97
};

vector<int> ScreenDrawer::backgrounds = {
	40, 41, 42, 43, 44, 45, 46, 47, 100, 101, 102, 103, 104, 105, 106, 107
};

void ScreenDrawer::Init()
{
    Clear();
}

void ScreenDrawer::Clear()
{
	for (int y = 0; y < SCREEN_HEIGHT; y++)
	{
		for (int x = 0; x < SCREEN_WIDTH; x++)
		{
			s_grid[x][y].symbol = ' ';
		}
	}
}

void ScreenDrawer::Clear(int x, int y)
{
	Set(x, y, ' ');
}

void ScreenDrawer::Set(int x, int y, char symbol)
{
	if (IsValidRange(x, y))
	{
		s_grid[x][y].symbol = symbol;
	}
}

void ScreenDrawer::Set(int x, int y, int number)
{
	// Convert int to char
	ostringstream oss;
	oss << number;
	Set(x, y, oss.str());
}

void ScreenDrawer::Set(int x, int y, string str)
{
	for (unsigned int i = 0; i < str.size(); i++)
	{
		if (i >= SCREEN_WIDTH) { break; }
		Set(x+i, y, str[i]);
	}
}

void ScreenDrawer::Draw()
{
	ClearConsole();
	for (int y = 0; y < SCREEN_HEIGHT;y++)
	{
		for (int x = 0; x < SCREEN_WIDTH; x++)
		{
			SetCoutStyle( s_grid[x][y].bgIndex, s_grid[x][y].fgIndex );
			cout << s_grid[x][y].symbol;
			ResetCoutStyle();
		}
		cout << endl;
	}
	cout << endl;
}

int ScreenDrawer::GetScreenWidth()
{
	return SCREEN_WIDTH;
}

int ScreenDrawer::GetScreenHeight()
{
	return SCREEN_HEIGHT;
}

void ScreenDrawer::ClearConsole()
{
	#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
		system("cls");
	#else
		system("clear");
	#endif
}

bool ScreenDrawer::IsValidRange(int x, int y)
{
	return (x >= 0 && x < SCREEN_WIDTH&& y >= 0 && y < SCREEN_HEIGHT);
}

void ScreenDrawer::SetCoutStyle(int bgIndex, int fgIndex)
{
	if (fgIndex < 0 || fgIndex >= foregrounds.size())
	{
		cout << "INVALID FG COLOR INDEX. MUST BE BETWEEN 0 AND " << foregrounds.size() - 1 << endl;
	}
	if (bgIndex < 0 || bgIndex >= backgrounds.size())
	{
		cout << "INVALID BG COLOR INDEX. MUST BE BETWEEN 0 AND " << foregrounds.size() - 1 << endl;
	}

	cout << "\033[3;" << foregrounds[fgIndex] << ";" << backgrounds[bgIndex] << "m";
}

void ScreenDrawer::ResetCoutStyle()
{
	cout << "\033[0m";
}

// TODO: Set up the colors to work with the coordinate screen drawer!
