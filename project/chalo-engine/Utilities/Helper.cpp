#include "Helper.hpp"

#include <sstream> // For conversions
#include <locale>  // For wstring
#include <codecvt> // For wstring
#include <cmath>

/***************************************************************/
/* User input functions ****************************************/
/***************************************************************/

bool Helper::s_needCinIgnore = false;

int Helper::GetIntInput()
{
    return StringToInt( GetStringInput() );
}

float Helper::GetFloatInput()
{
    return StringToFloat( GetStringInput() );
}

char Helper::GetCharInput()
{
    string str = GetStringInput();
    return str[0];
}

string Helper::GetStringInput()
{
    s_needCinIgnore = true;
    string strValue;
    cin >> strValue;
    return strValue;
}

string Helper::GetStringLine()
{
    if ( s_needCinIgnore )
    {
        cin.ignore();
    }

    s_needCinIgnore = false;
    string strValue;
    getline( cin, strValue );
    return strValue;
}

int Helper::GetIntInput( int min, int max )
{
    int input = GetIntInput();

    while ( input < min || input > max )
    {
        cout << "Invalid selection. Try again: " << endl;
        input = GetIntInput();
    }

    return input;
}


/***************************************************************/
/* Menu functionality ******************************************/
/***************************************************************/

void Helper::ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

void Helper::Pause()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "pause" );
    #else
        cout << endl << " Press ENTER to continue..." << endl;
        if ( s_needCinIgnore )
        {
            cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
        }
        else
        {
            cin.get();
        }
    #endif
}

void Helper::Header( const string& header )
{
    DrawHorizontalBar( 80 );
    string head = "| " + header + " |";
    cout << " " << head << endl << " ";
    DrawHorizontalBar( head.size() );
    cout << endl;
}

void Helper::DrawHorizontalBar( int width, char symbol /*= '-'*/ )
{
    cout << string( width, symbol ) << endl;
}

void Helper::ShowMenu( const vector<string>& options, bool vertical /*= true*/, bool startAtOne /*= true*/ )
{
    for ( size_t i = 0; i < options.size(); i++ )
    {
        int number = ( startAtOne ) ? i+1 : i;
        cout << " " << number << ".";
        if ( vertical ) { cout << "\t"; } else { cout << " "; }
        cout << options[i];
        if ( vertical ) { cout << "\n"; } else { cout << "\t"; }
    }
    cout << endl;
}

int  Helper::ShowMenuGetInt( const vector<string>& options, bool vertical /*= true*/, bool startAtOne /*= true*/ )
{
    ShowMenu( options, vertical, startAtOne );
    int minVal = ( startAtOne ) ? 1 : 0;
    int maxVal = ( startAtOne ) ? options.size() : options.size() - 1;
    int choice = GetIntInput( minVal, maxVal );
    return choice;
}


/***************************************************************/
/* System info *************************************************/
/***************************************************************/

void Helper::PrintPwd()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "echo %cd%" );
    #else
        system( "pwd" );
    #endif
}

void Helper::Sleep( int milliseconds )
{
    this_thread::sleep_for( chrono::milliseconds( milliseconds ) );
}

void Helper::DisplayDirectoryContents( string path )
{
    string command;

    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    command = "dir ";
    #else
    command = "ls ";
    #endif

    command += path;

    system( command.c_str() );
}


/***************************************************************/
/* Conversion functionality ************************************/
/***************************************************************/

int      Helper::StringToInt( const string& str )
{
    int value = 0;
    try
    {
        value = stoi( str );
    }
    catch( const std::invalid_argument& ex )
    {
        std::cout << "Cannot convert \"" << str << "\" to int!" << std::endl;
//        throw ex; // Still want it at a deeper level
        return -1;
    }

    return value;
}

float    Helper::StringToFloat( const string& str )
{
    float value = 0;
    try
    {
        value = stof( str );
    }
    catch( const std::invalid_argument& ex )
    {
        std::cout << "Cannot convert \"" << str << "\" to float!" << std::endl;
        return -1;
    }

    return value;
}

string   Helper::CurrencyToString( float currency )
{
    stringstream ss;
    ss << "$" << fixed << setprecision(2) << currency;
    return ss.str();
}

string   Helper::ToUpper( const string& val )
{
    string str = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        str += toupper( val[i] );
    }
    return str;
}

string   Helper::ToLower( const string& val )
{
    string str = "";
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        str += tolower( val[i] );
    }
    return str;
}

string   Helper::Trim( const string& val )
{
    int firstLetter = -1;
    int lastLetter = -1;
    for ( unsigned int i = 0; i < val.size(); i++ )
    {
        if ( firstLetter == -1 && val[i] != ' ' )
        {
            firstLetter = i;
        }

        if ( val[i] != ' ' )
        {
            lastLetter = i;
        }
    }

    int stringLength = lastLetter - firstLetter + 1;

    string trimmed = val.substr( firstLetter, stringLength );
    return trimmed;
}

void Helper::Test_Trim()
{
    {
        string input = "Nothing to trim";
        string expectedOutput = "Nothing to trim";
        string actualOutput = Helper::Trim( input );

        cout << "Test 1: \"" << input << "\"... ";
        if ( actualOutput != expectedOutput ) { cout << "FAIL" << endl; }
        else { cout << "PASS" << endl; }
        cout << "Expected output: \"" << expectedOutput << "\"" << endl;
        cout << "Actual output: \"" << actualOutput << "\"" << endl;
    }

    {
        string input = "  Space at the beginning";
        string expectedOutput = "Space at the beginning";
        string actualOutput = Helper::Trim( input );

        cout << "Test 2: \"" << input << "\"... ";
        if ( actualOutput != expectedOutput ) { cout << "FAIL" << endl; }
        else { cout << "PASS" << endl; }
        cout << "Expected output: \"" << expectedOutput << "\"" << endl;
        cout << "Actual output: \"" << actualOutput << "\"" << endl;
    }

    {
        string input = "Space at the end  ";
        string expectedOutput = "Space at the end";
        string actualOutput = Helper::Trim( input );

        cout << "Test 3: \"" << input << "\"... ";
        if ( actualOutput != expectedOutput ) { cout << "FAIL" << endl; }
        else { cout << "PASS" << endl; }
        cout << "Expected output: \"" << expectedOutput << "\"" << endl;
        cout << "Actual output: \"" << actualOutput << "\"" << endl;
    }

    {
        string input = "   Space on both ends   ";
        string expectedOutput = "Space on both ends";
        string actualOutput = Helper::Trim( input );

        cout << "Test 4: \"" << input << "\"... ";
        if ( actualOutput != expectedOutput ) { cout << "FAIL" << endl; }
        else { cout << "PASS" << endl; }
        cout << "Expected output: \"" << expectedOutput << "\"" << endl;
        cout << "Actual output: \"" << actualOutput << "\"" << endl;
    }
}


/***************************************************************/
/* String helpers **********************************************/
/***************************************************************/

bool     Helper::Contains( string haystack, string needle, bool caseSensitive /*= true*/ )
{
    string a = haystack;
    string b = needle;

    if ( !caseSensitive )
    {
        for ( unsigned int i = 0; i < a.size(); i++ )
        {
            a[i] = tolower( a[i] );
        }

        for ( unsigned int i = 0; i < b.size(); i++ )
        {
            b[i] = tolower( b[i] );
        }
    }

    return ( a.find( b ) != string::npos );
}

int      Helper::Find( string haystack, string needle )
{
    return haystack.find( needle );
}

string   Helper::Replace( string fulltext, string findme, string replacewith )
{
    string updated = fulltext;
    size_t index = updated.find( findme );

    while ( index != string::npos )
    {
        // Make the replacement
        updated = updated.replace( index, findme.size(), replacewith );

        // Look for the item again
        index = updated.find( findme );
    }

    return updated;
}

vector<string> Helper::Split( string str, string delim )
{
    vector<string> data;

    int begin = 0;
    int end = 0;

    while ( str.find( delim ) != string::npos )
    {
        end = str.find( delim );

        // Get substring up until delimiter
        int length = end - begin;
        string substring = str.substr( begin, length );
        data.push_back( substring );

        // Remove this chunk of string
        str = str.erase( begin, length+1 );
    }

    // Put last string in structure
    data.push_back( str );

    return data;
}


// From https://stackoverflow.com/questions/4804298/how-to-convert-wstring-into-string
std::string Helper::WStringToString( std::wstring text )
{
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;

    //use converter (.to_bytes: wstr->str, .from_bytes: str->wstr)
    std::string converted_str = converter.to_bytes( text );

    return converted_str;
}

// From https://stackoverflow.com/questions/2573834/c-convert-string-or-char-to-wstring-or-wchar-t
std::wstring Helper::StringToWString( std::string text )
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    std::wstring wide = converter.from_bytes( text );
    return wide;
}

std::string Helper::CoordinateToString( int x, int y )
{
    std::stringstream ss;
    ss << "(" << x << ", " << y << ")";
    return ss.str();
}

std::string Helper::DimensionsToString( int width, int height  )
{
    std::stringstream ss;
    ss << width << "x" << height;
    return ss.str();
}

/***************************************************************/
/* Timer functionality *****************************************/
/***************************************************************/

chrono::system_clock::time_point Helper::s_startTime;

void Helper::ClockStart()
{
    s_startTime = chrono::system_clock::now();
}

size_t Helper::GetElapsedSeconds()
{
    chrono::system_clock::time_point current_time = std::chrono::system_clock::now();
    return chrono::duration_cast<std::chrono::seconds>( current_time - s_startTime ).count();
}

size_t Helper::GetElapsedMilliseconds()
{
    chrono::system_clock::time_point current_time = std::chrono::system_clock::now();
    return chrono::duration_cast<std::chrono::milliseconds>( current_time - s_startTime ).count();
}


/***************************************************************/
/* Random functionality ****************************************/

void Helper::SeedRandomNumberGenerator()
{
    srand( time( NULL ) );
}

int  Helper::GetRandom( int min, int max )
{
    return rand() % ( max - min + 1 ) + min;
}

/***************************************************************/
/* Math functionality ******************************************/
/***************************************************************/
float Helper::GetDistance( float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2, bool fromCenter )
{
  if ( fromCenter )
  {
      x1 += w1 / 2;
      y1 += h1 / 2;

      x2 += w2 / 2;
      y2 += h2 / 2;
  }

  return sqrt( pow( x2 - x1, 2 ) + pow( y2 - y1, 2 ) );
}

float Helper::GetDistance( float x1, float y1, float x2, float y2 )
{
    return sqrt( pow( x2 - x1, 2 ) + pow( y2 - y1, 2 ) );
}

float Helper::DotProduct( float x1, float y1, float x2, float y2 )
{
    return ( x1 * x2 + y1 * y2 );
}

float Helper::Length( float x1, float y1 )
{
    return sqrt( x1 * x1 + y1 * y1 );
}

float Helper::AngleBetweenTwoPointsRadians( float x1, float y1, float x2, float y2 )
{
    // Reference: https://www.wikihow.com/Find-the-Angle-Between-Two-Vectors
    // Note that SFML uses degrees, cmath uses radians.

    float lengthA = Length( x1, y1 );
    float lengthB = Length( x2, y2 );
    float dotProduct = DotProduct( x1, y1, x2, y2 );
    float cos = dotProduct / ( lengthA * lengthB );
    // acos computes between [-1, +1]
    float angle = acos( cos );
    return angle;
}

float Helper::AngleBetweenTwoPointsDegrees( float x1, float y1, float x2, float y2 )
{
    float radians = AngleBetweenTwoPointsDegrees( x1, y1, x2, y2 );
    return radians * 180 / M_PI;
}

bool Helper::BoundingBoxCollision( float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2 )
{
    return (
      x1      < x2 + w2 &&
      x1 + w1 > x2 &&
      y1      < y2 + h2 &&
      y1 + h1 > y2
      );
}

bool Helper::PointInBoxCollision( float x1, float y1, float x2, float y2, float w2, float h2 )
{
    return (
      x1   >= x2 &&
      x1   <= x2 + w2 &&
      y1   >= y2 &&
      y1   <= y2 + h2
    );
}

/***************************************************************/
/* Additional functionality ************************************/

std::string Helper::GetTime()
{
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::time_t time = std::chrono::system_clock::to_time_t( now );
    return ctime( &time );
}

