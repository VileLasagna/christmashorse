#include "Image.h"

#include <fstream>
#include <iostream>

PpmImage::PpmImage()
{
    m_pixels = nullptr;
    m_colorDepth = 255;
}

PpmImage::PpmImage( int width, int height )
{
    m_pixels = nullptr;
    m_colorDepth = 255;
    Setup( width, height );
}

PpmImage::~PpmImage()
{
    Cleanup();
}

void PpmImage::Setup( int width, int height )
{
    m_width = width;
    m_height = height;

    m_pixels = new Pixel*[m_width];

    for ( int i = 0; i < m_width; i++ )
    {
        m_pixels[i] = new Pixel[m_height];
    }
}

void PpmImage::Cleanup()
{
    if ( m_pixels != nullptr )
    {
        for ( int i = 0; i < m_width; i++ )
        {
            delete [] m_pixels[i];
        }

        delete [] m_pixels;
    }
}

void PpmImage::ClearImage()
{
    for ( size_t y = 0; y < m_height; y++ )
    {
        for ( size_t x = 0; x < m_width; x++ )
        {
            m_pixels[x][y].r = 255;
            m_pixels[x][y].g = 255;
            m_pixels[x][y].b = 255;
        }
    }
}

void PpmImage::GridFill( int gridWidth, int gridHeight )
{
    int reds[] = { 50, 0 };
    int gree[] = { 0, 50 };
    int blue[] = { 0, 0 };

    int colorIndex = 0;

    int tileWidth = m_width / gridWidth;

    for ( size_t y = 0; y < m_height; y++ )
    {
        for ( size_t x = 0; x < m_width; x++ )
        {
            int tileIndex = ( x / gridWidth ) + ( y / gridHeight * tileWidth );

            colorIndex = tileIndex % 2;

            m_pixels[x][y].r = reds[colorIndex];
            m_pixels[x][y].g = gree[colorIndex];
            m_pixels[x][y].b = reds[colorIndex];
        }
    }
}

/**
@param      filename      image file to load
@return     true when image loaded successfully, false otherwise.

HEADER FORMAT:
P3
# Created by GIMP version 2.10.18 PNM plug-in
300 300
255

AFTER THE HEADER IS A SERIES OF RED, GREEN, BLUE VALUES FOR EACH PIXEL:
51
46
25
(ETC.)
*/
bool PpmImage::LoadImage( const std::string& filename )
{
    std::ifstream input( filename );

    if ( input.fail() )
    {
        return false;
    }

    std::string strBuffer;
    int intBuffer;

    getline( input, strBuffer ); // P3
    getline( input, strBuffer ); // Glimpse comment

    input >> m_width >> m_height; // width and height
    input >> m_colorDepth; // color depth
    Setup( m_width, m_height );

    // Read pixels
    for ( size_t y = 0; y < m_height; y++ )
    {
        for ( size_t x = 0; x < m_width; x++ )
        {
            input >> m_pixels[x][y].r;
            input >> m_pixels[x][y].g;
            input >> m_pixels[x][y].b;
        }
    }
    return true;
}


/**
@param      filename      filepath to save image to
@return     true when image saved successfully, false otherwise.

MAKE SURE TO OUTPUT ALL THIS INFORMATION,
WITH PROPER WHITESPACING:

P3
# Created by GIMP version 2.10.18 PNM plug-in
300 300
255
51
46
25

P3      - hard code the file type P3
# etc.  - Put whatever you want for the comment
300 300 - width and height, separated  by a space
255     - color depth
51      - pixel 0 red
46      - pixel 0 green
25      - pixel 0 blue
(etc.)
*/
bool PpmImage::SaveImage( const std::string& filename )
{
    std::ofstream output( filename );
    std::cout << "Saving image to \"" << filename << "\"" << std::endl;

    if ( output.fail() )
    {
        return false;
    }

    // Output header stuff
    output << "P3" << std::endl
        << "# Comment blah blah blah" << std::endl
        << m_width << " "
        << m_height << std::endl
        << m_colorDepth << std::endl;

    // Output all the pixels
    for ( size_t y = 0; y < m_height; y++ )
    {
        for ( size_t x = 0; x < m_width; x++ )
        {
            output << m_pixels[x][y].r << std::endl;
            output << m_pixels[x][y].g << std::endl;
            output << m_pixels[x][y].b << std::endl;
        }
    }

    std::cout << "Done" << std::endl;
    return true;
}

