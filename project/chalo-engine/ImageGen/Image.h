#ifndef _IMAGE_H
#define _IMAGE_H

#include <string>

#include "Pixel.h"

struct Pixel
{
    int r, g, b;
};

class PpmImage
{
    public:
    PpmImage();
    PpmImage( int width, int height );
    ~PpmImage();

    void Setup( int width, int height );
    void Cleanup();

    bool LoadImage( const std::string& filename );
    bool SaveImage( const std::string& filename );

    void ClearImage();
    void GridFill( int gridWidth, int gridHeight );

    private:
    int m_colorDepth;
    int m_width;
    int m_height;
    Pixel** m_pixels; // Dynamic 2D array
};

#endif
