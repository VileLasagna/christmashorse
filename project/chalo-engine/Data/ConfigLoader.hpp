#ifndef _CONFIG_LOADER_HPP
#define _CONFIG_LOADER_HPP

#include <map>
#include <string>

namespace chalo
{
    std::map< std::string, std::string > LoadConfig( std::string filename );
}

#endif
