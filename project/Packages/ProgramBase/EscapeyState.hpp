#ifndef _ESCAPEYSTATE
#define _ESCAPEYSTATE

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/DEPRECATED/Maps/WritableMap.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
//#include "../../chalo-engine/DEPRECATED/Managers/DrawManager.hpp"

#include <SFML/Audio.hpp>
#include <vector>

class EscapeyState : public chalo::IState
{
public:
    EscapeyState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

    void TogglePause( bool value );
    bool IsPaused() const;

    void LoadSavedOptions();
    void IncreaseSoundVolume();
    void DecreaseSoundVolume();
    void IncreaseMusicVolume();
    void DecreaseMusicVolume();

protected:
    std::string CLASSNAME;
    bool m_pauseEnabled;
    sf::Sound m_soundTest;
};

#endif
