#ifndef _STORY_SLIDE
#define _STORY_SLIDE

struct StorySlide
{
    sf::Sprite background;
    std::vector<sf::Text> text;
    std::vector<sf::Sprite> sprites;
};

#endif
