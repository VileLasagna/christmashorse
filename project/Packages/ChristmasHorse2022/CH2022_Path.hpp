#ifndef _CH2022_PATH
#define _CH2022_PATH

#include <SFML/Graphics.hpp>

class CH2022_Path
{
    public:
    void Setup( const sf::Texture& texture, int x, int y );
    void Draw( sf::RenderWindow& window );

    private:
    sf::Sprite m_sprite;
};

#endif
