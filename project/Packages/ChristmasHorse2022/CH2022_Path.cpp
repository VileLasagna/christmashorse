#include "CH2022_Path.hpp"

void CH2022_Path::Setup( const sf::Texture& texture, int x, int y )
{
    m_sprite.setTexture( texture );
    m_sprite.setPosition( x, y );
}

void CH2022_Path::Draw( sf::RenderWindow& window )
{
    window.draw( m_sprite );
}
