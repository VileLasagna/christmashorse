#include "CH2022_Character.hpp"

#include "../../chalo-engine/Managers/FontManager.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

void CH2022_Actor::Setup( const std::string& name, const sf::Texture& texture, sf::IntRect position, int maxHp )
{
    m_name = name;

    m_sprite.setTexture( texture );
    m_position = position;
    m_sprite.setPosition( m_position.left, m_position.top );
    m_hpMax = maxHp;
    m_hp = maxHp;
    m_speed = 2;
    m_readyToShoot = false;
    m_gotHurt = false;

    m_actionCooldown = 0;
    if ( name == "sniper" )
    {
        m_actionCooldownMax = 100;
    }
    else if ( name == "ally-tower-horse" )
    {
        m_actionCooldownMax = 200;
    }
    m_actionCooldown2 = 0;
    m_actionCooldown2Max = 50;
    m_actionCounter = 0;

    m_healthBG.setFillColor( sf::Color::Black );
    m_health.setFillColor( sf::Color::Green );

    m_healthBG.setPosition( m_position.left - 2, m_position.top - 22 );
    m_healthBG.setSize( sf::Vector2f( m_position.width + 4, 12 ) );

    m_health.setPosition( m_position.left, m_position.top - 20 );
    m_health.setSize( sf::Vector2f( m_position.width, 8 ) );

    m_statusText.setFont( chalo::FontManager::Get( "main" ) );
    m_statusText.setPosition( 0, 0 );
    m_statusText.setOutlineThickness( 2 );
    m_statusText.setOutlineColor( sf::Color::Black );
    m_statusText.setFillColor( sf::Color::Yellow );
    m_statusText.setScale( 0.8, 0.8 );

    if ( name == "ally-snow-moose"
        || name == "ally-antler-horse"
        || name == "ally-tower-horse"
        || name == "ally-chimnken"
        || name == "big-chimnken"
        )
    {
        m_isAlly = true;
    }
    else
    {
        m_isAlly = false;
    }
}

void CH2022_Actor::SetSpeed( float speed )
{
    m_speed = speed;
}

sf::IntRect CH2022_Actor::GetPosition()
{
    return m_position;
}

sf::Vector2f CH2022_Actor::GetPositionVec()
{
    return sf::Vector2f( m_position.left, m_position.top );
}

float CH2022_Actor::GetX()
{
    return m_position.left;
}

float CH2022_Actor::GetY()
{
    return m_position.top;
}

float CH2022_Actor::GetWidth()
{
    return m_position.width;
}

float CH2022_Actor::GetHeight()
{
    return m_position.height;
}

std::string CH2022_Actor::GetName()
{
    return m_name;
}

void CH2022_Actor::UpdateHealth( int amount )
{
    m_hp += amount;

    if ( m_hp > m_hpMax )
    {
        m_hp = m_hpMax;
    }
    else if ( m_hp < 0 )
    {
        m_hp = 0;
    }
}

int CH2022_Actor::GetHealth()
{
    return m_hp;
}

int CH2022_Actor::GetHealthMax()
{
    return m_hpMax;
}

void CH2022_Actor::SetStatusText( std::string text )
{
    m_statusText.setString( text );
    m_statusText.setPosition( GetPosition().left, GetPosition().top - 50 );
}

void CH2022_Actor::SetPosition( int x, int y )
{
    m_position.left = x;
    m_position.top = y;
}

float CH2022_Actor::GetDamageAmount()
{
    return m_damageAmount;
}

void CH2022_Actor::SetDamageAmount( float amount )
{
    m_damageAmount = amount;
}

void CH2022_Actor::UpdateSprite()
{
    m_sprite.setPosition( m_position.left, m_position.top );

    m_healthBG.setPosition( m_position.left - 2, m_position.top - 22 );

    float healthRatio = float(m_hp) / m_hpMax;

    m_health.setPosition( m_position.left, m_position.top - 20 );
    m_health.setSize( sf::Vector2f( m_position.width * healthRatio, 8 ) );

    if ( healthRatio >= 0.75 ) {
        m_health.setFillColor( sf::Color::Green );
    }
    else if ( healthRatio >= 0.50 ) {
        m_health.setFillColor( sf::Color::Yellow );
    }
    else {
        m_health.setFillColor( sf::Color::Red );
    }
}

void CH2022_Actor::Update()
{
    if ( m_statusText.getString() != "" )
    {
        m_statusText.setPosition( m_statusText.getPosition().x, m_statusText.getPosition().y - 1 );
    }
}

void CH2022_Actor::Update( CH2022_Actor& base, std::vector<CH2022_Actor>& targets )
{
    if ( !IsAlive() ) { return; }

    m_actionCounter++;

    if ( m_actionCooldown > 0 ) { m_actionCooldown--; }
    if ( m_actionCooldown2 > 0 ) { m_actionCooldown2--; }

    if ( m_name == "bomb" )
    {
        UpdateBomb( base );
    }
    else if ( m_name == "sniper" )
    {
        UpdateSniper( base, targets );
    }
    else if ( m_name == "bullet" )
    {
        UpdateBullet();
    }
    else if ( m_name == "ally-snow-moose" )
    {
        UpdateSnowMoose( targets );
    }
    else if ( m_name == "ally-antler-horse" )
    {
        UpdateAntlerHorse( targets );
    }
    else if ( m_name == "ally-tower-horse" )
    {
        UpdateTowerHorse( targets );
    }
    else if ( m_name == "ally-chimnken" )
    {
        UpdateChimnken( targets );
    }
    else if ( m_name == "snowball" )
    {
        UpdateSnowball();
    }
    else if ( m_name == "big-chimnken" )
    {
        UpdateBigChimnken();
    }

    if ( m_isAlly )
    {
    }
    else
    {
        // Check to see if any enemies arrived at the base
        if ( SFMLHelper::BoundingBoxCollision( base.GetPosition(), GetPosition() ) )
        {
            int damage = GetDamageAmount();
            base.UpdateHealth( damage );
            UpdateHealth( damage );
        }
    }

    // If it's off screen, kill it
    if ( m_position.left > 1500 )
    {
        m_hp = 0;
    }
    if ( m_position.left < -64 &&
        ( m_name == "big-chimnken" || m_name == "snowball" ) )
    {
        m_hp = 0;
    }
}

void CH2022_Actor::UpdateBomb( CH2022_Actor& target )
{
    if ( target.m_position.left > m_position.left )
    {
        // Move right
        m_position.left += m_speed;
    }
    else if ( target.m_position.left < m_position.left )
    {
        // Move left
        m_position.left -= m_speed;
    }
    else if ( target.m_position.top > m_position.top )
    {
        // Move down
        m_position.top += m_speed;
    }
    else if ( target.m_position.top < m_position.top )
    {
        // Move up
        m_position.top -= m_speed;
    }
}

void CH2022_Actor::UpdateSniper( CH2022_Actor& base, std::vector<CH2022_Actor>& allies )
{
    if ( m_actionCooldown <= 0 && m_position.left > 10 )
    {
        // Shoot if ally in at same "y" position.
        m_readyToShoot = false;
        for ( unsigned int a = 0; a < allies.size(); a++ )
        {
            // Is ally within vertical region?
            if ( allies[a].m_position.left > m_position.left &&
                allies[a].m_position.top <= m_position.top + m_position.height &&
                allies[a].m_position.top + allies[a].m_position.height >= m_position.top )
            {
                // This flag will tell the game state to
                // spawn a bullet
                m_readyToShoot = true;
            }
        }
    }

    if ( m_readyToShoot )
    {
        m_actionCooldown = m_actionCooldownMax;
        m_actionCooldown2 = m_actionCooldown2Max;
    }
    else
    {
        m_readyToShoot = false;

        // Move sniper forward if they're done shooting
        if ( m_actionCooldown2 <= 0 )
        {
            if ( m_position.left < 0 )
            {
                m_position.left += m_speed;
            }
            else
            {
                if ( m_actionCounter < 50 )
                {
                    if ( base.m_position.left > m_position.left+m_speed )
                    {
                        // Move right
                        m_position.left += m_speed;
                    }
                    else if ( base.m_position.left < m_position.left-m_speed )
                    {
                        // Move left
                        m_position.left -= m_speed;
                    }
                }
                else if ( m_actionCounter < 55 )
                {
                    if ( base.m_position.top > m_position.top )
                    {
                        // Move down
                        m_position.top += m_speed;
                    }
                    else if ( base.m_position.top < m_position.top )
                    {
                        // Move up
                        m_position.top -= m_speed;
                    }
                }
                else
                {
                    m_actionCounter = 0;
                }
            }
        }
    }
}

void CH2022_Actor::UpdateBullet()
{
    m_position.left += m_speed;
}

void CH2022_Actor::UpdateSnowball()
{
    m_position.left += m_speed;
}

void CH2022_Actor::UpdateBigChimnken()
{
    m_position.left += m_speed;
}

void CH2022_Actor::UpdateSnowMoose( std::vector<CH2022_Actor>& enemies )
{
    // Snow moose just sits still, but will damage/take damage from enemies
}

void CH2022_Actor::UpdateTowerHorse( std::vector<CH2022_Actor>& enemies )
{
    if ( m_actionCooldown <= 0 )
    {
        // Shoot if ally in at same "y" position.
        m_readyToShoot = false;
        for ( unsigned int e = 0; e < enemies.size(); e++ )
        {
            // Is enemy on the screen?
            if ( enemies[e].m_position.left <= 0 )
            {
                continue;
            }

            // Is enemy within vertical region?
            if ( enemies[e].m_position.top <= m_position.top + m_position.height &&
                enemies[e].m_position.top + enemies[e].m_position.height >= m_position.top )
            {
                // This flag will tell the game state to
                // spawn a bullet
                m_readyToShoot = true;
            }
        }
    }

    if ( m_readyToShoot )
    {
        m_actionCooldown = m_actionCooldownMax;
        m_actionCooldown2 = m_actionCooldown2Max;
    }
}

void CH2022_Actor::UpdateAntlerHorse( std::vector<CH2022_Actor>& enemies ) //CH2022_Actor& target )
{
    if ( !IsAlive() ) { return; }

    // Which enemy is closest?
    float minDistance = 2000;//SFMLHelper::GetDistance( ally.GetPosition(), m_enemies[0].GetPosition(), true );
    int minIndex = -1;
    float detectDistance = 700;

    for ( unsigned int e = 0; e < enemies.size(); e++ )
    {
        // Find enemy to move towards
        float distance = SFMLHelper::GetDistance( GetPosition(), enemies[e].GetPosition(), true );
        if ( distance > detectDistance )
        {
            // Too far away; don't detect.
            continue;
        }
        else if ( enemies[e].GetName() == "bullet" )
        {
            // Don't chase the bullets
            continue;
        }

        if ( distance < minDistance && enemies[e].IsAlive() )
        {
            // Mark this enemy as the closest one
            minDistance = distance;
            minIndex = e;
        }
    }

    // No enemies on board; skip
    if ( minIndex == -1 ) { return; }

    // Move to closest enemy
    if ( enemies[minIndex].m_position.left > m_position.left-m_speed )
    {
        // Move right
        m_position.left += m_speed;
    }
    else if ( enemies[minIndex].m_position.left < m_position.left+m_speed )
    {
        // Move left
        m_position.left -= m_speed;
    }
    if ( enemies[minIndex].m_position.top > m_position.top )
    {
        // Move down
        m_position.top += m_speed;
    }
    else if ( enemies[minIndex].m_position.top < m_position.top )
    {
        // Move up
        m_position.top -= m_speed;
    }
}

void CH2022_Actor::UpdateChimnken( std::vector<CH2022_Actor>& enemies )
{
    // Move randomly
    int direction = rand() % 10;

    if ( direction == 0 )
    {
        m_position.top += m_speed;
    }
    else if ( direction == 1 )
    {
        m_position.top -= m_speed;
    }
    else if ( direction == 2 )
    {
        m_position.left += m_speed;
    }
    else if ( direction == 3 )
    {
        m_position.left -= m_speed;
    }
}

bool CH2022_Actor::IsAlive()
{
    return ( m_hp > 0 || m_position.left > 1280 );
}

bool CH2022_Actor::ReadyToShoot()
{
    return m_readyToShoot;
}

void CH2022_Actor::DoneShooting()
{
    m_readyToShoot = false;
}

bool CH2022_Actor::GotHurt()
{
    return m_gotHurt;
}

void CH2022_Actor::DoneHurt()
{
    m_gotHurt = false;
}

void CH2022_Actor::Draw( sf::RenderWindow& window )
{
    if ( !IsAlive() ) { return; }

//    UpdateSprite();
//    window.draw( m_sprite );
//
    window.draw( m_healthBG );
    window.draw( m_health );
    window.draw( m_statusText );
}

sf::Sprite CH2022_Actor::GetSprite()
{
    UpdateSprite();
    return m_sprite;
}

void CH2022_Actor::CheckAllyEnemyCollision( CH2022_Actor& ally, CH2022_Actor& enemy )
{
    if ( enemy.IsAlive() && ally.IsAlive() &&
        SFMLHelper::BoundingBoxCollision( ally.GetPosition(), enemy.GetPosition() ) )
    {
        ally.UpdateHealth( enemy.GetDamageAmount() );
        enemy.UpdateHealth( ally.GetDamageAmount() );

        ally.m_gotHurt = true;
        enemy.m_gotHurt = true;
    }
}
