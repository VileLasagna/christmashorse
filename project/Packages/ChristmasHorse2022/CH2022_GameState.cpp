#include "CH2022_GameState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Managers/AudioManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

#include <list>

CH2022_GameState::CH2022_GameState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2022_GameState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2022_GameState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();
    EscapeyState::Setup();
    std::string gfxPath = "../Packages/ChristmasHorse2022/Graphics/Game/";
    std::string sndPath = "../Packages/ChristmasHorse2022/Audio/";

    m_timerText.setFont( chalo::FontManager::Get( "main" ) );
    m_timerText.setPosition( 1280-40, 10 );
    m_timerText.setOutlineThickness( 2 );
    m_timerText.setOutlineColor( sf::Color::Transparent );
    m_timerText.setFillColor( sf::Color::Black );
    m_timerText.setScale( 0.4, 0.4 );

    m_brushText.setFont( chalo::FontManager::Get( "main" ) );
    m_brushText.setOutlineThickness( 2 );
    m_brushText.setOutlineColor( sf::Color::Black );
    m_brushText.setFillColor( sf::Color::Green );
//    m_brushText.setScale( 0.4, 0.4 );

    m_unitNames[1] = "ally-snow-moose";
    m_unitNames[2] = "ally-antler-horse";
    m_unitNames[3] = "ally-tower-horse";
    m_unitNames[4] = "ally-chimnken";

    m_unitCounts[1] = 5;
    m_unitCounts[2] = 0;
    m_unitCounts[3] = 0;
    m_unitCounts[4] = 0;

    m_background.setTexture( chalo::TextureManager::AddAndGet( "snow", gfxPath + "snow.png" ) );

    chalo::TextureManager::Add( "base",                 gfxPath + "base.png" );
    // Enemies
    chalo::TextureManager::Add( "peppermint-bomb",      gfxPath + "peppermint-bomb.png" );
    chalo::TextureManager::Add( "sniper-elf",           gfxPath + "sniper-elf.png" );
    // Allies
    chalo::TextureManager::Add( m_unitNames[2],         gfxPath + "antler-horse.png" );
    chalo::TextureManager::Add( m_unitNames[1],         gfxPath + "snow-moose.png" );
    chalo::TextureManager::Add( m_unitNames[3],         gfxPath + "tower-horse.png" );
    chalo::TextureManager::Add( m_unitNames[4],         gfxPath + "chimnken-smol.png" );
    chalo::TextureManager::Add( "big-chimnken",         gfxPath + "chimnken-big.png" );
    // Additional
    chalo::TextureManager::Add( "spawn-node",           gfxPath + "spawn-node.png" );
    chalo::TextureManager::Add( "path",                 gfxPath + "path.png" );
    chalo::TextureManager::Add( "bullet",               gfxPath + "bullet.png" );
    chalo::TextureManager::Add( "snowball",             gfxPath + "snowball.png" );
    // UI
    chalo::TextureManager::Add( "hudbg",                gfxPath + "hudbg.png" );
    chalo::TextureManager::Add( "hudbg",                gfxPath + "hudbg.png" );
    chalo::TextureManager::Add( "buttonbg",             gfxPath + "buttonbg.png" );
    chalo::TextureManager::Add( "buttons",              gfxPath + "buttons.png" );
    chalo::TextureManager::Add( "longbutton",           gfxPath + "longbutton.png" );

    chalo::TextureManager::Add( "dialog-webeka",    "../Packages/ChristmasHorse2021/Graphics/dialog-webeka.png" );
    chalo::TextureManager::Add( "dialog-sneezy",    "../Packages/ChristmasHorse2021/Graphics/dialog-sneezy.png" );
    chalo::TextureManager::Add( "dialog-ratchet",   "../Packages/ChristmasHorse2021/Graphics/dialog-ratchet.png" );
    chalo::TextureManager::Add( "dialog-drabis",    "../Packages/ChristmasHorse2021/Graphics/dialog-drabis.png" );

    m_base.Setup( "base", chalo::TextureManager::Get( "base" ), sf::IntRect( 1100, 300, 128, 128 ), 100 );

    m_statusBackground.setPosition( 0, 0 );
    m_statusBackground.setSize( sf::Vector2f( 1280, 140 ) );
    m_statusBackground.setFillColor( sf::Color( 0, 0, 0, 150 ) );

    SetStatus( {
        "The elves are attacking! We have some units you can place to",
        "protect our base. We are prepping more as we speak.",
        "Once you've set up some defenses, press [NEXT WAVE] to face the onslaught."
        },
    "dialog-sneezy" );

    m_playArea = sf::IntRect( 150, 150, 1280-200, 720-300 );


    m_music.openFromFile( sndPath + "melanie_song.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();

    m_timer = 0;
    m_wave = 0;
    m_waveMax = 10;
    m_phase = GamePhase::BUILDING;

    m_currentUnitBrush = 0;
    m_brush.setColor( sf::Color( 255, 255, 255, 150 ) );

    m_unitSpawnCounterMax = 20;
    m_unitSpawnCounter = 0;

    m_endWaveCooldown = 0;
    m_endWaveCooldownMax = 100;

//    chalo::InputManager::Setup();

    Helper::SeedRandomNumberGenerator();

    chalo::DrawManager::SetBackgroundColor( sf::Color( 182, 201, 216 ) );

    SetupUI();

    m_soundBuffer["success"].loadFromFile( "../Packages/ChristmasHorse2020/Audio/carrot-collect.ogg" );
    m_soundEffects["success"].setBuffer( m_soundBuffer["success"] );
    m_soundEffects["success"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["explode1"].loadFromFile( "../Packages/ChristmasHorse2022/Audio/explode1.wav" );
    m_soundEffects["explode1"].setBuffer( m_soundBuffer["explode1"] );
    m_soundEffects["explode1"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["explode2"].loadFromFile( "../Packages/ChristmasHorse2022/Audio/explode2.wav" );
    m_soundEffects["explode2"].setBuffer( m_soundBuffer["explode2"] );
    m_soundEffects["explode2"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["hit"].loadFromFile( "../Packages/ChristmasHorse2022/Audio/hit.wav" );
    m_soundEffects["hit"].setBuffer( m_soundBuffer["hit"] );
    m_soundEffects["hit"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["bullet"].loadFromFile( "../Packages/ChristmasHorse2022/Audio/bullet.wav" );
    m_soundEffects["bullet"].setBuffer( m_soundBuffer["bullet"] );
    m_soundEffects["bullet"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["snowball"].loadFromFile( "../Packages/ChristmasHorse2022/Audio/snowball.wav" );
    m_soundEffects["snowball"].setBuffer( m_soundBuffer["snowball"] );
    m_soundEffects["snowball"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_soundBuffer["smol-chicken"].loadFromFile( "../Packages/ChristmasHorse2022/Audio/smol-chimnken.wav" );
    m_soundEffects["smol-chicken"].setBuffer( m_soundBuffer["smol-chicken"] );
    m_soundEffects["smol-chicken"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

//    m_soundBuffers["walkies4"].loadFromFile( "../Packages/ChristmasHorse2021/Audio/spirit/GLOBSFX_SpiritClop2.wav" );
//    m_sounds["walkies4"].setBuffer( m_soundBuffers["walkies4"] );
//    m_sounds["walkies4"].setVolume( 50 );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2022_GameState::SetupUI()
{
    chalo::MenuManager::LoadCsvMenu( "ch2022_gameui.csv" );
}

void CH2022_GameState::Cleanup()
{
    CLASSNAME = std::string( typeid( *this ).name() );
    Logger::Out( "", "CH2022_GameState::Cleanup", "function-trace" );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    chalo::MenuManager::Cleanup();
    m_soundBuffer.clear();
    m_soundEffects.clear();
    m_allies.clear();
    m_enemies.clear();
    m_statusTexts.clear();
    m_unitNames.clear();
    m_unitCounts.clear();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2022_GameState::Update()
{
    chalo::InputManager::Update();
    chalo::MenuManager::Update();

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::Escape ) )// && !IsPaused() )
    {
        TogglePause( true );
    }


    if ( IsPaused() )
    {
//      Logger::OutFuncEnd( "paused", CLASSNAME + "::" + std::string( __func__ ) );

        std::string clickedButton = chalo::MenuManager::GetClickedButton();

        if ( IsPaused() && clickedButton == "btnPlay" )
        {
            TogglePause( false );
            chalo::MenuManager::LoadCsvMenu( "ch2022_gameui.csv" );
        }
        else if ( IsPaused() && clickedButton == "btnMainMenu" )
        {
            SetGotoState( "startupstate" );
        }


      return;
    }

    // Update characters (effects)
    for ( auto& ally : m_allies )
    {
        ally.Update();
    }

    if ( m_phase == GamePhase::BUILDING )
    {
        UpdateBuildingPhase();
    }
    else if ( m_phase == GamePhase::WAVERUN )
    {
        m_currentUnitBrush = 0;
        UpdateWavePhase();
    }
    else if ( m_phase == GamePhase::WAVEENDING )
    {
        m_currentUnitBrush = 0;
        if ( m_endWaveCooldown > 0 )
        {
            m_endWaveCooldown--;
        }

        if ( m_endWaveCooldown <= 0 && m_wave == m_waveMax )
        {
            YouWin();
        }
        else if ( m_endWaveCooldown <= 0 && !m_base.IsAlive() )
        {
            GameOver();
        }
        else if ( m_endWaveCooldown <= 0 && m_base.IsAlive() )
        {
            EndWave();
        }
    }
    else if ( m_phase == GamePhase::GAMELOST )
    {
        if ( chalo::InputManager::IsLeftClick() )
        {
            SetGotoState( "startupstate" );
        }
    }
    else if ( m_phase == GamePhase::GAMEWON )
    {
        if ( chalo::InputManager::IsLeftClick() )
        {
            SetGotoState( "ch2022_epiloguestate" );
        }
    }

    if ( m_phase != GamePhase::BUILDING )
    {
        m_currentUnitBrush = 0;
    }
}

void CH2022_GameState::UpdateBuildingPhase()
{
    if ( m_unitSpawnCounter > 0 )
    {
        m_unitSpawnCounter--;
    }

    sf::Vector2i mousePos = chalo::InputManager::GetMousePosition();

    sf::Vector2i gameAreaMouse = mousePos;
    if ( gameAreaMouse.y < m_playArea.top )                     { gameAreaMouse.y = m_playArea.top; }
    if ( gameAreaMouse.y > m_playArea.top + m_playArea.height ) { gameAreaMouse.y = m_playArea.top + m_playArea.height; }
    if ( gameAreaMouse.x < m_playArea.left )                    { gameAreaMouse.x = m_playArea.left; }
    if ( gameAreaMouse.x > m_playArea.left + m_playArea.width ) { gameAreaMouse.x = m_playArea.left + m_playArea.width; }
    m_brush.setPosition( sf::Vector2f( gameAreaMouse.x, gameAreaMouse.y ) );
    m_brushText.setPosition( sf::Vector2f( gameAreaMouse.x - 15, gameAreaMouse.y - 15 ) );

    if ( m_currentUnitBrush == 0 )
    {
        m_brushText.setString( "" );
    }
    else
    {
        m_brushText.setString( Helper::ToString( m_unitCounts[m_currentUnitBrush] ) );

        if ( m_unitCounts[m_currentUnitBrush] > 0 )
        {
            m_brushText.setFillColor( sf::Color::Green );
            m_brush.setColor( sf::Color::White );
        }
        else
        {
            m_brushText.setFillColor( sf::Color::Red );
            m_brush.setColor( sf::Color::Red );
        }
    }

    // Check button clicks
    std::string clickedButton = chalo::MenuManager::GetClickedButton();
    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" && clickedButton != "" )
    {
      Logger::OutValue( "Clicked button", "\"" + clickedButton + "\"", "CH2022_GameState::Update" );
    }
    if ( clickedButton == "btnNextWave" )
    {
        NextWave();
    }
    else if ( clickedButton == "btnSnowMoose" )
    {
        m_currentUnitBrush = 1;
    }
    else if ( clickedButton == "btnAntlerHorse" )
    {
        m_currentUnitBrush = 2;
    }
    else if ( clickedButton == "btnTowerHorse" )
    {
        m_currentUnitBrush = 3;
    }
    else if ( clickedButton == "btnChimnken" )
    {
        m_currentUnitBrush = 4;
    }

    bool mouseClick = chalo::InputManager::IsLeftClick();

    // Unit placement
    if ( mouseClick && m_currentUnitBrush != 0
        && m_unitSpawnCounter <= 0
        && mousePos.y >= m_playArea.top
        && mousePos.y <= m_playArea.top + m_playArea.height
        && mousePos.x >= m_playArea.left )
    {
        if ( m_unitCounts[m_currentUnitBrush] <= 0 )
        {
            // Not enough of this unit
            SetStatus( {
            "You are out of that kind of unit, Bepis!",
            "",
            "Try placing something else, or click NEXT WAVE to start."
            }, "dialog-drabis" );
        }
        else
        {
            // Put the unit here
            std::string unit = m_unitNames[ m_currentUnitBrush ];
            CH2022_Actor ally;

            int width = 64, height = 64;
            if ( unit == m_unitNames[4] )
            {
                width = 38;
                height = 46;
                ally.SetDamageAmount( -1 );
            }
            else
            {
                ally.SetDamageAmount( -5 );
            }

            ally.Setup( unit, chalo::TextureManager::Get( unit ), sf::IntRect( mousePos.x, mousePos.y, width, height ), 10 );
            ally.SetSpeed( 2 );
            m_allies.push_back( ally );
            m_unitSpawnCounter = m_unitSpawnCounterMax;
            m_unitCounts[m_currentUnitBrush]--;
        }
    }

    chalo::MenuManager::GetButton( "1", "btnSnowMoose" ).GetLabel().SetText( Helper::ToString( m_unitCounts[1] ) );
    chalo::MenuManager::GetButton( "1", "btnAntlerHorse" ).GetLabel().SetText( Helper::ToString( m_unitCounts[2] ) );
    chalo::MenuManager::GetButton( "1", "btnTowerHorse" ).GetLabel().SetText( Helper::ToString( m_unitCounts[3] ) );
    chalo::MenuManager::GetButton( "1", "btnChimnken" ).GetLabel().SetText( Helper::ToString( m_unitCounts[4] ) );
}

void CH2022_GameState::UpdateWavePhase()
{
    // Check for collisions
    for ( unsigned int e = 0; e < m_enemies.size(); e++ )
    {
        for ( unsigned int a = 0; a < m_allies.size(); a++ )
        {
            CH2022_Actor::CheckAllyEnemyCollision( m_allies[a], m_enemies[e] );
        }
    }

    std::list<int> deleteIndices;

    for ( unsigned int e = 0; e < m_enemies.size(); e++ )
    {
        m_enemies[e].Update( m_base, m_allies );

        if ( m_enemies[e].ReadyToShoot() )
        {
            SpawnActor( "bullet", m_enemies[e].GetPosition().left, m_enemies[e].GetPosition().top + 25 );
            m_enemies[e].DoneShooting();
            m_soundEffects["bullet"].play();
        }

        if ( m_enemies[e].GotHurt() )
        {
            m_soundEffects["hit"].play();
            m_enemies[e].DoneHurt();
        }

        if ( !m_enemies[e].IsAlive() )
        {
            deleteIndices.push_front( e );
        }
    }

    // Remove enemies that are dead
    for ( auto& index : deleteIndices )
    {
        m_enemies.erase( m_enemies.begin() + index );

        SetStatus( {
        "WAVE " + Helper::ToString( m_wave ) + "!",
        "Total enemies: " + Helper::ToString( GetEnemyCount() ),
        "Base health: " + Helper::ToString( m_base.GetHealth() ) + "/" + Helper::ToString( m_base.GetHealthMax() )
        }, "dialog-ratchet" );
    }

    deleteIndices.clear();
    for ( unsigned int a = 0; a < m_allies.size(); a++ )
    {
        m_allies[a].Update( m_base, m_enemies );

        if ( m_allies[a].ReadyToShoot() )
        {
            SpawnActor( "snowball", m_allies[a].GetPosition().left, m_allies[a].GetPosition().top + 25 );
            m_allies[a].DoneShooting();
            m_soundEffects["snowball"].play();
        }

        if ( m_allies[a].GotHurt() )
        {
            if ( m_allies[a].GetName() == "ally-chimnken" && m_allies[a].IsAlive() )
            {
                m_soundEffects["smol-chicken"].play();
                SpawnActor( "big-chimnken", 1280, m_allies[a].GetPosition().top + 25 );
                m_allies[a].DoneHurt();
                m_allies[a].UpdateHealth( 0 );
            }
            else
            {
                m_soundEffects["hit"].play();
                m_allies[a].DoneHurt();
            }
        }

        if ( !m_allies[a].IsAlive() )
        {
            deleteIndices.push_front( a );
        }
    }

    // Remove allies that are dead
    for ( auto& index : deleteIndices )
    {
        if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::OutIntValue( "Remove ally index", index, "CH2022_GameState::Update" ); }
        m_allies.erase( m_allies.begin() + index );
    }

    if ( m_base.GotHurt() )
    {
        m_soundEffects["explode1"].play();
        m_base.DoneHurt();
    }

    // Are all the enemies gone?
    // Is the base dead?
    if ( m_base.IsAlive() == false )
    {
        m_soundEffects["explode2"].play();
        m_endWaveCooldown = m_endWaveCooldownMax * 2;
        m_phase = GamePhase::WAVEENDING;

        SetStatus( {
        "NO! OUR BASE!",
        "IT'S BEEN DESTROYED!",
        ""
        }, "dialog-drabis" );
    }
    else if ( GetEnemyCount() == 0 )
    {
        m_soundEffects["success"].play();

        m_endWaveCooldown = m_endWaveCooldownMax;
        m_phase = GamePhase::WAVEENDING;

        if ( m_wave >= m_waveMax )
        {
            // Win game
            SetStatus( {
            "...We got them all?...",
            "",
            "...I think we did it...!"
            }, "dialog-webeka" );
        }
        else
        {
            SetStatus( {
            "You got 'em all!",
            "Time to check how many units we still have...",
            ""
            }, "dialog-drabis" );

            for ( auto& ally : m_allies )
            {
                if ( ally.GetName() == "snowball" || ally.GetName() == "big-chimnken" ) { continue; }
                ally.SetStatusText( "+1" );
            }
        }
    }
}

int CH2022_GameState::GetEnemyCount()
{
    int enemyCount = 0;
    for ( auto& enemy : m_enemies )
    {
        if ( enemy.GetName() != "bullet" )
        {
            enemyCount++;
        }
    }
    return enemyCount;
}

void CH2022_GameState::EndWave()
{
    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::OutIntValue( "END WAVE ", m_wave, "CH2022_GameState::EndWave" ); }
    SetStatus( {
        "Time to prepare for the next wave!",
        "We've added additional units. Make sure to place then and",
        "then press [NEXT WAVE] to deal with the next attack!"
    }, "dialog-webeka" );
    m_phase = GamePhase::BUILDING;
    chalo::MenuManager::GetButton( "main", "btnNextWave" ).GetLabel().SetText( "Next wave" );
    chalo::MenuManager::GetLabel( "main", "lblNextWave" ).SetText( "WAVE " + Helper::ToString( m_wave+1 ) );

    for ( auto& ally : m_allies )
    {
        ally.SetStatusText( "" );
    }

    std::list<int> deleteIndices;
    // "Refund" all living allies
    for ( unsigned int a = 0; a < m_allies.size(); a++ )
    {
        for ( auto& unit : m_unitNames )
        {
            if ( m_allies[a].GetName() == unit.second ) { m_unitCounts[ unit.first ]++; }
        }

        if ( m_allies[a].GetName() != "base" )
        {
            deleteIndices.push_front( a );
        }
    }

    // Remove allies
    for ( auto& index : deleteIndices )
    {

    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::OutIntValue( "Remove ally index", index, "CH2022_GameState::Update" ); }
        m_allies.erase( m_allies.begin() + index );
    }

    m_enemies.clear();

    // Add additional units for each wave
    if ( m_wave == 1 )
    {
        m_unitCounts[1] = 5;
        m_unitCounts[2] = 2;
    }
    else if ( m_wave == 2 )
    {
        m_unitCounts[1] = 5;
        m_unitCounts[2] = 2;
        m_unitCounts[3] = 1;
    }
    else if ( m_wave == 3 )
    {
        m_unitCounts[1] = 5;
        m_unitCounts[2] = 2;
        m_unitCounts[3] = 1;
        m_unitCounts[4] = 2;
    }
    else
    {
        if ( m_wave % 2 == 0 )
        {
            m_unitCounts[3] += 1;
        }
        m_unitCounts[1] += 2;
        m_unitCounts[2] += 1;
        m_unitCounts[4] += 1;
    }

    if      ( m_unitCounts[1] < 5 ) { m_unitCounts[1] = 5; }
    else if ( m_unitCounts[2] < 2 ) { m_unitCounts[2] = 2; }
    else if ( m_unitCounts[3] < 2 ) { m_unitCounts[3] = 2; }
    else if ( m_unitCounts[4] < 2 ) { m_unitCounts[4] = 2; }
}

void CH2022_GameState::GameOver()
{
    m_phase = GamePhase::GAMELOST;
}

void CH2022_GameState::YouWin()
{
    m_phase = GamePhase::GAMEWON;
}

void CH2022_GameState::NextWave()
{
    m_brushText.setString( "" );
    m_wave++;
    m_currentUnitBrush = 0;
    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::OutIntValue( "START WAVE ", m_wave, "CH2022_GameState::NextWave" ); }
    m_phase = GamePhase::WAVERUN;
    chalo::MenuManager::GetButton( "main", "btnNextWave" ).GetLabel().SetText( "In progress..." );
    chalo::MenuManager::GetLabel( "main", "lblNextWave" ).SetText( "WAVE " + Helper::ToString( m_wave ) );

    // PLAY AREA Y: 150 - 420
    if ( m_wave == 1 )
    {
        SpawnActor( "bomb", -200, 400 );
        SpawnActor( "bomb", -200, 300 );
        SpawnActor( "bomb", -500, 500 );
        SpawnActor( "bomb", -500, 200 );
    }
    else if ( m_wave == 2 )
    {
        SpawnActor( "bomb", -200, 400 );
        SpawnActor( "bomb", -200, 300 );

        SpawnActor( "sniper", -100, 500 );
        SpawnActor( "sniper", -100, 200 );
    }
    else if ( m_wave == 3 )
    {
        int y = m_playArea.top;
        for ( int i = 0; i < 4; i++ )
        {
            SpawnActor( "bomb", -250, y );
            y += 128;
        }

        y = m_playArea.top;
        for ( int i = 0; i < 4; i++ )
        {
            SpawnActor( "bomb", -1000, y );
            y += 128;
        }
    }
    else if ( m_wave == 4 )
    {
        SpawnActor( "bomb", -200, 150 );
        SpawnActor( "bomb", -200, 550 );

        SpawnActor( "bomb", -300, 175 );
        SpawnActor( "bomb", -300, 525 );

        SpawnActor( "bomb", -400, 200 );
        SpawnActor( "bomb", -400, 500 );

        SpawnActor( "bomb", -1000, 300 );
        SpawnActor( "bomb", -1000, 400 );

        SpawnActor( "bomb", -1500, 150 );
        SpawnActor( "bomb", -1500, 550 );

        SpawnActor( "bomb", -1600, 175 );
        SpawnActor( "bomb", -1600, 525 );

        SpawnActor( "bomb", -1700, 200 );
        SpawnActor( "bomb", -1700, 500 );
    }
    else if ( m_wave == 5 )
    {
        SpawnActor( "bomb", -200, 150 );
        SpawnActor( "bomb", -200, 500 );

        SpawnActor( "sniper", -500, 400 );
        SpawnActor( "sniper", -500, 300 );

        SpawnActor( "bomb", -1000, 200 );
        SpawnActor( "bomb", -750, 300 );
        SpawnActor( "bomb", -750, 400 );
        SpawnActor( "bomb", -1000, 500 );
    }
    else if ( m_wave == 6 )
    {
        SpawnActor( "sniper", -200, 300 );
        SpawnActor( "sniper", -200, 400 );

        SpawnActor( "bomb", -500, 350 );

        SpawnActor( "sniper", -1000, 150 );
        SpawnActor( "sniper", -1000, 500 );

        SpawnActor( "bomb", -1000, 350 );

        SpawnActor( "sniper", -1500, 300 );
        SpawnActor( "sniper", -1500, 400 );
    }
    else if ( m_wave == 7 )
    {
        SpawnActor( "bomb", -750, 200 );
        SpawnActor( "bomb", -750, 300 );
        SpawnActor( "bomb", -1250, 400 );
        SpawnActor( "bomb", -1250, 500 );

        SpawnActor( "sniper", -500, 150 );

        SpawnActor( "sniper", -1000, 300 );

        SpawnActor( "sniper", -1500, 500 );
    }

    else if ( m_wave == 8 )
    {
        SpawnActor( "bomb", -500, 500 );
        SpawnActor( "bomb", -500, 100 );
        SpawnActor( "sniper", -500, 300 );

        SpawnActor( "bomb", -1500, 500 );
        SpawnActor( "bomb", -1500, 100 );
        SpawnActor( "sniper", -1500, 400 );

        SpawnActor( "bomb", -2500, 300 );
        SpawnActor( "bomb", -2500, 400 );
        SpawnActor( "sniper", -2500, 500 );
    }

    else if ( m_wave == 9 )
    {
        // update
        SpawnActor( "bomb", -500, 500 );
        SpawnActor( "bomb", -500, 100 );
        SpawnActor( "sniper", -500, 300 );
        SpawnActor( "sniper", -500, 400 );

        SpawnActor( "bomb", -1500, 500 );
        SpawnActor( "bomb", -1500, 100 );
        SpawnActor( "sniper", -1500, 300 );
        SpawnActor( "sniper", -1500, 400 );

        SpawnActor( "bomb", -2500, 300 );
        SpawnActor( "bomb", -2500, 400 );
        SpawnActor( "sniper", -2500, 100 );
        SpawnActor( "sniper", -2500, 500 );
    }

    else if ( m_wave == 10 )
    {
        // update
        SpawnActor( "bomb", -400, 500 );
        SpawnActor( "bomb", -400, 100 );
        SpawnActor( "sniper", -500, 500 );
        SpawnActor( "sniper", -500, 100 );

        SpawnActor( "bomb", -1400, 300 );
        SpawnActor( "bomb", -1400, 400 );
        SpawnActor( "sniper", -1500, 300 );
        SpawnActor( "sniper", -1500, 400 );

        SpawnActor( "bomb", -2400, 100 );
        SpawnActor( "bomb", -2400, 200 );
        SpawnActor( "bomb", -2400, 400 );
        SpawnActor( "bomb", -2400, 500 );
        SpawnActor( "sniper", -2500, 100 );
        SpawnActor( "sniper", -2500, 200 );
        SpawnActor( "sniper", -2500, 400 );
        SpawnActor( "sniper", -2500, 500 );
    }

    SetStatus( {
    "WAVE " + Helper::ToString( m_wave ) + "!",
    "Total enemies: " + Helper::ToString( m_enemies.size() ),
    "Base health: " + Helper::ToString( m_base.GetHealth() ) + "/" + Helper::ToString( m_base.GetHealthMax() )
    }, "dialog-ratchet" );
}

void CH2022_GameState::SetStatus( const std::vector<std::string>& texts, string image )
{
    m_statusTexts.clear();

    sf::Text sfText;
    sfText.setFont( chalo::FontManager::Get( "main" ) );
    sfText.setPosition( 150, 35 );
    sfText.setOutlineThickness( 2 );
    sfText.setOutlineColor( sf::Color::Black );
    sfText.setFillColor( sf::Color::White );
    sfText.setScale( 0.8, 0.8 );

    m_statusImage.setPosition( 10, 10 );
    m_statusImage.setScale( 0.9, 0.9 );
    m_statusName.setFont( chalo::FontManager::Get( "main" ) );
    m_statusName.setPosition( 150, 5 );
    m_statusName.setOutlineThickness( 2 );
    m_statusName.setScale( 0.7, 0.7 );
    m_statusName.setOutlineColor( sf::Color::Black );
    m_statusName.setFillColor( sf::Color::White );

    int x = 150, y = 35, inc = 30;

    for ( auto& text : texts )
    {
        sfText.setString( text );
        sfText.setPosition( x, y );
        y += inc;
        m_statusTexts.push_back( sfText );
    }

    m_statusImage.setTexture( chalo::TextureManager::Get( image ) );

    if      ( image == "dialog-webeka" ) { m_statusName.setString( "Webeka" ); }
    else if ( image == "dialog-sneezy" ) { m_statusName.setString( "Sneezy" ); }
    else if ( image == "dialog-ratchet" ) { m_statusName.setString( "Ratchet" ); }
    else if ( image == "dialog-drabis" ) { m_statusName.setString( "Drabis" ); }
}

void CH2022_GameState::SpawnActor( string type, sf::IntRect position )
{
    SpawnActor( type, position.left, position.top );
}

void CH2022_GameState::SpawnActor( string type, int x, int y )
{
    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::OutValue( "Spawn", type, "CH2022_GameState::SpawnActor" ); }
    if ( type == "bomb" )
    {
        CH2022_Actor enemy;
        enemy.Setup( "bomb", chalo::TextureManager::Get( "peppermint-bomb" ), sf::IntRect( x, y, 64, 64 ), 5 );
        enemy.SetSpeed( 5 );
        enemy.SetDamageAmount( -5 );
        m_enemies.push_back( enemy );
    }
    else if ( type == "sniper" )
    {
        CH2022_Actor enemy;
        enemy.Setup( "sniper", chalo::TextureManager::Get( "sniper-elf" ), sf::IntRect( x, y, 64, 64 ), 5 );
        enemy.SetSpeed( 3 );
        enemy.SetDamageAmount( -5 );
        m_enemies.push_back( enemy );
    }
    else if ( type == "bullet" )
    {
        CH2022_Actor enemy;
        enemy.Setup( "bullet", chalo::TextureManager::Get( "bullet" ), sf::IntRect( x+16, y, 16, 16 ), 1 );
        enemy.SetSpeed( 6 );
        enemy.SetDamageAmount( -5 );
        m_enemies.push_back( enemy );
    }
    else if ( type == "snowball" )
    {
        CH2022_Actor actor;
        actor.Setup( "snowball", chalo::TextureManager::Get( "snowball" ), sf::IntRect( x+16, y, 32, 32 ), 1 );
        actor.SetSpeed( -4 );
        actor.SetDamageAmount( -5 );
        m_allies.push_back( actor );
    }
    else if ( type == "big-chimnken" )
    {
        CH2022_Actor actor;
        actor.Setup( "big-chimnken", chalo::TextureManager::Get( "big-chimnken" ), sf::IntRect( x, y, 64, 64 ), 10 );
        actor.SetSpeed( -8 );
        actor.SetDamageAmount( -10 );
        m_allies.push_back( actor );
    }
}

void CH2022_GameState::Draw( sf::RenderWindow& window )
{
    window.draw( m_background );

    if ( m_phase == GamePhase::GAMELOST )
    {
        sf::RectangleShape bg;
        bg.setSize( sf::Vector2f( 1280, 720 ) );
        bg.setFillColor( sf::Color::Black );
        bg.setPosition( 0, 0 );
        window.draw( bg );

        sf::Text text;
        text.setFont( chalo::FontManager::Get( "main" ) );
        text.setFillColor( sf::Color::White );
        text.setPosition( 375, 250 );
        text.setScale( 4, 4 );
        text.setString( "GAME OVER" );
        window.draw( text );
    }
    else
    {
        chalo::DrawManager::AddSprite( m_base.GetSprite() );
        m_base.Draw( window );

        for ( auto& ally : m_allies )
        {
            if ( ally.IsAlive() )
            {
                chalo::DrawManager::AddSprite( ally.GetSprite() );
                ally.Draw( window );
            }
        }

        for ( auto& enemy : m_enemies )
        {
            if ( enemy.IsAlive() )
            {
                chalo::DrawManager::AddSprite( enemy.GetSprite() );
                enemy.Draw( window );
            }
        }

        window.draw( m_statusBackground );
        window.draw( m_statusName );
        window.draw( m_timerText );
        window.draw( m_statusImage );
        window.draw( m_brushText );
        for ( auto& text : m_statusTexts )
        {
            window.draw( text );
        }

        // HUD
        chalo::DrawManager::AddMenu();
        chalo::DrawManager::Draw( window );

        // Draw ghost cursor
        if ( m_currentUnitBrush != 0 )
        {
            window.draw( m_brush );
            m_brush.setTexture( chalo::TextureManager::Get( m_unitNames[ m_currentUnitBrush ] ) );
        }


    }

    EscapeyState::Draw( window );

}
