#ifndef _CH2020_GAME_STATE2
#define _CH2020_GAME_STATE2

#include <SFML/Audio.hpp>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/DEPRECATED/Managers/DrawManager.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../ProgramBase/EscapeyState.hpp"

#include <vector>

class CH2020_GameState2 : public EscapeyState//public chalo::IState
{
public:
    CH2020_GameState2();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    std::string CLASSNAME;
    void GetHurt();
    void Shoot( int index );
    void UpdatePlayerPos();
    void Accelerate( int x, int y );
    void Deaccelerate();

    float m_velocityMax;
    float m_accelerationMax;
    sf::Vector2f m_velocity;
    sf::Vector2f m_acceleration;

    std::vector<chalo::GameObject> m_carrots;
    std::vector<chalo::GameObject> m_stars;
    chalo::Character m_player;
    sf::RectangleShape m_background;
    sf::CircleShape m_barrier;
    int m_score;
    std::vector<chalo::UILabel> m_scoreText;
    std::vector<chalo::UILabel> m_levelText;
    std::vector<chalo::UILabel> m_livesText;
    std::vector<chalo::Character> m_baddies;
    std::vector<float> m_baddiesSpeeds;
    std::vector<int> m_baddiesTypes;
    std::vector<chalo::GameObject> m_bullets;
    float m_carrotSpeed;
    int m_lives;
    int m_hitTimer;
    int m_hitTimerMax;
    int m_carrotCounter;

    sf::SoundBuffer m_collectSoundBuffer;
    sf::Sound m_collectSound;
    sf::SoundBuffer m_walkSoundBuffer;
    sf::Sound m_walkSound;
    sf::SoundBuffer m_hurtSoundBuffer;
    sf::Sound m_hurtSound;
    int m_walkSoundCooldown;
    int m_walkSoundCooldownMax;
    sf::Music m_music;

    bool m_instructions;
    sf::RectangleShape m_insBg;
    std::vector<sf::Text> m_insText;
};

#endif
