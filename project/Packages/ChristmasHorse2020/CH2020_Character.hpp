#ifndef _CH2020_CHARACTER
#define _CH2020_CHARACTER

#include <SFML/Graphics.hpp>

#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GravityHandler.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"

#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Enums/Types.hpp"

#include <string>
#include <iostream>


class CH2020_Character : public chalo::GameObject
{
public:
    CH2020_Character();

    void Update();
    const sf::Sprite& GetSprite() const;

    void SetSpeed( int speed );
    void SetDirection( chalo::Direction direction );
    void SetValidPositionRegion( sf::IntRect rect );
    chalo::Direction GetDirection() const;
    chalo::CharacterType GetCharacterType() const;
    sf::IntRect GetValidPositionRegion() const;
    void RestrictMovement();
    void SetRestrictMovement( bool value );

    void SetAnimationInformation( int maxFrames, float animationSpeed );

//    void Move( chalo::Direction direction, const Map::ReadableMap& gameMap );
    void Move( chalo::Direction direction );
    sf::IntRect GetDesiredPosition( chalo::Direction direction );

    void ChangeDirection( int y );
    void Animate();
    void ForceSpriteUpdate();
    chalo::GravityHandler gravity;

protected:
    void MoveClipping( chalo::Direction direction );
    void BeginAttack();
    void EndAttack();

    chalo::Direction m_direction;
    int m_speed;
    float m_animationFrame;
    float m_maxFrames;
    float m_animationSpeed;
    sf::IntRect m_validPositionRegion;
    bool m_restrictMovement;

    chalo::SheetAction m_sheetAction;
    float m_sheetActionTimer;

    chalo::Action m_stateAction;

    chalo::CharacterType m_characterType;
};


#endif
