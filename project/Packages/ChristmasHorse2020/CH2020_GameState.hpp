#ifndef _CH2020_GAME_STATE
#define _CH2020_GAME_STATE

#include <SFML/Audio.hpp>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
#include "../ProgramBase/EscapeyState.hpp"

#include <vector>

class CH2020_GameState : public EscapeyState//public chalo::IState
{
public:
    CH2020_GameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    std::string CLASSNAME;
    std::vector<chalo::GameObject> m_carrots;
    chalo::Character m_player;
    sf::Sprite m_background1;
    sf::Sprite m_background2;
    int m_score;
    std::vector<chalo::UILabel> m_scoreText;
    std::vector<chalo::UILabel> m_levelText;

    sf::SoundBuffer m_collectSoundBuffer;
    sf::Sound m_collectSound;
    sf::SoundBuffer m_walkSoundBuffer;
    sf::Sound m_walkSound;
    sf::Music m_music;
    int m_walkSoundCooldown;
    int m_walkSoundCooldownMax;
    bool m_instructions;
    sf::RectangleShape m_insBg;
    std::vector<sf::Text> m_insText;
};

#endif
