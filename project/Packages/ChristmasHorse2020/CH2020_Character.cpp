#include "CH2020_Character.hpp"

#include "../../chalo-engine/Managers/ConfigManager.hpp"

CH2020_Character::CH2020_Character() : GameObject()
{
    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Initializing Character", "Character::Character" ); }
    m_speed = 5;
    m_direction = chalo::SOUTH;
    m_maxFrames = 5;
    m_animationFrame = 0;
    m_animationSpeed = 0.15;
    m_sheetAction = chalo::WALKING;
    m_restrictMovement = false;
}

void CH2020_Character::SetSpeed( int speed )
{
    m_speed = speed;
}

void CH2020_Character::Update()
{
    GameObject::Update();
    if ( m_sheetAction == chalo::ATTACKING )
    {
        Animate();
    }

    // Update direction
    sf::IntRect frame;
    m_textureCoordinates.left = int( m_animationFrame ) * m_textureCoordinates.width;
    m_textureCoordinates.top = m_direction * m_textureCoordinates.height + ( m_sheetAction * 640 ); // TODO: Change hard-coded sprite sheet height...
    m_sprite.setTextureRect( m_textureCoordinates );

    // Check for gravity
    if ( gravity.GetIsFalling() )
    {
        // Fall!
        m_position.y -= gravity.GetGravity();
    }

    m_sprite.setPosition( m_position );
}

sf::IntRect CH2020_Character::GetDesiredPosition( chalo::Direction direction )
{
    sf::IntRect desiredNewPosition;
    desiredNewPosition.left = m_position.x;
    desiredNewPosition.top = m_position.y;
    desiredNewPosition.width = m_textureCoordinates.width;
    desiredNewPosition.height = m_textureCoordinates.height;

    if          ( direction == chalo::WEST )
    {
        desiredNewPosition.left -= m_speed;
    }
    else if     ( direction == chalo::EAST )
    {
        desiredNewPosition.left += m_speed;
    }

    if          ( direction == chalo::NORTH )
    {
        desiredNewPosition.top -= m_speed;
    }
    else if     ( direction == chalo::SOUTH )
    {
        desiredNewPosition.top += m_speed;
    }

    return desiredNewPosition;
}

//void CH2020_Character::Move( chalo::Direction direction, const Map::ReadableMap& gameMap  )
//{
//    if ( m_sheetAction == ATTACKING )
//    {
//        // Can't move
//        return;
//    }
//
//    m_stateAction = MOVING;
//    m_direction = direction;
//
//    sf::IntRect desiredNewPosition;
//    desiredNewPosition.left = m_position.x;
//    desiredNewPosition.top = m_position.y;
//    desiredNewPosition.width = m_textureCoordinates.width;
//    desiredNewPosition.height = m_textureCoordinates.height;
//
//    if          ( direction == WEST )
//    {
//        desiredNewPosition.left -= m_speed;
//    }
//    else if     ( direction == EAST )
//    {
//        desiredNewPosition.left += m_speed;
//    }
//
//    if          ( direction == NORTH )
//    {
//        desiredNewPosition.top -= m_speed;
//    }
//    else if     ( direction == SOUTH )
//    {
//        desiredNewPosition.top += m_speed;
//    }
//
//    // Only move if it doesn't collide with a collidable tile.
//    // Pass in the new position AND the collision region for object vs. map
//    if ( !gameMap.IsCollision( desiredNewPosition, m_mapCollisionRectangle ) )
//    {
//        m_position.x = desiredNewPosition.left;
//        m_position.y = desiredNewPosition.top;
//    }
//
//    Animate();
//}

void CH2020_Character::Move( chalo::Direction direction )
{
    if ( m_sheetAction == chalo::ATTACKING )
    {
        // Can't move
        return;
    }

    m_stateAction = chalo::MOVING;
    m_direction = direction;

    sf::IntRect desiredNewPosition;
    desiredNewPosition.left = m_position.x;
    desiredNewPosition.top = m_position.y;
    desiredNewPosition.width = m_textureCoordinates.width;
    desiredNewPosition.height = m_textureCoordinates.height;

    if          ( direction == chalo::WEST )
    {
        desiredNewPosition.left -= m_speed;
    }
    else if     ( direction == chalo::EAST )
    {
        desiredNewPosition.left += m_speed;
    }

    if          ( direction == chalo::NORTH )
    {
        desiredNewPosition.top -= m_speed;
    }
    else if     ( direction == chalo::SOUTH )
    {
        desiredNewPosition.top += m_speed;
    }

    m_position.x = desiredNewPosition.left;
    m_position.y = desiredNewPosition.top;

    RestrictMovement();

    Animate();
}

void CH2020_Character::RestrictMovement()
{
    if ( m_restrictMovement )
    {
        if      ( m_position.x < m_validPositionRegion.left )
        {
            m_position.x = m_validPositionRegion.left;
        }
        else if ( m_position.x + (m_textureCoordinates.width * m_scale.x) > m_validPositionRegion.left + m_validPositionRegion.width )
        {
            m_position.x = m_validPositionRegion.left + m_validPositionRegion.width - (m_textureCoordinates.width * m_scale.x);
        }

        if      ( m_position.y < m_validPositionRegion.top )
        {
            m_position.y = m_validPositionRegion.top;
        }
        else if ( m_position.y + (m_textureCoordinates.height * m_scale.y) > m_validPositionRegion.top + m_validPositionRegion.height )
        {
            m_position.y = m_validPositionRegion.top + m_validPositionRegion.height - (m_textureCoordinates.height * m_scale.y);
        }
    }
}

void CH2020_Character::SetRestrictMovement( bool value )
{
    m_restrictMovement = value;
}

void CH2020_Character::MoveClipping( chalo::Direction direction )
{
    // This one ignores hitting walls, such as for projectiles.
    m_stateAction = chalo::MOVING;
    m_direction = direction;

    sf::IntRect desiredNewPosition;
    desiredNewPosition.left = m_position.x;
    desiredNewPosition.top = m_position.y;
    desiredNewPosition.width = m_textureCoordinates.width;
    desiredNewPosition.height = m_textureCoordinates.height;

    if          ( direction == chalo::WEST )
    {
        desiredNewPosition.left -= m_speed;
    }
    else if     ( direction == chalo::EAST )
    {
        desiredNewPosition.left += m_speed;
    }

    if          ( direction == chalo::NORTH )
    {
        desiredNewPosition.top -= m_speed;
    }
    else if     ( direction == chalo::SOUTH )
    {
        desiredNewPosition.top += m_speed;
    }

    m_position.x = desiredNewPosition.left;
    m_position.y = desiredNewPosition.top;

    Animate();
}

void CH2020_Character::Animate()
{
    m_animationFrame += m_animationSpeed;
    if ( m_animationFrame >= m_maxFrames )
    {
        m_animationFrame = 0;

        if ( m_sheetAction == chalo::ATTACKING )
        {
            EndAttack();
        }
    }
    ForceSpriteUpdate();
}

void CH2020_Character::ForceSpriteUpdate()
{
    m_sprite.setPosition( m_position );
    sf::Vector2f dim = GetDimensions();
    m_textureCoordinates.left = int( m_animationFrame ) * dim.x;
    m_sprite.setTextureRect( m_textureCoordinates );
//    m_sprite.setRotation( m_angle );
}

const sf::Sprite& CH2020_Character::GetSprite() const
{
    return m_sprite;
}

void CH2020_Character::BeginAttack()
{
    if ( m_sheetAction == chalo::WALKING )
    {
        m_animationFrame = 0;
        // This relates to location/offset on sprite sheet
        m_sheetAction = chalo::ATTACKING;
        m_sheetActionTimer = 10;
        // This relates to info being passed to GameState to make game logic decisions
        m_stateAction = chalo::BEGIN_ATTACK;
    }
}

void CH2020_Character::EndAttack()
{
    m_sheetAction = chalo::WALKING;
    m_sheetActionTimer = 0;
    m_stateAction = chalo::IDLE;
}

void CH2020_Character::SetDirection( chalo::Direction direction )
{
    m_direction = direction;
}

chalo::Direction CH2020_Character::GetDirection() const
{
    return m_direction;
}

void CH2020_Character::ChangeDirection( int y ) // specifically for Christmas Horse 2
{
    m_textureCoordinates.top = y;
}

void CH2020_Character::SetAnimationInformation( int maxFrames, float animationSpeed )
{
    m_maxFrames = maxFrames;
    m_animationSpeed = animationSpeed;
}

chalo::CharacterType CH2020_Character::GetCharacterType() const
{
    return m_characterType;
}

// m_validPositionRegion

void CH2020_Character::SetValidPositionRegion( sf::IntRect rect )
{
    m_validPositionRegion = rect;
}

sf::IntRect CH2020_Character::GetValidPositionRegion() const
{
    return m_validPositionRegion;
}

