#include "CH2020_EpilogueState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"

CH2020_EpilogueState::CH2020_EpilogueState()
{
    CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2020_EpilogueState::Init( const std::string& name )
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Init( name );
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_EpilogueState::Setup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    IState::Setup();

    chalo::TextureManager::Add( "background1", "../Packages/ChristmasHorse2020/Graphics/Backgrounds/endscene.png" );

    m_background1.setTexture( chalo::TextureManager::Get( "background1" ) );

//    chalo::InputManager::Setup();

    chalo::UILabel text;

    int x = 100;
    int y = chalo::Application::GetScreenHeight();
    int ft = 50;

    // void Setup( const std::string& key, const std::string& fontName, int characterSize, sf::Color fillColor, sf::Vector2f position, std::string text );
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "SANTA IS DEFEATED." );             m_text.push_back( text );   y += 100;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "YOU HAVE FREED THE WORLD" );       m_text.push_back( text );   y += 50;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "FROM SAINT NICK'S TYRANNY" );      m_text.push_back( text );   y += 50;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "AND JUDGEMENT." );                 m_text.push_back( text );   y += 100;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "YOU ARE THE NEW SANTA CLAUS." );   m_text.push_back( text );   y += 100;
    text.Setup( "lives", "main", ft, sf::Color::White, sf::Vector2f( x, y ), "NOW WHAT WILL YOU DO WITH YOUR NEW POWER?" );   m_text.push_back( text );   y += 100;

    m_music.openFromFile( "../Packages/ChristmasHorse2020/Audio/level1.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_EpilogueState::Cleanup()
{
    Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
    m_music.stop();
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    m_text.clear();
    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2020_EpilogueState::Update()
{
    chalo::InputManager::Update();

    if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Escape ) || chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::Enter ) )
    {
        SetGotoState( "startupstate" );
    }

    for ( auto & text : m_text )
    {
        sf::Vector2f pos = text.GetPosition();
        pos.y -= 1;
        text.SetPosition( pos );
    }
}

void CH2020_EpilogueState::Draw( sf::RenderWindow& window )
{
    window.draw( m_background1 );

    for ( auto & text : m_text )
    {
        window.draw( text.GetText() );
    }
}



