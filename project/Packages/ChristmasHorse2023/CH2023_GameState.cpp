#include "CH2023_GameState.hpp"

#include "../../chalo-engine/Managers/MenuManager.hpp"
#include "../../chalo-engine/Managers/InputManager.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"
#include "../../chalo-engine/Managers/AudioManager.hpp"
#include "../../chalo-engine/Utilities/Messager.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

#include <list>

/*
- Bug: Office door is already open without a keycard?
- Bug: Fix options menu
- Clean up what gets printed in the logger
- Add sound/music volume to pause menu
*/

CH2023_GameState::CH2023_GameState()
{
  CLASSNAME = std::string( typeid( *this ).name() );
}

void CH2023_GameState::Init( const std::string& name )
{
    Logger::Out( "Parameters - name: " + name, "CH2023_GameState::Init", "function-trace" );
    IState::Init( name );
}

void CH2023_GameState::Setup()
{
    Logger::OutFuncBegin( "Begin setup", CLASSNAME + "::" + std::string( __func__ ) );
    m_debugTimer = 0;
    m_endOfGame = false;

    IState::Setup();
    EscapeyState::Setup();
    std::string gfxPath = "../Packages/ChristmasHorse2022/Graphics/Game/";
    std::string sndPath = "../Packages/ChristmasHorse2022/Audio/";

    chalo::TextureManager::Add( "hud1",                         "../Packages/ChristmasHorse2021/Graphics/UI/hud1.png" );
    chalo::TextureManager::Add( "hud2",                         "../Packages/ChristmasHorse2021/Graphics/UI/hud2.png" );
    chalo::TextureManager::Add( "bepis-portrait",               "../Packages/ChristmasHorse2021/Graphics/bepis-portrait-200.png" );
    chalo::TextureManager::Add( "horse",                        "../Packages/ChristmasHorse2023/Graphics/characters/horse-sheet.png" );

    chalo::TextureManager::Add( "background",                   "../Packages/ChristmasHorse2023/Graphics/backgrounds/holding-cell-2.png" );

    chalo::TextureManager::Add( "closed-door-east",             "../Packages/ChristmasHorse2023/Graphics/items/closed-door-east.png" );
    chalo::TextureManager::Add( "closed-door-south-festive",    "../Packages/ChristmasHorse2023/Graphics/items/closed-door-south-festive.png" );
    chalo::TextureManager::Add( "closed-door-west-office",      "../Packages/ChristmasHorse2023/Graphics/items/closed-door-west-office.png" );
    chalo::TextureManager::Add( "closed-door-west-outside",     "../Packages/ChristmasHorse2023/Graphics/items/closed-door-west-outside.png" );
    chalo::TextureManager::Add( "closed-door-south-outside",    "../Packages/ChristmasHorse2023/Graphics/items/closed-door-south-outside.png" );
    chalo::TextureManager::Add( "closed-door-south-conference", "../Packages/ChristmasHorse2023/Graphics/items/closed-door-south-conference.png" );
    chalo::TextureManager::Add( "bathroom-sign-east",           "../Packages/ChristmasHorse2023/Graphics/items/bathroom-sign-east.png" );
    chalo::TextureManager::Add( "fat-chicken",                  "../Packages/ChristmasHorse2023/Graphics/items/fat-chicken.png" );
    chalo::TextureManager::Add( "bulletin-board",               "../Packages/ChristmasHorse2023/Graphics/items/bulletin-board.png" );
    chalo::TextureManager::Add( "coffee-pot",                   "../Packages/ChristmasHorse2023/Graphics/items/coffee-pot.png" );
    chalo::TextureManager::Add( "coffee",                       "../Packages/ChristmasHorse2023/Graphics/items/coffee.png" );
    chalo::TextureManager::Add( "software-elf",                 "../Packages/ChristmasHorse2023/Graphics/items/software-elf.png" );
    chalo::TextureManager::Add( "key2",                         "../Packages/ChristmasHorse2023/Graphics/items/key2.png" );
    chalo::TextureManager::Add( "keycard",                      "../Packages/ChristmasHorse2023/Graphics/items/keycard.png" );
    chalo::TextureManager::Add( "chicken2",                     "../Packages/ChristmasHorse2023/Graphics/items/chicken2.png" );
    chalo::TextureManager::Add( "chicken3",                     "../Packages/ChristmasHorse2023/Graphics/items/chicken3.png" );
    chalo::TextureManager::Add( "chicken4",                     "../Packages/ChristmasHorse2023/Graphics/items/chicken4.png" );
    chalo::TextureManager::Add( "chicken5",                     "../Packages/ChristmasHorse2023/Graphics/items/chicken5.png" );
    chalo::TextureManager::Add( "gift1",                        "../Packages/ChristmasHorse2023/Graphics/items/gift1.png" );
    chalo::TextureManager::Add( "gift2",                        "../Packages/ChristmasHorse2023/Graphics/items/gift2.png" );
    chalo::TextureManager::Add( "gift3",                        "../Packages/ChristmasHorse2023/Graphics/items/gift3.png" );
    chalo::TextureManager::Add( "poster1",                      "../Packages/ChristmasHorse2023/Graphics/items/poster1.png" );
    chalo::TextureManager::Add( "poster2",                      "../Packages/ChristmasHorse2023/Graphics/items/poster2.png" );
    chalo::TextureManager::Add( "poster3",                      "../Packages/ChristmasHorse2023/Graphics/items/poster3.png" );
    chalo::TextureManager::Add( "poster4",                      "../Packages/ChristmasHorse2023/Graphics/items/poster4.png" );
    chalo::TextureManager::Add( "whiteboard",                   "../Packages/ChristmasHorse2023/Graphics/items/whiteboard.png" );
    chalo::TextureManager::Add( "monitor",                      "../Packages/ChristmasHorse2023/Graphics/items/monitor.png" );
    chalo::TextureManager::Add( "outdoor-sign",                 "../Packages/ChristmasHorse2023/Graphics/items/outdoor-sign.png" );
    chalo::TextureManager::Add( "santa-tube",                   "../Packages/ChristmasHorse2023/Graphics/items/santa-tube.png" );
    chalo::TextureManager::Add( "horse-tube",                   "../Packages/ChristmasHorse2023/Graphics/items/horse-tube.png" );
    chalo::TextureManager::Add( "control-button",               "../Packages/ChristmasHorse2023/Graphics/items/control-button.png" );
    chalo::TextureManager::Add( "note",                         "../Packages/ChristmasHorse2023/Graphics/items/note.png" );
    chalo::TextureManager::Add( "drabis",                       "../Packages/ChristmasHorse2023/Graphics/items/drabis.png" );
    chalo::TextureManager::Add( "ratchet",                      "../Packages/ChristmasHorse2023/Graphics/items/ratchet.png" );
    chalo::TextureManager::Add( "sneezy",                       "../Packages/ChristmasHorse2023/Graphics/items/sneezy.png" );
    chalo::TextureManager::Add( "wabeka",                       "../Packages/ChristmasHorse2023/Graphics/items/wabeka.png" );
    chalo::TextureManager::Add( "elf1",                         "../Packages/ChristmasHorse2023/Graphics/characters/elf1.png" );
    chalo::TextureManager::Add( "elf2",                         "../Packages/ChristmasHorse2023/Graphics/characters/elf2.png" );
    chalo::TextureManager::Add( "elf3",                         "../Packages/ChristmasHorse2023/Graphics/characters/elf3.png" );
    chalo::TextureManager::Add( "elf4",                         "../Packages/ChristmasHorse2023/Graphics/characters/elf4.png" );
    chalo::TextureManager::Add( "elf5",                         "../Packages/ChristmasHorse2023/Graphics/characters/elf5.png" );
    chalo::TextureManager::Add( "elf6",                         "../Packages/ChristmasHorse2023/Graphics/characters/elf6.png" );
    chalo::TextureManager::Add( "boss",                         "../Packages/ChristmasHorse2023/Graphics/characters/boss.png" );
    chalo::TextureManager::Add( "just-wait",                    "../Packages/ChristmasHorse2023/Graphics/items/just-wait.png");
    chalo::TextureManager::Add( "attack-icon",                  "../Packages/ChristmasHorse2023/Graphics/items/attack-icon.png");
    chalo::TextureManager::Add( "goal-texture",                 "../Packages/ChristmasHorse2023/Graphics/items/mouse-cursor.png" );
    chalo::TextureManager::Add( "look-icon",                    "../Packages/ChristmasHorse2023/Graphics/items/look-icon.png" );
    chalo::TextureManager::Add( "talk-icon",                    "../Packages/ChristmasHorse2023/Graphics/items/icon-talk.png" );
    chalo::TextureManager::Add( "pickup-icon",                  "../Packages/ChristmasHorse2023/Graphics/items/pickup-icon.png" );
    chalo::TextureManager::Add( "move-north",                   "../Packages/ChristmasHorse2023/Graphics/items/move-north.png" );
    chalo::TextureManager::Add( "move-south",                   "../Packages/ChristmasHorse2023/Graphics/items/move-south.png" );
    chalo::TextureManager::Add( "move-east",                    "../Packages/ChristmasHorse2023/Graphics/items/move-east.png" );
    chalo::TextureManager::Add( "move-west",                    "../Packages/ChristmasHorse2023/Graphics/items/move-west.png" );

    chalo::AudioManager::AddSoundBuffer( "horse-attack", "../Packages/ChristmasHorse2023/Audio/HorseAttack_693790__pospecstudio__coconut-quieter.ogg" );
    chalo::AudioManager::AddSoundBuffer( "elf-attack",   "../Packages/ChristmasHorse2023/Audio/104183__ekokubza123__punch.ogg" );

    m_sounds["horse-attack"].setBuffer( chalo::AudioManager::GetSoundBuffer( "horse-attack" ) );
    m_sounds["horse-attack"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );
    m_sounds["elf-attack"].setBuffer( chalo::AudioManager::GetSoundBuffer( "elf-attack" ) );
    m_sounds["elf-attack"].setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "SOUND_VOLUME" ) ) );

    m_xOffset = 250;

    m_background.setTexture( chalo::TextureManager::Get( "background" ) );
    m_background.setPosition( m_xOffset, 0 );

    m_hudBackgrounds[0].setPosition( 0, 0 );
    m_hudBackgrounds[1].setPosition( 1000, 0 );
    m_hudBackgrounds[0].setTexture( chalo::TextureManager::Get( "hud1" ) );
    m_hudBackgrounds[1].setTexture( chalo::TextureManager::Get( "hud2" ) );

    m_hudPortrait.setTexture( chalo::TextureManager::Get( "bepis-portrait" ) );
    m_hudPortrait.setPosition( 25, 35 );

    m_hudText["name"] = sf::Text( "Bepis", chalo::FontManager::Get( "main" ), 30 );
    m_hudText["name"].setPosition( 10, 240 );

    m_hudText["inv"] = sf::Text( "Log", chalo::FontManager::Get( "main" ), 30 );
    m_hudText["inv"].setPosition( 10, 400 );

    m_hudMapBg.setPosition( 1054, 10 );
    m_hudMapBg.setSize( sf::Vector2f( 200, 200 ) );
    m_hudMapBg.setFillColor( sf::Color( 51, 0, 0 ) );
    m_hudMapBg.setOutlineColor( sf::Color( 255, 153, 102 ) );
    m_hudMapBg.setOutlineThickness( 1 );

    m_logBg.setPosition( 1054, 300 );
    m_logBg.setSize( sf::Vector2f( 200, 400 ) );
    m_logBg.setFillColor( sf::Color( 51, 0, 0 ) );
    m_logBg.setOutlineColor( sf::Color( 255, 153, 102 ) );
    m_logBg.setOutlineThickness( 1 );

    sf::Text logText;
    logText.setFont( chalo::FontManager::Get( "main" ) );
    logText.setCharacterSize( 12 );
    logText.setFillColor( sf::Color::White );
    logText.setOutlineColor( sf::Color::Black );
    logText.setOutlineThickness( 1 );
    m_maxLogLines = 25;
    for ( int y = 0; y < m_maxLogLines; y++ )
    {
      logText.setPosition( 1054+10, 310 + ( y * 15 ) );
      logText.setString( "" );
      m_actionLog.push_back( logText );
    }

    m_actors["player"].Setup();
    m_actors["player"].SetTexture( chalo::TextureManager::Get( "horse" ), 0, 0, 88, 78 );
    m_actors["player"].SetPosition( 1280/2, 720/2 );
    m_actors["player"].SetAnimationInformation( 4, 0.1 );
    m_actors["player"].SetRestrictMovement( true );
    m_actors["player"].SetValidPositionRegion( sf::IntRect( 365, 85, 550, 550 ) );
    m_actors["player"].SetObjectCollisionRectangle( sf::IntRect( 29, 95, 67, 23 ) );
    m_actors["player"].SetMouseoverText( "Bepis" );
    m_actors["player"].SetSpeed( 2 );
    m_actors["player"].SetPermanentObject( true );
    m_actors["player"].SetColor( sf::Color( 255, 153, 51 ) );
    m_actors["player"].SetStats( 100 );
    m_actors["player"].SetHP( 50 );

    m_wantToMove = false;
    m_moveGoalShape.setTexture( chalo::TextureManager::Get( "goal-texture" ) );
    m_cursorImage.setTexture( chalo::TextureManager::Get( "goal-texture" ) );
    m_nonWalkAction = false;

    m_doors["south"] = PointAndClickObject(); // sf::IntRect( m_xOffset + 333, 635, 115, 60  );
    m_doors["north"] = PointAndClickObject(); // sf::IntRect( m_xOffset + 333, 25,  115, 60  );
    m_doors["east"]  = PointAndClickObject(); // sf::IntRect( m_xOffset + 665, 302, 60,  115 );
    m_doors["west"]  = PointAndClickObject(); // sf::IntRect( m_xOffset + 55,  302, 60,  115 );

    m_doors["south"].Setup();
    m_doors["south"].SetTexture( chalo::TextureManager::Get( "background" ), 333, 635, 115, 60 );
    m_doors["south"].SetPosition( m_xOffset + 333, 635 );
    m_doors["south"].SetMouseoverText( "Go south" );

    m_doors["north"].Setup();
    m_doors["north"].SetTexture( chalo::TextureManager::Get( "background" ), 333, 25, 115, 60 );
    m_doors["north"].SetPosition( m_xOffset + 333, 25 );
    m_doors["north"].SetMouseoverText( "Go north" );

    m_doors["east"].Setup();
    m_doors["east"].SetTexture( chalo::TextureManager::Get( "background" ), 665, 302, 60, 115 );
    m_doors["east"].SetPosition( m_xOffset + 665, 302 );
    m_doors["east"].SetMouseoverText( "Go east" );

    m_doors["west"].Setup();
    m_doors["west"].SetTexture( chalo::TextureManager::Get( "background" ), 55, 302, 60, 115 );
    m_doors["west"].SetPosition( m_xOffset + 55, 302 );
    m_doors["west"].SetMouseoverText( "Go west" );

    std::string bgPathBase = "../Packages/ChristmasHorse2023/Graphics/backgrounds/";
    std::string dataPathBase = "../Packages/ChristmasHorse2023/Maps/";
    m_maps = std::vector<CH2023_Map>(21);
    // --------------------------------------------------------------------------------------------------------------------------------------------  NORTH    WEST     EAST    SOUTH
    m_maps[0].Setup(  sf::Vector2f( 1, 0 ), dataPathBase + "cell-left.csv",             "Holding left",       bgPathBase + "holding-cell-1.png",     -1,       -1,      1,      -1);
    m_maps[1].Setup(  sf::Vector2f( 2, 0 ), dataPathBase + "cell-right.csv",            "Holding right",      bgPathBase + "holding-cell-2.png",     -1,        0,      2,      -1);
    m_maps[2].Setup(  sf::Vector2f( 3, 0 ), dataPathBase + "cell-hallway.csv",          "Holding external",   bgPathBase + "cell-hallway.png",       -1,        1,      3,       4);
    m_maps[3].Setup(  sf::Vector2f( 4, 0 ), dataPathBase + "bathroom.csv",              "Bathroom",           bgPathBase + "bathroom.png",           -1,        2,      -1,     -1);
    m_maps[4].Setup(  sf::Vector2f( 3, 1 ), dataPathBase + "festive-hallway.csv",       "Festive hallway",    bgPathBase + "festive-hallway.png",     2,       -1,       5,      7);
    m_maps[5].Setup(  sf::Vector2f( 4, 1 ), dataPathBase + "festive-hallway-2.csv",     "Festive hallway2",   bgPathBase + "festive-hallway-2.png",  -1,        4,       6,     -1);
    m_maps[6].Setup(  sf::Vector2f( 5, 1 ), dataPathBase + "festive-hallway-3.csv",     "Festive hallway3",   bgPathBase + "festive-hallway-3.png",  -1,        5,      -1,     -1);
    m_maps[7].Setup(  sf::Vector2f( 3, 2 ), dataPathBase + "office-entrance.csv",       "Office entrance",    bgPathBase + "office1.png",             4,        9,      -1,      8);
    m_maps[8].Setup(  sf::Vector2f( 3, 3 ), dataPathBase + "office-cubicles.csv",       "Office cubicles",    bgPathBase + "office2.png",             7,       -1,      -1,     -1);
    m_maps[9].Setup(  sf::Vector2f( 2, 2 ), dataPathBase + "office-hallway.csv",        "Office hallway",     bgPathBase + "office3.png",            10,        11,      7,     -1);
    m_maps[10].Setup( sf::Vector2f( 2, 1 ), dataPathBase + "office-conference.csv",     "Office conference",  bgPathBase + "office4.png",            -1,        -1,     -1,      9);
    m_maps[11].Setup( sf::Vector2f( 1, 2 ), dataPathBase + "entrance.csv",              "HQ Entrance",        bgPathBase + "outdoors1.png",          -1,        14,      9,      12);
    m_maps[12].Setup( sf::Vector2f( 1, 3 ), dataPathBase + "secret-lab-1.csv",          "Secret lab",         bgPathBase + "lab1.png",               11,        -1,     13,     -1);
    m_maps[13].Setup( sf::Vector2f( 2, 3 ), dataPathBase + "secret-lab-2.csv",          "Control room",       bgPathBase + "lab2.png",               -1,        12,     -1,     -1);
    m_maps[14].Setup( sf::Vector2f( 0, 2 ), dataPathBase + "empty.csv",                 "Maze A",             bgPathBase + "maze.png",               14,        14,     14,     15);
    m_maps[15].Setup( sf::Vector2f( 0, 2 ), dataPathBase + "empty.csv",                 "Maze B",             bgPathBase + "maze.png",               15,        15,     15,     16);
    m_maps[16].Setup( sf::Vector2f( 0, 2 ), dataPathBase + "empty.csv",                 "Maze C",             bgPathBase + "maze.png",               16,        16,     17,     16);
    m_maps[17].Setup( sf::Vector2f( 0, 2 ), dataPathBase + "empty.csv",                 "Maze D",             bgPathBase + "maze.png",               17,        17,     17,     18);
    m_maps[18].Setup( sf::Vector2f( 1, 5 ), dataPathBase + "boss.csv",                  "Boss area",          bgPathBase + "boss.png",               -1,        -1,     19,     -1);
    m_maps[19].Setup( sf::Vector2f( 2, 5 ), dataPathBase + "after-boss.csv",            "After boss",         bgPathBase + "note-area.png",          -1,        -1,     20,     -1);
    m_maps[20].Setup( sf::Vector2f( 3, 5 ), dataPathBase + "end.csv",                   "End area",           bgPathBase + "end.png",                -1,        -1,     20,     -1);

    m_currentMapIndex = 1;
    LoadMap( m_currentMapIndex );

    DebugSetup();
    chalo::InputManager::ToggleCursor( false );

    // Temporary:

    m_music.openFromFile( "../Packages/ChristmasHorse2023/Audio/beans.ogg" );
    m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
    m_music.setLoop( true );
    m_music.play();


    m_startCountdown = 500;

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_GameState::DebugSetup()
{
  Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
  std::vector<std::string> keys = {
    "DEBUG_GOALPOS", "MOUSEPOS", "CURRENTMAP",
  };

  for ( auto& key : keys )
  {
    m_debugText[key].setFont( chalo::FontManager::Get( "main" ) );
    m_debugText[key].setCharacterSize( 16 );
    m_debugText[key].setFillColor( sf::Color::Yellow );
    m_debugText[key].setOutlineColor( sf::Color::Black );
    m_debugText[key].setOutlineThickness( 1 );
  }

  m_debugText["DEBUG_GOALPOS"].setPosition( 10, 500 );
  m_debugText["MOUSEPOS"].setPosition( 10, 525 );
  m_debugText["CURRENTMAP"].setPosition( 10, 550 );
  m_debugText["CURRENTMAP"].setString( "MAP: " + Helper::ToString( m_currentMapIndex ) );

  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_GameState::LoadMap( int newIndex, std::string fromDirection /*="NONE"*/ )
{
  Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
  if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::OutIntValue( "New map index", newIndex, CLASSNAME + "::" + std::string( __func__ ) ); }

  m_cursorImage.setTexture( chalo::TextureManager::Get( "goal-texture" ) );
  if ( m_currentMapIndex == 20 && newIndex == 20 )
  {
    // Game is over!!
    LoadMap( 0 );
    m_endOfGame = true;
    SetGotoState( "ch2023_epiloguestate" );
    return;
  }
  else if ( newIndex < 0 || newIndex >= m_maps.size() )
  {
    Logger::Error( "New index is out of range!", CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutIntValue( "m_maps.size()", m_maps.size(), CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutFuncEnd( "nonfatal error", CLASSNAME + "::" + std::string( __func__ ) );
    return;
  }

  // Stop the dialog
  m_currentDialog.Stop();

  DeleteNonPermanentActors();

  m_currentMapIndex = newIndex;
  chalo::TextureManager::Add( "background", m_maps[ m_currentMapIndex ].GetBackgroundPath() );

  m_actors["player"].SetPosition( GetStartingPosition( fromDirection ) );
  m_debugText["CURRENTMAP"].setString( "MAP: " + Helper::ToString( m_currentMapIndex ) );
  std::map<std::string, PointAndClickObject> loadedObjects = m_maps[ m_currentMapIndex ].Activate( m_inventory, m_switches );

  //m_actors.insert( loadedObjects );
  for ( auto& obj : loadedObjects )
  {
    if ( Helper::Contains( m_inventory, obj.first ) ) { continue; }
    if ( Helper::Contains( m_switches, obj.first ) ) { continue; }
    m_actors[ obj.first ] = obj.second;
  }

//  DebugOut();

  m_wantToMove = false;
  m_moveGoalShape.setPosition( -100, -100 );
  m_actors["player"].Update();

  chalo::InputManager::PlaceMouse( sf::Vector2i( 1280/2, 720/2 ) );

  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_GameState::DeleteNonPermanentActors()
{
  Logger::OutFuncBegin( "Clear non-permanent objects before loading new ones...", CLASSNAME + "::" + std::string( __func__ ) );

  // Create list of objects to destroy
  std::vector<std::string> deleteKeys;
  for ( auto& actor : m_actors )
  {
    if ( !actor.second.IsPermanentObject() )
    {
      deleteKeys.push_back( actor.first );
    }
  }

  // Erase them
  for ( auto& key : deleteKeys )
  {
    m_actors.erase( m_actors.find( key ) );
  }

  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

sf::Vector2f CH2023_GameState::GetStartingPosition( std::string fromDirection )
{
  if ( fromDirection == "south" )
  {
    // Enter from north
    return sf::Vector2f( m_doors["north"].GetPositionRegion().left, m_doors["north"].GetPositionRegion().top );
  }
  else if ( fromDirection == "north" )
  {
    // Enter from south
    return sf::Vector2f( m_doors["south"].GetPositionRegion().left, m_doors["south"].GetPositionRegion().top );
  }
  else if ( fromDirection == "east" )
  {
    // Enter from west
    return sf::Vector2f( m_doors["west"].GetPositionRegion().left, m_doors["west"].GetPositionRegion().top );
  }
  else if ( fromDirection == "west" )
  {
    // Enter from east
    return sf::Vector2f( m_doors["east"].GetPositionRegion().left, m_doors["east"].GetPositionRegion().top );
  }
  else
  {
    // Put at center
    return sf::Vector2f( 1280/2, 720/2 );
  }

  m_wantToMove = false;
}

//
//void CH2023_GameState::SetupUI()
//{
////    chalo::MenuManager::LoadCsvMenu( "ch2022_gameui.csv" );
//}

void CH2023_GameState::Cleanup()
{
  Logger::OutFuncBegin( "Function begin", CLASSNAME + "::" + std::string( __func__ ) );
  m_music.stop();
  m_sounds.clear();
  m_hudText.clear();
  m_doors.clear();
  m_debugText.clear();
  m_inventory.clear();
  m_switches.clear();
  m_actionLog.clear();
  m_actionLogText.clear();
  m_maps.clear();
  m_actors.clear();
  chalo::MenuManager::Cleanup();
  chalo::DrawManager::Reset();
  chalo::MenuManager::Cleanup();
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_GameState::Update()
{
    Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
    chalo::InputManager::Update();
    chalo::MenuManager::Update();
    chalo::EffectManager::Update();

    EscapeyState::Update();

    if ( IsPaused() )
    {
      Logger::OutFuncEnd( "paused", CLASSNAME + "::" + std::string( __func__ ) );
      return;
    }
    else
    {
      chalo::InputManager::ToggleCursor( false );
    }

    if ( m_startCountdown > 0 )
    {
      m_startCountdown--;
    }

    auto mousePos = chalo::InputManager::GetMousePosition();
    std::string currentAction = "goal-texture";
    if ( chalo::InputManager::IsLeftClick() )
    {
      // Did you click an object?
      for ( auto& obj : m_actors )
      {
        if ( obj.first == "player" )
        {
          continue;
        }

        if ( SFMLHelper::PointInBoxCollision( mousePos, obj.second.GetPositionRegion() ) )
        {
          if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Object clicked. Name: " + obj.second.GetName(), CLASSNAME + "::" + std::string( __func__ ) ); }

          if ( m_currentDialog.IsCurrentlyTalking() )
          {
            // Skip if they're currently talking
//            Logger::Out( "Dialog is already happening... don't start dialog", CLASSNAME + "::" + std::string( __func__ ) );
            continue;
          }

//          Logger::OutHighlight( "Set m_nonWalkAction to true", CLASSNAME + "::" + std::string( __func__ ) );
          m_nonWalkAction = true;
          InteractPayload result = obj.second.BeginInteraction( m_actors["player"].GetPosition(), m_inventory );
          currentAction = result.action;

          std::string speakerKey = obj.first;
          if ( result.action == "pickup" || result.action == "remove" || result.action == "look" || result.action == "heal" || result.action == "NORESULT" )
          {
            speakerKey = "player";
          }

          if ( result.action != "fight" )
          {
            m_currentDialog.Setup( result.dialog, speakerKey, m_actors[speakerKey].GetColor() );
            LogText( m_actors[speakerKey].GetMouseoverText() + ": " +result.dialog.GetFirstLine() );
          }

          if ( result.action == "pickup" )
          {
            m_inventory.push_back( obj.second.GetName() );
            obj.second.SetPosition( -100, -100 );

            // SPECIAL
            if ( obj.second.GetName() == "Power is on?" )
            {
              std::string bgPathBase = "../Packages/ChristmasHorse2023/Graphics/backgrounds/";
              std::string dataPathBase = "../Packages/ChristmasHorse2023/Maps/";
              m_maps[12].Setup( sf::Vector2f( 1, 3 ), dataPathBase + "secret-lab-1b.csv",          "Secret lab",         bgPathBase + "lab1b.png",               11,        -1,     13,     -1);
            }
          }
          else if ( result.action == "remove" )
          {
            m_switches.push_back( obj.second.GetName() );
            obj.second.SetPosition( -100, -100 );
          }
          else if ( Helper::Contains( result.action, "give" ) )
          {
            std::string receive = Helper::Split( result.action, "=" )[1];
            m_inventory.push_back( receive );
          }
          else if ( Helper::Contains( result.action, "heal" ) )
          {
            m_actors["player"].FullHeal();
          }
        }
      }

      if ( !m_nonWalkAction )
      {
        // Maybe just move instead...
        m_wantToMove = true;
        m_moveGoal = sf::Vector2i( mousePos.x, mousePos.y );
        m_moveGoalShape.setPosition( m_moveGoal.x, m_moveGoal.y );
      }
    }
    else if ( chalo::InputManager::IsLeftClickRelease() )
    {
      // Going from Click -> Not Click state
//      Logger::OutHighlight( "Set m_nonWalkAction to false", CLASSNAME + "::" + std::string( __func__ ) );
      m_nonWalkAction = false;
    }
    else
    {
      // Going from Not Click -> Not Click state
//      Logger::OutHighlight( "Set m_nonWalkAction to false", CLASSNAME + "::" + std::string( __func__ ) );
      m_nonWalkAction = false;
    }

    std::string activeCursorTexture = "goal-texture";
//    std::string val = ( m_nonWalkAction ) ? "true" : "false";
//    Logger::OutHighlight( "m_nonWalkAction " + val, CLASSNAME + "::" + std::string( __func__ ) );

    if ( m_wantToMove )
    {
      if ( m_actors["player"].GetPosition().x < m_moveGoal.x - 5 )
      {
        m_actors["player"].Move( chalo::EAST );
      }
      else if ( m_actors["player"].GetPosition().x > m_moveGoal.x + 5 )
      {
        m_actors["player"].Move( chalo::WEST );
      }

      if ( m_actors["player"].GetPosition().y < m_moveGoal.y - 5 )
      {
        m_actors["player"].Move( chalo::SOUTH );
      }
      else if ( m_actors["player"].GetPosition().y > m_moveGoal.y + 5 )
      {
        m_actors["player"].Move( chalo::NORTH );
      }
    }

    // Is the player at a door and the goal spot is on a door?
    for ( auto& door : m_doors )
    {
      int neighborIndex = m_maps[ m_currentMapIndex ].GetNeighborIndex( door.first );
      bool moveGoalIsDoor = SFMLHelper::PointInBoxCollision( m_moveGoal, door.second.GetPositionRegion() );
      float distanceFromDoor = SFMLHelper::GetDistance( door.second.GetPositionRegion(), m_actors["player"].GetPositionRegion(), true );

      // Temp
      m_debugText["DEBUG_" + door.first + "_DIST"].setString( "Distance (" + door.first + "): " + Helper::ToString( distanceFromDoor ) );
      m_debugText["DEBUG_" + door.first + "_GOAL"].setString( "GOAL? " + Helper::ToString( moveGoalIsDoor ) );
      m_debugText["DEBUG_" + door.first + "_NEIGH"].setString( "ROOM INDEX: " + Helper::ToString( neighborIndex ) );

      if ( m_wantToMove && moveGoalIsDoor && distanceFromDoor <= 100 && !m_currentDialog.IsCurrentlyTalking() )
      {
        if ( neighborIndex != -1 )
        {
          LoadMap( neighborIndex, door.first );
        }
      }

      if ( SFMLHelper::PointInBoxCollision( mousePos, door.second.GetPositionRegion() ) )
      {
        if      ( door.first == "north" )
        {
          activeCursorTexture = "move-north";
          if ( chalo::InputManager::IsLeftClick() ) { currentAction = activeCursorTexture; }
        }
        else if ( door.first == "south" )
        {
          activeCursorTexture = "move-south";
          if ( chalo::InputManager::IsLeftClick() ) { currentAction = activeCursorTexture; }
        }
        else if ( door.first == "east" )
        {
          activeCursorTexture = "move-east";
          if ( chalo::InputManager::IsLeftClick() ) { currentAction = activeCursorTexture; }
        }
        else if ( door.first == "west" )
        {
          activeCursorTexture = "move-west";
          if ( chalo::InputManager::IsLeftClick() ) { currentAction = activeCursorTexture; }
        }
      }
    }

    // Update NPC interaction
    for ( auto& obj : m_actors )
    {
      obj.second.Update();

      // Update cursor
      if ( SFMLHelper::PointInBoxCollision( mousePos, obj.second.GetPositionRegion() ) )
      {
        std::string actionType = obj.second.GetDefaultResultAction();
        if ( actionType == "fight" )
        {
          activeCursorTexture = "attack-icon";
        }
        else if ( actionType == "pickup" )
        {
          activeCursorTexture = "pickup-icon" ;
        }
        else if ( actionType == "talk" || Helper::Contains( actionType, "give" ) )
        {
          activeCursorTexture = "talk-icon";
        }
        else if ( actionType == "look" )
        {
          activeCursorTexture = "look-icon";
        }
      }

      if ( obj.second.IsAFighter() )
      {
        sf::IntRect posRegion = obj.second.GetPositionRegion();
        posRegion.left -= posRegion.width / 2;
        posRegion.top -= posRegion.height / 2;
        float withinRange = ( SFMLHelper::GetDistance( obj.second.GetPositionRegion(), m_actors["player"].GetPositionRegion(), true ) < 100 );
        bool clickingObject = ( chalo::InputManager::IsLeftClick() && SFMLHelper::PointInBoxCollision( mousePos, posRegion ) );
        bool playerCanAttack = m_actors["player"].CanAttack();
        sf::Vector2f playerPosition = m_actors["player"].GetPosition();
        sf::Vector2f objectPosition = obj.second.GetPosition();

        // Try to approach Horse
        if ( obj.second.GetHP() > 0 )
        {
          if ( objectPosition.x < playerPosition.x - 10 )
          {
            obj.second.Move( chalo::EAST );
          }
          else if ( objectPosition.x > playerPosition.x + 10 )
          {
            obj.second.Move( chalo::WEST );
          }
          if ( objectPosition.y < playerPosition.y - 10 )
          {
            obj.second.Move( chalo::SOUTH );
          }
          else if ( objectPosition.y > playerPosition.y + 10 )
          {
            obj.second.Move( chalo::NORTH );
          }
        }

        // Are we clicking on them? Horse will attack this object
        if ( clickingObject && playerCanAttack && withinRange )
        {
          int dmg = rand() % 5 + 1;
          obj.second.Hurt( dmg );
          m_actors["player"].BeginAttack();
          m_sounds[ "horse-attack" ].play();

          chalo::Effect newEffect;
          newEffect.text.setFont( chalo::FontManager::Get( "main" ) );
          newEffect.text.setCharacterSize( 15 );
          newEffect.text.setFillColor( sf::Color::Yellow );
          newEffect.text.setOutlineColor( sf::Color::Black );
          newEffect.text.setOutlineThickness( 1 );
          newEffect.text.setString( Helper::ToString( dmg ) );
          newEffect.text.setPosition( obj.second.GetPosition() );
          newEffect.lifeCounter = 100;
          newEffect.behavior = chalo::Behavior::FLOAT_UP;
          chalo::EffectManager::AddEffect( newEffect );
        }

        if ( obj.second.CanAttack() && withinRange )
        {
          int dmg = rand() % 5 + 1;
          m_actors["player"].Hurt( dmg );
          obj.second.BeginAttack();
          m_sounds[ "elf-attack" ].play();

          chalo::Effect newEffect;
          newEffect.text.setFont( chalo::FontManager::Get( "main" ) );
          newEffect.text.setCharacterSize( 15 );
          newEffect.text.setFillColor( sf::Color::Red );
          newEffect.text.setOutlineColor( sf::Color::Black );
          newEffect.text.setOutlineThickness( 1 );
          newEffect.text.setString( Helper::ToString( dmg ) );
          newEffect.text.setPosition( m_actors["player"].GetPosition() );
          newEffect.lifeCounter = 100;
          newEffect.behavior = chalo::Behavior::FLOAT_UP;
          chalo::EffectManager::AddEffect( newEffect );
        }

      }
    }


    if ( m_currentDialog.IsCurrentlyTalking() )
    {
      activeCursorTexture = "just-wait";
    }

    m_cursorImage.setTexture( chalo::TextureManager::Get( activeCursorTexture ) );
//    m_moveGoalShape.setTexture( chalo::TextureManager::Get( currentAction ) );

    std::string newText = m_currentDialog.Update();
    if ( newText == "DONE" )
    {
      m_cursorImage.setTexture( chalo::TextureManager::Get( "goal-texture" ) );
    }
    else if ( newText != "" )
    {
      LogText( m_actors[m_currentDialog.GetSpeakerKey()].GetMouseoverText() + ": " + newText );
    }

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::Q ) )
    {
      m_debugTimer = 250;
      DebugOut();
    }
    if ( m_debugTimer > 0 )
    {
      m_debugTimer--;
    }

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::E ) )
    {
      Logger::Out( "DEBUG: Reload map", CLASSNAME + "::" + std::string( __func__ ) );
      LoadMap( m_currentMapIndex, "south" );
    }

    if ( m_actors["player"].GetHP() <= 0 )
    {
      LoadMap( 2 );
      m_actors["player"].SetHP( 10 );
    }

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_GameState::LogText( std::string text )
{
  int maxLength = 25;

  std::vector<std::string> words = Helper::Split( text, " " );
  std::string stringBuild = "";
  std::vector<std::string> spliced;
  for ( auto& word : words )
  {
    if ( stringBuild.size() + word.size() + 1 > maxLength )
    {
      spliced.push_back( stringBuild );
      stringBuild = " ";
    }
    stringBuild += word + " ";
  }
  spliced.push_back( stringBuild );


  for ( auto& str : spliced )
  {
    m_actionLogText.push_back( str );
  }

  // If we're out of space, remove old log items
  int logCapacityDiff = m_maxLogLines - m_actionLogText.size();

  if ( logCapacityDiff < 0 )
  {
    logCapacityDiff = -logCapacityDiff;
    m_actionLogText.erase( m_actionLogText.begin(), m_actionLogText.begin() + logCapacityDiff );
  }

  for ( size_t i = 0; i < m_actionLogText.size(); i++ )
  {
    m_actionLog[i].setString( m_actionLogText[i] );
  }
}

void CH2023_GameState::DebugUpdate()
{
  auto mousePos = chalo::InputManager::GetMousePosition();
  m_debugText["DEBUG_GOALPOS"].setString( "MOVEGOAL:" + SFMLHelper::CoordinateToString( m_moveGoal ) );
  m_debugText["MOUSEPOS"].setString( "MOUSEPOS: " + SFMLHelper::CoordinateToString( mousePos ) );
}

void CH2023_GameState::DebugOut()
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
  chalo::TextureManager::DebugOut();
  Logger::OutIntValue( "Total maps", m_maps.size(), CLASSNAME + "::" + std::string( __func__ ) );
  for ( auto& mappy : m_maps )
  {
    mappy.DebugOut();
  }
  Logger::OutIntValue( "Total actors", m_actors.size(), CLASSNAME + "::" + std::string( __func__ ) );
  for ( auto& actor : m_actors )
  {
    Logger::OutValue( "actor key", actor.first, CLASSNAME + "::" + std::string( __func__ ) );
    actor.second.DebugOut();
  }
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_GameState::DrawMinimap( sf::RenderWindow& window )
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
  int blockWidth = 30;
  // Minimap
  window.draw( m_hudMapBg );
  for ( size_t i = 0; i < m_maps.size(); i++ )
  {
    if ( Helper::Contains( m_maps[ i ].GetName(), "Maze" ) )
    {
      continue;
    }
    sf::RectangleShape shape;
    sf::Vector2f pos = sf::Vector2f(
      m_hudMapBg.getPosition().x + ( m_maps[i].GetPosition().x * blockWidth ) + 5,
      m_hudMapBg.getPosition().y + ( m_maps[i].GetPosition().y * blockWidth ) + 5
    );

    for ( auto& neigh : m_maps[i].GetNeighborIndices() )
    {
      sf::Vector2f newPos = pos;
      if ( neigh.second == -1 || Helper::Contains( m_maps[ neigh.second ].GetName(), "Maze" ) )
      {
        continue;
      }

      if      ( neigh.first == "south" )
      {
        newPos.y += 20;
        newPos.x += 5;
        shape.setSize( sf::Vector2f( 10, 10 ) );
      }
      else if ( neigh.first == "west" )
      {
        newPos.x -= 10;
        newPos.y += 5;
        shape.setSize( sf::Vector2f( 10, 10 ) );
      }
      else { continue; }

      shape.setFillColor( sf::Color( 150, 150, 150 ) );
      shape.setPosition( newPos );
      window.draw( shape );
    }

    // TODO: Better to store as member variable? Not important for now.
    if ( m_currentMapIndex == i )
    {
      shape.setFillColor( sf::Color::Yellow );
    }
    else
    {
      shape.setFillColor( sf::Color::White );
    }
    shape.setPosition( pos );
    shape.setSize( sf::Vector2f( 20, 20 ) );
    window.draw( shape );
  }
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_GameState::DrawStats( sf::RenderWindow& window )
{
  // TODO: Better to store as member variable? Not important for now.
  sf::RectangleShape shape;
  shape.setPosition( 10, 300 );
  shape.setFillColor( sf::Color::Red );
  shape.setOutlineColor( sf::Color::Black );
  shape.setOutlineThickness( 2 );
  shape.setSize( sf::Vector2f( 220, 30 ) );
  window.draw( shape );

  float healthRatio = float( m_actors["player"].GetHP() ) / m_actors["player"].GetMaxHP();

  // Health bar
  shape.setFillColor( sf::Color::Green );
  shape.setSize( sf::Vector2f( 220 * healthRatio, 30 ) );
  window.draw( shape );
}

void CH2023_GameState::Draw( sf::RenderWindow& window )
{
    Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
    sf::Vector2i mousePosition = chalo::InputManager::GetMousePosition();

    window.draw( m_background );

    for ( auto& door : m_doors )
    {
      if ( m_maps[ m_currentMapIndex ].GetNeighborIndex( door.first ) != -1 )
      {
        door.second.Draw( window, mousePosition );
      }
    }

    window.draw( m_moveGoalShape );

    for ( auto& obj : m_actors )
    {
      obj.second.Draw( window, mousePosition );
      if ( obj.second.IsAFighter() )
      {
        // Draw health bar
        float healthRatio = float( obj.second.GetHP() ) / obj.second.GetMaxHP();
        sf::RectangleShape health;
        health.setPosition( obj.second.GetPosition().x - 30, obj.second.GetPosition().y - 50 );

        health.setSize( sf::Vector2f( 50, 5 ) );
        health.setFillColor( sf::Color::Black );
        window.draw( health );

        health.setSize( sf::Vector2f( 50 * healthRatio, 5 ) );
        health.setFillColor( sf::Color::Green );
        window.draw( health );
      }
    }

    // HUD
    window.draw( m_hudBackgrounds[0] );
    window.draw( m_hudBackgrounds[1] );
    window.draw( m_hudPortrait );
    for ( auto& tx : m_hudText )
    {
      window.draw( tx.second );
    }

    // Inventory
    int y = 450;
    for ( auto& inv : m_inventory )
    {
      sf::Text text;
      text.setFillColor( sf::Color::White );
      text.setOutlineColor( sf::Color::Black );
      text.setOutlineThickness( 1 );
      text.setCharacterSize( 15 );
      text.setPosition( sf::Vector2f( 10, y ) );
      text.setFont( chalo::FontManager::Get( "main" ) );
      text.setString( inv );
      y += 20;
      window.draw( text );
    }

    // Dialog log
    window.draw( m_logBg );
    for ( auto& txt : m_actionLog )
    {
      window.draw( txt );
    }

    DrawMinimap( window );
    DrawStats( window );

    m_currentDialog.Draw( window, m_actors[ m_currentDialog.GetSpeakerKey() ].GetCenterPosition() );
    m_cursorImage.setPosition( sf::Vector2f( mousePosition.x, mousePosition.y ) );
    window.draw( m_cursorImage );

    if ( m_debugTimer > 0 )
    {
      DebugDraw( window );
    }

    if ( m_startCountdown > 0 )
    {
      m_startCountdown--;

      sf::RectangleShape bg;
      bg.setPosition( 0, 0 );
      bg.setSize( sf::Vector2f( 1280, 720 ) );
      if ( m_startCountdown >= 100 )
      {
        bg.setFillColor( sf::Color( 0, 0, 0, 255 ) );
        window.draw( bg );

        if ( m_startCountdown < 400 )
        {
          sf::Text txt;
          txt.setFont( chalo::FontManager::Get( "main" ) );
          txt.setCharacterSize( 40 );
          txt.setPosition( 1280/2 - 200, 720/2 - 50 );
          txt.setString( "Where am I...?" );
          txt.setFillColor( sf::Color( 255, 204, 102 ) );
          window.draw( txt );
        }
      }
      else
      {
        bg.setFillColor( sf::Color( 0, 0, 0, 255 * ( float( m_startCountdown ) / 100 ) ) );
        window.draw( bg );

        m_music.setVolume( m_music.getVolume() + 0.5 );
        if ( m_music.getVolume() > Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) )
        {
          m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
        }
      }

      if ( m_startCountdown == 0 )
      {
        m_music.setVolume( Helper::StringToInt( chalo::ConfigManager::Get( "MUSIC_VOLUME" ) ) );
      }
    }

    chalo::EffectManager::Draw( window );

    EscapeyState::Draw( window );

    Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

void CH2023_GameState::DebugDraw( sf::RenderWindow& window )
{
  auto mousePos = chalo::InputManager::GetMousePosition();
  m_debugText["MOUSEPOS"].setPosition( mousePos.x, mousePos.y );
  m_debugText["MOUSEPOS"].setString( SFMLHelper::CoordinateToString( mousePos ) );

  for ( auto& tx : m_debugText )
  {
    window.draw( tx.second );
  }

  for ( auto& actor : m_actors )
  {
    actor.second.DebugDraw( window );
  }
}
