#include "CH2023_Map.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Utilities/CsvParser.hpp"
#include "../../chalo-engine/Managers/ConfigManager.hpp"

CH2023_Map::CH2023_Map()
{
  CLASSNAME = std::string( typeid( *this ).name() );

}

CH2023_Map::CH2023_Map( sf::Vector2f relpos, std::string mapDataPath, std::string name, std::string backgroundPath, int north /*= -1*/, int west /*= -1*/, int east /*= -1*/, int south /*= -1*/ )
{
  CLASSNAME = std::string( typeid( *this ).name() );
  Setup( relpos, mapDataPath, name, backgroundPath, north, west, east, south );
}

void CH2023_Map::Setup( sf::Vector2f relpos, std::string mapDataPath, std::string name, std::string backgroundPath, int north /*= -1*/, int west /*= -1*/, int east /*= -1*/, int south /*= -1*/ )
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
  m_name = name;
  m_backgroundPath = backgroundPath;
  m_mapPath = mapDataPath;
  m_relativePos = relpos;
  SetNeighbors( north, west, east, south );
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

std::string CH2023_Map::GetName()
{
  return m_name;
}

std::map<std::string, PointAndClickObject> CH2023_Map::Activate( const std::vector<std::string>& inventory, const std::vector<std::string>& switches )
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
  LoadMapData( m_mapPath, inventory, switches );
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
  return m_objects;
}

void CH2023_Map::SetNeighbors( int north, int west, int east, int south )
{
  m_neighborIndex["north"] = north;
  m_neighborIndex["west"]  = west;
  m_neighborIndex["east"]  = east;
  m_neighborIndex["south"] = south;
}

std::string CH2023_Map::GetBackgroundPath() const
{
  return m_backgroundPath;
}

int CH2023_Map::GetNeighborIndex( std::string direction ) const
{
  if ( m_neighborIndex.find( direction ) == m_neighborIndex.end() )
  {
    return -1;
  }
  return m_neighborIndex.at( direction );
}

//std::map<std::string, PointAndClickObject>& CH2023_Map::GetObjects()
//{
//  return m_objects;
//}

void CH2023_Map::LoadMapData( std::string file, const std::vector<std::string>& inventory, const std::vector<std::string>& switches )
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
  Logger::Out( "Reading map file \"" + file + "\"", CLASSNAME + "::" + std::string( __func__ ) );

  if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" )
  {
    Logger::OutValue( "Inventory data", Helper::VectorToString( inventory ), CLASSNAME + "::" + std::string( __func__ ) );
    Logger::OutValue( "Switch data", Helper::VectorToString( switches ), CLASSNAME + "::" + std::string( __func__ ) );
  }

  rach::CsvDocument mapdoc = rach::CsvParser::Parse( file );

  std::string name, texture, positionx, positiony, width, height, mouseover;
  sf::Color color;
  CH2023_Dialog dialog_info_default;
  CH2023_Dialog dialog_info_1;
  for ( size_t r = 0; r < mapdoc.rows.size(); r++ )
  {
    for ( size_t c = 0; c < mapdoc.rows[r].size(); c++ )
    {
      if      ( mapdoc.header[c] == "name" )                    { name = mapdoc.rows[r][c]; }
      else if ( mapdoc.header[c] == "texture" )                 { texture = mapdoc.rows[r][c]; }
      else if ( mapdoc.header[c] == "position_x" )              { positionx = mapdoc.rows[r][c]; }
      else if ( mapdoc.header[c] == "position_y" )              { positiony = mapdoc.rows[r][c]; }
      else if ( mapdoc.header[c] == "width" )                   { width = mapdoc.rows[r][c]; }
      else if ( mapdoc.header[c] == "height" )                  { height = mapdoc.rows[r][c]; }
      else if ( mapdoc.header[c] == "character_color" )         { color = StringToColor( mapdoc.rows[r][c] ); }
      else if ( mapdoc.header[c] == "mouseover_text" )          { mouseover = mapdoc.rows[r][c]; }
      else if ( mapdoc.header[c] == "dialog_default_text" )     { dialog_info_default.SetText( Helper::Split( mapdoc.rows[r][c], "|" ) ); }
      else if ( mapdoc.header[c] == "dialog_default_timeout" )  { dialog_info_default.SetTimeoutMax( Helper::StringToInt( mapdoc.rows[r][c] ) ); }
      else if ( mapdoc.header[c] == "dialog_default_action" )   { dialog_info_default.SetResultAction( mapdoc.rows[r][c] ); }
      else if ( mapdoc.header[c] == "dialog_1_condition" )      { dialog_info_1.SetTriggerCondition( mapdoc.rows[r][c] ); }
      else if ( mapdoc.header[c] == "dialog_1_text" )           { dialog_info_1.SetText( Helper::Split( mapdoc.rows[r][c], "|" ) ); }
      else if ( mapdoc.header[c] == "dialog_1_timeout" )        { dialog_info_1.SetTimeoutMax( Helper::StringToInt( mapdoc.rows[r][c] ) ); }
      else if ( mapdoc.header[c] == "dialog_1_action" )         { dialog_info_1.SetResultAction( mapdoc.rows[r][c] ); }
    }

    if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" )
    {
      Logger::Out( "name: \"" + name + "\"", CLASSNAME + "::" + std::string( __func__ ) );
      Logger::Out( "mouseover: \"" + mouseover + "\"", CLASSNAME + "::" + std::string( __func__ ) );
    }

    bool loadObject = true;
    for ( size_t i = 0; i < inventory.size(); i++ )
    {
      if ( inventory[i] == name )
      {
        loadObject = false;
        if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Skip loading this item" ); }
      }
    }

    for ( size_t i = 0; i < switches.size(); i++ )
    {
      if ( switches[i] == name )
      {
        loadObject = false;
        if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Skip loading this item" ); }
      }
    }

    if ( loadObject )
    {
      m_objects[ name ] = PointAndClickObject();
      m_objects[ name ].Setup();
      m_objects[ name ].SetTexture( chalo::TextureManager::Get( texture ), 0, 0, Helper::StringToInt( width ), Helper::StringToInt( height ) );
      m_objects[ name ].SetPosition( Helper::StringToInt( positionx ), Helper::StringToInt( positiony ) );
      m_objects[ name ].SetMouseoverText( mouseover );
      m_objects[ name ].SetName( name );
      m_objects[ name ].SetColor( color );
      m_objects[ name ].SetSpeed( 1 );

      if ( dialog_info_default.GetResultAction() == "fight" )
      {
        m_objects[ name ].SetTextureRegion( sf::IntRect( 0, 0, 52, 69 ) );
        m_objects[ name ].SetAnimationInformation( 4, 0.1 );
      }

      if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "GetName(): \"" + m_objects[ name ].GetName() + "\"", CLASSNAME + "::" + std::string( __func__ ) ); }

      dialog_info_default.SetTriggerCondition( "" );

      if ( chalo::ConfigManager::Get( "DEBUG_LOG" ) == "1" ) { Logger::Out( "Object name: " + m_objects[ name ].GetName(), "CH2023_Map::LoadMapData" ); }
      m_objects[ name ].AddDialog( "dialog_default", dialog_info_default );
      m_objects[ name ].AddDialog( "dialog_1", dialog_info_1 );
    }
  }
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}

sf::Color CH2023_Map::StringToColor( std::string rgb )
{
  Logger::OutFuncBegin( "Function begin, rgb=\"" + rgb + "\"", CLASSNAME + "::" + std::string( __func__ ) );

  sf::Color color = sf::Color::Cyan;
  std::vector<std::string> colorArray = Helper::Split( rgb, "|" );
  if ( colorArray.size() == 3 )
  {
//    Logger::Out( "String: \"" + rgb + "\", R: " + colorArray[0] + ", G: " + colorArray[1] + ", B: " + colorArray[2], "CH2023_Map::StringToColor" );
    color.r = Helper::StringToInt( colorArray[0] );
    color.g = Helper::StringToInt( colorArray[1] );
    color.b = Helper::StringToInt( colorArray[2] );
  }

  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
  return color;
}

sf::Vector2f CH2023_Map::GetPosition() const
{
  return m_relativePos;
}

std::map<std::string, int>& CH2023_Map::GetNeighborIndices()
{
  return m_neighborIndex;
}

void CH2023_Map::DebugOut()
{
  Logger::OutFuncBegin( "", CLASSNAME + "::" + std::string( __func__ ) );
  Logger::OutValue( "m_name", m_name, CLASSNAME + "::" + std::string( __func__ ) );
  Logger::OutValue( "m_backgroundPath", m_backgroundPath, CLASSNAME + "::" + std::string( __func__ ) );
  Logger::OutValue( "m_mapPath", m_mapPath, CLASSNAME + "::" + std::string( __func__ ) );
  Logger::OutFuncEnd( "normal", CLASSNAME + "::" + std::string( __func__ ) );
}


