#ifndef _CH2023_GAME_STATE
#define _CH2023_GAME_STATE

#include <SFML/Audio.hpp>
#include <vector>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
#include "../../chalo-engine/Managers/EffectManager.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"
#include "../ProgramBase/EscapeyState.hpp"

#include "CH2023_PACObject.hpp"
#include "CH2023_Map.hpp"

class CH2023_GameState : public EscapeyState //public chalo::IState
{
public:
    CH2023_GameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

    void DebugSetup();
    void DebugUpdate();
    void DebugDraw( sf::RenderWindow& window );

private:
    std::string CLASSNAME;
    sf::Music m_music;
    std::map<std::string, sf::SoundBuffer> m_soundBuffers;
    std::map<std::string, sf::Sound> m_sounds;

    sf::Sprite m_background;
    sf::Sprite m_hudBackgrounds[2];
    sf::Sprite m_hudPortrait;
    sf::RectangleShape m_hudMapBg;
    sf::RectangleShape m_logBg;
    std::map<std::string, sf::Text> m_hudText;
    std::map<std::string, PointAndClickObject> m_doors;
    std::map<std::string, sf::Text> m_debugText;
    std::vector<std::string> m_inventory;
    std::vector<std::string> m_switches;
    std::vector<sf::Text> m_actionLog;
    std::vector<std::string> m_actionLogText;
    int m_maxLogLines;
    int m_debugTimer;

    std::vector<CH2023_Map> m_maps;
    int m_currentMapIndex;

    sf::Vector2i m_moveGoal;
    bool m_wantToMove;
    bool m_nonWalkAction;
    sf::Sprite m_moveGoalShape;
    sf::Sprite m_cursorImage;
    int m_xOffset;
    bool m_endOfGame;

    CH2023_Dialog m_currentDialog;
    int m_startCountdown;

    std::map<std::string, PointAndClickObject> m_actors;

    sf::Vector2f GetStartingPosition( std::string fromDirection );
    void LoadMap( int newIndex, std::string fromDirection = "NONE" );
    void DeleteNonPermanentActors();
    void LogText( std::string text );
    void DrawMinimap( sf::RenderWindow& window );
    void DrawStats( sf::RenderWindow& window );
    void DebugOut();
};

#endif
