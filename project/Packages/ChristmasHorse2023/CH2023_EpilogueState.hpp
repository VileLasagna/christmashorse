#ifndef _CH2023_EPILOGUE_STATE
#define _CH2023_EPILOGUE_STATE

#include <SFML/Audio.hpp>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/DEPRECATED/Maps/WritableMap.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
//#include "../../chalo-engine/DEPRECATED/Managers/DrawManager.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"

#include <vector>

class CH2023_EpilogueState : public chalo::IState
{
public:
    CH2023_EpilogueState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    std::string CLASSNAME;
    sf::Sprite m_background1;
    std::vector<sf::Text> m_text;
    sf::Music m_music;
};

#endif
