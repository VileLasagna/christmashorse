#ifndef _CH2021_CHARACTER
#define _CH2021_CHARACTER

#include <SFML/Graphics.hpp>

#include "CH2021_Types.hpp"

class CH2021_Horse
{
public:
    void Setup();
    void Update();
    void Animate();
    void Draw( sf::RenderWindow& win );
    void UpdateSprite();
    void ChangeDirection( Direction direction );
    Direction CompareMousePos( sf::Vector2i mousePos );
    void AddAnimationSpeed( float amount );

    sf::Vector2f GetSpritePosition();
    sf::Vector2f GetActualPosition()
    {
      return m_actualPosition;
    }
    //sf::Vector2f GetActualPosition();
    void Move( float xAmt, float yAmt );

    sf::IntRect GetRegion();

private:
    sf::Sprite m_sprite;
    sf::Vector2f m_actualPosition;
    sf::IntRect m_textureCoordinates;
    sf::IntRect m_directionRegions[3][3];
    Direction m_directions[3][3];

    float m_frame;      // x on spritesheet
    float m_maxFrame;
    float m_animateSpeed;
    int m_direction;    // y on spritesheet
};

#endif
