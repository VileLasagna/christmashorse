#include "CH2021_Character.hpp"

#include "../../chalo-engine/Application/Application.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Utilities/Helper.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

void CH2021_Horse::Setup()
{
    int screenWidth = chalo::Application::GetScreenWidth();
    int screenHeight = chalo::Application::GetScreenHeight();

    m_sprite.setTexture( chalo::TextureManager::Get( "horse" ) );
    m_sprite.setOrigin( sf::Vector2f( 100, 100 ) );
    m_actualPosition.x = 1280;
    m_actualPosition.y = 720;
    m_sprite.setPosition( sf::Vector2f( screenWidth/2, screenHeight/2 ) );
    m_maxFrame = 14;
    m_frame = 0;
    m_animateSpeed = 0.25;
    m_direction = 0;
    m_textureCoordinates.width = 200;
    m_textureCoordinates.height = 200;

/*
|---|---|---|
|   |   |   |
|---|---|---|
|   | H |   |
|---|---|---|
|   |   |   |
|---|---|---|
*/

    for ( int y = 0; y <= 2; y++ )
    {
        for ( int x = 0; x <= 2; x++ )
        {
            m_directionRegions[x][y].width  = 200; //screenWidth / 3;
            m_directionRegions[x][y].height = 200; //screenHeight / 3;
            m_directionRegions[x][y].left   = ((x-1) * 200) + m_sprite.getPosition().x - 100; //x * screenWidth / 3;
            m_directionRegions[x][y].top    = ((y-1) * 200) + m_sprite.getPosition().y - 100; //y * screenHeight / 3;
        }
    }

    // NW
//    m_directionRegions[0][0].width = screenWidth/2 - 100;
//    m_directionRegions[0][0].height = screenHeight/3;
//    m_directionRegions[0][0].left = 0;
//    m_directionRegions[0][0].top = 0;

    // W
//    m_directionRegions[1][0].width = screenWidth/2 - 100;
//    m_directionRegions[1][0].height = screenHeight/3;
//    m_directionRegions[1][0].left = 0;
//    m_directionRegions[1][0].top = m_directionRegions[0][0].height;

    // SW
//    m_directionRegions[2][0].width = screenWidth/2 - 100;
//    m_directionRegions[2][0].height = screenHeight/3;
//    m_directionRegions[2][0].left = 0;
//    m_directionRegions[2][0].top = m_directionRegions[1][0].top + m_directionRegions[1][0].height;


    m_directions[0][0] = Direction::NORTHWEST;
    m_directions[1][0] = Direction::NORTH;
    m_directions[2][0] = Direction::NORTHEAST;

    m_directions[0][1] = Direction::WEST;
    m_directions[1][1] = Direction::CENTER;
    m_directions[2][1] = Direction::EAST;

    m_directions[0][2] = Direction::SOUTHWEST;
    m_directions[1][2] = Direction::SOUTH;
    m_directions[2][2] = Direction::SOUTHEAST;
}

void CH2021_Horse::Update()
{
}

void CH2021_Horse::Animate()
{
    m_frame += m_animateSpeed;
    if ( m_frame >= m_maxFrame )
    {
        m_frame = 0;
    }

    m_textureCoordinates.left = int( m_frame ) * m_textureCoordinates.width;
}

void CH2021_Horse::UpdateSprite()
{
    m_sprite.setTextureRect( m_textureCoordinates );
}

void CH2021_Horse::ChangeDirection( Direction direction )
{
    switch ( direction )
    {
        case Direction::EAST:         m_textureCoordinates.top = 0*200; break;
        case Direction::NORTHEAST:    m_textureCoordinates.top = 1*200; break;
        case Direction::NORTH:        m_textureCoordinates.top = 2*200; break;
        case Direction::NORTHWEST:    m_textureCoordinates.top = 3*200; break;
        case Direction::WEST:         m_textureCoordinates.top = 4*200; break;
        case Direction::SOUTHWEST:    m_textureCoordinates.top = 5*200; break;
        case Direction::SOUTH:        m_textureCoordinates.top = 6*200; break;
        case Direction::SOUTHEAST:    m_textureCoordinates.top = 7*200; break;
    }
}

Direction CH2021_Horse::CompareMousePos( sf::Vector2i mousePos )
{
    sf::Vector2f playerPos = m_sprite.getPosition();

    int gridwh = m_textureCoordinates.width; // / 3;

    Direction direction;

    for ( int y = 0; y <= 2; y++ )
    {
        for ( int x = 0; x <= 2; x++ )
        {
            if ( SFMLHelper::PointInBoxCollision( mousePos, m_directionRegions[x][y] ) )
            {
                return m_directions[x][y];
            }
        }
    }

    return Direction::CENTER;
}

void CH2021_Horse::Draw( sf::RenderWindow& win )
{
    // Debug rectangles
//    for ( int y = 0; y <= 2; y++ )
//    {
//        for ( int x = 0; x <= 2; x++ )
//        {
//            sf::RectangleShape grid;
//            grid.setPosition( m_directionRegions[x][y].left, m_directionRegions[x][y].top );
//            grid.setOutlineColor( sf::Color( x * 100, 0, 0 ) );
//            grid.setOutlineThickness( 2 );
//            grid.setFillColor( sf::Color::Transparent );
//            grid.setSize( sf::Vector2f( m_directionRegions[x][y].width, m_directionRegions[x][y].height ) );
//            win.draw( grid );
//        }
//    }

    // Walk region
    float radius = ( 200 * 3 ) / 2;
    sf::CircleShape shape;
    shape.setFillColor( sf::Color( 255, 255, 255, 25 ) );
    shape.setPosition( m_sprite.getPosition() );
    shape.setRadius( radius );
    shape.setOrigin( radius, radius );

    win.draw( shape );

    UpdateSprite();
    win.draw( m_sprite );
}

sf::Vector2f CH2021_Horse::GetSpritePosition()
{
    return m_sprite.getPosition();
}

//sf::Vector2f CH2021_Horse::GetActualPosition()
//{
//    return m_actualPosition;
//}

void CH2021_Horse::AddAnimationSpeed( float amount )
{
    m_animateSpeed += amount;
}

void CH2021_Horse::Move( float xAmt, float yAmt )
{
    m_actualPosition.x += xAmt;
    m_actualPosition.y += yAmt;
}

sf::IntRect CH2021_Horse::GetRegion()
{
    sf::IntRect region;
    region.left = m_sprite.getPosition().x;
    region.top = m_sprite.getPosition().y;
    region.width = m_textureCoordinates.width;
    region.height = m_textureCoordinates.height;
    return region;
}
