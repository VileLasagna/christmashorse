#ifndef _CH2021_QUEST
#define _CH2021_QUEST

#include "../../../chalo-engine/Application/Application.hpp"

struct Quest
{
    sf::Text        text;
    std::string     textStr;
    bool            completed;
    std::string     name;
    int             required;
    int             have;

    void Draw( sf::RenderWindow& window )
    {
        window.draw( text );

        if ( completed )
        {
            sf::RectangleShape strike;
            strike.setFillColor( sf::Color::Yellow );
            strike.setOutlineColor( sf::Color::Black );
            strike.setOutlineThickness( 1 );
            strike.setPosition( text.getPosition().x, text.getPosition().y + 10 );
            strike.setSize( sf::Vector2f( 200, 2 ) );
            window.draw( strike );
        }
    }

    void Setup( int index, std::string questName, std::string questText, int requiredAmount )
    {
        name = questName;
        completed = false;
        required = requiredAmount;
        have = 0;

        text.setFont( chalo::FontManager::Get( "main" ) );
        text.setCharacterSize( 16 );
        text.setFillColor( sf::Color::Yellow );
        text.setOutlineColor( sf::Color::Black );
        text.setOutlineThickness( 2 );
        textStr = questText;
        text.setString( textStr + " (" + Helper::ToString( have ) + "/" + Helper::ToString( required ) + ")" );

        int x = chalo::Application::GetScreenWidth() - 225;
        int y = 300 + ( index * 50 );

        text.setPosition( x, y );
    }

    void GotItem()
    {
        have++;
        if ( have >= required )
        {
            completed = true;
        }
        UpdateText();
    }

    void UpdateText()
    {
        text.setString( textStr + " (" + Helper::ToString( have ) + "/" + Helper::ToString( required ) + ")" );
    }
};

#endif
