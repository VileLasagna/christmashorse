#ifndef _CH2021_PROLOGUE_STATE
#define _CH2021_PROLOGUE_STATE

#include <SFML/Audio.hpp>
#include <vector>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/DEPRECATED/Maps/WritableMap.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
//#include "../../chalo-engine/DEPRECATED/Managers/DrawManager.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"
#include "../ProgramBase/StorySlide.hpp"

#include "CH2021_Character.hpp"
#include "CH2021_Types.hpp"

class CH2021_PrologueState : public chalo::IState
{
public:
    CH2021_PrologueState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    std::string CLASSNAME;
    std::vector<StorySlide> m_slides;
    int m_currentSlide;
    float m_mouseCooldown;
    float m_mouseCooldownMax;
    sf::Music m_music;
};

#endif
