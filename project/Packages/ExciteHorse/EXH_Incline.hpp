#ifndef _INCLINE_HPP
#define _INCLINE_HPP

#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"

namespace ExciteHorse
{

enum class ObstacleType { INCLINE, SPEEDUP, SLOWDOWN, FINISHLINE };

class TrackObstacle : public chalo::Character
{
    public:
    TrackObstacle();
    void Update();
    bool CollisionWith( sf::IntRect horseRegion );
    void DrawDebug( sf::RenderWindow& window );
    ObstacleType GetType();
    void SetType( ObstacleType type );
    void SetScale( float x, float y );

    protected:
    std::vector<sf::IntRect> m_collisionRegions;
    ObstacleType m_type;
};

}

#endif
