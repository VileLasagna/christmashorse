#include "EXH_Incline.hpp"
#include "../../chalo-engine/Utilities_SFML/SFMLHelper.hpp"

namespace ExciteHorse
{

TrackObstacle::TrackObstacle()
{
    sf::IntRect region;
    region.left = 10;
    region.top = 10;
    region.width = 40;
    region.height = 40;
    m_collisionRegions.push_back( region );
}

void TrackObstacle::Update()
{
    chalo::Character::Update();
}

void TrackObstacle::DrawDebug( sf::RenderWindow& window )
{
//    for ( auto& region : m_collisionRegions )
//    {
        sf::IntRect collisionRegionAtPosition;
        collisionRegionAtPosition.left = m_position.x + m_objectCollisionRectangle.left;
        collisionRegionAtPosition.top = m_position.y + m_objectCollisionRectangle.top;
        collisionRegionAtPosition.width = m_objectCollisionRectangle.width;
        collisionRegionAtPosition.height = m_objectCollisionRectangle.height;

//        std::cout << m_name << "\t" << chalo::StringUtility::RectangleToString( m_objectCollisionRectangle ) << std::endl;

        sf::RectangleShape shape;
        shape.setPosition( collisionRegionAtPosition.left, collisionRegionAtPosition.top );
        shape.setSize( sf::Vector2f( collisionRegionAtPosition.width, collisionRegionAtPosition.height ) );
        shape.setFillColor( sf::Color::Red );
        window.draw( shape );
//    }
}

bool TrackObstacle::CollisionWith( sf::IntRect horseRegion )
{
    bool collisions = false;
    for ( auto& region : m_collisionRegions )
    {
        sf::IntRect collisionRegionAtPosition;
        collisionRegionAtPosition.left = m_position.x + region.left;
        collisionRegionAtPosition.top = m_position.y + region.top;
        collisionRegionAtPosition.width = region.width;
        collisionRegionAtPosition.height = region.height;

        if ( SFMLHelper::BoundingBoxCollision( horseRegion, collisionRegionAtPosition ) )
        {
            collisions = true;
        }
    }
    return collisions;
}

ObstacleType TrackObstacle::GetType()
{
    return m_type;
}

void TrackObstacle::SetType( ObstacleType type )
{
    m_type = type;
}

void TrackObstacle::SetScale( float x, float y )
{
    m_sprite.setScale( x, y );
}

}
