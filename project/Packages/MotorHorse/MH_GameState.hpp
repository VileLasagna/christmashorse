#ifndef _MH_GAME_STATE
#define _MH_GAME_STATE

#include <SFML/Audio.hpp>
#include <vector>

#include "../../chalo-engine/States/IState.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/GameObject.hpp"
//#include "../../chalo-engine/DEPRECATED/Maps/WritableMap.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Managers/FontManager.hpp"
//#include "../../chalo-engine/DEPRECATED/Managers/DrawManager.hpp"
#include "../../chalo-engine/DEPRECATED/GameObjects/Character.hpp"
#include "../../chalo-engine/UI/UILabel.hpp"

struct MH_Player
{
    float x, y, width, height;
    float frame, frameSpeed, frameMax;
    float rotation;
    sf::IntRect collisionRect;
    float acceleration, accelerationMax, accelerationIncrement;
    float velocity, velocityMax;
};

struct MH_Obstacle
{
    std::string type;
    std::string subtype;
    float x, y;
    float width, height;
    int score;
    sf::IntRect collisionRect;
    float moveCounter;
    float moveCounterMax;
    float originalY;
    float speed;
    int imageIndex;
    float bulletCooldown;
    float frameCounter;
    sf::Sprite sprite;

    MH_Obstacle() { }
    MH_Obstacle( std::string type, float x, float y )
    {
        this->type = type;
        this->x = x;
        this->y = y;
        moveCounter = -1;
        imageIndex = 0;

        if ( type == "cloud" || type == "cloudborder" )
        {
            width = 50;
            height = 50;
            score = 0;
            subtype = "enemy";
            collisionRect = sf::IntRect( 10, 12, 30, 24 );
            sprite.setTexture( chalo::TextureManager::Get( "cloud" ) );
        }
        else if ( type == "cloud2" )
        {
            width = 50;
            height = 50;
            score = 0;
            subtype = "enemy";
            collisionRect = sf::IntRect( 10, 12, 30, 24 );
            moveCounter = 0;
            moveCounterMax = 360;
            originalY = y;
            sprite.setTexture( chalo::TextureManager::Get( "cloud" ) );
        }
        else if ( type == "elf" )
        {
            width = 56;
            height = 64;
            score = 0;
            subtype = "enemy";
            collisionRect = sf::IntRect( 19, 11, 27, 42 );
            moveCounter = 0;
            moveCounterMax = 360;
            originalY = y;
            speed = -3;
            imageIndex = 0;
            sprite.setTexture( chalo::TextureManager::Get( "tblElves" ) );
        }
        else if ( type == "elf2" )
        {
            width = 56;
            height = 64;
            score = 0;
            subtype = "enemy";
            collisionRect = sf::IntRect( 10, 11, 27, 42 );
            moveCounter = 0;
            moveCounterMax = 360;
            originalY = y;
            speed = 0;
            imageIndex = 1;
            sprite.setTexture( chalo::TextureManager::Get( "tblElves" ) );
        }
        else if ( type == "santa" )
        {
            width = 108;
            height = 107;
            score = 0;
            subtype = "enemy";
            collisionRect = sf::IntRect( 10, 11, 27, 42 );
            moveCounter = 0;
            moveCounterMax = 360;
            originalY = y;
            speed = 0;
            imageIndex = 0;
            bulletCooldown = 0;
            sprite.setTexture( chalo::TextureManager::Get( "tblSanta" ) );
        }
        else if ( type == "bullet" )
        {
            width = 15;
            height = 15;
            score = 0;
            subtype = "enemy";
            y += 50;
            frameCounter = 0;
            collisionRect = sf::IntRect( 0, 0, 15, 15 );
            sprite.setTexture( chalo::TextureManager::Get( "tblBullet" ) );
        }
        else if ( type == "bird1" )
        {
            width = 40;
            height = 40;
            score = 10;
            subtype = "trinket";
            collisionRect = sf::IntRect( 10, 10, 20, 20 );
            imageIndex = 0;
            sprite.setTexture( chalo::TextureManager::Get( "tblBirds" ) );
        }
        else if ( type == "bird2" )
        {
            width = 40;
            height = 40;
            score = 20;
            subtype = "trinket";
            collisionRect = sf::IntRect( 10, 10, 20, 20 );
            imageIndex = 1;
            moveCounter = 0;
            moveCounterMax = 360;
            originalY = y;
            sprite.setTexture( chalo::TextureManager::Get( "tblBirds" ) );
        }
        else if ( type == "bird3" )
        {
            width = 40;
            height = 40;
            score = 50;
            subtype = "trinket";
            collisionRect = sf::IntRect( 10, 10, 20, 20 );
            imageIndex = 2;
            moveCounter = 0;
            moveCounterMax = 360;
            originalY = y;
            sprite.setTexture( chalo::TextureManager::Get( "tblBirds" ) );
        }
    }
};

struct MH_LevelData
{
    std::vector<std::string> instructions;
    float scrollSpeed;
    float levelWidth;
    float gravity;
    float accelerationMax;
    float velocityMax;
    float accIncrement;
    std::vector<MH_Obstacle> obstacles;
    float startX;
};

struct MH_Game
{
    int level;
    std::string state;
    float scrollX;
    float scrollSpeed;
    std::vector<std::string> instructions;
    int deathAnimation;
    bool dead;
    int score;
    int scoreMax;
    int stars;
};

class MH_GameState : public chalo::IState
{
public:
    MH_GameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    std::string CLASSNAME;
    std::map<std::string, sf::SoundBuffer> m_soundBuffer;
    std::map<std::string, sf::Sound> m_soundEffects;
    std::map<std::string, sf::Sprite> m_images;
    std::map<std::string, sf::Text> m_texts;
    sf::RectangleShape m_background;
    sf::Music m_music;
    MH_LevelData m_level;
    MH_Player m_player;
    MH_Game m_game;

    float m_gravity;
    float m_deAccRate;

    void SetupLevelData( int level );
    void CreateObstacle( std::string type, float x, float y );
    void PrepareLevel();
    void BeginLevel();
    void Update_Game_Physics();
    void UpdatePlayerInput();
    void UpdatePlayerMovement();
    void DeathAnimation();
    void ScreenScroll();
    void UpdateObstacles();
    void CollectTrinket( int key );
    void PlayerDie();
    void PlayerWin();
    void Draw_LevelIntro( sf::RenderWindow& window );
    void Draw_Game( sf::RenderWindow& window );
    void Draw_LevelEnd( sf::RenderWindow& window );
    void Draw_GameEnd( sf::RenderWindow& window );
    void HandleInput();
};

#endif
